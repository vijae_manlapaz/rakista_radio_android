package com.rakista.radio.core;

import java.net.UnknownHostException;
import java.util.HashMap;

import org.apache.http.HttpException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.rakista.radio.R;
import com.rakista.radio.internal.RequestSenderCallback;
import com.rakista.radio.utility.JSONParser;
import com.rakista.radio.utility.Preferences;
import com.rakista.radio.utility.Utility;

@SuppressWarnings("unused")
public class RequestSender extends AsyncTask<String, Void, HashMap<String, String>> {

	private static String TAG = RequestSender.class.getSimpleName();
	
	private Context mContext;
	private String requestURL = "http://rakista.com/radio/api/rradio.php?method=requestSong&songid=";
	private RequestSenderCallback callback;
	private String paramDeviceId = "&deviceid=";

	public RequestSender(Context context, RequestSenderCallback listener) {
		mContext = context;
		callback = listener;
	}

	public RequestSenderCallback getListener() {
		return callback;
	}

	public void setListener(RequestSenderCallback listener) {
		this.callback = listener;
	}

	public void exec(String songID) {
		Preferences.Load(mContext);
		new RequestSender(mContext, callback).execute(requestURL + songID + paramDeviceId + IDGenerator.getDeviceId());
	}

	@Override
	protected HashMap<String, String> doInBackground(String... params) {

		Log.e(TAG, "REQUEST SENDER URL QUERY: " + params[0]);
		// / JSON Node names
		final String TAG_RESULT = "success";
		final String TAG_MESSAGE = "err_msg";
		final String TAG_REQUEST_ID = "requestid";

		// Creating JSON Parser instance
		JSONParser jParser = new JSONParser();

		HashMap<String, String> res = new HashMap<String, String>();

		// getting JSON string from URL
		JSONObject json = null;
		try {
			json = jParser.getJSONFromUrl(params[0]);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (ConnectTimeoutException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

		try {
			int Success = json.getInt(TAG_RESULT);
			String Message = json.getString(TAG_MESSAGE);

			if (Success == 1) {
				String Request_ID = json.getString(TAG_REQUEST_ID);
				res.put(TAG_MESSAGE, Message);
				res.put(TAG_REQUEST_ID, Request_ID);
			} else {
				res.put(TAG_MESSAGE, Message);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

		return res;
	}

	protected void onPostExecute(HashMap<String, String> result) {
		callback.onComplete(result);
	}
}
