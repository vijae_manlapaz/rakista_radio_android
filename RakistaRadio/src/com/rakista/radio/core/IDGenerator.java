package com.rakista.radio.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.SecureRandom;

import android.content.Context;
import android.os.Environment;
import android.support.v4.os.EnvironmentCompat;
import android.util.Log;

import com.rakista.radio.utility.Preferences;

public class IDGenerator {

	private static String TAG = IDGenerator.class.getSimpleName();

	private static volatile SecureRandom numberGenerator = null;
	private static final long MSB = 0x8000000000000000L;
	private Context mContext;

	public IDGenerator(Context context) {
		mContext = context;
	}

	/**
	 * Generates a unique 32 bytes hex string.
	 * 
	 * @return a 32 bytes hex string.
	 */
	public void generateID() {
		SecureRandom ng = numberGenerator;
		if (ng == null) {
			numberGenerator = ng = new SecureRandom();
		}
		
		String deviceId = "AND_" + Long.toHexString(MSB | ng.nextLong()) + Long.toHexString(MSB | ng.nextLong());

		if (mContext == null) {
			new Throwable("Context is null");
		} else {
			Log.e(TAG, "GENERATED DEVICE ID: " + deviceId);
			
			boolean canWriteInStorage = Environment.getExternalStorageDirectory().canWrite();
			
			if(canWriteInStorage) {
				File file = getFileAndStoragePath();
				if(!file.exists()) {
					Log.e(TAG, "File " + file.getName() + " has been successfully created");
					try {
						writeIdToFile(file, deviceId);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
//					try {
//						writeIdToFile(file, deviceId);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
				}
			} else {
				Log.e(TAG, "Cannot write in storage " + getFileAndStoragePath());
			}
		}
	}
	
	public static File getFileAndStoragePath() {
		String storage_path = Environment.getExternalStorageDirectory().getAbsolutePath();
		Log.e(TAG, "STORAGE PATH: " + storage_path);
		File dir = new File(storage_path + "/RakistaRadio");
		File file = new File(dir, "generate.txt");
		return file;
	}
	
	public void writeIdToFile(File file, String generated_id) throws IOException {
		FileOutputStream f = new FileOutputStream(file);
		f.write(generated_id.getBytes());
		f.flush();
		f.close();
	}
	
	public static String getDeviceId() {
		try {
			StringBuilder text = new StringBuilder();
			BufferedReader br = new BufferedReader(new FileReader(getFileAndStoragePath()));
		    String line;
		    while ((line = br.readLine()) != null) {
		        text.append(line);
//		        text.append('\n');
		    }
		    br.close();
		    Log.e(TAG, "GET DEVICE ID: " + text.toString());
		    return text.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}
}
