package com.rakista.radio.core;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import com.rakista.radio.R;
import com.rakista.radio.internal.AsyncTaskCompleteListener;
import com.rakista.radio.utility.JSONParser;

import android.os.AsyncTask;
import android.util.Log;

public class GetCurrentSongDetails extends AsyncTask<String, Void, ArrayList<HashMap<String, String>>> {

	private AsyncTaskCompleteListener listener = null;
	private volatile boolean requestCancel = false;

	public GetCurrentSongDetails(AsyncTaskCompleteListener callback) {
		this.listener = callback;
	}

	public GetCurrentSongDetails() {}

	public AsyncTaskCompleteListener getListener() {
		return listener;
	}

	public void setListener(AsyncTaskCompleteListener callback) {
		this.listener = callback;
	}

	public void cancelTask() {
		Log.i(getClass().getSimpleName(), "Cancelling Task");
		this.cancel(true);
		this.requestCancel = true;
	}

	protected void onCancelled() {
		//if(!this.getStatus().equals(AsyncTask.Status.RUNNING) || this.isCancelled()) {
			listener.onTaskCancelled(requestCancel);
			this.requestCancel = true;
		//}
	}
	
	public static AsyncTask.Status getState() {
		return getState();
	}

	@Override
	protected ArrayList<HashMap<String, String>> doInBackground(String... params) {
		Log.i(getClass().getSimpleName(), "Getting Current Song Details");

		// JSON Node names
		final String RESULT = "result";
		final String TAG_ID = "ID";
		final String TAG_ARTIST = "artist";
		final String TAG_TITLE = "title";
		final String TAG_LYRICS = "lyrics";
		final String TAG_ALBUM = "album";
		final String TAG_ALBUMYEAR = "albumyear";
		final String TAG_DURATION = "duration";
		final String TAG_DURATIONDISPLAY = "durationDisplay";
		final String TAG_DATEPLAYED = "date_played";
		final String TAG_BUY = "buycd";
		final String TAG_HASPICTURE = "haspicture";
		final String TAG_PICTURE = "picture";
		final String TAG_ISREQUESTED = "isRequested";
		final String TAG_ISDEDICATED = "isDedication";
		final String TAG_DEDICATIONMESSAGE = "dedicationMessage";
		final String TAG_DEDICATIONNAME = "dedicationName";

		// contacts JSONArray
		JSONArray currentsong = null;

		// Hashmap for ListView
		ArrayList<HashMap<String, String>> currentSongs = new ArrayList<HashMap<String, String>>();

		// Creating JSON Parser instance
		JSONParser jParser = new JSONParser();

		// getting JSON string from URL
		JSONObject json = null;

		while(!requestCancel) {
			try {
				json = jParser.getJSONFromUrl(params[0]);
			} catch (HttpException e) {
				e.printStackTrace();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (ConnectTimeoutException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			try {
				if(json != null) {
					// Getting Array of Contacts
					currentsong = json.getJSONArray(RESULT);

					// looping through All Contacts
					for (int i = 0; i < currentsong.length(); i++) {
						JSONObject c = currentsong.getJSONObject(i);

						String ID = c.getString(TAG_ID);
						String Artist = c.getString(TAG_ARTIST);
						String Title = c.getString(TAG_TITLE);
						String Lyrics = c.getString(TAG_LYRICS);
						String Album = c.getString(TAG_ALBUM);
						String AlbumYear = c.getString(TAG_ALBUMYEAR);
						String Duration = c.getString(TAG_DURATION);
						String DuratioDisplay = c.getString(TAG_DURATIONDISPLAY);
						String BUYCD = c.getString(TAG_BUY);
						String Date_Played = c.getString(TAG_DATEPLAYED);
						boolean HasPicture = c.getBoolean(TAG_HASPICTURE);
						String Picture = c.getString(TAG_PICTURE);
						String isRequested = c.getString(TAG_ISREQUESTED);
						boolean isDedicated = c.getBoolean(TAG_ISDEDICATED);
						String dedicationMessage = c.getString(TAG_DEDICATIONMESSAGE);
						String dedicationName = c.getString(TAG_DEDICATIONNAME);
						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						map.put(TAG_ID, ID);
						map.put(TAG_ARTIST, Artist);
						map.put(TAG_TITLE, Title);
						map.put(TAG_LYRICS, Lyrics);
						map.put(TAG_ALBUM, Album);
						map.put(TAG_ALBUMYEAR, AlbumYear);
						map.put(TAG_DURATION, Duration);
						map.put(TAG_DURATIONDISPLAY, DuratioDisplay);
						map.put(TAG_BUY, BUYCD);
						map.put(TAG_DATEPLAYED, Date_Played);
						map.put(TAG_HASPICTURE, Boolean.toString(HasPicture));
						map.put(TAG_PICTURE, Picture);
						map.put(TAG_ISREQUESTED, isRequested);
						map.put(TAG_ISDEDICATED, Boolean.toString(isDedicated));
						map.put(TAG_DEDICATIONMESSAGE, dedicationMessage);
						map.put(TAG_DEDICATIONNAME, dedicationName);
						// adding HashList to ArrayList
						currentSongs.add(map);
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
			}
			return currentSongs;
		}
		return null;
	}

	protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
		if(requestCancel) {
			listener.onTaskCancelled(requestCancel);
		} else {
			listener.onTaskComplete(result);
		}
	}
}
