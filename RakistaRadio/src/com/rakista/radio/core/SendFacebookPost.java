package com.rakista.radio.core;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
//import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.FrameLayout.LayoutParams;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.BaseRequestListener;
import com.facebook.android.Facebook;
import com.rakista.radio.R;
//import com.rakista.radio.R.id;
//import com.rakista.radio.R.layout;
import com.rakista.radio.internal.ToastHelper;
import com.rakista.radio.utility.Utility;

/**
 * Popups a window for sending post message to <string>Facebook</string>
 * @author VjManlapaz
 */
public class SendFacebookPost extends Dialog implements android.view.View.OnClickListener {

	private Context mContext;
	private Utility util;
	private ToastHelper toast;
	
	private Facebook mFacebook;
	private BaseRequestListener callback;

	private String Message;
	private EditText inputMessage;
	private Button _Send, _Cancel, _Clear;
	
	static final float[] DIMENSIONS_LANDSCAPE = { 460, 260 };
	static final float[] DIMENSIONS_PORTRAIT = { 280, 420 };
	
	static final FrameLayout.LayoutParams WRAP_CONTENT = new FrameLayout.LayoutParams(
			ViewGroup.LayoutParams.WRAP_CONTENT,
			ViewGroup.LayoutParams.WRAP_CONTENT);
	
	static final FrameLayout.LayoutParams FILL_PARENT = new FrameLayout.LayoutParams(
			ViewGroup.LayoutParams.FILL_PARENT,
			ViewGroup.LayoutParams.FILL_PARENT);
	
	static final int MARGIN = 4;
	static final int PADDING = 2;
	
	private LinearLayout mContent;

	/**
	 * Constractor method for instantiating <strong>SendFacebookPost class</strong>  
	 * @param context of calling Activity.
	 * @param facebook Instance.
	 * @param listener Must be a <strong>BaserRequestListener</strong>
	 * @author Vijae Manlapaz
	 */
	public SendFacebookPost(Context context, Facebook facebook, BaseRequestListener listener, String nowplaying) {
		super(context);
		mContext = context;
		mFacebook = facebook;
		callback = listener;
		Message = nowplaying;
	}

	/**
	 * Set message to be post to user facebook timeline.
	 * @param message to be posted.
	 */
	public void setMessage(String message) {
		Message = message;
	}
	
	/**
	 * Sets facebook instance.
	 * @param facebook instance to set
	 * @author Vijae Manlapaz
	 */
	public void setFacebook(Facebook facebook) {
		mFacebook = facebook;
	}

	/**
	 * Set BaseRequestListener for callback.
	 * @param listener BaseRequestListener for callback
	 * @author Vijae Manlapaz
	 */
	public void setCallBack(BaseRequestListener listener) {
		callback = listener;
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.app.Dialog#onCreate(android.os.Bundle)
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//setContentView(R.layout.sendfacebook_post_dialog_layout);

		mContent = new LinearLayout(getContext());
		mContent.setBackgroundResource(R.drawable.toast_bg);
		mContent.setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));
		mContent.setLayoutParams(FILL_PARENT);
		mContent.setOrientation(LinearLayout.VERTICAL);
		
		util = new Utility(mContext);
		toast = new ToastHelper(mContext, Toast.LENGTH_LONG);
		
		setupContent();
		
		Display display = getWindow().getWindowManager().getDefaultDisplay();
		final float scale = getContext().getResources().getDisplayMetrics().density;
		float[] dimensions = display.getWidth() < display.getHeight() ? DIMENSIONS_PORTRAIT : DIMENSIONS_LANDSCAPE;
		
		addContentView(mContent, new FrameLayout.LayoutParams((int) (dimensions[0] * scale + 0.5f),
				LayoutParams.WRAP_CONTENT));
		
	}
	
	private void setupContent() {
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.sendfacebook_post_dialog_layout, null);
		
		_Send = (Button) view.findViewById(R.id.sendfbpost_dialog_button_post);
		_Send.setOnClickListener(this);
		_Cancel = (Button) view.findViewById(R.id.sendfbpost_dialog_button_cancel);
		_Cancel.setOnClickListener(this);
		_Clear = (Button) view.findViewById(R.id.sendfbpost_dialog_button_clearall);
		_Clear.setOnClickListener(this);
		
		inputMessage = (EditText) view.findViewById(R.id.sendfbpost_dialog_input);
		if(Message.length() == 0) {
			Message = "\n#NowListening via #rakistaradio Android App.";
		} else {
			Message = "\n#NowPlaying " + Message + " via #rakistaradio Android App.";
		}
		
		inputMessage.setText(Message);
		
		mContent.addView(view);
	}

	/*
	 * (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.sendfbpost_dialog_button_post:
			if(util.isConnected()) {
				if(inputMessage.length() != 0) {
					Utility.sendSocialInteraction("Facebookq", "Share", v.getContext());
					postToFacebook(inputMessage.getText().toString());
				} else {
					this.dismiss();
				}
			} else {
				toast.show(getContext().getResources().getString(R.string.Network), getContext().getResources().getString(R.string.connection_error_message));
			}
			break;

		case R.id.sendfbpost_dialog_button_cancel:
			this.dismiss();
			break;

		case R.id.sendfbpost_dialog_button_clearall:
			inputMessage.setText("");
			break;
		default:
			break;
		}
	}

	/**
	 * Method that will post user message to facebook.
	 * this will be Asynchronous.
	 * @param message user message.
	 * @author Vijae Manlapaz
	 */
	private void postToFacebook(String message) {

		AsyncFacebookRunner mAsyncFbRunner = new AsyncFacebookRunner(mFacebook);

		Bundle params = new Bundle();

		params.putString("message", message);
		params.putString("picture", "http://sphotos-c.ak.fbcdn.net/hphotos-ak-ash4/389483_124837320958134_1270296079_n.jpg");
		params.putString("name", "Rakista Radio! 100% Pinoy Rock!");
		params.putString("link", "http://www.rakista.com/radio");
		params.putString("caption", "Rakista Radio for Android");
		params.putString("description", "http://www.rakista.com/radio");

		mAsyncFbRunner.request("me/feed", params, "POST", new WallPostListener());
	}

	private class WallPostListener extends BaseRequestListener {
		public void onComplete(final String response) {
			Log.i(getClass().getSimpleName(), response.toString());
			toast.show("Facebook", "Posted successfully");
			callback.onComplete(response);
			dismiss();
		}
	}

}
