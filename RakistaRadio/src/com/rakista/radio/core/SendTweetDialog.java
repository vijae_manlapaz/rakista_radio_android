package com.rakista.radio.core;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.FrameLayout.LayoutParams;

import com.rakista.radio.R;
import com.rakista.radio.callback.TweeterPostCallback;
import com.rakista.radio.internal.ToastHelper;
import com.rakista.radio.utility.Utility;
import com.sugree.twitter.Tweeter;

public class SendTweetDialog extends Dialog implements android.view.View.OnClickListener {

	private Context mContext;
	private TweeterPostCallback callback;
	private EditText inputMessage;
	private Button _send, _cancel, _clear;
	private String Message;
	
	private Utility util;
	private ToastHelper toast;
	
	static final float[] DIMENSIONS_LANDSCAPE = { 460, 260 };
	static final float[] DIMENSIONS_PORTRAIT = { 280, 420 };
	
	static final FrameLayout.LayoutParams WRAP_CONTENT = new FrameLayout.LayoutParams(
			ViewGroup.LayoutParams.WRAP_CONTENT,
			ViewGroup.LayoutParams.WRAP_CONTENT);
	
	static final FrameLayout.LayoutParams FILL_PARENT = new FrameLayout.LayoutParams(
			ViewGroup.LayoutParams.MATCH_PARENT,
			ViewGroup.LayoutParams.MATCH_PARENT);
	
	static final int MARGIN = 4;
	static final int PADDING = 2;
	
	private LinearLayout mContent;
	
	private ProgressDialog mProgress;
	
	public SendTweetDialog(Context context, TweeterPostCallback listener, String message) {
		super(context);
		mContext = context;
		callback = listener;
		Message = message;
		mProgress = new ProgressDialog(context);
	}
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		mContent = new LinearLayout(getContext());
		mContent.setBackgroundResource(R.drawable.toast_bg);
		mContent.setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));
		mContent.setLayoutParams(FILL_PARENT);
		mContent.setOrientation(LinearLayout.VERTICAL);
		
		util = new Utility(mContext);
		toast = new ToastHelper(mContext, Toast.LENGTH_LONG);
		
		setupContent();
		
		Display display = getWindow().getWindowManager().getDefaultDisplay();
		final float scale = getContext().getResources().getDisplayMetrics().density;
		float[] dimensions = display.getWidth() < display.getHeight() ? DIMENSIONS_PORTRAIT : DIMENSIONS_LANDSCAPE;
		
		addContentView(mContent, new FrameLayout.LayoutParams((int) (dimensions[0] * scale + 0.5f), LayoutParams.WRAP_CONTENT));

	}
	
	public void setupContent() {
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.sendtweet_dialog_layout, null);
		
		inputMessage = (EditText) view.findViewById(R.id.sendtweet_dialog_input);
		
		_send = (Button) view.findViewById(R.id.sendtweet_dialog_button_sendtweet);
		_send.setOnClickListener(this);
		_cancel = (Button) view.findViewById(R.id.sendtweet_dialog_button_cancel);
		_cancel.setOnClickListener(this);
		_clear = (Button) view.findViewById(R.id.sendtweet_dialog_button_clearall);
		_clear.setOnClickListener(this);
		
		if (Message.length() == 0) {
			Message = "\n#NowListening via @rakistaradio #rakistaradio Android App. #SupportOPM ";
		} else {
			Message = "\n#NowPlaying "
					+ Message
					+ " via @rakistaradio #rakistaradio Android App. #SupportOPM ";
		}

		inputMessage.setText(Message);

		mContent.addView(view);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.sendtweet_dialog_button_sendtweet:
			if(util.isConnected()) {
				if (inputMessage.length() != 0) {
					if (!(inputMessage.length() > 140)) {
						Utility.sendSocialInteraction("Twitter", "Tweet", v.getContext());
						
						mProgress.setMessage("Sending Tweet...");
						mProgress.setCancelable(false);
						mProgress.setCanceledOnTouchOutside(false);
						mProgress.show();
						
						new Tweeter(mContext,
								new callBackInterface()).tweet(inputMessage.getText().toString());
					} else {
						toast.show("Twitter", "We are unable to send your tweet\nmessage is over 140 characters.");
					}
				} else {
					this.dismiss();
				}
			} else {
				toast.show(getContext().getResources().getString(R.string.Network), getContext().getResources().getString(R.string.connection_error_message));
			}
			break;

		case R.id.sendtweet_dialog_button_cancel:
			this.dismiss();
			//
			break;
			
		case R.id.sendtweet_dialog_button_clearall:
			inputMessage.setText("");
			break;
		}
	}
	
	private class callBackInterface implements TweeterPostCallback {
		@Override
		public void onSuccess(String response) {
			
			if(mProgress != null && mProgress.isShowing()) {
				mProgress.dismiss();
			}
			
			callback.onSuccess(response);
			dismiss();
		}
	}
}
