package com.rakista.radio.core;

import java.net.UnknownHostException;

import org.apache.http.HttpException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
//import android.util.Log;
import android.widget.Toast;

//import com.rakista.radio.R;
import com.rakista.radio.internal.ToastHelper;
import com.rakista.radio.utility.JSONParser;
import com.rakista.radio.utility.Preferences;

public class VoteSender {
	private static String TAG = VoteSender.class.getSimpleName();
	
	private Context context;
	private String voteURL = "http://www.rakista.com/radio/api/rradio.php?method=voteSong&songid=";
	private String paramDeviceId = "&deviceid=";
	
	public VoteSender(Context context) {
		this.context = context;
	}

	public void sendVote(String SongID) {
		Preferences.Load(context);
		new sendVote().execute(voteURL + SongID + paramDeviceId + IDGenerator.getDeviceId());
//				Preferences.getDeviceID());
	}

	public class sendVote extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			Log.e(TAG, "VOTE SENDER URL QUERY STRING: " + params[0]);

			// / JSON Node names
			final String TAG_RESULT = "success";
			final String TAG_MESSAGE = "err_msg";
			
			String msg = null;
			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			JSONObject json = null;
			
			try {
				json = jParser.getJSONFromUrl(params[0]);
			} catch (HttpException e1) {
				e1.printStackTrace();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				Log.e(TAG, "Oops! something went wrong try again later");
			} catch (ConnectTimeoutException e) {
				e.printStackTrace();
			}

			try {
				int Success = json.getInt(TAG_RESULT);
				String Message = json.getString(TAG_MESSAGE);

				if (Success == 1) {
					msg = Message;
				} else {
					msg = Message;
				}

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			return msg;
		}

		protected void onPostExecute(String result) {
			Log.e(TAG, "VOTE SENDER RESPONSE: " + result);
			ToastHelper toast = new ToastHelper(context, Toast.LENGTH_SHORT);
			toast.show("Vote", result);
		}
	}
}
