package com.rakista.radio.core;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import com.rakista.radio.R;
import com.rakista.radio.internal.AsyncTaskCompleteListener;
import com.rakista.radio.utility.JSONParser;

import android.os.AsyncTask;
import android.util.Log;

public class GetNextSongDetails extends AsyncTask<String, Void, ArrayList<HashMap<String, String>>> {
	
	private AsyncTaskCompleteListener listener = null;
	private volatile boolean requestCancel = false;

	public GetNextSongDetails(AsyncTaskCompleteListener callback) {
		this.listener = callback;
	}

	public GetNextSongDetails() {}

	public void setListener(AsyncTaskCompleteListener callback) {
		this.listener = callback;
	}
	
	public AsyncTaskCompleteListener getListenerCallback() {
		return listener;
	}

	public void cancelTask() {
		Log.i(getClass().getSimpleName(), "Cancelling Task");
		this.cancel(true);
		this.requestCancel = true;
	}
	
	protected void onCancelled() {
		//if(!this.getStatus().equals(AsyncTask.Status.RUNNING) || this.isCancelled()) {
			listener.onTaskCancelled(requestCancel);
			this.requestCancel = true;
		//}
	}

	@Override
	protected ArrayList<HashMap<String, String>> doInBackground(String... params) {
		Log.i(getClass().getSimpleName(), "Getting Next Song Details");

		// JSON Node names
		final String RESULT = "result";
		final String TAG_ARTIST = "ARTIST";
		final String TAG_TITLE = "TITLE";
		final String TAG_ISREQUESTED = "REQUESTID";

		// contacts JSONArray
		JSONArray contacts = null;

		// Hashmap for ListView
		ArrayList<HashMap<String, String>> nextSongs = new ArrayList<HashMap<String, String>>();

		// Creating JSON Parser instance
		JSONParser jParser = new JSONParser();

		// getting JSON string from URL
		JSONObject json = null;
		while(!requestCancel) {
			try {
				json = jParser.getJSONFromUrl(params[0]);
			} catch (HttpException e1) {
				e1.printStackTrace();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (ConnectTimeoutException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			try {

				if (json != null) {
					// Getting Array of Contacts
					contacts = json.getJSONArray(RESULT);

					// looping through All Contacts
					for (int i = 0; i <= 0; i++) {
						JSONObject c = contacts.getJSONObject(i);

						// Storing each json item in variable
						String Artist = c.getString(TAG_ARTIST);
						String Title = c.getString(TAG_TITLE);
						String isRequested = c.getString(TAG_ISREQUESTED);

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						map.put(TAG_ARTIST, Artist);
						map.put(TAG_TITLE, Title);
						map.put(TAG_ISREQUESTED, isRequested);

						// adding HashList to ArrayList
						nextSongs.add(map);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			return nextSongs;
		}
		return null;
	}

	protected void onPostExecute(ArrayList<HashMap<String, String>> nextsongresult) {
		if(requestCancel) {
			listener.onTaskCancelled(requestCancel);
		} else {
			listener.onTaskComplete(nextsongresult);
		}
	}
}