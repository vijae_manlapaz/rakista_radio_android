package com.rakista.radio.core;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
//import android.util.Log;

//import com.rakista.radio.R;
import com.rakista.radio.internal.AsyncTaskCompleteListener;
import com.rakista.radio.utility.JSONParser;

public class GetTopRequested extends AsyncTask<String, Void, ArrayList<HashMap<String, String>>> {
	
	private AsyncTaskCompleteListener callback = null;
	
	public GetTopRequested(AsyncTaskCompleteListener listener) {
		this.callback = listener;
	}

	@Override
	protected ArrayList<HashMap<String, String>> doInBackground(String... params) {
		// / JSON Node names
		final String RESULT = "result";
		final String TAG_ID = "ID";
		final String TAG_ARTIST = "artist";
		final String TAG_TITLE = "title";
		final String TAG_LYRICS = "lyrics";
		final String TAG_ALBUM = "album";
		final String TAG_ALBUMYEAR = "albumyear";
		final String TAG_DURATION = "duration";
		final String TAG_DURATIONDISPLAY = "durationDisplay";
		final String TAG_BUY = "buycd";
		final String TAG_COUNT = "cnt";
		final String TAG_DATEPLAYED = "date_played";

		// contacts JSONArray
		JSONArray contacts = null;

		// Hashmap for ListView
		ArrayList<HashMap<String, String>> topRequested = new ArrayList<HashMap<String, String>>();

		// Creating JSON Parser instance
		JSONParser jParser = new JSONParser();

		// getting JSON string from URL
		JSONObject json = null;
		try {
			json = jParser.getJSONFromUrl(params[0]);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (ConnectTimeoutException e) {
			e.printStackTrace();
		}

		try {
			// Getting Array of Contacts
			contacts = json.getJSONArray(RESULT);

			// looping through All Contacts
			for (int i = 0; i < contacts.length(); i++) {
				JSONObject c = contacts.getJSONObject(i);

				// Storing each json item in variable
				String ID = c.getString(TAG_ID);
				String Artist = c.getString(TAG_ARTIST);
				String Title = c.getString(TAG_TITLE);
				String Lyrics = c.getString(TAG_LYRICS);
				String Album = c.getString(TAG_ALBUM);
				String AlbumYear = c.getString(TAG_ALBUMYEAR);
				String Duration = c.getString(TAG_DURATION);
				String DuratioDisplay = c.getString(TAG_DURATIONDISPLAY);
				String BUYCD = c.getString(TAG_BUY);
				String Count = c.getString(TAG_COUNT);
				String DataPlayed = c.getString(TAG_DATEPLAYED);

				// creating new HashMap
				HashMap<String, String> map = new HashMap<String, String>();

				// adding each child node to HashMap key => value
				map.put(TAG_ID, ID);
				map.put(TAG_ARTIST, Artist);
				map.put(TAG_TITLE, Title);
				map.put(TAG_LYRICS, Lyrics);
				map.put(TAG_ALBUM, Album);
				map.put(TAG_ALBUMYEAR, AlbumYear);
				map.put(TAG_DURATION, Duration);
				map.put(TAG_DURATIONDISPLAY, DuratioDisplay);
				map.put(TAG_BUY, BUYCD);
				map.put(TAG_COUNT, Count);
				map.put(TAG_DATEPLAYED, DataPlayed);

				// adding HashList to ArrayList
				topRequested.add(map);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return topRequested;
	}

	protected void onPreExecute(ArrayList<HashMap<String, String>> result) {
//		Log.i(getClass().getSimpleName(), result.toString());
		callback.onTaskComplete(result);
	}
}
