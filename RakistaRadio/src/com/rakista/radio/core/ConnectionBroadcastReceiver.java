package com.rakista.radio.core;

import com.rakista.radio.utility.Utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

@SuppressWarnings("unused")
public class ConnectionBroadcastReceiver extends BroadcastReceiver {
	private String TAG = getClass().getSimpleName();
	private Intent x = new Intent().setAction("CONNECTION");
	
	private String shet = "Network State Changed to ";

	@Override
	public void onReceive(Context context, Intent intent) {

		if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
			Log.i(getClass().getSimpleName(), intent.getAction().toString());

			/*
			 * ConnectivityManager connmgr = (ConnectivityManager)
			 * context.getSystemService(Context.CONNECTIVITY_SERVICE);
			 * NetworkInfo Wifi =
			 * connmgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			 * NetworkInfo Mobile =
			 * connmgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			 * NetworkInfo allnetwork = connmgr.getActiveNetworkInfo(); //
			 * String ConnectionType =
			 * connmgr.getActiveNetworkInfo().getTypeName();
			 * 
			 * if(Mobile.isConnected()) {
			 * util.setisConnected(Mobile.isConnected());
			 * Log.i(getClass().getSimpleName(), "MOBILE: " +
			 * Boolean.valueOf(Mobile.isConnectedOrConnecting()).toString()); }
			 * else if(Mobile.isConnected() == false){
			 * util.setisConnected(false); }
			 * 
			 * if(Wifi.isConnected()) { util.setisConnected(Wifi.isConnected());
			 * Log.i(getClass().getSimpleName(), "WIFI: " +
			 * Boolean.valueOf(Wifi.isConnectedOrConnecting()).toString()); }
			 * else { util.setisConnected(Wifi.isConnected()); }
			 */

			

			NetworkInfo info = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);

//			if (intent.getAction() == intent.getAction()) {
//				Log.i(TAG, "Intent is: " + intent.getAction().toString());
//			}

			if (info.getType() == ConnectivityManager.TYPE_MOBILE) {

				Log.i(TAG, shet + "MOBILE");
				
				if (info.isConnected()) {

					Log.i(TAG, "MOBILE Connection State Connected");
//					x.putExtra("isconnected", true);
//					context.sendBroadcast(x);

				} else if (info.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED) {
					
					Log.i(TAG, "Mobile Data Disconnected");

//					x.putExtra("isconnected", false);
//					context.sendBroadcast(x);

				}

			} else if (info.getType() == ConnectivityManager.TYPE_WIFI) {

				Log.i(TAG, shet + "WIFI");

				if (info.isConnected()) {
					
					Log.i(TAG, "WIFI is Connected"); 
//					x.putExtra("isconnected", true);
//					context.sendBroadcast(x);

				} else if (info.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED) {
					Log.i(TAG, "WIFI is OFF or Disconnected");

//					x.putExtra("isconnected", false);
//					context.sendBroadcast(x);
				}
			}
		}
	}
}
