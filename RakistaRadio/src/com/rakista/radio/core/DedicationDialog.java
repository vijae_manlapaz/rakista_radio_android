package com.rakista.radio.core;

import java.net.URLEncoder;
import java.net.UnknownHostException;

import org.apache.http.HttpException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.rakista.radio.R;
import com.rakista.radio.internal.ToastHelper;
import com.rakista.radio.utility.JSONParser;
import com.rakista.radio.utility.Utility;

@SuppressWarnings("unused")
public class DedicationDialog extends Dialog implements android.view.View.OnClickListener {

	private Context mContext;
	private String Name;
	private String Message;
	private String ID;
	private String DedicationURL = "http://www.rakista.com/radio/api/rradio.php?method=requestDedication";
	private String _RequestID = "&requestid=";
	private String _Message = "&msg=";
	private String _Name = "&name=";
	private EditText inputName;
	private AutoCompleteTextView inputMessage;
	private Button _send, _cancel;
	
	public DedicationDialog(Context context, String requestID) {
		super(context);
		mContext = context;
		this.ID = requestID;
	}

	public DedicationDialog(Context context) {
		super(context);
		this.mContext = context;
	}

	public void setRequestID(String requestID) {
		this.ID = requestID;
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dedication_layout);

		inputName = (EditText) findViewById(R.id.dedication_layout_name);
		inputMessage = (AutoCompleteTextView) findViewById(R.id.dedication_layout_input);

		_send = (Button) findViewById(R.id.dedication_layout_button_send);
		_send.setOnClickListener(this);
		_cancel = (Button) findViewById(R.id.dedication_layout_button_cancel);
		_cancel.setOnClickListener(this);

	}

	public void sendDedication(String name, String message) {
		Log.i(getClass().getSimpleName(), "URL: " + DedicationURL + _RequestID + ID + _Message + URLEncoder.encode(message) + _Name + URLEncoder.encode(name));
		new PostDedication().execute(DedicationURL + _RequestID + ID + _Message + URLEncoder.encode(message) + _Name + URLEncoder.encode(name));
	}
;
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.dedication_layout_button_send:
			Utility.sendClickEvent("SEND_DEDICATION", getContext());
			if(inputName.getText().length() == 0) {
				sendDedication("Rakista Radio Android App User", inputMessage.getText().toString());
			} else {
				sendDedication(inputName.getText().toString(), inputMessage.getText().toString());
			}
			break;

		case R.id.dedication_layout_button_cancel:
			this.dismiss();
			break;
		}
	}

	public class PostDedication extends AsyncTask<String, Void, String> {
		private ProgressDialog mProgress;

		protected void onPreExecute() {
			mProgress = new ProgressDialog(getContext());
			mProgress.setMessage("Sending your dedication..");
			mProgress.setIndeterminate(true);
			mProgress.setCancelable(false);
			mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgress.show();
		}

		@Override
		protected String doInBackground(String... params) {
			// / JSON Node names
			final String TAG_RESULT = "success";
			final String TAG_MESSAGE = "err_msg";

			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();

			String msg = null;

			// getting JSON string from URL
			JSONObject json = null;

			try {
				json = jParser.getJSONFromUrl(params[0]);
			} catch (HttpException e1) {
				e1.printStackTrace();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (ConnectTimeoutException e) {
				e.printStackTrace();
			}

			try {
				int Success = json.getInt(TAG_RESULT);
				String Message = json.getString(TAG_MESSAGE);

				if (Success == 1) {
					msg = Message;
				} else {
					msg = Message;
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return msg;
		}

		protected void onPostExecute(String result) {

			if (mProgress.isShowing()) {
				mProgress.dismiss();
			} else {
				mProgress.dismiss();
			}
			
			ToastHelper toast = new ToastHelper(getContext(), Toast.LENGTH_LONG);
			toast.show("Dedication", result == null? "" : result.toString());
			dismiss();
		}
	}
}
