package com.rakista.radio.callback;

public interface PlayerCallback {

	void playerStopped(boolean state);
	void playerPlaying(boolean state);
	void playerPaused(boolean state);
}
