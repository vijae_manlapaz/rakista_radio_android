package com.rakista.radio.callback;

//import android.content.Context; 
import android.os.Bundle;
//import android.util.Log;

import com.sugree.twitter.DialogError;
import com.sugree.twitter.Twitter;
import com.sugree.twitter.TwitterError;
//import com.sugree.twitter.TwitterSessionStore;

public class TwitterLoginListener implements Twitter.DialogListener {
	
	//public final Context mContext;
	private TwitterCallback callback = null;
	
	public TwitterLoginListener(TwitterCallback listener) {
		//this.mContext = context;
		this.callback = listener;
	}
	@Override
	public void onComplete(Bundle values) {
		//TwitterSessionStore.setAccessToken(values.getString("access_token"), mContext);
		//TwitterSessionStore.setSecretToken(values.getString("secret_token"), mContext);
		callback.onLoginComplete(values);
	}

	@Override
	public void onTwitterError(TwitterError e) {
	}

	@Override
	public void onError(DialogError e) {
	}

	@Override
	public void onCancel() {
	}

}
