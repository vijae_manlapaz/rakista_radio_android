package com.rakista.radio.callback;

import java.util.ArrayList;
import java.util.HashMap;

public interface BaseCallback {
	
	void onComplete(ArrayList<HashMap<String, String>> data);
}
