package com.rakista.radio.callback;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

//import android.content.Context;
//import android.util.Log;

import com.sugree.twitter.AsyncTwitterRunner;
import com.sugree.twitter.TwitterError;
//import com.sugree.twitter.TwitterSessionStore;

public class TwitterLogoutListener implements AsyncTwitterRunner.RequestListener {

	//public final Context mContext;
	private TwitterCallback callback = null;
	
	public TwitterLogoutListener(TwitterCallback listener) {
		//this.mContext = context;
		this.callback = listener;
	}
	@Override
	public void onComplete(String response) {
		
		//TwitterSessionStore.setAccessandSecret(null, null, mContext);
		//TwitterSessionStore.setSecretToken(null, mContext);
		callback.onLogoutComplete(response);
	}

	@Override
	public void onIOException(IOException e) {
	}

	@Override
	public void onFileNotFoundException(FileNotFoundException e) {
	}

	@Override
	public void onMalformedURLException(MalformedURLException e) {
	}

	@Override
	public void onTwitterError(TwitterError e) {
	}
	
}
