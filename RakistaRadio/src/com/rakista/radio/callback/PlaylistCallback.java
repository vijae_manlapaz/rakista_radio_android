package com.rakista.radio.callback;

import java.util.ArrayList;
import java.util.HashMap;

public interface PlaylistCallback {
	void onComplete(ArrayList<HashMap<String, String>> data);
}
