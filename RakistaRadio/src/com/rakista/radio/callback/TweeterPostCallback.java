package com.rakista.radio.callback;

/**
 * Callback interface for posting on twitter
 * 
 * <h1>List of Method</h1>
 * <li>void onSuccess(String response)</li>
 * <br>
 * @author Vijae Manlapaz
 */
public interface TweeterPostCallback {
	/**
	 * This method will only be invoke when response from twitter is successful without error.
	 * @param response response from twitter.
	 */
	public void onSuccess(String response);
}
