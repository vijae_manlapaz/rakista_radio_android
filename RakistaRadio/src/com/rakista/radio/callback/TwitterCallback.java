package com.rakista.radio.callback;

import android.os.Bundle;

public interface TwitterCallback {

	public void onLogoutComplete(String response);
	public void onLoginComplete(Bundle values);
}
