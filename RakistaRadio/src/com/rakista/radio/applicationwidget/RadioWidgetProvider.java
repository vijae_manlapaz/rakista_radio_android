package com.rakista.radio.applicationwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
//import android.widget.TextView;

import com.rakista.radio.R;
import com.rakista.radio.internal.ConstantCommand;
import com.rakista.radio.utility.Preferences;
import com.rakista.radio.widget.AutoScrollingTextView;

public class RadioWidgetProvider extends AppWidgetProvider {

	private static AutoScrollingTextView autoscrollTextView;
//	private final static String TAG = "RadioWidgetProvider";
	
	/*
	 * (non-Javadoc)
	 * @see android.appwidget.AppWidgetProvider#onUpdate(android.content.Context, android.appwidget.AppWidgetManager, int[])
	 * 
	 * Called once only
	 */
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		Log.i(getClass().getSimpleName(), "onUpdate");
		
		autoscrollTextView = new AutoScrollingTextView(context);
		autoscrollTextView.setText("Rakista Radio");
//		TextView txtview = autoscrollTextView;
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
		remoteViews.setOnClickPendingIntent(R.id.widgetplaystop, buildButtonPendingIntent(context));
		
		//RemoteViews Logo = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
		remoteViews.setOnClickPendingIntent(R.id.widgetRadioLogo, buildIconPendingIntent(context));

		//RemoteViews titleview = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
		remoteViews.setTextViewText(R.id.widgetTitleText, "Rakista Radio!");
		
		//RemoteViews status = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
		//status.setTextViewText(R.id.widgetStatus, "100% Pinoy Rock!");
		
		pushWidgetUpdate(context, remoteViews);
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.appwidget.AppWidgetProvider#onEnabled(android.content.Context)
	 * 
	 * this will be called when widget is place on the home screen.
	 * once.
	 */
	public void onEnabled(Context context) {
		super.onEnabled(context);
		Log.i(getClass().getSimpleName(), "onEnabled");
		Preferences.Load(context);
		
		/*RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
		
		if(RadioActivity.mService.getMediaPlayer().isPlaying()) {
			Log.i(getClass().getSimpleName(), Boolean.valueOf(RadioActivity.mService.getMediaPlayer().isPlaying()).toString());
			remoteViews.setImageViewResource(R.id.widgetplaystop, R.drawable.stop1);
			pushWidgetUpdate(context, remoteViews);
		}
		
		pushWidgetUpdate(context, remoteViews);*/
	}
	
	public static PendingIntent buildPendingIntentTitleView(Context context, String title) {
		Intent intent = new Intent(context, WidgetIntentReceiver.class);
		return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}
	
	public static PendingIntent buildPendingIntentStatusTitle(Context context, String statusmsg) {
		Intent intent = new Intent(context, WidgetIntentReceiver.class);
		return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}
	
	public static PendingIntent buildButtonPendingIntent(Context context, boolean isplaying) {
		Intent intent = new Intent(context, WidgetIntentReceiver.class);
		
		if(isplaying == true) {
			intent.setAction(ConstantCommand.STOP_STREAM);
		} else {
			intent.setAction(ConstantCommand.START_STREAM);
		}
	    
	    return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}
	
	public static PendingIntent buildButtonPendingIntent(Context context) {
		Intent intent = new Intent(context, WidgetIntentReceiver.class);
		intent.setAction(ConstantCommand.START_STREAM);
	    return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}
	
	public static PendingIntent buildIconPendingIntent(Context context) {
		Intent intent = new Intent(context, WidgetIntentReceiver.class);
		intent.setAction(ConstantCommand.OPEN_ACTIVITY);
	    return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}
	
	public static void pushWidgetUpdate(Context context, RemoteViews remoteViews) {
		ComponentName myWidget = new ComponentName(context, RadioWidgetProvider.class);
	    AppWidgetManager manager = AppWidgetManager.getInstance(context);
	    manager.updateAppWidget(myWidget, remoteViews);		
	}
}
