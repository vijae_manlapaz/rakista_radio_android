package com.rakista.radio.applicationwidget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.rakista.radio.R;
import com.rakista.radio.RadioActivity;
import com.rakista.radio.internal.CONSTANTS;
import com.rakista.radio.internal.ConstantCommand;
//import com.rakista.radio.internal.IMediaPlayerServiceClient;
//import com.rakista.radio.internal.StatefulMediaPlayer;
//import com.rakista.radio.service.MediaPlayerService;
import com.rakista.radio.internal.ToastHelper;
import com.rakista.radio.utility.Preferences;
import com.rakista.radio.utility.Utility;

/**
 * This class will handle and sets/updates widget views.
 * 
 * @author Vijae Manlapaz
 */
public class WidgetIntentReceiver extends BroadcastReceiver {

	boolean isplaying = false;
	String TAG = getClass().getSimpleName();
	static int clickCount = 0;
//	private IMediaPlayerServiceClient mClient;
//	private MediaPlayerService mService;
//	private StatefulMediaPlayer mPlayer;
//	private MediaPlayerService.MediaPlayerBinder mBinder;
	static String action = null;
	/*
	 * (non-Javadoc)
	 * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
	 */
	public void onReceive(Context context, Intent intent) {
		Log.i(TAG, "onReceive Intent Action: " + intent.getAction().toString());
		Preferences.Load(context);
		Utility util = new Utility(context);
		
		/*mService = mBinder.getService();
		mPlayer = mService.getMediaPlayer();
		mClient = mService.getClient();*/
		
		/*
		 * Intent coming from widget
		 */
		if(intent.getAction().equals(ConstantCommand.START_STREAM)) {
			Preferences.Load(context);
			updateWidgetButtonListener(context, true);
			
			if(util.isActivityRunning(context)) {
				if(util.isConnected()) {
					RadioActivity.mService.initializePlayer(CONSTANTS.STATIONS_TOSTRING[Preferences.getStreamSelectedSetting()]);
				} else {
					ToastHelper toast = new ToastHelper(context, Toast.LENGTH_LONG);
					toast.show(context.getResources().getString(R.string.error), context.getResources().getString(R.string.connection_error_message));
					updateWidgetButtonListener(context, false);
				}
			} else {
				openActivity(context, true);
			}
			
		}
		
		/*
		 * Intent coming from widget
		 */
		if(intent.getAction().equals(ConstantCommand.STOP_STREAM)) {
			Preferences.Load(context);
			updateWidgetButtonListener(context, false);
			updateTitle(context, "Rakista Radio");
			updateStatus(context, "100% Pinoy Rock!");
			RadioActivity.mService.stopMediaPlayer();
		}
		
		/*
		 * Intent coming from RadioActivity.class or MediaPlayerService.class
		 */
		if(intent.getAction().equals(ConstantCommand.UPDATE_BUTTON)) {
			updateWidgetButtonListener(context, intent.getBooleanExtra("extra", false));
			updateStatus(context, "100% Pinoy Rock!");
			if(intent.getBooleanExtra("extra", false) == false) {
				updateTitle(context, "Rakista Radio");
			}
		}
		
		/*
		 * Intent coming from RadioActivity.class or MediaPlayerService.class
		 */
		if(intent.getAction().equals(ConstantCommand.UPDATE_TITLE)) {
			updateTitle(context, intent.getCharSequenceExtra("title").toString());
		}
		
		if(intent.getAction().equals(ConstantCommand.UPDATE_STATUS)) {
			updateStatus(context, intent.getStringExtra("statusExtra").toString());
		}
		
		/*
		 * Intent coming from widget.
		 */
		if(intent.getAction().equals(ConstantCommand.OPEN_ACTIVITY)) {
			openActivity(context, false);
		}
	}
	
	/**
	 * Opens Main Activity (RadioActivity.class) if not running else Brings activity to front.
	 * @param context who's sending this update.
	 * @param startnew true if Main Activity <strong>(RadioActivity.class)</strong> is not running false is default.
	 */
	public static void openActivity(Context context, boolean startnew) {
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
		remoteViews.setOnClickPendingIntent(R.id.widgetRadioLogo, RadioWidgetProvider.buildIconPendingIntent(context));
		
		Intent x = new Intent(context, RadioActivity.class);
		
		if(startnew) {
			x.putExtra("com.rakista.radio.widget.REQUEST", true);
		}
		
		x.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		context.startActivity(x);
		RadioWidgetProvider.pushWidgetUpdate(context.getApplicationContext(), remoteViews);
	}
	
	/**
	 * Updates widget Title text area and Status text.
	 * @param context who's sending this update.
	 * 
	 * @author Vijae Manlapaz
	 */
	public static void updateTitleandStatus(Context context) {
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
		remoteViews.setTextViewText(R.id.widgetTitleText, "Rakista Radio!");
		remoteViews.setTextViewText(R.id.widgetStatus, "100% Pinoy Rock!");
		
		RadioWidgetProvider.pushWidgetUpdate(context, remoteViews);
	}
	
	/**
	 * Updates widget Title text area.
	 * @param context who's sending this update.
	 * @param title to set.
	 * 
	 * @author Vijae Manlapaz
	 */
	public static void updateTitle(Context context, String title) {
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
		remoteViews.setTextViewText(R.id.widgetTitleText, title);
		
		RadioWidgetProvider.pushWidgetUpdate(context, remoteViews);
	}
	
	/**
	 * Updates widget Status text area.
	 * @param context context who's sending this update.
	 * @param status to set.
	 * 
	 * @author Vijae Manlapaz
	 */
	public static void updateStatus(Context context, String status) {
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
		remoteViews.setTextViewText(R.id.widgetStatus, status);
		
		RadioWidgetProvider.pushWidgetUpdate(context, remoteViews);
	}
	
	/**
	 * Updates widget play and stop button.
	 * @param context context who's sending this update.
	 * @param playing true if we want to start playing, false otherwise.
	 * 
	 * @author Vijae Manlapaz
	 */
	public static void updateWidgetButtonListener(Context context, boolean playing) {
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
		remoteViews.setImageViewResource(R.id.widgetplaystop, getImageToSet(playing));
		
		//REMEMBER TO ALWAYS REFRESH YOUR BUTTON CLICK LISTENERS!!!
		if(playing == true) {
			action = "START_STREAM";
		} else {
			action = "STOP_STREAM";
		}
		
		remoteViews.setOnClickPendingIntent(R.id.widgetplaystop, RadioWidgetProvider.buildButtonPendingIntent(context, playing));
		
		RadioWidgetProvider.pushWidgetUpdate(context.getApplicationContext(), remoteViews);
	}
	
	/**
	 * Updates and sets widget play and stop button drawable.
	 * @param isPlaying true if we are playing, false otherwise.
	 * @return int Resource drawable.
	 * 
	 * @author Vijae Manlapaz
	 */
	private static int getImageToSet(boolean isPlaying) {
		clickCount++; //clickCount % 2 == 0? 
		return isPlaying == true? R.drawable.stop1 : R.drawable.play1;
	}
}
