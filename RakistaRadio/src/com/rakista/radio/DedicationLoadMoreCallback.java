package com.rakista.radio;

import java.util.ArrayList;
import java.util.HashMap;

public interface DedicationLoadMoreCallback {
	void hasLoaded(int page, ArrayList<HashMap<String, String>> result);
}
