package com.rakista.radio.receiver;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;

public class MyIntentService extends IntentService {
	private String TAG = MyIntentService.class.getSimpleName();

	public MyIntentService(String name) {
		super("MyIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
        // Do the work that requires your app to keep the CPU running.
        // ...
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        MyWakefulReceiver.completeWakefulIntent(intent);
	}

}
