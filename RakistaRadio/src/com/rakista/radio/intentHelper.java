package com.rakista.radio;

/**
 * 
 * @author Vijae Manlapaz
 *
 */
public class intentHelper {

	static String imageLink;
	static String webLink;
	static String updateLink;
	static long alarmTime;
	
	public static void setImageLink(String link) {
		imageLink = link;
	}
	
	public static String getImageLink() {
		return imageLink;
	}
	
	public static void setWebLink(String link) {
		webLink = link;
	}
	
	public static String getWebLink() {
		return webLink;
	}
	
	public static void setupdateLink(String link) {
		updateLink = link;
	}
	
	public static String getupdateLink() {
		return updateLink;
	}
	
	public static void setAlarmTime(long alarmtime) {
		alarmTime = alarmtime;
	}
	
	public static long getAlarmTime() {
		return alarmTime;
	}
}
