package com.rakista.radio;

import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.rakista.radio.internal.ToastHelper;
import com.rakista.radio.utility.Utility;

public class PushNotificationViewer extends Activity implements OnClickListener {

	private ImageView imageHolder;
	private Button viewDetails;
	
	private com.rakista.radio.utility.Utility util;
	private ToastHelper toast;
	private String weblink;
	private ProgressDialog mp;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pushnotificatinpreview);
		
		util = new Utility(this);
		toast = new ToastHelper(this, Toast.LENGTH_LONG);
		mp = new ProgressDialog(this);
		
		imageHolder = (ImageView) findViewById(R.id.pushnotificationpreview_imageholder);
		viewDetails = (Button) findViewById(R.id.pushnotificationpreview_viewdetails_button);
		viewDetails.setOnClickListener(this);
		
		if(getIntent().getAction().equals("com.rakista.radio.PushNotificationViewer.NEW_INTENT")) {
			weblink = intentHelper.getWebLink();
			if(util.isConnected()) {
				new ImageDownloader().execute(intentHelper.getImageLink());
			} else {
				toast.show(getString(R.string.error), getString(R.string.connection_error_message));
			}
		}
	}
	
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.pushnotificationpreview_viewdetails_button:
			if(util.isConnected()) {
				if(!weblink.equals(null)) {
					Utility.openWebview(weblink, this);
				} else {
					toast.show("Info", "There's no web link.");
				}
			} else {
				toast.show(getString(R.string.error), getString(R.string.connection_error_message));
			}
			break;
		}
	}
	
	public void onDestroy() {
		Log.i(getClass().getSimpleName(), "Activity Destroyed");
		weblink = "";
		imageHolder.setImageBitmap(null);
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		Log.i(getClass().getSimpleName(), "onBackPressed");
		finish();
	}
	
	private class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
		
        protected Bitmap doInBackground(String... param) {
            return downloadBitmap(param[0]);
        }
 
        @Override
        protected void onPreExecute() {
            mp = ProgressDialog.show(PushNotificationViewer.this, "", "Loading Image");
            imageHolder.setImageResource(R.drawable.rakista_radio_logo);
        }
 
        protected void onPostExecute(Bitmap result) {
            imageHolder.setImageBitmap(result);
            mp.dismiss();
 
        }
 
        private Bitmap downloadBitmap(String url) {
            // initilize the default HTTP client object
            final DefaultHttpClient client = new DefaultHttpClient();
 
            //forming a HttpGet request 
            final HttpGet getRequest = new HttpGet(url);
            try {
 
                HttpResponse response = client.execute(getRequest);
 
                //check 200 OK for success
                final int statusCode = response.getStatusLine().getStatusCode();
 
                if (statusCode != HttpStatus.SC_OK) {
                    Log.w("ImageDownloader", "Error " + statusCode + " while retrieving bitmap from " + url);
                    
                    return BitmapFactory.decodeResource(getResources(),R.drawable.rakista_radio_logo);
                    //return BitmapFactory.decodeResource(getResources(),R.drawable.asd);
                }
 
                final HttpEntity entity = response.getEntity();
                if (entity != null) {
                    InputStream inputStream = null;
                    try {
                        // getting contents from the stream 
                        inputStream = entity.getContent();
 
                        // decoding stream data back into image Bitmap that android understands
                        final Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
 
                        return bitmap;
                    } finally {
                        if (inputStream != null) {
                            inputStream.close();
                        }
                        entity.consumeContent();
                    }
                }
            } catch (Exception e) {
                // You Could provide a more explicit error message for IOException
                getRequest.abort();
                Log.e("ImageDownloader", "Something went wrong while" + " retrieving bitmap from " + url + e.toString());
             // There's an error show default.
                return BitmapFactory.decodeResource(getResources(),R.drawable.rakista_radio_logo);
            } 
 
            return null;
        }
    }
}
