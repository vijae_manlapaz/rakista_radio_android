package com.rakista.radio;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;
import com.rakista.radio.utility.Preferences;
import com.rakista.radio.utility.ServerUtility;
import com.rakista.radio.utility.Utility;

public class GCMIntentService extends GCMBaseIntentService {
	private static String TAG = GCMIntentService.class.getSimpleName();

	final HashMap<String, String> values = new HashMap<String, String>();
	static String SenderID = "45106899021";
	
	public GCMIntentService() {
		super(SenderID);
	}
	
	@Override
	protected void onError(Context context, String errorId) {
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		
		Log.i(getClass().getSimpleName(),"Push Notification Type: " + intent.getStringExtra("type"));
		Preferences.Load(getApplicationContext());
		
		HashMap<String, String> GCMdata = new HashMap<String, String>();
		RadioNotification.setContext(context);
		
		if(Preferences.getgcmEnabled()) {
			
			if(intent.getStringExtra("type").equalsIgnoreCase("Events")) {
				Log.i(getClass().getSimpleName(), "Expiration: " + Boolean.valueOf(System.currentTimeMillis() > Utility.convertStringtimeToMillis(intent.getStringExtra("validity"))).toString());
				if(!(System.currentTimeMillis() > Utility.convertStringtimeToMillis(Utility.convertServerTimeToLocaleTime(intent.getStringExtra("validity"))))) {
					
					GCMdata.put("type", intent.getStringExtra("type"));
					GCMdata.put("message", intent.getStringExtra("message"));
					GCMdata.put("image", intent.getStringExtra("image"));
					GCMdata.put("link", intent.getStringExtra("link"));
					GCMdata.put("validity", intent.getStringExtra("validity"));
					
					RadioNotification.showEventNotification(GCMdata);
				}
				
			} else if(intent.getStringExtra("type").equalsIgnoreCase("Shoutout")) {
				Log.i(getClass().getSimpleName(), "Expiration: " + Boolean.valueOf(System.currentTimeMillis() > Utility.convertStringtimeToMillis(intent.getStringExtra("validity"))).toString());
				if(!(System.currentTimeMillis() > Utility.convertStringtimeToMillis(Utility.convertServerTimeToLocaleTime(intent.getStringExtra("validity"))))) {
					GCMdata.put("message", intent.getStringExtra("message"));
					Log.i(getClass().getSimpleName(), "SYSTEM TIME: " + System.currentTimeMillis());
					Log.i(getClass().getSimpleName(), "LOCAL TIME: " + Utility.convertServerTimeToLocaleTime(intent.getStringExtra("validity")));
					Log.i(getClass().getSimpleName(), "TRUE");
					RadioNotification.showShoutoutNotification(GCMdata);
				}
				
			} else if(intent.getStringExtra("type").equalsIgnoreCase("AppUpdate")) {
				Log.i(getClass().getSimpleName(), "Incoming App version: " + Double.valueOf(Utility.convertAppVersionToDouble(intent.getStringExtra("version").toString())).doubleValue());
				Log.i(getClass().getSimpleName(), "Current App version: " + Double.valueOf(Utility.getCurrentAppVersion(context).toString()).doubleValue());
				if(Double.valueOf(Utility.convertAppVersionToDouble(intent.getStringExtra("version").toString())).floatValue() > Double.valueOf(Utility.getCurrentAppVersion(context).toString()).floatValue()) {
					GCMdata.put("message", intent.getStringExtra("message"));
					GCMdata.put("link", intent.getStringExtra("link"));
					
					RadioNotification.showAppUpdateNotification(GCMdata);
				}
			}
		} else {
			Log.i(getClass().getSimpleName(), "Notification is disabled in settings");
		}
		
		if(intent.getStringExtra("type").equalsIgnoreCase("Clear Data")) {
			clearApplicationData();
		}
	}
	
	public void clearApplicationData() {
		File cache = getCacheDir();
		File appDir = new File(cache.getParent());
		if (appDir.exists()) {
			String[] children = appDir.list();
			for (String s : children) {
				if (!s.equals("lib")) {
					deleteDir(new File(appDir, s));
					Log.i("TAG", "**************** File /data/data/APP_PACKAGE/" + s + " DELETED *******************");
				}
			}
		}
	}
	public static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}
	
	@Override
	protected void onUnregistered(Context context, String id) {
		Log.e(TAG, "UNREGISTERED ID: " + id);
		GCMRegistrar.setRegisteredOnServer(context, false);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onRegistered(Context context, String id) {
		Log.i(getClass().getSimpleName(), "REGISTERED ID: " + id);
		Preferences.Load(getApplicationContext());
		
		long expirationTime = GCMRegistrar.getRegisterOnServerLifespan(context);

		Log.i(getClass().getSimpleName(),"Registration ID Expiration Time: " + Long.valueOf(expirationTime).toString());
			
		Preferences.setgcmExpirationTime(System.currentTimeMillis() + expirationTime);
		Preferences.setgcmExpirationOnServer(expirationTime);
		Preferences.setgcmCurrentRegistrationId(id);
		
		TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE); 
		String serialNumber = tm.getSimSerialNumber();
		String number = tm.getLine1Number();
		String networkname = tm.getNetworkOperatorName();
		Log.i("mobile number", "Phone Number: " +number + "\nSerial Number: " +serialNumber + "\nNetwork Name: " + networkname);
		
		/*Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
		Account[] accounts = AccountManager.get(this).getAccounts();
		Log.i("accounts", Integer.valueOf(accounts.length).toString());
		String possibleEmail = null;
		
		for (int i = 0; i < accounts.length; i++) {
			Account useremail;
			useremail = accounts[i];
			possibleEmail = accounts[i].name;
			Log.i("vijae", possibleEmail);
			if(emailPattern.matcher(useremail.name).matches()) {
				Log.i("EMAILS", useremail.name);
				//values.put("email", useremail.name);
			}
		//for (Account account : accounts) {
		    if (emailPattern.matcher(account.name).matches()) {
		        possibleEmail = account.name;
		        if(!possibleEmail.equals(possibleEmail)) {
		        	Log.i("Email", account.name);
				}
		    }
		    
		}*/
		
		values.put("id", id);
		values.put("mobile", number);
		values.put("carrier", networkname);
		values.put("firstname", Utility.getUsername(getApplicationContext()));
		values.put("email", Utility.getEmail(getApplicationContext()));
		values.put("lastname", "");
		
		new AsyncGCMRegistration().execute(values);
		
	}
	
	class AsyncGCMRegistration extends AsyncTask<HashMap<String, String>, Void, Void> {

		@Override
		protected Void doInBackground(HashMap<String, String>... params) {
			Map<String, String> param = params[0];
			String registrationId = param.get("id");
			String mobile = param.get("mobile");
			String carrier = param.get("carrier");
			String email = param.get("email");
			String firstname = param.get("firstname");
			String lastname = param.get("lastname");
			
			ServerUtility.register(getApplicationContext(), registrationId, mobile, carrier, email, firstname, lastname);
			
			return null;
		}
		
		protected void onPostExecute() {
			GCMRegistrar.setRegisteredOnServer(getApplicationContext(), true);
		}
	}

}
