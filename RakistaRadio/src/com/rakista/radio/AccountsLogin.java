package com.rakista.radio;

import org.json.JSONObject;
import org.json.JSONTokener;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.android.FbDialogError;
import com.facebook.android.SessionStore;

import com.rakista.radio.internal.CONSTANTS;
import com.rakista.radio.internal.ToastHelper;
import com.rakista.radio.social.TwitterBase;
import com.rakista.radio.social.TwitterConstant;
import com.rakista.radio.social.TwitterLogOutCallback;
import com.rakista.radio.social.TwitterLoginCallback;
import com.rakista.radio.utility.Preferences;
import com.rakista.radio.utility.Utility;
import com.sugree.twitter.TwitterSessionStore;

public class AccountsLogin extends Activity implements OnClickListener, TwitterLoginCallback, TwitterLogOutCallback {
	private static Button FacebookLoginButton, TwitterLoginButton;
	private static TextView FbDescription, TDescription;
	
	private Facebook mFacebook;
	
	private ProgressDialog mProgress;
	private static final String[] PERMISSIONS = new String[] {"offline_access", "publish_stream", "user_photos", "publish_checkins", "photo_upload"};
	private String name = null;
	private ToastHelper toast;
	
//	private static final String TWITTER_OAUTH_REQUEST_TOKEN_ENDPOINT = "http://api.twitter.com/oauth/request_token";
//	private static final String TWITTER_OAUTH_ACCESS_TOKEN_ENDPOINT = "http://api.twitter.com/oauth/access_token";
//	private static final String TWITTER_OAUTH_AUTHORIZE_ENDPOINT = "http://api.twitter.com/oauth/authorize";
//	private CommonsHttpOAuthProvider commonsHttpOAuthProvider;
//    private CommonsHttpOAuthConsumer commonsHttpOAuthConsumer;
    
    private static final String TAG = AccountsLogin.class.getSimpleName();

	private Utility util;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Preferences.Load(getBaseContext());

		setContentView(R.layout.accountslogin);
		
		util = new Utility(getApplicationContext());
		toast = new ToastHelper(this, Toast.LENGTH_LONG);
		
		TwitterLoginButton = (Button) findViewById(R.id.account_layout_twitter_login_button);
		TwitterLoginButton.setOnClickListener(this);
		TDescription = (TextView) findViewById(R.id.account_layout_twitter_login_description);
		
		FacebookLoginButton = (Button)findViewById(R.id.account_layout_facebook_login_button);
		FacebookLoginButton.setOnClickListener(this);
		FbDescription = (TextView) findViewById(R.id.account_layout_facebook_login_description);
		
		mFacebook = new Facebook(getString(R.string.FB_APP_ID));
		
		SessionStore.restore(mFacebook, this);
		
		if(TwitterSessionStore.isValidSession(this)) {
			updateTwitterViews();
		}
        
        if (mFacebook.isSessionValid()) {
			name = SessionStore.getName(this);
			name = (name.equals("")) ? "Unknown" : name;
		}
        
        updateViews();
	}
	
	public void onClick(View view) {
		switch(view.getId()) {
		
		case R.id.account_layout_facebook_login_button:
			if(util.isConnected()) {
				if(mFacebook.isSessionValid()) {
					fbLogout();
				} else {
					mFacebook.authorize(this, PERMISSIONS, -1, new FbLoginDialogListener());
				}
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			break;
			
		case R.id.account_layout_twitter_login_button:
			if(util.isConnected()) {
				if(!TwitterSessionStore.isValidSession(this)) {
					TwitterBase.logIn(this, TwitterConstant.CONSUMER_KEY, TwitterConstant.CONSUMER_SECRET, TwitterConstant.CALLBACK_URL, this, true);
				} else {
					AccessToken at = new AccessToken(TwitterSessionStore.getAccessToken(this),
							TwitterSessionStore.getAccessTokenSecret(this),
							Long.valueOf(TwitterSessionStore.getUserId(this)).longValue());
					
					Twitter twitter = TwitterFactory.getSingleton();
					twitter.setOAuthConsumer(TwitterConstant.CONSUMER_KEY, TwitterConstant.CONSUMER_SECRET);
					twitter.setOAuthAccessToken(at);
					TwitterBase.logOut(twitter, this, this);
				}
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			break;
		}
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
	}
    
	public static Handler updateTwitterViews = new Handler() {
		public void handleMessage(Message msg) {
			if(msg.what == 1) {
			TwitterLoginButton.setText("Sign in");
			TDescription.setText("Not Connected");
			} else {
				TwitterLoginButton.setText("Logout");
				TDescription.setText("Connected as " + msg.getData().getString("username"));
			}
		};
	};
	
	public void updateViews() {
		if(mFacebook.isSessionValid()) {
			FacebookLoginButton.setText("LogOut");
			FbDescription.setText("Connected as: " + name);
		} else {
			FacebookLoginButton.setText("Sign in");
			FbDescription.setText("Not Connected");
		}
	}
	
	public void updateTwitterViews() {
		
		if(TwitterSessionStore.isValidSession(this)) {
			TwitterLoginButton.setText("Log Out");
			TDescription.setText("Connected as " + TwitterSessionStore.getName(this));
		} else {
			TwitterLoginButton.setText("Log Out");
			TDescription.setText("Not Connected");
		}
	}
    
    public final class FbLoginDialogListener implements DialogListener {
        
    	public void onComplete(Bundle values) {
            SessionStore.save(mFacebook, getApplicationContext());
            getFbName();
        }

        public void onFacebookError(FacebookError error) {
           Log.e(getClass().getSimpleName(), "Facebook Error connection failed: " + error.toString());
        }
        
        public void onError(FbDialogError error) {
        	toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
        	Log.e(getClass().getSimpleName(), "Facebook Dialog Error: " + error.toString());
        	updateViews();
        }

        public void onCancel() {
        	updateViews();
        }
    }
    
    protected Dialog onCreateDialog(int id) {
    	switch(id) {

    	case CONSTANTS.FBSPINNER_CONNECTING:
    		mProgress = new ProgressDialog(getApplicationContext());
    		mProgress.setMessage("Finalizing ...");
    		mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    		mProgress.setIndeterminate(true);
    		mProgress.setCancelable(false);
    		return mProgress;
    		
    	case CONSTANTS.FBSPINNER_DISCONNECTING:
    		mProgress = new ProgressDialog(getApplicationContext());
    		mProgress.setMessage("Logging Out! Please wait...");
    		mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    		mProgress.setIndeterminate(true);
    		mProgress.setCancelable(false);
    		return mProgress;
    		
    	default:
    		return null;
    	}
    }
    
	private void getFbName() {
		new Thread() {
			@Override
			public void run() {
		        String name = "";
		        try {
		        	String me = mFacebook.request("me");
		        	JSONObject jsonObj = (JSONObject) new JSONTokener(me).nextValue();
		        	name = jsonObj.getString("name");
		        	Log.e(getClass().getSimpleName(), "FBNAME " + name.toString());
		        } catch (Exception ex) {
		        	ex.printStackTrace();
		        }
		        mFbHandler.sendMessage(mFbHandler.obtainMessage(0, name));
			}
		}.start();
	}
	
	private void fbLogout() {
		new Thread() {
			@Override
			public void run() {
				SessionStore.clear(getApplicationContext());
				int what = 1;
		        try {
		        	mFacebook.logout(getApplicationContext());
		        	what = 0;
		        } catch (Exception ex) {
		        	ex.printStackTrace();
		        }
		        mHandler.sendMessage(mHandler.obtainMessage(what));
			}
		}.start();
	}
	
	public Handler mFbHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (msg.what == 0) {
				String username = (String) msg.obj.toString();
				Log.e(getClass().getSimpleName(), "OBTAINED MESSAGE: " + username);
		            
		        SessionStore.saveName(username, getApplicationContext());
		        name = username;
		        updateViews();
		        Toast.makeText(getApplicationContext(), "Connected to Facebook as " + username, Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(getApplicationContext(), "Connected to Facebook", Toast.LENGTH_SHORT).show();
			}
		}
	};
	
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (msg.what == 1) {
				Toast.makeText(getApplicationContext(), "Facebook logout failed", Toast.LENGTH_SHORT).show();
			} else {
				updateViews();
				Toast.makeText(getApplicationContext(), "Disconnected from Facebook", Toast.LENGTH_SHORT).show();
			}
		}
	};

	@Override
	public void onAuthorizationSuccess(Object[] object) {
		User user = (User)object[0];
		AccessToken at = (AccessToken)object[1];
		
		Log.e(TAG, "Screen Name: " + user.getScreenName());
		Log.e(TAG, "User ID: " + user.getId());
		Log.e(TAG, "User Name: " + user.getName());
		
		Log.e(TAG, "Access Token: " + at.getToken());
		Log.e(TAG, "Access Token Secret: " + at.getTokenSecret());
		
		TwitterSessionStore.save(at.getToken(), at.getTokenSecret(), this);
		TwitterSessionStore.setName(this, user.getName());
		TwitterSessionStore.setUserId(this, user.getId());
		updateTwitterViews();
	}

	@Override
	public void onLogOutSuccess() {
		updateTwitterViews();
	}
	
}
