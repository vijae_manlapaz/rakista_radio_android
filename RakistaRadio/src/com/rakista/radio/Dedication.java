package com.rakista.radio;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.rakista.radio.internal.DataBaseHandler;
import com.rakista.radio.internal.ToastHelper;
import com.rakista.radio.utility.JSONParser;
import com.rakista.radio.utility.Utility;
import com.rakista.radio.widget.AutoScrollingTextView;

public class Dedication extends ListFragment implements OnScrollListener, OnRefreshListener<ListView> {
//	private Utility util;
	private DataBaseHandler dbh;
//	private SQLiteDatabase SQLDB;
	//private SimpleAdapter adapter;
	private SampleAdapter adapter;
	private ToastHelper toast;

	private final String serverURL = "http://rakista.com/radio/api/rradio.php?method=getDedications";
//	private final String[] from = new String[] {"ARTIST", "TOTAL" };
//	private final int[] to = new int[] { R.id.playlist_artist, R.id.playlist_artist_song_count };
	private String TABLE_DEDICATION = "tbldedication";
//	private View header;
//	private View footer;
//	private View emptyView;
//	private LayoutInflater inflater;
	/*private ImageView refreshLogo;
	private Button refreshButton;*/

//	private Animation performAnimation;
	private static boolean loadmore;

	private ArrayList<HashMap<String, String>> myListItems = new ArrayList<HashMap<String, String>>();
//	private ArrayList<HashMap<String, String>> tempList;

//	private TextView LastUpdatelabel, LastUpdateValue;
	private PullToRefreshListView lv;

	static int pageCount = 10;
	private dedicationContextUpdater contextUpdater;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.topdedicated_layout, container, false);
		
		lv = (PullToRefreshListView) v.findViewById(R.id.dedication_pull_refresh_list);
		return v;
	}

	public void onDestroyView() {
		super.onDestroyView();
		setListAdapter(null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

//		util = new Utility(getActivity().getApplicationContext());
		dbh = new DataBaseHandler(getActivity().getApplicationContext());
//		SQLDB = dbh.getWritableDatabase();
		toast = new ToastHelper(getActivity().getApplicationContext(), Toast.LENGTH_LONG);

//		performAnimation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.refreshrotate);

//		Drawable y = getResources().getDrawable(R.drawable.listdivider);
		adapter = new SampleAdapter(getActivity().getApplicationContext(), myListItems);

		lv.setOnRefreshListener(this);
		
		lv.setOnScrollListener(this);
//		lv.setDivider(y);

		setListAdapter(adapter);

		if(!dbh.isTableExists(TABLE_DEDICATION)) {
			dbh.createTable(TABLE_DEDICATION);
		}

		if(Utility.isConnected()) {
			if(dbh.getDataCount(TABLE_DEDICATION) > 0) {
				myListItems.clear();
				myListItems.addAll(loadDataFromDatabase());
			} else {
				new LoadDedications(false, false, null).execute(serverURL);
			}
		} else {
			myListItems.clear();
			myListItems.addAll(loadDataFromDatabase());
		}

		adapter.notifyDataSetChanged();

		EasyTracker.getInstance(getActivity().getApplicationContext()).activityStart(getActivity());
		
		pageCount = 10;
	}

	class dedicationContextUpdater extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equals("updatededicationcontent")) {
				if(Utility.isConnected()) {
					new LoadDedications(true, false, null).execute(serverURL);
				} else {
					toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
				}
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		loadmore = false;
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		contextUpdater = new dedicationContextUpdater();
		getActivity().registerReceiver(contextUpdater, new IntentFilter("updatededicationcontent"));
//		
	}

	private ArrayList<HashMap<String, String>> loadDataFromDatabase() {
		return dbh.getAllData(TABLE_DEDICATION);
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

		//what is the bottom item that is visible
		int lastInScreen = firstVisibleItem + visibleItemCount;

		//is the bottom item visible & not loading more already ? Load more !
		if (lastInScreen == totalItemCount) {
			loadmore = true;
			Log.i(getClass().getSimpleName(), "onScroll: " + "lastInScreen: " + Integer.valueOf(lastInScreen).toString() + " loadmore?: " + Boolean.valueOf(loadmore).toString());

			if(loadmore) {
				if(Utility.isConnected()) {
					new LoadDedications(true, true, new testcallback()).execute(serverURL);
				} else {
					toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
				}
			}
		} else {
			loadmore = false;
		}
	}

	private class testcallback implements DedicationLoadMoreCallback {

		@Override
		public void hasLoaded(int page, ArrayList<HashMap<String, String>> result) {
			pageCount = pageCount + page;
			Log.i(getClass().getSimpleName(), "new page number: " + Integer.valueOf(pageCount).toString());
			myListItems.addAll(result);
			adapter.notifyDataSetChanged();
			loadmore = false;
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		if(scrollState == 0) {
			loadmore = true;
		}
	}

	/*======================================================================================
	 * 									ADAPTER CLASS
	 *======================================================================================
	 */
	public static class SampleAdapter extends BaseAdapter {

		static ArrayList<HashMap<String, String>> Data = new ArrayList<HashMap<String, String>>();
		private final Context mContext;

		public SampleAdapter(Context context, ArrayList<HashMap<String, String>> data) {
			this.mContext = context;
			Data = data;
		}

		public View getView(final int position, View view, ViewGroup parent) {

			if (view == null) {
				LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = vi.inflate(R.layout.topdedicated_view_group, null);
			}

			TextView Position = (TextView) view.findViewById(R.id.topdedicated_position_label);
			Position.setText(this.getItemId(position) + 1 + ". ");

			AutoScrollingTextView Title = (AutoScrollingTextView) view.findViewById(R.id.topdedicated_group_title);
			Title.setText(Data.get(position).get("title").toString());

			AutoScrollingTextView Artist = (AutoScrollingTextView) view.findViewById(R.id.topdedicated_group_artist);
			Artist.setText(Data.get(position).get("artist").toString());

			TextView Message = (TextView)view.findViewById(R.id.topdedicated_group_message);
			Message.setText(" \" " + Data.get(position).get("dedicationMessage").toString() + " \" ");

			TextView DedicatedBy = (TextView) view.findViewById(R.id.topdedicated_group_requestedby);
			DedicatedBy.setTypeface(null, Typeface.ITALIC);
			DedicatedBy.setText(Data.get(position).get("dedicationName").toString());

			TextView TimeAndDate = (TextView) view.findViewById(R.id.topdedicated_group_timeanddate);
			TimeAndDate.setText(Utility.convertTimeZones(Data.get(position).get("request_date").toString()));

			return view;
		}

		@Override
		public int getCount() {
			return Data.size();
		}

		@Override
		public Object getItem(int position) {
			return Data.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}
	}

	/*==================================================================================
	 * 									ASYNCTASK
	 *==================================================================================
	 * */
	public class LoadDedications extends AsyncTask<String, Void, ArrayList<HashMap<String,String>>> implements DialogInterface.OnCancelListener {
		private boolean showProgressDialog;
		private ProgressDialog mProgress;
		private boolean paging = false;
		private DedicationLoadMoreCallback callback;

		public LoadDedications(boolean showProgress, boolean PageMode , DedicationLoadMoreCallback listener) {
			showProgressDialog = showProgress;
			callback = listener;
			paging = PageMode;
		}

		protected void onPreExecute() {
			if(showProgressDialog) { // if true show progress dialog
				mProgress = new ProgressDialog(getActivity());
				mProgress.setMessage("Updating\nPlease Wait...");
				mProgress.setIndeterminate(true);
				mProgress.setCancelable(false);
//				mProgress.setOnCancelListener(this);
				mProgress.setCanceledOnTouchOutside(false);
				mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				mProgress.show();
			}
		}

		@Override
		public void onCancel(DialogInterface dialog) {
			Log.i(getClass().getSimpleName(), "Cancelling Thread");
			this.cancel(true);
		}

		protected void onCancelled() {
			Log.i(getClass().getSimpleName(), "Thread successfully cancelled");
			mProgress.dismiss();
		}

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(String... params) {
			/// JSON Node names
			final String RESULT = "result";
			final String TAG_ARTIST = "title";
			final String TAG_TITLE = "artist";
			final String TAG_DEDICATEDBY = "dedicationName";
			final String TAG_MESSAGE = "dedicationMessage";
			final String TAG_DATEPLAYED = "request_date";

			// contacts JSONArray
			JSONArray contacts = null;

			// Hashmap for ListView
			ArrayList<HashMap<String, String>> DataList = new ArrayList<HashMap<String, String>>();

			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			JSONObject json = null;
			String url = null;

			while(!isCancelled()) {

				if(paging == false) {
					Log.i(getClass().getSimpleName(), "PAGING false");
					url = params[0];
				} else {
					Log.i(getClass().getSimpleName(), "PAGING true");
					url = params[0] + "&start=" + pageCount;
					Log.i(getClass().getSimpleName(), url);
				}  

				try {
					json = jParser.getJSONFromUrl(url);
				} catch (HttpException e1) {
					e1.printStackTrace();
					json = null;
				} catch (UnknownHostException e) {
					e.printStackTrace();
					json = null;
				} catch (ConnectTimeoutException e) {
					json = null;
					e.printStackTrace();
				} catch (NullPointerException e) {
					e.printStackTrace();
				}

				try {

					if(json != null) {
						// Getting Array of Contacts
						contacts = json.getJSONArray(RESULT);

						// looping through All Contacts
						for(int i = 0; i < contacts.length(); i++){
							JSONObject c = contacts.getJSONObject(i);

							// Storing each json item in variable
							String Title = c.getString(TAG_TITLE);
							String Artist = c.getString(TAG_ARTIST);
							String Name = c.getString(TAG_DEDICATEDBY);
							String Message = c.getString(TAG_MESSAGE);
							String Date_Played = c.getString(TAG_DATEPLAYED);

							// creating new HashMap
							HashMap<String, String> map = new HashMap<String, String>();

							// adding each child node to HashMap key => value
							map.put(TAG_TITLE, Title);
							map.put(TAG_ARTIST, Artist);
							map.put(TAG_DEDICATEDBY, Name);
							map.put(TAG_MESSAGE, Message);
							map.put(TAG_DATEPLAYED, Date_Played);
							//map.put("position", Integer.valueOf(i + 1).toString());

							// adding HashList to ArrayList
							DataList.add(map);
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
				return DataList;
			}
			return null;
		}

		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {

			if(showProgressDialog) {
				mProgress.dismiss();
			}

			if(!result.isEmpty() || result != null) {
				if(paging == true) {
					paging = false;
					callback.hasLoaded(10, result);
				} else {
					myListItems.clear();
					myListItems.addAll(result);
					lv.onRefreshComplete();
				}
				adapter.notifyDataSetChanged();
				dbh.updateTable(TABLE_DEDICATION, result);
			} /*else {
				toast.show(getString(R.string.error),getString(R.string.connection_error_message));
			}*/
		}
	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		if(Utility.isConnected()) {
			new LoadDedications(true, false, new testcallback()).execute(serverURL);
		} else {
			toast.show(getString(R.string.error),getString(R.string.connection_error_message));
		}
	}
}