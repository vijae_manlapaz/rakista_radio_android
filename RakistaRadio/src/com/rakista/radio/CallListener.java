package com.rakista.radio;

import android.content.Context;
import android.media.AudioManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * This class will listen/handle incoming call and will mute the AudioManager Music Stream Volume
 * @author Vijae Manlapaz
 * @company Ridzler Entertainment Company
 */
public class CallListener extends PhoneStateListener {
	private Context mContext;
	
	public CallListener(Context context) {
		this.mContext = context;
	}
	
	@Override
    public void onCallStateChanged(int state, String incomingNumber) {
		
		AudioManager Audiomanager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		
        switch (state) {
        case TelephonyManager.CALL_STATE_RINGING:
        	
                Log.i(getClass().getSimpleName(), "Phone is Ringing\nthere is an incoming call\nmuting the music volume stream.");
                Audiomanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
            break;

        case TelephonyManager.CALL_STATE_IDLE:
        	Audiomanager.setStreamMute(AudioManager.STREAM_MUSIC, false);
        	break;
        }

        super.onCallStateChanged(state, incomingNumber);

    }
}
