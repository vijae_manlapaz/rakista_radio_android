package com.rakista.radio;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.BaseDialogListener;
import com.facebook.android.BaseRequestListener;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.android.FbDialogError;
import com.facebook.android.SessionStore;
import com.rakista.radio.internal.CONSTANTS;
import com.rakista.radio.utility.Utility;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class FacebookShare extends Activity implements OnClickListener {

	private Facebook mFacebook;
	private Button shareButton, addImage;
	private AutoCompleteTextView inputbox;
	private ImageView ImagePreview;

	private static int MAX_IMAGE_DIMENSION = 720;
	private final int SELECT_PHOTO = 100;

	private static AsyncFacebookRunner mAsyncRunner;

	private Utility util;

	private static Uri srcImage;

	public ProgressDialog mProgress;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.facebookshare_layout);

		util = new Utility(getApplicationContext());

		mFacebook = new Facebook(getString(R.string.FB_APP_ID));
		SessionStore.restore(mFacebook, getApplicationContext());
		mAsyncRunner = new AsyncFacebookRunner(mFacebook);

		shareButton = (Button) findViewById(R.id.facebookshare_layout_button_share);
		shareButton.setOnClickListener(this);

		addImage = (Button) findViewById(R.id.facebookshare_layout_button_addimage);
		addImage.setOnClickListener(this);

		ImagePreview = (ImageView) findViewById(R.id.facebookshare_layout_attachment);

		inputbox = (AutoCompleteTextView) findViewById(R.id.facebookshare_layout_textview_inputbox);
	}

	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.facebookshare_layout_button_share:
			if (util.isConnected()) {
				showDialog(SHOWDIALOG_LOADING);
				postToFacebookWithImage(srcImage, inputbox.getText().toString());
			}
			break;

		case R.id.facebookshare_layout_button_addimage:
			Intent intentPick = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			intentPick.setType("image/*");
			startActivityForResult(intentPick, SELECT_PHOTO);
			break;

		default:
			break;
		}
	}

	private final int SHOWDIALOG_LOADING = 1;

	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case SHOWDIALOG_LOADING:
			mProgress = new ProgressDialog(this);
			mProgress.setMessage("Sending your post please wait...");
			mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgress.setIndeterminate(true);
			mProgress.setCancelable(false);
			return mProgress;

		default:
			return null;
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {

		case SELECT_PHOTO:

			// if(resultCode == RESULT_OK) {
			/*
			 * Uri selectedImage = imageReturnedIntent.getData(); String[]
			 * filePathColumn = {MediaStore.Images.Media.DATA};
			 * 
			 * Cursor cursor = getContentResolver().query(selectedImage,
			 * filePathColumn, null, null, null); cursor.moveToFirst();
			 * 
			 * int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			 * String filePath = cursor.getString(columnIndex); String fileName
			 * = cursor.getColumnName(columnIndex); cursor.close();
			 * 
			 * 
			 * Log.i(getClass().getSimpleName(), "FileName: " + fileName +
			 * "\nFile Path: " + filePath); Bitmap yourSelectedImage =
			 * BitmapFactory.decodeFile(filePath);
			 */

			if (resultCode == Activity.RESULT_OK) {
				Uri photoUri = data.getData();
				srcImage = photoUri;
				try {
					displayPreview();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				Log.i(getClass().getSimpleName(), "File Path: "
						+ getPath(photoUri));
				break;
			}
		}
	}

	public void displayPreview() throws FileNotFoundException, IOException {
		ImagePreview.setImageBitmap(MediaStore.Images.Media.getBitmap(
				getContentResolver(), srcImage));
	}

	/**
	 * Post a message/status to <strong>Rakista Page</strong> with an image
	 * attachment coming from external source. (e.g. SD-CARD and such).
	 * 
	 * @param ImageUri image from SD-CARD or any external source.
	 * @param message Message to be posted as caption of photo.
	 */
	public void postToFacebookWithImage(Uri ImageUri, String message) {
		if (SessionStore.restore(mFacebook, getApplicationContext())) {
			if (ImageUri != null) {
				Log.i(getClass().getSimpleName(), "photo uri not null");
				Bundle params = new Bundle();
				try {
					// params.putByteArray("photo",
					params.putByteArray("source", scaleImage(getApplicationContext(), ImageUri));
				} catch (IOException e) {
					e.printStackTrace();
				}
				// params.putString("caption",
				//params.putString("message", message);
				mAsyncRunner.request(CONSTANTS.FBRADIO_APP_PAGE_ID + "/photos", params, "POST", new PhotoUploadListener());
				//357241287717735
			} else {
				Toast.makeText(getApplicationContext(), "Error selecting image from the gallery.", Toast.LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(getApplicationContext(), "No image selected for upload.", Toast.LENGTH_SHORT).show();
		}
	}

	public class SampleDialogListener extends BaseDialogListener {

		public void onComplete(Bundle values) {

			final String postId = values.getString("post_id");
			if (postId != null) {
				Log.d("Facebook-Example", "Dialog Success! post_id=" + postId);
				mAsyncRunner.request(postId, new WallPostListener());

			} else {
				Log.d("Facebook-Example", "No wall post made");
			}
		}
	}

	public final class WallPostListener extends BaseRequestListener {
		public void onComplete(final String response) {
			Log.i(getClass().getSimpleName(),
					"Post Successful! " + response.toString());
		}
	}

	/*
	 * callback for the photo upload
	 */
	public class PhotoUploadListener extends BaseRequestListener {

		@Override
		public void onComplete(final String response) {
			mProgress.dismiss();
			Log.i(getClass().getSimpleName(),
					"Upload Complete: " + response.toString());
			finish();
		}

		public void onFacebookError(FacebookError error) {
		}
	}

	@SuppressWarnings("unused")
	private final class LoginDialogListener implements DialogListener {

		Bundle params = new Bundle();

		@Override
		public void onComplete(Bundle values) {
			// JSONObject attachment = new JSONObject();
			//
			// JSONObject media = new JSONObject();
			// media.put("type", "image");
			// media.put("src", image); // where 'image' is the image url or
			// path stored in your device
			//
			// attachment.put("media", new JSONArray().put(media));
			// params.putString("attachment", attachment.toString());
			//
			// mFacebook.dialog(getApplicationContext(), "stream.publish",
			// params, new SampleDialogListener());

		}

		@Override
		public void onFacebookError(FacebookError e) {
		}

		@Override
		public void onError(FbDialogError e) {
		}

		@Override
		public void onCancel() {
		}
	}

	// ==========================================================================
	// HELPER METHOD
	// ==========================================================================

	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = getContentResolver().query(uri, projection, null, null,
				null);

		if (cursor != null) {
			// HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
			// THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} else {
			return null;
		}
	}

	public String getRealPathFromURI(Uri contentUri) {
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(contentUri, proj, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	public byte[] scaleImage(Context context, Uri photoUri) throws IOException {
		InputStream is = context.getContentResolver().openInputStream(photoUri);
		BitmapFactory.Options dbo = new BitmapFactory.Options();
		dbo.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(is, null, dbo);
		is.close();

		int rotatedWidth, rotatedHeight;
		int orientation = getOrientation(context, photoUri);

		if (orientation == 90 || orientation == 270) {
			rotatedWidth = dbo.outHeight;
			rotatedHeight = dbo.outWidth;
		} else {
			rotatedWidth = dbo.outWidth;
			rotatedHeight = dbo.outHeight;
		}

		Bitmap srcBitmap;
		is = context.getContentResolver().openInputStream(photoUri);
		if (rotatedWidth > MAX_IMAGE_DIMENSION || rotatedHeight > MAX_IMAGE_DIMENSION) {
			float widthRatio = ((float) rotatedWidth) / ((float) MAX_IMAGE_DIMENSION);
			float heightRatio = ((float) rotatedHeight) / ((float) MAX_IMAGE_DIMENSION);
			float maxRatio = Math.max(widthRatio, heightRatio);

			// Create the bitmap from file
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inSampleSize = (int) maxRatio;
			srcBitmap = BitmapFactory.decodeStream(is, null, options);
		} else {
			srcBitmap = BitmapFactory.decodeStream(is);
		}
		is.close();

		/*
		 * if the orientation is not 0 (or -1, which means we don't know), we
		 * have to do a rotation.
		 */
		if (orientation > 0) {
			Matrix matrix = new Matrix();
			matrix.postRotate(orientation);

			srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, true);
		}

		String type = context.getContentResolver().getType(photoUri);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		if (type.equals("image/png")) {
			srcBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
		} else if (type.equals("image/jpg") || type.equals("image/jpeg")) {
			srcBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		}
		byte[] bMapArray = baos.toByteArray();
		baos.close();
		return bMapArray;
	}

	public int getOrientation(Context context, Uri photoUri) {
		/* it's on the external media. */
		Cursor cursor = context.getContentResolver().query(photoUri,
				new String[] { MediaStore.Images.ImageColumns.ORIENTATION },
				null, null, null);

		if (cursor.getCount() != 1) {
			return -1;
		}

		cursor.moveToFirst();
		int orientation = cursor.getInt(0);
		cursor.close();

		return orientation;
	}
}
