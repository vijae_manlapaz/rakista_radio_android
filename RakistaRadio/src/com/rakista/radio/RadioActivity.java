package com.rakista.radio;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.MenuDrawer.OnDrawerStateChangeListener;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SlidingDrawer;
import android.widget.SlidingDrawer.OnDrawerCloseListener;
import android.widget.SlidingDrawer.OnDrawerOpenListener;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.android.BaseRequestListener;
import com.facebook.android.Facebook;
import com.facebook.android.SessionStore;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gcm.GCMRegistrar;
import com.rakista.radio.applicationwidget.WidgetIntentReceiver;
import com.rakista.radio.callback.TweeterPostCallback;
import com.rakista.radio.core.GetCurrentSongDetails;
import com.rakista.radio.core.GetNextSongDetails;
import com.rakista.radio.core.IDGenerator;
import com.rakista.radio.core.SendFacebookPost;
import com.rakista.radio.core.SendTweetDialog;
import com.rakista.radio.core.VoteSender;
import com.rakista.radio.internal.AsyncTaskCompleteListener;
import com.rakista.radio.internal.CONSTANTS;
import com.rakista.radio.internal.ConstantCommand;
import com.rakista.radio.internal.IMediaPlayerServiceClient;
import com.rakista.radio.internal.PagerAdapter;
import com.rakista.radio.internal.StatefulMediaPlayer.MPStates;
import com.rakista.radio.internal.ToastHelper;
import com.rakista.radio.service.MediaPlayerService;
import com.rakista.radio.service.MediaPlayerService.MediaPlayerBinder;
import com.rakista.radio.service.ServiceHelper;
import com.rakista.radio.sites.twitterwebview;
import com.rakista.radio.sites.youTubewebviewactivity;
import com.rakista.radio.utility.Preferences;
import com.rakista.radio.utility.ServerUtility;
import com.rakista.radio.utility.Utility;
import com.rakista.radio.widget.ActionItem;
import com.rakista.radio.widget.AutoScrollingTextView;
import com.rakista.radio.widget.MyViewPager;
import com.rakista.radio.widget.QuickAction;
import com.rakista.radio.widget.QuickAction.OnActionItemClickListener;
import com.sugree.twitter.TwitterSessionStore;
import com.viewpagerindicator.TitlePageIndicator;
//import com.winsontan520.wversionmanager.library.WVersionManager;

/**
 * 
 * @author Vijae Manlapaz
 * 
 */
public class RadioActivity extends FragmentActivity implements ViewPager.OnPageChangeListener, View.OnClickListener, IMediaPlayerServiceClient, OnDrawerOpenListener, OnDrawerCloseListener, OnActionItemClickListener, OnDrawerStateChangeListener {

//	TabHost.OnTabChangeListener,
//	private testclass mTabHost;
//	private TabHost mTabHost;
//	private TabSpec tabspec;
//	private TextView nextsongLabel;
//	private static ImageView imageView;
//	private List<String> PERMISSIONS = Arrays.asList("publish_actions", "manage_pages", "publish_stream", "email");
//	private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
//	private boolean pendingPublishReauthorization = false;
//	private ProgressDialog mDialog;
//	private AsyncTask<HashMap<String, String>, Void, Void> mAsyncTask;
//	private WVersionManager VersionManager = new WVersionManager(this);
//	private HashMap<String, TabInfo> mapTabInfo = new HashMap<String, RadioActivity.TabInfo>();
//	private HorizontalScrollView tabscrollview;
//	private final static int DISCONNECTING_PROGRESS_DIALOG = 2;
//	private final static int EXIT_DIALOG = 4;
//	private StatefulMediaPlayer mMediaplayer;
//	private StreamStation station = CONSTANTS.DEFAULT_STREAM_STATION;
	
	private MyViewPager mViewPager;

	private static String TAG = RadioActivity.class.getSimpleName();
	private PagerAdapter mPagerAdapter;
	private MenuDrawer menuDrawer;
	private ImageView menuButton;
	private SlidingDrawer slidingdrawer;
	private TextView _Home, _Playlist, _TopMusic, _Dedications, _History,
	_Feed, _Profile, _Members, _Friends, _Mail, _Forum, _Blogs,
	_FeaturedEvents, _Marketplace, _Polls, _Quizzes, _Jokes, _Vidoes,
	_Photos, _Pages, _MusicNews, _FacebookPage, _FacebookGroup, _FacebookWall,
	 _Youtube, _Twitter, _Support, _About, _Exit, _Sleep, _Alarm,
	_Contact, _Rec; //_FBFanPhoto,
	private ImageButton _Settings, _Programs, _Share, PlayStop, _Chat;
	private TextView slidingDrawerHandle;
	private final static int CONNECTING_PROGRESS_DIALOG = 1;
	private final static int STOPPING_PROGRESS_DIALOG = 2;
	private ProgressDialog mConnectingDialog, mStoppingDialog, mBufferingDialog;
	public boolean mBound;
	public static MediaPlayerService mService;
	
	private Utility util;
	private VoteSender Vote;
	private ToastHelper toast;
	private IntentFilter filter, filter2, filter3; //filter1
	private TextView _sdArtist, _sdAlbum; //labelConnection;
	private TextView _sdLyrics, _sdAlbumyear, _sdDuration;
	private Button slidingdrawerVote, slidingdrawerBuy;
	private String _BuyCD = "";
	private String currentSongID = null;
	private AutoScrollingTextView _sdTitle;
	private AutoScrollingTextView nextSong;
	private TitlePageIndicator mPageIndicator;
	private Facebook mFacebook;
	private QuickAction quickAction;
	private final int AI_FACEBOOK = 1;
	private final int AI_TWITTER = 2;
	public static ImageView refresh1, searchButton;
	public int selectedItem;
	public int Buffkey;
	private AlarmManager alarmManager;
	public boolean tempHasPicture;
	public String tempPicture;
	
	private GetCurrentSongDetails currentSongDetails = new GetCurrentSongDetails();
	private GetNextSongDetails nextSongDetails = new GetNextSongDetails();
	
	private currrentSongDetailsCallbackListener currentSongCallback = new currrentSongDetailsCallbackListener();
	private nextSongDetailsCallbackListener nextSongCallback = new nextSongDetailsCallbackListener();

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);
		
		bindToService();
		/*
		 * ========================================================= 
		 * Initialize Facebook and Twitter Client.
		 * ========================================================
		 */
		mFacebook = new Facebook(getString(R.string.FB_APP_ID));
		toast = new ToastHelper(getApplicationContext(), Toast.LENGTH_LONG);
		util = new Utility(getApplicationContext());

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove Render Dithering.
		// getWindow().setFormat(PixelFormat.RGB_888);
//		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DITHER);

		if(!Utility.isTablet(getApplicationContext())) {
			menuDrawer = MenuDrawer.attach(this, MenuDrawer.MENU_DRAG_WINDOW);
			menuDrawer.setContentView(R.layout.menu_layout);
			menuDrawer.setMenuView(R.layout.menu_layout);
			menuDrawer.setOnDrawerStateChangeListener(this);
		}

		// =================================================
		// INFLATE THE MAIN LAYOUT
		// =================================================
		setContentView(R.layout.activity_main);

		ActionItem AIfacebook = new ActionItem(AI_FACEBOOK, "Share on Facebook");
		ActionItem AItwitter = new ActionItem(AI_TWITTER, "Tweet on Twitter");

		quickAction = new QuickAction(getApplicationContext(), QuickAction.VERTICAL);
		quickAction.setAnimStyle(QuickAction.ANIM_REFLECT);
		quickAction.addActionItem(AIfacebook);
		quickAction.addActionItem(AItwitter);
		quickAction.setOnActionItemClickListener(this);

		filter = new IntentFilter();
		filter.addAction("CURRENTSONG_METADATA");
		registerReceiver(dataReceiver, filter);

		filter2 = new IntentFilter();
		filter2.addAction("CURRENTSONG_DETAILS");
		registerReceiver(dataReceiver, filter2);

		filter3 = new IntentFilter();
		filter3.addAction("NEXTSONG_DETAILS");
		registerReceiver(dataReceiver, filter3);

		IntentFilter IF = new IntentFilter();
		IF.addAction("push");
		registerReceiver(dataReceiver, IF);

		IntentFilter IF1 = new IntentFilter();
		IF1.addAction("com.rakista.radio.dev.ALARM");
		registerReceiver(dataReceiver, IF1);

		Preferences.Load(getApplication().getApplicationContext());

		intialiseViewPager();
		mPageIndicator = (TitlePageIndicator) findViewById(R.id.indicator);
		mPageIndicator.setViewPager(mViewPager);
		mPageIndicator.setOnPageChangeListener(this);

		// Initialize Buttons.
		initialiseButtons();

		mConnectingDialog = new ProgressDialog(getApplicationContext());
		mStoppingDialog = new ProgressDialog(getApplicationContext());
		mBufferingDialog = new ProgressDialog(getApplicationContext());
		
		if(getIntent().getBooleanExtra("com.rakista.radio.widget.REQUEST", false)) {
			if (Utility.isConnected()) {
				try {
					mService.initializePlayer(CONSTANTS.STATIONS_TOSTRING[Preferences.getStreamSelectedSetting()]);
					PlayStop.setSelected(true);
					showDialog(CONNECTING_PROGRESS_DIALOG);
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
				WidgetIntentReceiver.updateWidgetButtonListener(getApplicationContext(), false);
			}
		}

		if(getIntent().getBooleanExtra("ALARM", false)) {
			Log.i(getClass().getSimpleName(), "Alarm Receive getIntent");
			if (Utility.isConnected() == false) {

				AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

				int maxvolume = audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
				audio.setStreamVolume(AudioManager.STREAM_MUSIC, maxvolume, 0);

				mp = MediaPlayer.create(this, R.raw.alarm);//onReceive gives you context
				mp.setLooping(true);
				mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
				mp.start();

				String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

				AlertDialog.Builder ADB = new AlertDialog.Builder(getApplicationContext())
				.setTitle("Alarm")
				.setMessage(currentDateTimeString)
				.setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						mp.stop();
						mp.release();
						mp = null;
					}
				});

				AlertDialog AD = ADB.create();
				AD.show();
			} else {
				PlayStop.setSelected(true);
				showDialog(CONNECTING_PROGRESS_DIALOG);
				mService.initializePlayer(CONSTANTS.STATIONS_TOSTRING[Preferences.getStreamSelectedSetting()]);
			}
		}

		selectedItem = Preferences.getSleepTimeIndex();

		EasyTracker.getInstance(this).activityStart(this);

		if(Utility.isTablet(getApplicationContext())) {
			mViewPager.setPagingEnabled(false);
		}
		
		if(_BuyCD.equals("")) {
			slidingdrawerBuy.setEnabled(false);
			slidingdrawerBuy.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.buy_inactive), null, null, null);
			slidingdrawerBuy.setTextColor(Color.DKGRAY);
		}
		
		if(currentSongID != null) {
			slidingdrawerVote.setEnabled(true);
			slidingdrawerVote.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.vote),null, null, null);
			slidingdrawerVote.setTextColor(Color.WHITE);
		} else {
			slidingdrawerVote.setEnabled(false);
			slidingdrawerVote.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.vote_inactive), null, null, null);
			slidingdrawerVote.setTextColor(Color.DKGRAY);
		}
	}

	// =============================================================
	// QuickAction onClick Listener
	// =============================================================
	@SuppressWarnings("unused")
	@Override
	public void onItemClick(QuickAction source, int pos, int actionId) {
		ActionItem Item = quickAction.getActionItem(pos);

		if (actionId == AI_FACEBOOK) {
			if (SessionStore.restore(mFacebook, getApplicationContext())) {
				Utility.sendSocialInteraction("Facebook", "SHARE", this);
				SendFacebookPost facebookPost = new SendFacebookPost(this, mFacebook, new WallPostListener(), slidingDrawerHandle.getText().toString());
				facebookPost.show();
			} else {
				Toast.makeText(getApplicationContext(), "Please Login to your Facebook account\nGo to Settings > Accounts > Facebook", Toast.LENGTH_LONG).show();
			}
		} else if (actionId == AI_TWITTER) {

			if (TwitterSessionStore.isValidSession(this)) {
				SendTweetDialog sendTweet = new SendTweetDialog(this, new SendTweetCallback(), slidingDrawerHandle.getText().toString());
				sendTweet.show();
			} else {
				Toast.makeText(getApplicationContext(), "Please Login to your Twitter account\nGo to Settings > Accounts > Twitter", Toast.LENGTH_LONG).show();
			}
		}
	}

	private class WallPostListener extends BaseRequestListener {
		public void onComplete(final String response) {
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreateDialog(int)
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case CONNECTING_PROGRESS_DIALOG:
			mConnectingDialog = new ProgressDialog(this);
			mConnectingDialog.setMessage("Connecting Please wait...");
			mConnectingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mConnectingDialog.setIndeterminate(true);
			mConnectingDialog.setCancelable(true);
			mConnectingDialog.setCanceledOnTouchOutside(true);
			mConnectingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					Log.i(getClass().getSimpleName(), "Connecting Dialog Cancelled");
					//new Runnable() {
						//@Override
						//public void run() {
							Log.i(getClass().getSimpleName(), "Showing Stop Dialog.");
							showDialog(STOPPING_PROGRESS_DIALOG);
							stoptest();
						//}
					//}.run();
				}
			});

			return mConnectingDialog;
			
		case STOPPING_PROGRESS_DIALOG: 
			mStoppingDialog = new ProgressDialog(this);
			mStoppingDialog.setMessage("Stopping\nPlease wait...");
			mStoppingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mStoppingDialog.setIndeterminate(true);
			mStoppingDialog.setCancelable(false);
			mStoppingDialog.setCanceledOnTouchOutside(false);

			return mStoppingDialog;
			
		case 701:
			
			mBufferingDialog = new ProgressDialog(this);
			mBufferingDialog.setMessage("Buffering...");
			mBufferingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mBufferingDialog.setIndeterminate(true);
			mBufferingDialog.setCancelable(true);
			mBufferingDialog.setCanceledOnTouchOutside(true);
			mBufferingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					new Runnable() {
						
						@Override
						public void run() {
							showDialog(STOPPING_PROGRESS_DIALOG);
						}
					}.run();
					
					Log.i(getClass().getSimpleName(), "Connecting Dialog Cancelled");
					new ServiceHelper(mService).stopMediaPlayer();
					
				}
			});

			return mBufferingDialog;
		}
		return null;
	}
	
	final Runnable stopPlayerTest  = new Runnable() {
		@Override
		public void run() {
			new ServiceHelper(mService).stopMediaPlayer();
		}
	};
	
	void stoptest() {
		Log.i(getClass().getSimpleName(), "stopping player.");
		new Thread(new Runnable() {
			@Override
			public void run() {
				uiHandler.post(stopPlayerTest);
			}
		}).start();
	}
	
	public Handler stopPlayer = new Handler() {
		public void handleMessage(Message msg) {
			switch(msg.what) {
			case 1:
				if(mService.getMediaPlayer().getState() == MPStates.STARTED) {
					mService.stopMediaPlayer();
				}
				break;
				
			case 2:
				Log.i(getClass().getSimpleName(), "Stopping Player");
				mService.stopMediaPlayer();
				Log.i(getClass().getSimpleName(), "Player Stopped");
				break;
			}
		}
	};

	MediaPlayer mp = null;

	public void showAlarmReceiveDialog() {

		AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

		int maxvolume = audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		audio.setStreamVolume(AudioManager.STREAM_MUSIC, maxvolume, 0);

		mp = MediaPlayer.create(this, R.raw.alarm);//Onreceive gives you context
		mp.setLooping(true);
		mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mp.start(); //and this to play it

		String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
		AlertDialog.Builder ADB = new AlertDialog.Builder(this)
		.setTitle("Alarm")
		.setMessage(currentDateTimeString)
		.setCancelable(false)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				mp.stop();
				mp.release();
				mp = null;
			}
		});

		AlertDialog AD = ADB.create();
		AD.show();
	}

	/**
	 * Receives the broadcast from MediaPlayerService and handles its Intent
	 * Extras.
	 */
	private BroadcastReceiver dataReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, final Intent intent) {
			
			if(intent.getAction().equals("com.rakista.radio.dev.ALARM")) {
				if (!Utility.isConnected()) {
					showAlarmReceiveDialog();
				} else {
					/*
					 * Set Music Volume to MAX.
					 */
					AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
					int maxvolume = audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
					audio.setStreamVolume(AudioManager.STREAM_MUSIC, maxvolume, 0);
					
					PlayStop.setSelected(true);
					showDialog(CONNECTING_PROGRESS_DIALOG);
					mService.initializePlayer(CONSTANTS.STATIONS_TOSTRING[Preferences.getStreamSelectedSetting()]);
				}
			}

			if (intent.getAction().equals("CURRENTSONG_DETAILS")) {
				currentSongDetails = new GetCurrentSongDetails();
				currentSongDetails.setListener(currentSongCallback);
				if (mService.getMediaPlayer().getState().equals(MPStates.STARTED) || mService.getMediaPlayer().getState().equals(MPStates.PREPARED)) {
					currentSongDetails.execute(CONSTANTS.getCurrentSongURL);
				}
			}

			if (intent.getAction().equals("NEXTSONG_DETAILS")) {
				nextSongDetails = new GetNextSongDetails();
				nextSongDetails.setListener(nextSongCallback);
				if (mService.getMediaPlayer().getState().equals(MPStates.STARTED) || mService.getMediaPlayer().getState().equals(MPStates.PREPARED)) {
					nextSongDetails.execute(CONSTANTS.getNextSongURL);
				}
			}
		}
	};
	
	class currrentSongDetailsCallbackListener implements AsyncTaskCompleteListener {

		@Override
		public void onTaskComplete(ArrayList<HashMap<String, String>> temp) {
			Log.i(getClass().getSimpleName(), "onTaskComplete");
			if (temp.size() != 0) {
				String title = temp.get(0).get("title");
				String artist = temp.get(0).get("artist");
				String album = temp.get(0).get("album");
				String albumYear = temp.get(0).get("albumyear");
				String duration = temp.get(0).get("durationDisplay");
				String lyrics = temp.get(0).get("lyrics");
				String buycd = temp.get(0).get("buycd");
				String dedicationmessage = temp.get(0).get("dedicationMessage");
				String dedicationname = temp.get(0).get("dedicationName");
				
				if (lyrics == "null") {
					_sdLyrics.setText("Not Available");
				} else {
					_sdLyrics.setText(lyrics);
				}

				_sdTitle.setText(title);
				_sdArtist.setText(artist);
				_sdAlbum.setText(album);
				_sdAlbumyear.setText(albumYear);
				_sdDuration.setText(duration);
				currentSongID = temp.get(0).get("ID");

				if (buycd.length() > 4) {
					_BuyCD = buycd;
					slidingdrawerBuy.setEnabled(true);
					slidingdrawerBuy.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.buy),null, null, null);
					slidingdrawerBuy.setTextColor(Color.WHITE);
				} else {
					_BuyCD = "";
					slidingdrawerBuy.setEnabled(false);
					slidingdrawerBuy.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.buy_inactive), null, null, null);
					slidingdrawerBuy.setTextColor(Color.DKGRAY);
				}
				
				if(currentSongID != null) {
					slidingdrawerVote.setEnabled(true);
					slidingdrawerVote.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.vote),null, null, null);
					slidingdrawerVote.setTextColor(Color.WHITE);
				} else {
					slidingdrawerVote.setEnabled(false);
					slidingdrawerVote.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.vote_inactive), null, null, null);
					slidingdrawerVote.setTextColor(Color.DKGRAY);
				}
				
				Intent albumArtIntent = new Intent();
				albumArtIntent.setAction(CONSTANTS.ALBUMART);

				if(Boolean.parseBoolean(temp.get(0).get("isRequested"))) {
					slidingDrawerHandle.setText(artist + " - " + title + " �requested");
				} else {
					slidingDrawerHandle.setText(artist + " - " + title);
				}

				if(Boolean.parseBoolean(temp.get(0).get("isDedication"))) {
					albumArtIntent.putExtra("isDedicated", Boolean.TRUE);
					albumArtIntent.putExtra("message", dedicationmessage);
					albumArtIntent.putExtra("name", dedicationname);
				} else {
					albumArtIntent.putExtra("isDedicated", Boolean.FALSE);
				}

				if(temp.get(0).get("haspicture").equalsIgnoreCase("true")) {
					albumArtIntent.putExtra("picture", temp.get(0).get("picture"));
					albumArtIntent.putExtra("haspicture", Boolean.TRUE);
				} else {
					albumArtIntent.putExtra("picture", "");
					albumArtIntent.putExtra("haspicture", Boolean.FALSE);
				}

				tempHasPicture = temp.get(0).get("haspicture").equalsIgnoreCase("true")? Boolean.TRUE : Boolean.FALSE;
				tempPicture = temp.get(0).get("picture");

				Preferences.setMediaHasPicture(tempHasPicture);
				
				sendBroadcast(albumArtIntent);
			}
		}

		@Override
		public void onTaskCancelled(boolean iscancelled) {
			Log.i(getClass().getSimpleName(), "Get current song onTaskCancelled");
			String text = (String) slidingDrawerHandle.getText();
			if(text.length() > 0 || text != null) {
				Log.i("GetNextSongDetails", "CURRENT SONG INDICATOR is not empty");
				slidingDrawerHandle.setText("");
			}
		}
	}
	
	class nextSongDetailsCallbackListener implements AsyncTaskCompleteListener {

		@Override
		public void onTaskComplete(ArrayList<HashMap<String, String>> result) {
			String title = "";
			String artist = "";
			String requestedIndicator = "";
			if (result.size() != 0) {
				title = result.get(0).get("TITLE");
				artist = result.get(0).get("ARTIST");
				
				if(result.get(0).get("REQUESTID").length() > 1) {
					requestedIndicator = " �requested";
				}
				nextSong.setText("Next: " + artist + " - " + title + requestedIndicator);
			}
		}
		
		@Override
		public void onTaskCancelled(boolean iscancelled) {
			Log.i(getClass().getSimpleName(), "Next song onTaskCancelled");
			String text = (String) nextSong.getText();
			if(text.length() > 0 || text != null) {
				Log.i(getClass().getSimpleName(), "NEXT SONG INDICATOR is not empty");
				nextSong.setText("");
			}
		}
	}

	/**
	 * Initialize all clickable views.
	 * 
	 * @author Vijae Manlapaz
	 * @Company Rakista Media Production.
	 */
	private void initialiseButtons() {
		// ===============================================================
		// Tab views Scroller
		// ===============================================================
		// tabscrollview =
		// (HorizontalScrollView)findViewById(R.id.tabscrollview);

		// ===============================================================
		// Menu Call Button
		// ===============================================================
		menuButton = (ImageView) findViewById(R.id.menu_button);
		if(Utility.isTablet(getApplicationContext())) {
			menuButton.setVisibility(View.GONE);
		} else {
			menuButton.setVisibility(View.VISIBLE);
			menuButton.setOnClickListener(this);
		}

		// ===============================================================
		// Media Control Button.
		// ===============================================================
		PlayStop = (ImageButton) findViewById(R.id.media_control_button_playstop);
		PlayStop.setOnClickListener(this);
		
		// ===============================================================
		// Menu Button Views.
		// ===============================================================
		if(!Utility.isTablet(this)) {
			_Home = (TextView) findViewById(R.id.menu_layout_button_home);
			_Home.setOnClickListener(this);

			_Playlist = (TextView) findViewById(R.id.menu_layout_button_playlist);
			_Playlist.setOnClickListener(this);

			_TopMusic = (TextView) findViewById(R.id.menu_layout_button_topmusic);
			_TopMusic.setOnClickListener(this);

			_Dedications = (TextView) findViewById(R.id.menu_layout_button_dedication);
			_Dedications.setOnClickListener(this);

			_History = (TextView) findViewById(R.id.menu_layout_button_history);
			_History.setOnClickListener(this);

			_Feed = (TextView) findViewById(R.id.menu_layout_button_feed);
			_Feed.setOnClickListener(this);

			_Profile = (TextView) findViewById(R.id.menu_layout_button_profile);
			_Profile.setOnClickListener(this);

			_Members = (TextView) findViewById(R.id.menu_layout_button_members);
			_Members.setOnClickListener(this);

			_Friends = (TextView) findViewById(R.id.menu_layout_button_friends);
			_Friends.setOnClickListener(this);

			_Mail = (TextView) findViewById(R.id.menu_layout_button_mail);
			_Mail.setOnClickListener(this);

			_Forum = (TextView) findViewById(R.id.menu_layout_button_forum);
			_Forum.setOnClickListener(this);

			_Blogs = (TextView) findViewById(R.id.menu_layout_button_blog);
			_Blogs.setOnClickListener(this);

			_FeaturedEvents = (TextView) findViewById(R.id.menu_layout_button_featuredevents);
			_FeaturedEvents.setOnClickListener(this);

			_Marketplace = (TextView) findViewById(R.id.menu_layout_button_marketplace);
			_Marketplace.setOnClickListener(this);

			_Polls = (TextView) findViewById(R.id.menu_layout_button_polls);
			_Polls.setOnClickListener(this);

			_Quizzes = (TextView) findViewById(R.id.menu_layout_button_quizzes);
			_Quizzes.setOnClickListener(this);

			_Jokes = (TextView) findViewById(R.id.menu_layout_button_jokes);
			_Jokes.setOnClickListener(this);

			_Vidoes = (TextView) findViewById(R.id.menu_layout_button_vidoes);
			_Vidoes.setOnClickListener(this);

			_Photos = (TextView) findViewById(R.id.menu_layout_button_photos);
			_Photos.setOnClickListener(this);

			_Pages = (TextView) findViewById(R.id.menu_layout_button_pages);
			_Pages.setOnClickListener(this);

			_MusicNews = (TextView) findViewById(R.id.menu_layout_button_musicnews);
			_MusicNews.setOnClickListener(this);

			_FacebookPage = (TextView) findViewById(R.id.menu_layout_button_facebook_page);
			_FacebookPage.setOnClickListener(this);

			_FacebookGroup = (TextView) findViewById(R.id.menu_layout_button_facebook_group);
			_FacebookGroup.setOnClickListener(this);

			_FacebookWall = (TextView) findViewById(R.id.menu_layout_button_facebookwall);
			_FacebookWall.setOnClickListener(this);

			/*_FBFanPhoto = (TextView) findViewById(R.id.menu_layout_button_fbfanphoto);
			_FBFanPhoto.setOnClickListener(this);*/

			_Youtube = (TextView) findViewById(R.id.menu_layout_button_youtube);
			_Youtube.setOnClickListener(this);

			_Twitter = (TextView) findViewById(R.id.menu_layout_button_twitter);
			_Twitter.setOnClickListener(this);

			_Support = (TextView) findViewById(R.id.menu_layout_button_support);
			_Support.setOnClickListener(this);

			_Contact = (TextView) findViewById(R.id.menu_layout_button_contact);
			_Contact.setOnClickListener(this);

			_About = (TextView) findViewById(R.id.menu_layout_button_about);
			_About.setOnClickListener(this);

			_Rec = (TextView) findViewById(R.id.menu_layout_button_rec);
			_Rec.setOnClickListener(this);

			_Exit = (TextView) findViewById(R.id.menu_layout_button_exit);
			_Exit.setOnClickListener(this);

			_Sleep = (TextView) findViewById(R.id.menu_layout_button_sleep);
			_Sleep.setOnClickListener(this);

			_Alarm = (TextView) findViewById(R.id.menu_layout_button_alarm);
			_Alarm.setOnClickListener(this);
		}

		_Chat = (ImageButton) findViewById(R.id.media_control_chatButton);
		_Chat.setOnClickListener(this);

		_Settings = (ImageButton) findViewById(R.id.media_control_button_setting);
		_Settings.setOnClickListener(this);

		_Programs = (ImageButton) findViewById(R.id.media_control_button_programs);
		_Programs.setOnClickListener(this);

		_Share = (ImageButton) findViewById(R.id.media_control_button_share);
		_Share.setOnClickListener(this);

		// ===============================================================
		// Sliding Drawer Views.
		// ===============================================================
		_sdLyrics = (TextView) findViewById(R.id.slidingDrawer_lyrics);
		_sdLyrics.setMovementMethod(new ScrollingMovementMethod());

		_sdTitle = (AutoScrollingTextView) findViewById(R.id.slidingdrawer_title);

		_sdArtist = (TextView) findViewById(R.id.slidingdrawer_artist);
		_sdAlbum = (TextView) findViewById(R.id.slidingdrawer_album);
		_sdAlbumyear = (TextView) findViewById(R.id.slidingdrawer_year);
		_sdDuration = (TextView) findViewById(R.id.slidingdrawer_duration);
		slidingdrawerVote = (Button) findViewById(R.id.slidingdrawer_button_vote);
		slidingdrawerVote.setOnClickListener(this);
		slidingdrawerBuy = (Button) findViewById(R.id.slidingdrawer_button_buy);

		// ===============================================================
		// Sliding Drawer Handle
		// ===============================================================
		slidingdrawer = (SlidingDrawer) findViewById(R.id.slidingDrawer1);
		slidingDrawerHandle = (TextView) findViewById(R.id.handle);
		slidingdrawer.setOnDrawerOpenListener(this);
		slidingdrawer.setOnDrawerCloseListener(this);

		// ===============================================================
		// Connection Notifier
		// ===============================================================
		//imageView = (ImageView) findViewById(R.id.imageView3);
		//labelConnection = (TextView) findViewById(R.id.media_control_label_connection);

		// ===============================================================
		// Next Song Indicator
		// ===============================================================
		nextSong = (AutoScrollingTextView) findViewById(R.id.media_control_nextsong);
		// nextsongLabel = (TextView)
		// findViewById(R.id.media_control_nextsong_label);

//		refresh = (ImageView) findViewById(R.id.refreshButton);
//		refresh.setOnClickListener(this);
//		refresh.setVisibility(View.GONE);

		searchButton = (ImageView) findViewById(R.id.searchButton);
		searchButton.setOnClickListener(this);
		searchButton.setVisibility(View.GONE);
	}

	//	private static Handler showLoader = new Handler() {
	//		public void handleMessage(Message msg) {
	//			if (msg.what == 1) {
	//				AnimationDrawable frameAnimation = (AnimationDrawable) imageView
	//						.getBackground();
	//				frameAnimation.start();
	//			} else if (msg.what == 0) {
	//				AnimationDrawable frameAnimation = (AnimationDrawable) imageView
	//						.getBackground();
	//				frameAnimation.stop();
	//			}
	//		}
	//	};

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onStart()
	 */
	public void onStart() {
		super.onStart();
		Log.i(getClass().getSimpleName(), "onStart");
		Log.i(getClass().getSimpleName(),"ExpirationTime: " + Long.valueOf(Preferences.getgcmExpirationTime()).toString());

		if(GCMRegistrar.isRegisteredOnServer(getApplicationContext())) {
			if(System.currentTimeMillis() > Preferences.getgcmExpirationTime()) {
				Log.i(getClass().getSimpleName(), "Registration ID has Expired Requesting for new Registration ID");
				unRegisterFromRcom();
				GCMRegistrar.register(getBaseContext(), CONSTANTS.GCM_SENDERID);
			}
		} else {
			GCMRegistrar.register(getBaseContext(), CONSTANTS.GCM_SENDERID);
		}
		
		new IDGenerator(getApplicationContext()).generateID();
	}

	public void unRegisterFromRcom() {
		new Thread() {
			public void run() {
				ServerUtility.unregister(getApplicationContext(), Preferences.getgcmCurrentRegistrationId());
			}
		}.start();
	}

	/**
	 * Handle all button click event include Menu buttons and Media control
	 * button.
	 */
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.menu_button:
			menuDrawer.openMenu(true);
			Utility.sendClickEvent("MENU_BUTTON", this);
			break;

		case R.id.menu_layout_button_home:
			mViewPager.setCurrentItem(0, true);
			menuDrawer.closeMenu();
			Utility.sendClickEvent("MENU_HOME_BUTTON", this);
			break;

		case R.id.menu_layout_button_playlist:
			mViewPager.setCurrentItem(1, true);
			menuDrawer.closeMenu();
			Utility.sendClickEvent("MENU_PLAYLIST_BUTTON", this);
			break;

		case R.id.menu_layout_button_history:
			mViewPager.setCurrentItem(2, true);
			menuDrawer.closeMenu();
			Utility.sendClickEvent("MENU_HISTORY_BUTTON", this);
			break;

		case R.id.menu_layout_button_topmusic:
			mViewPager.setCurrentItem(3, true);
			menuDrawer.closeMenu();
			Utility.sendClickEvent("MENU_TOPMUSIC_BUTTON", this);
			break;

		case R.id.menu_layout_button_dedication:
			mViewPager.setCurrentItem(4, true);
			menuDrawer.closeMenu();
			Utility.sendClickEvent("MENU_DEDICATION_BUTTON", this);
			break;

		case R.id.menu_layout_button_feed:
			if (Utility.isConnected()) {
				Utility.openWebview(getString(R.string.feed_url), this);
				menuDrawer.closeMenu(true);
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_FEED_BUTTON", this);
			break;

		case R.id.menu_layout_button_profile:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.profile_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_PROFILE_BUTTON", this);
			break;

		case R.id.menu_layout_button_members:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.members_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_MEMBERS_BUTTON", this);
			break;

		case R.id.menu_layout_button_friends:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.friends_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_FRIENDS_BUTTON", this);
			break;

		case R.id.menu_layout_button_mail:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.mail_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_MAIL_BUTTON", this);
			break;

		case R.id.menu_layout_button_forum:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.forum_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_FORUMS_BUTTON", this);
			break;

		case R.id.menu_layout_button_blog:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.blogs_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_BLOGS_BUTTON", this);
			break;

		case R.id.menu_layout_button_musicnews:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.musicnews_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_MUSICNEWS_BUTTON", this);
			break;

		case R.id.menu_layout_button_featuredevents:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.featuredevents_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_FEATUREDEVENTS_BUTTON", this);
			break;

		case R.id.menu_layout_button_marketplace:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.marketplace_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_MARKETPLACE_BUTTON", this);
			break;

		case R.id.menu_layout_button_polls:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.polls_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_POLLS_BUTTON", this);
			break;

		case R.id.menu_layout_button_quizzes:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.quizzes_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_QUIZZES_BUTTON", this);
			break;

		case R.id.menu_layout_button_jokes:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.jokes_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_JOKES_BUTTON", this);
			break;

		case R.id.menu_layout_button_vidoes:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.vidoes_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_VIDEOS_BUTTON", this);
			break;

		case R.id.menu_layout_button_photos:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.photos_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_PHOTOS_BUTTON", this);
			break;

		case R.id.menu_layout_button_pages:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.pages_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_PAGES_BUTTON", this);
			break;

		case R.id.media_control_chatButton:
			if (Utility.isConnected()) {
				Utility.openWebview("http://www.rakista.com/chatrooms/mobile/", this);
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("CHAT_BUTTON", this);
			break;

		case R.id.menu_layout_button_facebook_page:
			if (Utility.isConnected()) {
				Utility.openWebview("http://www.facebook.com/rakistaradio", this);
				menuDrawer.closeMenu(true);
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_FACEBOOKPAGE_BUTTON", this);

			break;

		case R.id.menu_layout_button_facebookwall:
			if(Utility.isConnected()) {
				Utility.openWebview("http://rakista.com/radio/mobile/fbwall.html", this);
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_FACEBOOKWALL_BUTTON", this);
			break;

		case R.id.menu_layout_button_facebook_group:
			if(Utility.isConnected()) {
				Utility.openWebview("https://www.facebook.com/groups/rakistajam/", this);
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_FACEBOOKGROUP_BUTTON", this);
			break;

			/*case R.id.menu_layout_button_fbfanphoto:
			if (Utility.isConnected()) {
				if (SessionStore.restore(mFacebook, getApplicationContext())) {
					Intent x = new Intent(this, FacebookShare.class);
					startActivity(x);
					menuDrawer.closeMenu(true);
				} else {
					Toast.makeText(
							getApplicationContext(),
							"Please Login to your facebook account\nGo to Settings > Accounts > Facebook",
							Toast.LENGTH_LONG).show();
				}
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			break;*/

		case R.id.menu_layout_button_youtube:
			if (Utility.isConnected()) {
				final Intent youtubeIntent = new Intent(this, youTubewebviewactivity.class);
				startActivity(youtubeIntent);
				menuDrawer.closeMenu(true);
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_YOUTUBE_BUTTON", this);
			break;

		case R.id.menu_layout_button_twitter:
			if (Utility.isConnected()) {
				final Intent twitterIntent = new Intent(this, twitterwebview.class);
				startActivity(twitterIntent);
				menuDrawer.closeMenu(true);
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_TWITTER_BUTTON", this);
			break;

		case R.id.menu_layout_button_support:
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.rakista.radio&hl=en"));
			startActivity(intent);
			Utility.sendClickEvent("MENU_SUPPORT_SUPPORT", this);
			break;

		case R.id.menu_layout_button_contact:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.contactus_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_CONTACT_BUTTON", this);
			break;
		case R.id.menu_layout_button_about:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.about_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_ABOUT_BUTTON", this);
			break;

		case R.id.menu_layout_button_rec:
			if(Utility.isConnected()) {
				Utility.openWebview("http://www.recph.com", this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("MENU_REC_BUTTON", this);
			break;

		case R.id.menu_layout_button_exit:
			try {
				if(mService.getMediaPlayer().isPlaying()) {
					stopPlayer.sendEmptyMessage(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			Utility.sendClickEvent("MENU_EXIT_BUTTON", this);
			moveTaskToBack(true);
			break;

			//TODO
		case R.id.media_control_button_playstop:
			if (PlayStop.isPressed()) {
				if (PlayStop.isSelected()) {

					PlayStop.setSelected(false);
					slidingDrawerHandle.setText("");
					nextSong.setText("");

					Utility.sendClickEvent("STOP_BUTTON", this);

					stopPlayer.sendEmptyMessage(1);

				} else {
					if (Utility.isConnected()) {
						
						Utility.sendClickEvent("PLAY_BUTTON", this);
						
						PlayStop.setSelected(true);
						mService.initializePlayer(CONSTANTS.STATIONS_TOSTRING[Preferences.getStreamSelectedSetting()]);

						showDialog(CONNECTING_PROGRESS_DIALOG);
						
						Preferences.setMediaIsPlaying(true);
					} else {
						toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
					}
				}
			}
			break;

		case R.id.media_control_button_setting:
			final Intent settingsIntent = new Intent(this, Settings.class);
			startActivity(settingsIntent);
			Utility.sendClickEvent("SETTINGS_BUTTON", this);
			break;

		case R.id.media_control_button_programs:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.programs_url), this);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("PROGRAMS_BUTTON", this);
			break;

		case R.id.media_control_button_share:
			if (Utility.isConnected()) {
				quickAction.show(findViewById(R.id.media_control_button_share));
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("SHARE_BUTTON", this);
			break;

		case R.id.slidingdrawer_button_buy:
			Utility.sendClickEvent("DRAWER_BUY_BUTTON", this);
			if (Utility.isConnected()) {
				if (_BuyCD != null && _BuyCD.length() > 4) {
					Intent buycd = new Intent(Intent.ACTION_VIEW);
					buycd.setData(Uri.parse(_BuyCD.toString()));
					startActivity(buycd);
				} else {
					Log.e(TAG, "Sliding Drawer buy button is disabled\nReason: current song buy cd url is null");
				}
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			break;

		case R.id.slidingdrawer_button_vote:
			Utility.sendClickEvent("DRAWER_VOTE_BUTTON", this);
			if (Utility.isConnected()) {
				if (currentSongID != null) {
					Vote = new VoteSender(this);
					Vote.sendVote(currentSongID);
					currentSongID = null;
				} else {
					Log.e(TAG, "Sliding Drawer vote button is disabled\nReason: current song id is null");
				}
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			break;

		case R.id.searchButton:
			final Intent searchActivity = new Intent(this, Search.class);
			startActivity(searchActivity);
			Utility.sendClickEvent("SEARCH_BUTTON", this);
			break;

//		case R.id.refreshButton:
//			Utility.sendClickEvent("REFRESH_BUTTON", this);
//
//			switch (mViewPager.getCurrentItem()) {
//			case 0: // Home Fragment
//				break;
//
//			case 1: // Playlist Fragment
//				refresh.startAnimation(performAnimation);
//				final Intent playlistIntent = new Intent();
//				playlistIntent.setAction("updateplaylistcontent");
//				sendBroadcast(playlistIntent);
//				Utility.sendClickEvent("PLAYLIST_REFRESH_BUTTON", this);
//				break;
//
//			case 2: // History Fragment
//				refresh.startAnimation(performAnimation);
//				final Intent HistoryIntent = new Intent();
//				HistoryIntent.setAction("updatehistorycontent");
//				sendBroadcast(HistoryIntent);
//				Utility.sendClickEvent("HISTORY_REFRESH_BUTTON", this);
//				break;
//
//			case 3: // TopMusic Fragment
//				refresh.startAnimation(performAnimation);
//				final Intent topmusicIntent = new Intent();
//				topmusicIntent.setAction("updatetopmusiclist");
//				sendBroadcast(topmusicIntent);
//				Utility.sendClickEvent("TOPMUSIC_REFRESH_BUTTON", this);
//				break;
//
//			case 4: // Dedication Fragment
//				refresh.startAnimation(performAnimation);
//				final Intent dedicationIntent = new Intent();
//				dedicationIntent.setAction("updatededicationcontent");
//				sendBroadcast(dedicationIntent);
//				Utility.sendClickEvent("DEDICATION_REFRESH_BUTTON", this);
//				break;
//			}
//			break;

		case R.id.menu_layout_button_sleep:

			Utility.sendClickEvent("Menu_Button_Sleep", this);
			// Follow this sequence order (zero base).
			final CharSequence[] choiceList = {"Off", "15 Minutes", "20 Minutes", "25 Minutes", "30 Minutes", "35 Minutes","40 Minutes", "50 Minutes", "55 Minutes", "1 Hour"};

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Select sleep time");

			builder.setSingleChoiceItems(choiceList, Preferences.getSleepTimeIndex(), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, final int which) {
					Buffkey = which;	
				}
			});

			builder.setPositiveButton("Set", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Preferences.setSleepTimeIndex(Buffkey);
					if(mService.getMediaPlayer().getState() == MPStates.STARTED) {
						setSleepTime(Buffkey);
					} else {
						setSleepTime(Buffkey);
					}
				}
			});
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			AlertDialog SleepDialog = builder.create();
			SleepDialog.show();

			break;

		case R.id.menu_layout_button_alarm:

			AlarmDialog AD = new AlarmDialog(this);
			AD.show();

			break;
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(dataReceiver);
		Preferences.setMediaIsPlaying(false);
	}

	public void setSleepTime(int position) {
		int x;
		alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		Intent intent = new Intent(getBaseContext(), RadioAlarmManagerReceiver.class);
		intent.setAction(ConstantCommand.START_SLEEP);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		// Getting current time and add the seconds in it
		Calendar cal = Calendar.getInstance();

		switch (position) {
		case 1:
			x = 60 * 15;
			//x = 3;
			cal.add(Calendar.SECOND, x);
			break;
		case 2:
			x = 60 * 20;
			cal.add(Calendar.SECOND, x);
			break;
		case 3:
			x = 60 * 25;
			cal.add(Calendar.SECOND, x);
			break;
		case 4:
			x = 60 * 30;
			cal.add(Calendar.SECOND, x);
			break;
		case 5:
			x = 60 * 25;
			cal.add(Calendar.SECOND, x);
			break;

		case 6:
			x = 60 * 40;
			cal.add(Calendar.SECOND, x);
			break;
		case 7:
			x = 60 * 45;
			cal.add(Calendar.SECOND, x);
			break;
		case 8:
			x = 60 * 50;
			cal.add(Calendar.SECOND, x);
			break;

		case 9:
			x = 60 * 55;
			cal.add(Calendar.SECOND, x);
			break;

		case 10:
			x = 60 * 60;
			cal.add(Calendar.SECOND, x);
			break;
		}

		if(position == 0) {
			alarmManager.cancel(pendingIntent);
		} else {
			alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
					pendingIntent);
		}
	}

	class SendTweetCallback implements TweeterPostCallback {

		@Override
		public void onSuccess(String response) {
			toast.show("Twitter", "Tweet successfully sent");
		}
	}

	/**
	 * Binds to the instance of MediaPlayerService. If no instance of
	 * MediaPlayerService exists, it first starts a new instance of the service.
	 */
	public void bindToService() {
		Intent intent = new Intent(getApplicationContext(), MediaPlayerService.class);

		if (Utility.MediaPlayerServiceRunning(getApplicationContext())) {
			// Bind to LocalService
			bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
		} else {
			startService(intent);
			bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
		}
	}

	/**
	 * Defines callback for service binding, passed to bindService()
	 */
	public ServiceConnection mConnection = new ServiceConnection() {
		
		/*
		 * (non-Javadoc)
		 * @see android.content.ServiceConnection#onServiceConnected(android.content.ComponentName, android.os.IBinder)
		 */
		public synchronized void onServiceConnected(ComponentName className, IBinder serviceBinder) {
			Log.d(getClass().getSimpleName(), "service connected");

			/**
			 *  bound with Service. get Service instance
			 */
			MediaPlayerBinder binder = (MediaPlayerBinder) serviceBinder;
			mService = binder.getService();

			/**
			 *  send this instance to the service, so it can make callback on
			 *  this instance as a client
			 */
			mService.setClient(RadioActivity.this);
			mBound = true;
		}
		
		/*
		 * (non-Javadoc)
		 * @see android.content.ServiceConnection#onServiceDisconnected(android.content.ComponentName)
		 */
		public synchronized void onServiceDisconnected(ComponentName classname) {
			Log.i(getClass().getSimpleName(), "Service Disconnected");
			mBound = false;
		}
	};

	/*
	 * (non-Javadoc)
	 * @see android.widget.SlidingDrawer.OnDrawerOpenListener#onDrawerOpened()
	 */
	@Override
	public void onDrawerOpened() {
		slidingdrawerBuy.setOnClickListener(this);
		mViewPager.setPagingEnabled(false);

		if(Utility.isTablet(getApplicationContext())) {
			mViewPager.setPagingEnabled(false);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.SlidingDrawer.OnDrawerCloseListener#onDrawerClosed()
	 */
	@Override
	public void onDrawerClosed() {
		if(Utility.isTablet(getApplicationContext())) {
			mViewPager.setPagingEnabled(false);
		} else {
			mViewPager.setPagingEnabled(true);
		}
	}

	@Override
	public void onDrawerStateChange(int oldState, int newState) {
		
		if(oldState == 8 && newState == 2) {
			menuButton.setImageResource(R.drawable.menu_close);
		} else {
			menuButton.setImageResource(R.drawable.menu_open);
		}
		
		switch(newState) {
		case 1:
			menuButton.setImageResource(R.drawable.menu_close);
			break;
			
		case 0: //CLOSE
			menuButton.setImageResource(R.drawable.menu_close);
			break;
			
		case 4:
			menuButton.setImageResource(R.drawable.menu_open);
			break;
			
		case 8: //OPEN
			menuButton.setImageResource(R.drawable.menu_open);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onKeyUp(int, android.view.KeyEvent)
	 */
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			if(!Utility.isTablet(getApplicationContext())) {
				menuDrawer.toggleMenu(true);
			}
			return true;
		} else {
			return super.onKeyUp(keyCode, event);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		if(!Utility.isTablet(this)) {
			if(menuDrawer.isMenuVisible()) {
				menuDrawer.closeMenu(true);
			} else {
				moveTaskToBack(true);
			}
		} else {
			moveTaskToBack(true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.rakista.radio.internal.IMediaPlayerServiceClient#onInitializePlayerStart(java.lang.String)
	 */
	public void onInitializePlayerStart(String message) {
		WidgetIntentReceiver.updateStatus(this, message);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.rakista.radio.internal.IMediaPlayerServiceClient#onInitializePlayerSuccess()
	 */
	public void onInitializePlayerSuccess() {

		// dismiss connecting dialog
		mConnectingDialog.dismiss();

		WidgetIntentReceiver.updateWidgetButtonListener(this, true);
		WidgetIntentReceiver.updateStatus(this, "100% Pinoy Rock!");

		PlayStop.setSelected(true);

		Preferences.setMediaIsPlaying(true);

	}

	/*
	 * (non-Javadoc)
	 * @see com.rakista.radio.internal.IMediaPlayerServiceClient#onError()
	 */
	public void onError() {}

	/*
	 * (non-Javadoc)
	 * @see com.rakista.radio.internal.IMediaPlayerServiceClient#onStop(boolean)
	 */
	@Override
	public void onStop(boolean state) {
		Log.i(getClass().getSimpleName(), "onStop Command Receive");
		PlayStop.setSelected(false);

		WidgetIntentReceiver.updateTitleandStatus(this);
		WidgetIntentReceiver.updateWidgetButtonListener(this, false);

		Preferences.setMediaIsPlaying(false);
		Preferences.setIsDedicated(false);

		uiHandler.post(UpdateUI);
		
		if(mViewPager.getCurrentItem() == 0) {
			Log.i(getClass().getSimpleName(), "Sending Broadcast is dedicated false");
			ImageView HomeLogo = (ImageView) findViewById(R.id.home_layout_radiologo);
			HomeLogo.setImageResource(R.drawable.rakista_radio_logo);
			
			Intent i = new Intent(this, NowPlaying.class);
			i.setAction(CONSTANTS.ALBUMART);
			i.putExtra("isDedicated", false);
			sendBroadcast(i);
		}
		
		if(mConnectingDialog.isShowing()) {
			mConnectingDialog.dismiss();
		} else if(mStoppingDialog.isShowing()) {
			mStoppingDialog.dismiss();
		} else if(mBufferingDialog.isShowing()) {
			mBufferingDialog.dismiss();
		}
	}
	
	final Handler uiHandler = new Handler();
	
	final Runnable UpdateUI = new Runnable() {
		@Override
		public void run() {
			Log.i("TEST", "CHECKING INDICATORS");
			if(currentSongDetails.getStatus().equals(AsyncTask.Status.RUNNING) || nextSongDetails.getStatus().equals(AsyncTask.Status.RUNNING)) {
				currentSongDetails.cancelTask();
				nextSongDetails.cancelTask();
			} else if((currentSongDetails.getStatus().equals(AsyncTask.Status.FINISHED) || nextSongDetails.getStatus().equals(AsyncTask.Status.FINISHED)) || (currentSongDetails.getStatus().equals(AsyncTask.Status.PENDING) || nextSongDetails.getStatus().equals(AsyncTask.Status.PENDING))) {
				slidingDrawerHandle.setText("");
				nextSong.setText("");
			} else {
				slidingDrawerHandle.setText("");
				nextSong.setText("");
			}
		}
	};
	
	@Override
	public void onReset() {
		Log.i(getClass().getSimpleName(), "on Reset");
	}
	
//	final Timer t = new Timer();
	
	//TODO
	@Override
	public void streamBufferingStarted() {
		if(mService.getMediaPlayer().isPlaying() && mBufferingDialog.isShowing()) {
			if(mBufferingDialog.isShowing()) {
				mBufferingDialog.dismiss();
			}
		} else {
			showDialog(701);
			
            /*t.schedule(new TimerTask() {
                public void run() {
                	mBufferingDialog.dismiss(); // when the task active then close the dialog
                    t.cancel(); // also just stop the timer thread, otherwise, you may receive a crash report.
                    mService.stopMediaPlayer(); // stop the media service.
                }
            }, 30000);*/ // 30 seconds time out.
		}
	}

	@Override
	public void streamBufferingFinished() {
		if(mBufferingDialog.isShowing()) {
			mBufferingDialog.dismiss();
		}
		
		//t.cancel(); // cancel the timer.
	}

	protected void onSaveInstanceState(Bundle outState) {}

	/**
	 * Initialize ViewPager
	 */
	private void intialiseViewPager() {

		List<Fragment> fragments = new Vector<Fragment>();
		fragments.add(Fragment.instantiate(this, NowPlaying.class.getName())); //Index position 0
		fragments.add(Fragment.instantiate(this, Playlist.class.getName())); //Index position 1
		fragments.add(Fragment.instantiate(this, History.class.getName())); //Index position 2
		fragments.add(Fragment.instantiate(this, TopMusic.class.getName())); //Index position 3
		fragments.add(Fragment.instantiate(this, Dedication.class.getName())); //Index position 4
		this.mPagerAdapter = new PagerAdapter(super.getSupportFragmentManager(), fragments);

		this.mViewPager = (MyViewPager) super.findViewById(R.id.viewpager);
		this.mViewPager.setAdapter(this.mPagerAdapter);
		this.mViewPager.setOnPageChangeListener(this);
	}

	int currentPage;

	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

	public void onPageSelected(int position) {

		if (position == 1) {
			searchButton.setVisibility(View.VISIBLE);
//			refresh.setVisibility(View.GONE);
		} else if(position == 1){
//			refresh.setVisibility(View.VISIBLE);
			searchButton.setVisibility(View.VISIBLE);
		} else {
//			refresh.setVisibility(View.VISIBLE);
			searchButton.setVisibility(View.GONE);
		}
	}

	public void onPageScrollStateChanged(int state) {}

	@Override
	public void onPlayback() {
		if(mConnectingDialog.isShowing() && mService.getMediaPlayer().isPlaying()) {
			mConnectingDialog.dismiss();
		}
	}
	
}