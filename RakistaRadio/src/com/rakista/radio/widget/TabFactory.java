package com.rakista.radio.widget;

import android.content.Context;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.TabContentFactory;

/**
 * A simple factory that returns dummy views to the Tabhost
 * @author Vijae Manlapaz
 */
public class TabFactory implements TabContentFactory {

    private final Context mContext;
    private final TabHost mTabHost;

    /**
     * @param context
     */
    public TabFactory(Context context, TabHost tabhost) {
        mContext = context;
        mTabHost = tabhost;
    }

    /** (non-Javadoc)
     * @see android.widget.TabHost.TabContentFactory#createTabContent(java.lang.String)
     */
    public View createTabContent(String tag) {
        View v = new View(mContext);
        v.setMinimumWidth(0);
        v.setMinimumHeight(0);
        return v;
    }
    
    /**
     * Sets a custom height for the TabWidget, in dp-units
     * @param height The height value, corresponding to dp-units
     */
    public void setTabHeight(int height) {
        for (int i = 0; i < mTabHost.getTabWidget().getTabCount(); i++) {
            mTabHost.getTabWidget().getChildAt(i).getLayoutParams().height /= 2;
                //= (int) (height * getResources().getDisplayMetrics().density);
        }
    }
}
