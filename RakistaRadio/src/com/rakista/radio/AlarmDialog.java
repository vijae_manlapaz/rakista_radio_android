package com.rakista.radio;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.rakista.radio.internal.ConstantCommand;
import com.rakista.radio.internal.ToastHelper;
import com.rakista.radio.utility.Preferences;

@SuppressLint("SimpleDateFormat")
@SuppressWarnings("unused")
public class AlarmDialog extends Dialog implements android.view.View.OnClickListener, OnCheckedChangeListener {

	private Context mContext;
	private LinearLayout mContent, mRepeatButton, mEnableButton, mTimeButton, mTimePreview;
	private CheckBox checkbox;
	private TextView mTitle, mTimeValue, mTime, mRepeatValue, mRepeat, mTimeset, mTimesetLabel;
	private ImageView mTimesetSeperator;
	private Button Set, Cancel;
	
	static final float[] DIMENSIONS_LANDSCAPE = { 460, 260 };
	static final float[] DIMENSIONS_PORTRAIT = { 280, 420 };
	
	static final FrameLayout.LayoutParams WRAP_CONTENT = new FrameLayout.LayoutParams(
			ViewGroup.LayoutParams.WRAP_CONTENT,
			ViewGroup.LayoutParams.WRAP_CONTENT);
	
	static final FrameLayout.LayoutParams FILL_PARENT = new FrameLayout.LayoutParams(
			ViewGroup.LayoutParams.MATCH_PARENT,
			ViewGroup.LayoutParams.MATCH_PARENT);
	
	static final int MARGIN = 4;
	static final int PADDING = 2;
	
	private Calendar cal;
	Date date;
	boolean[] item;
	final CharSequence[] days = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
	
	private int mHours, mMins;
	private Calendar calendar = GregorianCalendar.getInstance();
	private AlarmManager SUNDAY, Am;
	private Calendar currentDateTime = GregorianCalendar.getInstance();
	
	private ToastHelper toast;
	
	private Calendar timePickerCalendar;
	int[] time;
	
	boolean[] temp = Preferences.getSelectedDay();
	public ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();
	public ArrayList<PendingIntent> oneShotintentArray = new ArrayList<PendingIntent>();
	
	public AlarmDialog(Context context) {
		super(context);
		mContext = context;
	}
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		Preferences.Load(getContext());
		
		mContent = new LinearLayout(getContext());
		mContent.setBackgroundResource(R.drawable.toast_bg);
		mContent.setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));
		mContent.setLayoutParams(FILL_PARENT);
		mContent.setOrientation(LinearLayout.VERTICAL);
		
		timePickerCalendar = Calendar.getInstance();
		cal = Calendar.getInstance();
		cal = new GregorianCalendar();
		date = cal.getTime();
		time = Preferences.getAlarmTime1();
		toast = new ToastHelper(getContext(), 10000);
		
		//Log.i("oncreate", "TimePickerCalendar: " + timePickerCalendar.getTime().toString());
		
		/*MONDAY = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
		TUESDAY  = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
		WEDNESDAY  = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
		THURSDAY = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
		FRIDAY = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
		SATURDAY = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);*/
		SUNDAY = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
		Am = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
		
		setupTitle();
		setupContent();
		
		Display display = getWindow().getWindowManager().getDefaultDisplay();
		final float scale = getContext().getResources().getDisplayMetrics().density;
		float[] dimensions = display.getWidth() < display.getHeight() ? DIMENSIONS_PORTRAIT : DIMENSIONS_LANDSCAPE;
		
		addContentView(mContent, new FrameLayout.LayoutParams((int) (dimensions[0] * scale + 0.5f), LayoutParams.WRAP_CONTENT));
				//(int) (dimensions[1] * scale + 0.5f))); // Height
				//LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
	}
	
	public void setupTitle() {
		//Drawable icon = getContext().getResources().getDrawable(R.drawable.ic_alarm);
		mTitle = new TextView(getContext());
        mTitle.setText("Set Alarm");
        mTitle.setTextColor(Color.WHITE);
        mTitle.setTypeface(Typeface.DEFAULT_BOLD);
        mTitle.setBackgroundColor(Color.DKGRAY);
        mTitle.setPadding(MARGIN, MARGIN + MARGIN, MARGIN, MARGIN + MARGIN);
        //mTitle.setCompoundDrawablePadding(MARGIN + PADDING);
        mTitle.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        mTitle.setBackgroundColor(Color.BLACK);
        //mTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        mContent.addView(mTitle);
	}
	
	public void setupContent() {
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.alarm_layout, null);
		
		Set = (Button) view.findViewById(R.id.alarm_layout_set_button);
		Set.setOnClickListener(this);
		Cancel = (Button) view.findViewById(R.id.alarm_layout_cancel_button);
		Cancel.setOnClickListener(this);
		
		mEnableButton = (LinearLayout) view.findViewById(R.id.alarm_layout_enable_button);
		checkbox = (CheckBox) view.findViewById(R.id.alarm_layout_checkbox);
		mTime = (TextView) view.findViewById(R.id.alarm_layout_time);
		mTimeButton = (LinearLayout) view.findViewById(R.id.alarm_layout_time_button);
		mTimeValue = (TextView) view.findViewById(R.id.alarm_layout_time_value);
		mRepeat = (TextView) view.findViewById(R.id.alarm_layout_repeatindays);
		mRepeatValue = (TextView) view.findViewById(R.id.alarm_layout_repeat_value);
		mRepeatButton = (LinearLayout) view.findViewById(R.id.alarm_layout_repeat_button);
		mTimePreview = (LinearLayout) view.findViewById(R.id.alarm_layout_prev_container);
		mTimeset = (TextView) view.findViewById(R.id.alarm_layout_timepreview);
		mTimesetLabel = (TextView) view.findViewById(R.id.alarm_layout_timepreview_label);
		mTimesetSeperator = (ImageView) view.findViewById(R.id.alarm_layout_timeprev_seperator);
		
		checkbox.setOnCheckedChangeListener(this);
		checkbox.setOnClickListener(this);
		checkbox.setChecked(Preferences.getSetupAlarmIsEnabled());
		
		mEnableButton.setOnClickListener(this);
		mTimeValue.setText(Preferences.getAlarmTime());
		mRepeatButton.setOnClickListener(this);
		mRepeatValue.setText(selectedDays(Preferences.getSelectedDay()));
		mTimeButton.setOnClickListener(this);
		
		timePickerCalendar.set(Calendar.HOUR_OF_DAY, time[0]);
		timePickerCalendar.set(Calendar.MINUTE, time[1]);
		
		if(selectedDays(temp).equals("Never")) {
			mTimesetSeperator.setVisibility(View.VISIBLE);
			mTimePreview.setVisibility(View.VISIBLE);
			mTimeset.setText(Preferences.getTimeAlarm());
		} else {
			mTimePreview.setVisibility(View.GONE);
			mTimesetSeperator.setVisibility(View.GONE);
		}
		
		if(!checkbox.isChecked()) {
			mTimeButton.setEnabled(false);
			mRepeatButton.setEnabled(false);
			mRepeat.setTextColor(Color.GRAY);
			mTime.setTextColor(Color.GRAY);
			
			mTimeValue.setTextColor(Color.DKGRAY);
			mRepeatValue.setTextColor(Color.DKGRAY);
						
			mTimeset.setTextColor(Color.DKGRAY);
			mTimesetLabel.setTextColor(Color.DKGRAY);
		} else {
			mTimeset.setTextColor(Color.WHITE);
			mTimesetLabel.setTextColor(Color.WHITE);
		}
		
		mContent.addView(view);
	}
	
	private String selectedDays(boolean[] value) {
		String y = "";
		int j = 0;
		
		for(int i = 0; i < value.length; i++) {
			if(value[i] == Boolean.TRUE) {
				y += (String) days[i].subSequence(0, 3) + ", ";
				mTimePreview.setVisibility(View.GONE);
				mTimesetSeperator.setVisibility(View.GONE);
			} else if(value[i] == Boolean.FALSE) {
				j++;
			}
		}
		
		if(j == value.length) {
			y = "Never";
			mTimePreview.setVisibility(View.VISIBLE);
			mTimesetSeperator.setVisibility(View.VISIBLE);
		}
		
		return y;
	}
	
	@Override
	public void onClick(View view) {
		
		switch(view.getId()) {
		
		case R.id.alarm_layout_repeat_button:
			
			item = Preferences.getSelectedDay();
			
			AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
			builder.setTitle("Select Day")
			.setMultiChoiceItems(days, item, new OnMultiChoiceClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which, boolean isChecked) {
					if(isChecked == true) {
						item[which] = true;
					} else {
						item[which] = false;
					}
				}
			});
			builder.setPositiveButton("Save", new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Preferences.setSelectedDay(item);
					mRepeatValue.setText(selectedDays(item));
					mTimeset.setText(Preferences.getTimeAlarm());
				}
			});
			builder.setNegativeButton("Cancel", new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {}
			});
			
			AlertDialog x = builder.create();
			x.show();
			
			break;
			
		case R.id.alarm_layout_enable_button:
			if(checkbox.isChecked()) {
				checkbox.setChecked(false);
			} else {
				checkbox.setChecked(true);
			}
			break;
			
		case R.id.alarm_layout_checkbox:
			if(checkbox.isPressed()) {
				if(checkbox.isChecked()) {
					checkbox.setChecked(true);
				} else {
					checkbox.setChecked(false);
				}
			}
			break;
			
		case R.id.alarm_layout_set_button:
			
			Preferences.setSetupAlarmIsEnabled(checkbox.isChecked());
			
			if(checkbox.isChecked()) {
				
				setAlarm(timePickerCalendar);
			} else {
				Preferences.setSetupAlarmIsEnabled(false);
				
				cancelAllPendingAlarm();
				cancellAllMultipleOnceShotAlarm();
			}
			
			this.dismiss();
			break;
			
		case R.id.alarm_layout_cancel_button:
			this.dismiss();
			break;
			
		case R.id.alarm_layout_time_button:
			
			TimePickerDialog TPD = new TimePickerDialog(getContext(), new OnTimeSetListener() {
				
				@Override
				public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
							mHours = hourOfDay;
							mMins = minute;
							
							DateFormat datef = new SimpleDateFormat("hh:mm a");
							DateFormat df = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
							Calendar c = Calendar.getInstance();
							Calendar calSet = (Calendar) c.clone();
							Date d;
							
							calSet.set(Calendar.MINUTE, minute);
							calSet.set(Calendar.HOUR_OF_DAY, hourOfDay);
							
							if(calSet.compareTo(c) <= 0){
							    //Today Set time passed, count to tomorrow
							    calSet.add(Calendar.DATE, 1);
							}	
							
							//Log.i("TimerPickerDialog", c.getTime().toString());
							
							d = calSet.getTime();
							
							time[0] = mHours;
							time[1] = mMins;
							
							timePickerCalendar = calSet;
							
							Preferences.setAlarmTime(datef.format(d).toString());
							
							mTimeValue.setText(datef.format(d).toString());
							
							mTimeset.setText(df.format(d).toString());
							Preferences.setTimeAlarm(df.format(d).toString());
							
							//Log.i(getClass().getSimpleName(), "Alarm Time: Hours:" + Integer.valueOf(time[0]) + " Minutes: " + Integer.valueOf(time[1]));
							Preferences.setAlarmTime1(time);
						}
			}, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false);
			
			TPD.show();
			
			break;
		}
	}
	
	private void cancelAllPendingAlarm() {
		for(int t = 0; t < intentArray.size(); t++) {
			SUNDAY.cancel(intentArray.get(t));
			Preferences.saveRepeatingAlarm(t, false, 0, "", true);
		}
	}
	
	private void cancellAllMultipleOnceShotAlarm() {
		for(int y = 0; y < oneShotintentArray.size(); y++) {
			Am.cancel(oneShotintentArray.get(y));
		}
	}
	
	public void setAlarm(Calendar pCal) {
		int hour = pCal.get(Calendar.HOUR_OF_DAY);
		int mins = pCal.get(Calendar.MINUTE);
		
		Log.i("setAlarm", "Hours: " + Integer.valueOf(hour).toString());
		Log.i("setAlarm", "Minutes: " + Integer.valueOf(mins).toString());
		
		/*
		 * Log.i(getClass().getSimpleName(),"Hours: " + Integer.valueOf(time[0]));
		 * Log.i(getClass().getSimpleName(),"Minutes: " + Integer.valueOf(time[1]));
		 * 
		 * Intent intent1 = new Intent(getContext(), RadioAlarmManagerReceiver.class);
		 * intent1.setAction(ConstantCommand.START_ALARM);
		 * intent1.putExtra("disableAlarm", false);
		 * PendingIntent pendingIntent2 = PendingIntent.getBroadcast(getContext(), 101, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
		 * 
		 * Intent i = new Intent(getContext(), RadioAlarmManagerReceiver.class);
		 * i.setAction(ConstantCommand.START_ALARM);
		 * i.putExtra("disableAlarm", false);
		 * PendingIntent pIntent = PendingIntent.getBroadcast(getContext(), 103, i, PendingIntent.FLAG_UPDATE_CURRENT);
		 */
		
		//Cancel all pending alarms.
		cancelAllPendingAlarm();
				
		if (!mRepeatValue.getText().equals("Never")) {
			
			mTimePreview.setVisibility(View.GONE);
			
			for (int j = 0; j < temp.length; j++) {
				if (temp[j] == Boolean.TRUE) {
					if (days[j].equals("Monday")) {

						//Compute day of week every week
						//int days = Calendar.MONDAY + (7 - calendar.get(Calendar.DAY_OF_WEEK));
						//calendar.add(Calendar.DATE, days);
						
						calendar = Calendar.getInstance();
						calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
						currentDateTime = Calendar.getInstance();		
						
						if(currentDateTime.after(calendar)) {
							calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
							calendar.add(Calendar.DATE, 7);
						} else if(currentDateTime.before(calendar)) {
							calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
						} else {
							if(currentDateTime.get(Calendar.HOUR_OF_DAY) > pCal.get(Calendar.HOUR_OF_DAY) || currentDateTime.get(Calendar.MINUTE) > pCal.get(Calendar.MINUTE)){
								calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
								calendar.add(Calendar.DATE, 7);
							} else {
								calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
							}
						}
						
						calendar.set(Calendar.HOUR_OF_DAY, hour);
						calendar.set(Calendar.MINUTE, mins);
						calendar.set(Calendar.SECOND, 0);
						
						Preferences.saveRepeatingAlarm(0, Boolean.FALSE, calendar.getTimeInMillis(), calendar.getTime().toString(), false);
						setRepeatingAlarm(calendar.getTimeInMillis(), j, calendar);

					} else if (days[j].equals("Tuesday")) {
						
						calendar = Calendar.getInstance();
						calendar.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
						currentDateTime = Calendar.getInstance();		
						
						if(currentDateTime.after(calendar)) {
							calendar.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
							calendar.add(Calendar.DATE, 7);
						} else if(currentDateTime.before(calendar)) {
							calendar.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
						} else {
							if(currentDateTime.get(Calendar.HOUR_OF_DAY) > pCal.get(Calendar.HOUR_OF_DAY) || currentDateTime.get(Calendar.MINUTE) > pCal.get(Calendar.MINUTE)){
								calendar.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
								calendar.add(Calendar.DATE, 7);
							} else {
								calendar.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
							}
						}
						
						calendar.set(Calendar.HOUR_OF_DAY, hour);
						calendar.set(Calendar.MINUTE, mins);
						calendar.set(Calendar.SECOND, 0);
						
						Preferences.saveRepeatingAlarm(1, Boolean.FALSE, calendar.getTimeInMillis(), calendar.getTime().toString(), false);
						setRepeatingAlarm(calendar.getTimeInMillis(), j, calendar);

					} else if (days[j].equals("Wednesday")) {
						calendar = Calendar.getInstance();
						calendar.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
						currentDateTime = Calendar.getInstance();		
						
						if(currentDateTime.after(calendar)) {
							calendar.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
							calendar.add(Calendar.DATE, 7);
						} else if(currentDateTime.before(calendar)) {
							calendar.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
						} else {
							if(currentDateTime.get(Calendar.HOUR_OF_DAY) > pCal.get(Calendar.HOUR_OF_DAY) || currentDateTime.get(Calendar.MINUTE) > pCal.get(Calendar.MINUTE)){
								calendar.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
								calendar.add(Calendar.DATE, 7);
							} else {
								calendar.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
							}
						}
						
						calendar.set(Calendar.HOUR_OF_DAY, hour);
						calendar.set(Calendar.MINUTE, mins);
						calendar.set(Calendar.SECOND, 0);
						
						Preferences.saveRepeatingAlarm(2, Boolean.FALSE, calendar.getTimeInMillis(), calendar.getTime().toString(), false);
						setRepeatingAlarm(calendar.getTimeInMillis(), j, calendar);

					} else if (days[j].equals("Thursday")) {
						
						calendar = Calendar.getInstance();
						calendar.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
						currentDateTime = Calendar.getInstance();		
						
						if(currentDateTime.after(calendar)) {
							calendar.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
							calendar.add(Calendar.DATE, 7);
						} else if(currentDateTime.before(calendar)) {
							calendar.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
						} else {
							if(currentDateTime.get(Calendar.HOUR_OF_DAY) > pCal.get(Calendar.HOUR_OF_DAY) || currentDateTime.get(Calendar.MINUTE) > pCal.get(Calendar.MINUTE)){
								calendar.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
								calendar.add(Calendar.DATE, 7);
							} else {
								calendar.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
							}
						}
						
						calendar.set(Calendar.HOUR_OF_DAY, hour);
						calendar.set(Calendar.MINUTE, mins);
						calendar.set(Calendar.SECOND, 0);
						
						Preferences.saveRepeatingAlarm(3, Boolean.FALSE, calendar.getTimeInMillis(), calendar.getTime().toString(), false);
						setRepeatingAlarm(calendar.getTimeInMillis(), j, calendar);

					} else if (days[j].equals("Friday")) {
						
						calendar = Calendar.getInstance();
						calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
						currentDateTime = Calendar.getInstance();		
						
						if(currentDateTime.after(calendar)) {
							calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
							calendar.add(Calendar.DATE, 7);
						} else if(currentDateTime.before(calendar)) {
							calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
						} else {
							if(currentDateTime.get(Calendar.HOUR_OF_DAY) > pCal.get(Calendar.HOUR_OF_DAY) || currentDateTime.get(Calendar.MINUTE) > pCal.get(Calendar.MINUTE)){
								calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
								calendar.add(Calendar.DATE, 7);
							} else {
								calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
							}
						}
						
						calendar.set(Calendar.HOUR_OF_DAY, hour);
						calendar.set(Calendar.MINUTE, mins);
						calendar.set(Calendar.SECOND, 0);
						
						Preferences.saveRepeatingAlarm(4, Boolean.FALSE, calendar.getTimeInMillis(), calendar.getTime().toString(), false);
						setRepeatingAlarm(calendar.getTimeInMillis(), j, calendar);

					} else if (days[j].equals("Saturday")) {
						 
						calendar = Calendar.getInstance();
						calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
						currentDateTime = Calendar.getInstance();		
						
						if(currentDateTime.after(calendar)) {
							calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
							calendar.add(Calendar.DATE, 7);
						} else if(currentDateTime.before(calendar)) {
							calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
						} else {
							if(currentDateTime.get(Calendar.HOUR_OF_DAY) > pCal.get(Calendar.HOUR_OF_DAY) || currentDateTime.get(Calendar.MINUTE) > pCal.get(Calendar.MINUTE)){
								calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
								calendar.add(Calendar.DATE, 7);
							} else {
								calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
							}
						}
						
						calendar.set(Calendar.HOUR_OF_DAY, hour);
						calendar.set(Calendar.MINUTE, mins);
						calendar.set(Calendar.SECOND, 0);
						
						Preferences.saveRepeatingAlarm(5, Boolean.FALSE, calendar.getTimeInMillis(), calendar.getTime().toString(), false);
						setRepeatingAlarm(calendar.getTimeInMillis(), j, calendar);

					} else if (days[j].equals("Sunday")) {
						
						calendar = Calendar.getInstance();
						calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
						currentDateTime = Calendar.getInstance();		
						
						if(currentDateTime.after(calendar)) {
							calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
							calendar.add(Calendar.DATE, 7);
						} else if(currentDateTime.before(calendar)) {
							calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
						} else {
							if(currentDateTime.get(Calendar.HOUR_OF_DAY) > pCal.get(Calendar.HOUR_OF_DAY) || currentDateTime.get(Calendar.MINUTE) > pCal.get(Calendar.MINUTE)){
								calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
								calendar.add(Calendar.DATE, 7);
							} else {
								calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
							}
						}
						
						calendar.set(Calendar.HOUR_OF_DAY, hour);
						calendar.set(Calendar.MINUTE, mins);
						calendar.set(Calendar.SECOND, 0);
						
						Preferences.saveRepeatingAlarm(6, Boolean.FALSE, calendar.getTimeInMillis(), calendar.getTime().toString(), false);
						setRepeatingAlarm(calendar.getTimeInMillis(), j, calendar);
					}
				}
			}
			
		} else { //Repeating not set. set alarm for the time set. 
			
			mTimePreview.setVisibility(View.VISIBLE);
			
			Log.i(getClass().getSimpleName(), "Alarm is Repeating NO");
			currentDateTime = Calendar.getInstance();
			calendar = Calendar.getInstance();
			
			if(currentDateTime.get(Calendar.HOUR_OF_DAY) > time[0] || currentDateTime.get(Calendar.MINUTE) > time[1]) {
				calendar.add(Calendar.DATE, 1);
			}
			
			calendar.set(Calendar.HOUR_OF_DAY, hour);
			calendar.set(Calendar.MINUTE, mins);
			calendar.set(Calendar.SECOND, 0);
			
			Log.i(getClass().getSimpleName(), "One Time Alarm: " + calendar.getTime().toString());
			//toast.show("Alarm", "Alarm is set to: " + calendar.getTime().toString());
			
			mTimeset.setText(calendar.getTime().toString());
			
			Intent intent3 = new Intent(getContext(), RadioAlarmManagerReceiver.class);
			intent3.setAction(ConstantCommand.START_ALARM);
			intent3.putExtra("disableAlarm", true);
			PendingIntent pendingIntent1 = PendingIntent.getBroadcast(getContext(), 50, intent3, PendingIntent.FLAG_CANCEL_CURRENT);
			
			Am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent1);
		}
	}
	
	private void setRepeatingAlarm(long timeInMillis, int pos, Calendar cal) {
		Intent repeatingAlarmIntent = new Intent(getContext(), RadioAlarmManagerReceiver.class);
		repeatingAlarmIntent.setAction(ConstantCommand.START_ALARM);
		repeatingAlarmIntent.putExtra("disableAlarm", false);
		repeatingAlarmIntent.putExtra("calendar", cal.getTime().toString());
		PendingIntent pIntent = PendingIntent.getBroadcast(getContext(), pos, repeatingAlarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		
		SUNDAY.setRepeating(AlarmManager.RTC_WAKEUP, timeInMillis, AlarmManager.INTERVAL_DAY * 7, pIntent);
		
		intentArray.add(pIntent);
	}
	
	public void show() {
		super.show();
	}
	
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		//Preferences.setSetupAlarmIsEnabled(isChecked);
		
		if(isChecked) { //TRUE
			mTimeButton.setEnabled(true);
			mTimeButton.setClickable(true);
			mRepeatButton.setEnabled(true);
			mRepeatButton.setClickable(true);
			mRepeat.setTextColor(Color.WHITE);
			mTime.setTextColor(Color.WHITE);
			mTimeValue.setTextColor(Color.GRAY);
			mRepeatValue.setTextColor(Color.GRAY);
			mTimeset.setTextColor(Color.WHITE);
			mTimesetLabel.setTextColor(Color.WHITE);
		} else { // FALSE
			mTimeButton.setEnabled(false);
			mTimeButton.setClickable(false);
			mRepeatButton.setEnabled(false);
			mRepeatButton.setClickable(false);
			mTimeValue.setTextColor(Color.DKGRAY);
			mRepeatValue.setTextColor(Color.DKGRAY);	
			mRepeat.setTextColor(Color.GRAY);
			mTime.setTextColor(Color.GRAY);
			mTimeset.setTextColor(Color.DKGRAY);
			mTimesetLabel.setTextColor(Color.DKGRAY);
		}
	}
}
