package com.rakista.radio;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;

import com.rakista.radio.internal.CONSTANTS;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

public class RadioNotification {
	
	private static NotificationManager nM;
	static Context mContext;
	static int cnt = 0;
	
	public RadioNotification() {
	}
	
	public RadioNotification(Context context) {
		mContext = context;
	}
	
	public static void setContext(Context context) {
		mContext = context;
	}
	
	public static Context getContext() {
		return mContext;
	}
	
	public static void showAppUpdateNotification(HashMap<String, String> param) {
		nM = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
		
		RemoteViews contentViews = new RemoteViews(getContext().getPackageName(), R.layout.notification_layout);
		//contentViews.setTextViewText(R.id.notification_layout_type_label, param.get("type"));
		contentViews.setTextViewText(R.id.notification_layout_description_label, param.get("message"));

		/*String image = param.get("image");
		String link = param.get("link");*/
		
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(param.get("link")));
		//i.putExtra("image", image);
		//i.putExtra("link", link);
		
		PendingIntent pIntent = PendingIntent.getActivity(getContext(), 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
		
		NotificationCompat.Builder n = new NotificationCompat.Builder(getContext())
		.setContent(contentViews)
		.setContentIntent(pIntent)
		.setSmallIcon(R.drawable.notification_logo);
		
		/*Notification noti = new Notification(R.drawable.notification_logo, "Rakista Radio!", System.currentTimeMillis());
		noti.contentView = contentViews;
		noti.contentIntent = pIntent;
		noti.flags |= Notification.FLAG_AUTO_CANCEL;*/
		
		nM.notify(CONSTANTS.APPUPDATENOTIFICATION_ID, n.build());
	}
	
	public static void showShoutoutNotification(HashMap<String, String> param) {
		nM = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
		
		RemoteViews contentViews = new RemoteViews(getContext().getPackageName(), R.layout.notification_layout);
		contentViews.setTextViewText(R.id.notification_layout_description_label, param.get("message"));
		
		Intent i = new Intent(getContext(), RadioActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		
		PendingIntent pIntent = PendingIntent.getActivity(getContext(), 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
		
		NotificationCompat.Builder n = new NotificationCompat.Builder(getContext())
		.setContent(contentViews)
		.setContentIntent(pIntent)
		.setSmallIcon(R.drawable.notification_logo);
		
		/*Notification noti = new Notification(R.drawable.notification_logo, "Rakista Radio!", System.currentTimeMillis());
		noti.contentView = contentViews;
		noti.contentIntent = pIntent;
		noti.flags |= Notification.FLAG_AUTO_CANCEL;*/
		
		nM.notify(CONSTANTS.SHOUTOUTNOTIFICATION_ID, n.build());
	}
	
	public static void showEventNotification(HashMap<String, String> param) {
		
		nM = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
		
		RemoteViews contentViews = new RemoteViews(getContext().getPackageName(), R.layout.notification_layout);
		//contentViews.setTextViewText(R.id.notification_layout_type_label, param.get("type"));
		contentViews.setTextViewText(R.id.notification_layout_description_label, param.get("message"));
		
		Log.i("RadioNotification","Data: " + param.toString());

		String image = param.get("image");
		String link = param.get("link");
		
		intentHelper.setImageLink(image);
		intentHelper.setWebLink(link);
		
		Intent i = new Intent(getContext(), PushNotificationViewer.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.setAction("com.rakista.radio.PushNotificationViewer.NEW_INTENT");
		
		PendingIntent pIntent = PendingIntent.getActivity(getContext(), 0, i, 0);
		
		NotificationCompat.Builder n = new NotificationCompat.Builder(getContext())
		.setContent(contentViews)
		.setContentIntent(pIntent)
		.setSmallIcon(R.drawable.notification_logo);
		
		/*Notification noti = new Notification(R.drawable.notification_logo, "Rakista Radio!", System.currentTimeMillis());
		noti.contentView = contentViews;
		noti.contentIntent = pIntent;
		noti.flags |= Notification.FLAG_AUTO_CANCEL;*/
		
		nM.notify(CONSTANTS.EVENTNOTIFICATION_ID + (++cnt), n.build());
	}
	
	public static void showAlarmNotification() {
		String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
		
		nM = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
		
		Intent i = new Intent(getContext(), RadioActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		
		PendingIntent pIntent = PendingIntent.getActivity(getContext(), 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
		
		Notification noti = new Notification(com.rakista.radio.R.drawable.notification_logo, "Rakista Radio! 100% Pinoy Rock!", System.currentTimeMillis());
		noti.setLatestEventInfo(getContext(), "Rakista Radio! 100% Pinoy Rock!", "Alarm: " + currentDateTimeString, pIntent);
		noti.flags |= Notification.FLAG_AUTO_CANCEL;
		
		nM.notify(CONSTANTS.ALARMNOTIFICATIONID, noti);
	}
}
