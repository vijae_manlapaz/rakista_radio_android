package com.rakista.radio.internal;

import java.util.ArrayList;
import java.util.HashMap;
//import java.util.List;
//import java.util.Map;

//import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
//import android.database.CharArrayBuffer;
//import android.database.ContentObserver;
import android.database.Cursor;
//import android.database.DataSetObserver;
//import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteDatabase.CursorFactory;
//import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
//import android.net.Uri;
//import android.os.Bundle;
//import android.util.Log;

public class DataBaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "RadioDB";
 
    // Table names
    public static final String TABLE_HISTORY = "tblhistory";
    public static final String TABLE_PLAYLIST = "tblplaylist";
    public static final String TABLE_DEDICATION = "tbldedication";
    public static final String TABLE_TOPVOTED = "tbltopvoted";
    public static final String TABLE_TOPREQUESTED = "tbltoprequested";
    public static final String TABLE_ALARM = "tblalarm";
    
    private static final String KEY_ID = "ID";
//    private static final String KEY_SONGID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_ARTIST = "artist";
    private static final String KEY_LYRICS = "lyrics";
    private static final String KEY_ALBUM = "album";
    private static final String KEY_ALBUMYEAR = "albumyear";
    private static final String KEY_DURATION = "duration";
    private static final String KEY_DURATIONDISPLAY	= "durationDisplay";
    private static final String KEY_BUYCD = "buycd";
    private static final String KEY_COUNT = "cnt";
    private static final String KEY_DATEPLAYED = "date_played";
    private static final String KEY_REQUESTDATE = "request_date";
    private static final String KEY_ISREQUESTED = "isRequested";
    private static final String KEY_ISDEDICATED = "isDedicated";
    
    private static final String KEY_DEDICATIONMESSAGE = "dedicationMessage";
    private static final String KEY_DEDICATIONBY = "dedicationName";
    
    private static final String KEY_ALARM_ACTIONSTRING = "ALARMACTIONSTRING";
    private static final String KEY_ALARM_DISABLELARM = "DISABLEALARM";
    private static final String KEY_ALARM_ALARMDATEANDTIME = "ALARMDATEANDTIME";
    private static final String KEY_ALARM_TIMEINMILLIS = "TIMEINMILLIS";
    private static final String KEY_ALARM_INTERVAL_WEEK = "INTERVAL_WEEK"; 
    
    private Cursor cursor;
    private SQLiteDatabase SQLDB;

    public DataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
//        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
//                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
//                + KEY_PH_NO + " TEXT" + ")";
//       db.execSQL(CREATE_CONTACTS_TABLE);
    }
    
    /**
     * Checks if the table name specified exist or not.
     * @param tableName name of the table to be check if exist.
     * @return TRUE if the table exist false otherwise.
     * @Author Vijae Manlapaz.
     */
	public boolean isTableExists(String tableName) {
//		SQLDB = 
		opendatabase();
		
		cursor = SQLDB.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+ tableName + "'", null);
		if (cursor != null) {
			if (cursor.getCount() > 0) {
				cursor.close();
				closedatabase();
				return true;
			}
			cursor.close();
		}
		
		closedatabase();
		return false;
		
	}
	
	private SQLiteDatabase opendatabase() {
		SQLDB = getWritableDatabase();
		SQLDB.setLockingEnabled(false);
		return SQLDB;
	}
	
	private boolean closedatabase() {
		if(SQLDB.isOpen()) {
			SQLDB.close();
			close();
			return true;
		}
		close();
		return false;
	}
    
    public void createTable(String tableName) {
//    	SQLiteDatabase SQLDB
    	opendatabase();
    	
    	//new Thread(new Runnable() {
			//@Override
			//public void run() {
				
				if(tableName == TABLE_HISTORY) {
		    		String SQL_STATEMENT = "CREATE TABLE " + tableName + "("
		    				+ KEY_ID + " TEXT,"
		                    + KEY_TITLE + " TEXT,"
		    				+ KEY_ARTIST + " TEXT,"
		    				+ KEY_LYRICS + " TEXT,"
		    				+ KEY_ALBUM + " TEXT,"
		    				+ KEY_ALBUMYEAR + " TEXT,"
		    				+ KEY_DURATION + " TEXT,"
		    				+ KEY_DURATIONDISPLAY + " TEXT,"
		    				+ KEY_BUYCD + " TEXT,"
		    				+ KEY_DATEPLAYED + " TEXT, "
		    				+ KEY_ISREQUESTED + " TEXT, "
		    				+ KEY_ISDEDICATED + " TEXT "
		                    + ")";
		            SQLDB.execSQL(SQL_STATEMENT);
		            
		    	} else if (tableName == TABLE_PLAYLIST) {
		    		String SQL_STATEMENT = "CREATE TABLE " + tableName + "("
		                    + "TOTAL" + " TEXT,"
		    				+ "ARTIST" + " TEXT "
		                    + ")";
		            SQLDB.execSQL(SQL_STATEMENT);
		            
		    	} else if (tableName == TABLE_TOPVOTED) {
		    		String SQL_STATEMENT = "CREATE TABLE " + tableName + "("
		    				+ KEY_ID + " TEXT,"
		                    + KEY_TITLE + " TEXT,"
		    				+ KEY_ARTIST + " TEXT,"
		    				+ KEY_LYRICS + " TEXT,"
		    				+ KEY_ALBUM + " TEXT,"
		    				+ KEY_ALBUMYEAR + " TEXT,"
		    				+ KEY_DURATION + " TEXT,"
		    				+ KEY_DURATIONDISPLAY + " TEXT,"
		    				+ KEY_BUYCD + " TEXT,"
		    				+ KEY_COUNT + " TEXT,"
		    				+ KEY_DATEPLAYED + " TEXT "
		                    + ")";
		            SQLDB.execSQL(SQL_STATEMENT);
		            
		    	} else if (tableName == TABLE_DEDICATION) {
		    		String SQL_STATEMENT = "CREATE TABLE " + tableName + "("
		                    + KEY_TITLE + " TEXT,"
		    				+ KEY_ARTIST + " TEXT,"
		    				+ KEY_DEDICATIONBY + " TEXT,"
		    				+ KEY_DEDICATIONMESSAGE + " TEXT,"
		    				+ KEY_REQUESTDATE + " TEXT "
		                    + ")";
		            SQLDB.execSQL(SQL_STATEMENT);
		            
		    	} else if (tableName == TABLE_TOPREQUESTED) {
		    		String SQL_STATEMENT = "CREATE TABLE " + tableName + "("
		    				+ KEY_ID + " TEXT,"
		                    + KEY_TITLE + " TEXT,"
		    				+ KEY_ARTIST + " TEXT,"
		    				+ KEY_LYRICS + " TEXT,"
		    				+ KEY_ALBUM + " TEXT,"
		    				+ KEY_ALBUMYEAR + " TEXT,"
		    				+ KEY_DURATION + " TEXT,"
		    				+ KEY_DURATIONDISPLAY + " TEXT,"
		    				+ KEY_BUYCD + " TEXT,"
		    				+ KEY_COUNT + " TEXT,"
		    				+ KEY_DATEPLAYED + " TEXT "
		                    + ")";
		            SQLDB.execSQL(SQL_STATEMENT);
		            
		    	} else if(tableName == TABLE_ALARM) {
		    		String SQL_STATEMENT = "CREATE TABLE " + tableName + "("
		    				+ KEY_ALARM_ACTIONSTRING + " TEXT,"
		                    + KEY_ALARM_ALARMDATEANDTIME + " TEXT,"
		    				+ KEY_ALARM_DISABLELARM + " BOOLEAN,"
		    				+ KEY_ALARM_INTERVAL_WEEK + " LONG,"
		    				+ KEY_ALARM_TIMEINMILLIS + " LONG "
		                    + ")";
		    		
		    		SQLDB.execSQL(SQL_STATEMENT);
		    	}
			//}}).start();
    	
//    	SQLDB.close();
    	closedatabase();
    }
    
    public void updateTable(String tableName, ArrayList<HashMap<String, String>> data) {
//    	SQLiteDatabase SQLDB
    	SQLDB = getWritableDatabase();
    	//new Thread(new Runnable() {
			//@Override
			//public void run() {
				if(tableName == TABLE_HISTORY) {
		    		//Drop The Table if exist.
		    		SQLDB.execSQL("DROP TABLE IF EXISTS " + tableName);
		    		
		    		//Create the table again.
		    		String SQL_STATEMENT = "CREATE TABLE " + tableName + "("
		    				+ KEY_ID + " TEXT,"
		                    + KEY_TITLE + " TEXT,"
		    				+ KEY_ARTIST + " TEXT,"
		    				+ KEY_LYRICS + " TEXT,"
		    				+ KEY_ALBUM + " TEXT,"
		    				+ KEY_ALBUMYEAR + " TEXT,"
		    				+ KEY_DURATION + " TEXT,"
		    				+ KEY_DURATIONDISPLAY + " TEXT,"
		    				+ KEY_BUYCD + " TEXT,"
		    				+ KEY_DATEPLAYED + " TEXT, "
		    				+ KEY_ISREQUESTED + " TEXT, "
		    				+ KEY_ISDEDICATED + " TEXT "
		                    + ")";
		            SQLDB.execSQL(SQL_STATEMENT);
		            addData(data, tableName);
		            
		    	} else if(tableName == TABLE_PLAYLIST) {
		    		//Drop The Table if exist.
		    		SQLDB.execSQL("DROP TABLE IF EXISTS " + tableName);
		    		
		    		//Create the table again.
		    		String SQL_STATEMENT = "CREATE TABLE " + tableName + "("
		                    + "TOTAL" + " TEXT,"
		    				+ "ARTIST" + " TEXT "
		                    + ")";
		            SQLDB.execSQL(SQL_STATEMENT);
		            addData(data, tableName);
		            
		    	} else if (tableName == TABLE_TOPVOTED) {
		    		//Drop The Table if exist.
		    		SQLDB.execSQL("DROP TABLE IF EXISTS " + tableName);
		    		
		    		//Create the table again.
		    		String SQL_STATEMENT = "CREATE TABLE " + tableName + "("
		    				+ KEY_ID + " TEXT,"
		                    + KEY_TITLE + " TEXT,"
		    				+ KEY_ARTIST + " TEXT,"
		    				+ KEY_LYRICS + " TEXT,"
		    				+ KEY_ALBUM + " TEXT,"
		    				+ KEY_ALBUMYEAR + " TEXT,"
		    				+ KEY_DURATION + " TEXT,"
		    				+ KEY_DURATIONDISPLAY + " TEXT,"
		    				+ KEY_BUYCD + " TEXT,"
		    				+ KEY_COUNT + " TEXT,"
		    				+ KEY_DATEPLAYED + " TEXT "
		                    + ")";
		            SQLDB.execSQL(SQL_STATEMENT);
		            addData(data, tableName);
		    	} else if (tableName == TABLE_TOPREQUESTED) {
		    		//Drop The Table if exist.
		    		SQLDB.execSQL("DROP TABLE IF EXISTS " + tableName);
		    		
		    		//Create the table again.
		    		String SQL_STATEMENT = "CREATE TABLE " + tableName + "("
		    				+ KEY_ID + " TEXT,"
		                    + KEY_TITLE + " TEXT,"
		    				+ KEY_ARTIST + " TEXT,"
		    				+ KEY_LYRICS + " TEXT,"
		    				+ KEY_ALBUM + " TEXT,"
		    				+ KEY_ALBUMYEAR + " TEXT,"
		    				+ KEY_DURATION + " TEXT,"
		    				+ KEY_DURATIONDISPLAY + " TEXT,"
		    				+ KEY_BUYCD + " TEXT,"
		    				+ KEY_COUNT + " TEXT,"
		    				+ KEY_DATEPLAYED + " TEXT "
		                    + ")";
		            SQLDB.execSQL(SQL_STATEMENT);
		            addData(data, tableName);
		    	} else if (tableName == TABLE_DEDICATION) {
		    		//Drop The Table if exist.
		    		SQLDB.execSQL("DROP TABLE IF EXISTS " + tableName);
		    		
		    		//Create the table again.
		    		String SQL_STATEMENT = "CREATE TABLE " + tableName + "("
		                    + KEY_TITLE + " TEXT,"
		    				+ KEY_ARTIST + " TEXT,"
		    				+ KEY_DEDICATIONBY + " TEXT,"
		    				+ KEY_DEDICATIONMESSAGE + " TEXT,"
		    				+ KEY_REQUESTDATE + " TEXT, "
		    				+ "position" + " TEXT "
		                    + ")";
		            SQLDB.execSQL(SQL_STATEMENT);
		            addData(data, tableName);
		    	}
			//}
		//}).start();
    	
    	SQLDB.close();
    	closedatabase(); 
    }
 
    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */
    
    /**					+------------------------------------------------------------------------------------------------+
     * 	Column Index =>	| id | songid | title | artist | lyrics | album | albumyear | duration | durationDisplay | buycd |
     * 					+------------------------------------------------------------------------------------------------+
     * 	Data Type	=>	|TEXT|	TEXT  | TEXT  | TEXT   | TEXT   | TEXT  |   TEXT    |   TEXT   |		TEXT	 |	TEXT |
     * 					+------------------------------------------------------------------------------------------------+
     */	
    // Adding new contact
    public void addData(final ArrayList<HashMap<String, String>> data, final String tableName) {
    	
    	
        SQLiteDatabase db = opendatabase(); 
//        = SQLDB;
//        		getWritableDatabase();
        ContentValues values = new ContentValues();
        
        //new Thread(new Runnable() {
			//@Override
			//public void run() {
				if (tableName == TABLE_HISTORY) {
					for (int x = 0; x < data.size(); x++) {
						values.put(KEY_ID, data.get(x).get(KEY_ID));
						values.put(KEY_TITLE, data.get(x).get(KEY_TITLE));
						values.put(KEY_ARTIST, data.get(x).get(KEY_ARTIST));
						values.put(KEY_LYRICS, data.get(x).get(KEY_LYRICS));
						values.put(KEY_ALBUM, data.get(x).get(KEY_ALBUM));
						values.put(KEY_ALBUMYEAR, data.get(x).get(KEY_ALBUMYEAR));
						values.put(KEY_DURATION, data.get(x).get(KEY_DURATION));
						values.put(KEY_DURATIONDISPLAY, data.get(x).get(KEY_DURATIONDISPLAY));
						values.put(KEY_BUYCD, data.get(x).get(KEY_BUYCD));
						values.put(KEY_DATEPLAYED, data.get(x).get(KEY_DATEPLAYED));
						values.put(KEY_ISREQUESTED, data.get(x).get(KEY_ISREQUESTED));
						values.put(KEY_ISDEDICATED, data.get(x).get(KEY_ISDEDICATED));
						db.insert(tableName, null, values);
						/*
						 * Log.i(getClass().getSimpleName(),
						 * "====================================================");
						 * Log.i(getClass().getSimpleName(),
						 * "History Data add to Database: \n" + "ID: " +
						 * data.get(x).get(KEY_SONGID) + "\nTitle: " +
						 * data.get(x).get(KEY_TITLE) + "\nArtist: " +
						 * data.get(x).get(KEY_ARTIST) + "\nLyrics: " +
						 * data.get(x).get(KEY_LYRICS) + "\nAlbum: " +
						 * data.get(x).get(KEY_ALBUM) + "\nYear: " +
						 * data.get(x).get(KEY_ALBUMYEAR) + "\nDuration: " +
						 * data.get(x).get(KEY_DURATION) + "\nDurationDisplay: " +
						 * data.get(x).get(KEY_DURATIONDISPLAY) + "\nBuyCD: " +
						 * data.get(x).get(KEY_BUYCD) );
						 * Log.i(getClass().getSimpleName(),
						 * "====================================================");
						 */
					}

				} else if (tableName == TABLE_PLAYLIST) {

					for (int x = 0; x < data.size(); x++) {
						values.put("TOTAL", data.get(x).get("TOTAL"));
						values.put("ARTIST", data.get(x).get("ARTIST"));
						/*
						 * Log.i(getClass().getSimpleName(),
						 * "================================");
						 * Log.i(getClass().getSimpleName(), "Inserting to Database");
						 * Log.i(getClass().getSimpleName(), "TOTAL: " +
						 * data.get(x).get("TOTAL") + "\nARTIST: " +
						 * data.get(x).get("ARTIST")); Log.i(getClass().getSimpleName(),
						 * "================================");
						 */
						db.insert(tableName, null, values);

					}
				} else if (tableName == TABLE_TOPVOTED) {
					
					for (int x = 0; x < data.size(); x++) {
						values.put(KEY_ID, data.get(x).get(KEY_ID));
						values.put(KEY_TITLE, data.get(x).get(KEY_TITLE));
						values.put(KEY_ARTIST, data.get(x).get(KEY_ARTIST));
						values.put(KEY_LYRICS, data.get(x).get(KEY_LYRICS));
						values.put(KEY_ALBUM, data.get(x).get(KEY_ALBUM));
						values.put(KEY_ALBUMYEAR, data.get(x).get(KEY_ALBUMYEAR));
						values.put(KEY_DURATION, data.get(x).get(KEY_DURATION));
						values.put(KEY_DURATIONDISPLAY, data.get(x).get(KEY_DURATIONDISPLAY));
						values.put(KEY_BUYCD, data.get(x).get(KEY_BUYCD));
						values.put(KEY_COUNT, data.get(x).get(KEY_COUNT));
						values.put(KEY_DATEPLAYED, data.get(x).get(KEY_DATEPLAYED));
						
						//Insert to Database
						db.insert(tableName, null, values);
						
						/*Log.i(getClass().getSimpleName(), "===================================================="
						+ "\nTop Music Data add to Database: "
						+ "\nID: " + data.get(x).get(KEY_SONGID)
						+ "\nTitle: " + data.get(x).get(KEY_TITLE)
						+ "\nArtist: " + data.get(x).get(KEY_ARTIST)
						+ "\nLyrics: " + data.get(x).get(KEY_LYRICS)
						+ "\nAlbum: " + data.get(x).get(KEY_ALBUM)
						+ "\nYear: " + data.get(x).get(KEY_ALBUMYEAR)
						+ "\nDuration: " + data.get(x).get(KEY_DURATION)
						+ "\nDurationDisplay: " + data.get(x).get(KEY_DURATIONDISPLAY)
						+ "\nBuyCD: " + data.get(x).get(KEY_BUYCD)
						+ "\nCount: " + data.get(x).get(KEY_COUNT)
						+ "\nDate Played: " + data.get(x).get(KEY_DATEPLAYED)
						+ "\n====================================================");*/
					}
				} else if (tableName == TABLE_TOPREQUESTED) {
					
					for (int x = 0; x < data.size(); x++) {
						values.put(KEY_ID, data.get(x).get(KEY_ID));
						values.put(KEY_TITLE, data.get(x).get(KEY_TITLE));
						values.put(KEY_ARTIST, data.get(x).get(KEY_ARTIST));
						values.put(KEY_LYRICS, data.get(x).get(KEY_LYRICS));
						values.put(KEY_ALBUM, data.get(x).get(KEY_ALBUM));
						values.put(KEY_ALBUMYEAR, data.get(x).get(KEY_ALBUMYEAR));
						values.put(KEY_DURATION, data.get(x).get(KEY_DURATION));
						values.put(KEY_DURATIONDISPLAY, data.get(x).get(KEY_DURATIONDISPLAY));
						values.put(KEY_BUYCD, data.get(x).get(KEY_BUYCD));
						values.put(KEY_COUNT, data.get(x).get(KEY_COUNT));
						values.put(KEY_DATEPLAYED, data.get(x).get(KEY_DATEPLAYED));
						
						//Insert to Database
						db.insert(tableName, null, values);
					}
				} else if (tableName == TABLE_DEDICATION) {
					
					for (int x = 0; x < data.size(); x++) {
						values.put(KEY_TITLE, data.get(x).get(KEY_TITLE));
						values.put(KEY_ARTIST, data.get(x).get(KEY_ARTIST));
						values.put(KEY_DEDICATIONBY, data.get(x).get(KEY_DEDICATIONBY));
						values.put(KEY_DEDICATIONMESSAGE, data.get(x).get(KEY_DEDICATIONMESSAGE));
						values.put(KEY_REQUESTDATE, data.get(x).get(KEY_REQUESTDATE));
						values.put("position", data.get(x).get("position"));
						
						//Insert to Database
						db.insert(tableName, null, values);
						
						/*Log.i(getClass().getSimpleName(), "===================================================="
						+ "\nTop Music Data add to Database: "
						+ "\nTitle: " + data.get(x).get(KEY_TITLE)
						+ "\nArtist: " + data.get(x).get(KEY_ARTIST)
						+ "\nDedicated By: " + data.get(x).get(KEY_DEDICATIONBY)
						+ "\nDedicated Message: " + data.get(x).get(KEY_DEDICATIONMESSAGE)
						+ "\nDate Played: " + data.get(x).get(KEY_DATEPLAYED)
						+ "\n====================================================");*/
					}
				}
			//}
		//}).start();
     
		closedatabase();
        db.close(); // Closing database connection
    }
    
    /**					+------------------------------------------------------------------------------------------------+
     * 	Column Index =>	| id | songid | title | artist | lyrics | album | albumyear | duration | durationDisplay | buycd |
     * 					+------------------------------------------------------------------------------------------------+
     * 	Data Type	=>	|TEXT|	TEXT  | TEXT  | TEXT   | TEXT   | TEXT  |   TEXT    |   TEXT   |		TEXT	 |	TEXT |
     * 					+------------------------------------------------------------------------------------------------+
     */	
     
    // Getting All Contacts
    public ArrayList<HashMap<String, String>> getAllData(final String tableName) {
    	
        ArrayList<HashMap<String, String>> dataList = new ArrayList<HashMap<String, String>>();
//        String selectQuery = null;
        
        SQLiteDatabase db = opendatabase();
//        		this.getWritableDatabase();
        
        //new Thread(new Runnable() {
		//	@Override
			//public void run() {
				if(tableName == TABLE_HISTORY) {
		        	
		        	final String selectQuery = "SELECT * FROM " + tableName;
		        	cursor = db.rawQuery(selectQuery, null);
		        	
		            if (cursor.moveToFirst()) {
		                do {
		                	
		                	HashMap<String, String> map = new HashMap<String, String>();
		                	map.put(KEY_ID, cursor.getString(0));
		                	map.put(KEY_TITLE, cursor.getString(1));
		                	map.put(KEY_ARTIST, cursor.getString(2));
		                	map.put(KEY_LYRICS, cursor.getString(3));
		                	map.put(KEY_ALBUM, cursor.getString(4));
		                	map.put(KEY_ALBUMYEAR, cursor.getString(5));
		                	map.put(KEY_DURATION, cursor.getString(6));
		                	map.put(KEY_DURATIONDISPLAY, cursor.getString(7));
		                	map.put(KEY_BUYCD, cursor.getString(8));
		                	map.put(KEY_DATEPLAYED, cursor.getString(9));
		                	map.put(KEY_ISREQUESTED, cursor.getString(10));
		                	map.put(KEY_ISDEDICATED, cursor.getString(11));
		                	
		                    dataList.add(map);
		                    
		                } while (cursor.moveToNext());
		            }
		            cursor.close();
//		            Log.i(getClass().getSimpleName(), cursor1.toString());
		        } else if(tableName == TABLE_PLAYLIST) {
		        	final String selectQuery = "SELECT * FROM " + tableName;
		        	cursor = db.rawQuery(selectQuery, null); 
		        	
		            if (cursor.moveToFirst()) {
		                do {
		                	HashMap<String, String> map = new HashMap<String, String>();
		                	map.put("TOTAL", cursor.getString(0));
		                	map.put("ARTIST", cursor.getString(1));
		                    
		                    dataList.add(map);
		                    
		                } while (cursor.moveToNext());
		            }
		            cursor.close();
		        } else if(tableName == TABLE_TOPVOTED) {
		        	
		        	final String selectQuery = "SELECT * FROM " + tableName;
		        	cursor = db.rawQuery(selectQuery, null); 
		        	 
		            if (cursor.moveToFirst()) {
		                do {
		                	HashMap<String, String> map = new HashMap<String, String>();
		                	map.put(KEY_ID, cursor.getString(0));
		                	map.put(KEY_TITLE, cursor.getString(1));
		                	map.put(KEY_ARTIST, cursor.getString(2));
		                	map.put(KEY_LYRICS, cursor.getString(3));
		                	map.put(KEY_ALBUM, cursor.getString(4));
		                	map.put(KEY_ALBUMYEAR, cursor.getString(5));
		                	map.put(KEY_DURATION, cursor.getString(6));
		                	map.put(KEY_DURATIONDISPLAY, cursor.getString(7));
		                	map.put(KEY_BUYCD, cursor.getString(8));
		                	map.put(KEY_COUNT, cursor.getString(9));
		                	map.put(KEY_DATEPLAYED, cursor.getString(10));
		                    
		                    dataList.add(map);
		                    
		                } while (cursor.moveToNext()); 
		            }
		            cursor.close();
		            
		        } else if(tableName == TABLE_TOPREQUESTED) {
		        	
		        	final String selectQuery = "SELECT * FROM " + tableName;
		        	Cursor cursor = db.rawQuery(selectQuery, null); 
		        	 
		            if (cursor.moveToFirst()) {
		                do {
		                	HashMap<String, String> map = new HashMap<String, String>();
		                	map.put(KEY_ID, cursor.getString(0));
		                	map.put(KEY_TITLE, cursor.getString(1));
		                	map.put(KEY_ARTIST, cursor.getString(2));
		                	map.put(KEY_LYRICS, cursor.getString(3));
		                	map.put(KEY_ALBUM, cursor.getString(4));
		                	map.put(KEY_ALBUMYEAR, cursor.getString(5));
		                	map.put(KEY_DURATION, cursor.getString(6));
		                	map.put(KEY_DURATIONDISPLAY, cursor.getString(7));
		                	map.put(KEY_BUYCD, cursor.getString(8));
		                	map.put(KEY_COUNT, cursor.getString(9));
		                	map.put(KEY_DATEPLAYED, cursor.getString(10));
		                    
		                    dataList.add(map);
		                    
		                } while (cursor.moveToNext()); 
		            }
		            cursor.close();
		        } else if(tableName == TABLE_DEDICATION) {
		        	
		        	final String selectQuery = "SELECT * FROM " + tableName;
		        	cursor = db.rawQuery(selectQuery, null); 
		        	 
		            if (cursor.moveToFirst()) {
		                do {
		                	HashMap<String, String> map = new HashMap<String, String>();
		                	map.put(KEY_TITLE, cursor.getString(0));
		                	map.put(KEY_ARTIST, cursor.getString(1));
		                	map.put(KEY_DEDICATIONBY, cursor.getString(2));
		                	map.put(KEY_DEDICATIONMESSAGE, cursor.getString(3));
		                	map.put(KEY_REQUESTDATE, cursor.getString(4));
		                	map.put("position", cursor.getString(5));
		                    
		                    dataList.add(map);
		                    
		                } while (cursor.moveToNext());
		            }
		            cursor.close();
		        }
			//}
		//}).start();
        
		closedatabase();
        db.close();
        
        return dataList;
    }
    
 // Getting Table Count
    /**
     * Count/s the number of rows in that table.
     * @param tableName name of table.
     * @return the number of rows in that table.<br>
     * 
     * @author Manlapaz Vijae
     */
    public synchronized int getDataCount(String tableName) {
    	
    	String countQuery = null;
    	
    	if(tableName == TABLE_HISTORY) {
    		countQuery = "SELECT * FROM " + tableName;
    	} else if(tableName == TABLE_PLAYLIST) {
    		countQuery = "SELECT * FROM " + tableName;
    	} else if (tableName == TABLE_TOPVOTED) {
    		countQuery = "SELECT * FROM " + tableName;
    	} else if (tableName == TABLE_TOPREQUESTED) {
    		countQuery = "SELECT * FROM " + tableName;
    	}  else if (tableName == TABLE_DEDICATION) {
    		countQuery = "SELECT * FROM " + tableName;
    	}
        
        SQLiteDatabase db = opendatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
        closedatabase();
        
        return count;
    }
    
/*===================================================================================
 * 							UPGRADE DATABASE
 *=================================================================================== */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    	
    	/*if(tableName == TABLE_HISTORY) {
    		db.execSQL("DROP TABLE IF EXISTS " + tableName);
    	} else if(tableName == TABLE_PLAYLIST) {
    		db.execSQL("DROP TABLE IF EXISTS " + tableName);
    	}
 
        // Create tables again
        onCreate(db);*/
    }
    
    // Getting single contact
    /*public Data getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
 
        Cursor cursor = db.query(TABLE_CONTACTS, new String[] { KEY_ID,
                KEY_NAME, KEY_PH_NO }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
 
        Data data = new Data(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        // return contact
        return data;
    }*/
 
    // Updating single contact
    /*public int updateData(Data data) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, data.getName());
        values.put(KEY_PH_NO, data.getPhoneNumber());
 
        // updating row
        return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(data.getID()) });
    }*/
 
    // Deleting single contact
    /*public void deleteContact(Data data) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(data.getID()) });
        db.close();
    }*/
}
