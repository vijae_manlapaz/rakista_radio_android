package com.rakista.radio.internal;

import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * The <code>PagerAdapter</code> serves the fragments when paging.
 * @author Manlapaz Vijae B.
 */
public class PagerAdapter extends FragmentPagerAdapter {
 
    private static List<Fragment> fragments;
    private static String[] Titles = new String[] { "Home", "Playlist", "History", "Top Music", "Dedications" };
    /**
     * @param fm
     * @param fragments
     */
    public PagerAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        PagerAdapter.fragments = fragments;
    }
    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentPagerAdapter#getItem(int)
     */
    @Override
    public Fragment getItem(int position) {
        return PagerAdapter.fragments.get(position);
    }
    
    @Override
    public CharSequence getPageTitle(int position) {
      return PagerAdapter.Titles[position];
    }
 
    /* (non-Javadoc)
     * @see android.support.v4.view.PagerAdapter#getCount()
     */
    @Override
    public int getCount() {
        return PagerAdapter.fragments.size();
    }
}