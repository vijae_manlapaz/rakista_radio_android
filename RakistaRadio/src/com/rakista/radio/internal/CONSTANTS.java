package com.rakista.radio.internal;


public class CONSTANTS {
    public static final String Low_Quality_URL				= "http://www.rakista.com:8000/listen32.mp3";
    public static final String Mid_Quality_URL				= "http://www.rakista.com:8000/listen.mp3";
    public static final String High_Quality_URL				= "http://www.rakista.com:8000/listen128.mp3";
    
    public static final String LQ_LABEL						= "Low Quality Stream (32kb/s)";
    public static final String MQ_LABEL						= "Medium Quality Stream (64kb/s)";
    public static final String HQ_LABEL						= "High Quality Stream (128kb/s)";
    
    public static final String getCurrentSongURL 			= "http://rakista.com/radio/api/rradio.php?method=getCurrentSong";
    public static final String getNextSongURL 				= "http://rakista.com/radio/api/rradio.php?method=getComingSongs";

    public static final String RequestURL 					= "http://rakista.com/radio/api/rradio.php?method=requestSong&songid=";
    
    public static final String TWITTER_CONSUMER_KEY 		= "DOv3xzWqUvxN5GstfuFBQ";
    public static final String TWITTER_CONSUMER_SECRET 		= "dmfqnvt0pXsNo2LqQAydMh1G8rKpkqBY0XJQNPU6gM";
    
    public static final int FBSPINNER_CONNECTING 			= 1;
    public static final int FBSPINNER_DISCONNECTING 		= 0;
    public static final String FBRADIO_COMMUNITY_PAGE_ID 	= "112682252956";
    public static final String FBRADIO_APP_PAGE_ID 			= "124828830958983";
    
    public static final String NETWORK 						= "network_error";
    public static final String VOTE							= "Vote";
    public static final String BUY 							= "Buy";
    public static final String DEDICATE 					= "Dedicate";
    public static final String REQUEST 						= "Request";
    
    public static final String GCM_APIKEY					= "AIzaSyDfQBbtpM9VoCTi4XzA3DIyb6Pp0KkDFrc";
    public static final String RADIO_GCM_REGISTRATION_ENDPOINT = "http://www.rakista.com/radio/api/gcm/register.php";
    public static final String RADIO_GCM_UNREGISTER_ENDPOINT = "http://www.rakista.com/radio/api/gcm/delete.php";
    public static final String GCM_SENDERID					= "45106899021";
    
    public static int NETWORK_ERROR 						= 4;
    
    public static int EVENTNOTIFICATION_ID					= 10;
    public static int SHOUTOUTNOTIFICATION_ID				= 100;
    public static int APPUPDATENOTIFICATION_ID				= 500;
    public static int ALARMNOTIFICATIONID					= 1000;
    
    public static final float LOW_LEVEL						= 0.75f;
    public static final float MEDIUM_LEVEL					= 1.0f;
    public static final float HIGH_LEVEL					= 1.5f;
    public static final float EXTRA_HIGH_LEVEL				= 2.0f;
    
    public static String ALBUMART	= "FetchAlbumArt";
    
    public static final String[] TOAST_HEADER 				= { VOTE, REQUEST, BUY, NETWORK, DEDICATE };
    
    public static final StreamStation[] STATIONS =
        {
            new StreamStation(LQ_LABEL, Low_Quality_URL), //position 0
            new StreamStation(MQ_LABEL, Mid_Quality_URL), //position 1
            new StreamStation(HQ_LABEL, High_Quality_URL) //position 2
        };
    
    /**
     * @return URL of the specified index.
     */
    public static final String[] STATIONS_TOSTRING =
        {
            Low_Quality_URL, //position 0
            Mid_Quality_URL, //position 1
            High_Quality_URL //position 2
        };
    
    public static final StreamStation DEFAULT_STREAM_STATION = STATIONS[0];
}
