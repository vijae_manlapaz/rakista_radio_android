package com.rakista.radio.internal;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;

public class Reachability {
	static boolean responseCode = false;
	static String url;
	
	public Reachability(String host) {
		url = host;
	}
	
	public static boolean ping(String host) {
		Reachability.url = host;
		return startPing();
	}
	
	private static boolean startPing() {
		test testThread = new test();
		testThread.start();
		Log.i("REACHABILITY", "Waiting for reachability");
		synchronized(testThread) {
			try {
				testThread.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			return responseCode;
		}
	}
	
	static class test extends Thread {
		@Override
		public void run() {
			synchronized(this) {
				try {
					// defaultHttpClient
					DefaultHttpClient httpClient = new DefaultHttpClient();
					HttpPost httpPost = new HttpPost(url);

					HttpResponse httpResponse = httpClient.execute(httpPost);
					StatusLine x  = httpResponse.getStatusLine();
					responseCode = (x.getStatusCode() == 200);
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					responseCode = false;
				} catch (ClientProtocolException e) {
					e.printStackTrace();
					responseCode = false;
				} catch (IOException e) {
					e.printStackTrace();
					responseCode = false;
				}
				notify();
				Log.i(getClass().getSimpleName(), "REACHABLE: " + Boolean.valueOf(responseCode));
			}
		}
	}
}
