package com.rakista.radio.internal;

import java.util.HashMap;

public interface RequestSenderCallback {
	public void onComplete(HashMap<String, String> result);
}
