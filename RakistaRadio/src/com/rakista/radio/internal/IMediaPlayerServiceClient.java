package com.rakista.radio.internal;

/**
 * @author Vijae Manlapaz
 */
public interface IMediaPlayerServiceClient {
 
    /**
     * A callback made by a MediaPlayerService onto its clients to indicate that a player is Initializing.
     * @param message A message to propagate to the client
     */
    public void onInitializePlayerStart(String message);
 
    /**
     * A callback made by a MediaPlayerService onto its clients to indicate that a player was Successfully Initialized.
     */
    public void onInitializePlayerSuccess();
 
    /**
     *  A callback made by a MediaPlayerService onto its clients to indicate that a player encountered an Error.
     */
    public void onError();
    
    /**
     *  A callback made by a MediaPlayerService onto its clients to indicate that a player encountered has Stop.
     */
    public void onStop(boolean state);
    
    public void onPlayback();
    
    /**
     *  A callback made by a MediaPlayerService onto its clients to indicate that a player has been reset.
     */
    public void onReset();
    
    /**
     *  A callback made by a MediaPlayerService onto its clients to indicate that a player is buffering.
     */
    public void streamBufferingStarted();
    
    /**
     *  A callback made by a MediaPlayerService onto its clients to indicate that a player has finished buffering.
     */
    public void streamBufferingFinished();
}