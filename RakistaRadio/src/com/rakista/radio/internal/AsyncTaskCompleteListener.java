package com.rakista.radio.internal;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This is a useful callback mechanism so we can abstract our AsyncTasks out into separate, re-usable
 * and testable classes yet still retain a hook back into the calling activity. Basically, it'll make classes
 * cleaner and easier to unit test.
 *
 * @param <T>
 */
public interface AsyncTaskCompleteListener {
    /**
     * Invoked when the AsyncTask has completed its execution.
     * @param result The resulting object from the AsyncTask.
     */
	public abstract void onTaskComplete(ArrayList<HashMap<String, String>> result);
	public abstract void onTaskCancelled(boolean iscancelled);
	
}