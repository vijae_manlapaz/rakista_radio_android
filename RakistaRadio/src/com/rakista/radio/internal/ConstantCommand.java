package com.rakista.radio.internal;

public class ConstantCommand {
	
	/*
     * Broadcast Receiver Commands for widget
     */
    public static final String START_STREAM					= "com.rakista.dev.START_STREAM";
    public static final String STOP_STREAM					= "com.rakista.dev.STOP_STREAM";
    public static final String UPDATE_TITLE					= "com.rakista.dev.UPDATE_TITLE";
    public static final String UPDATE_BUTTON				= "com.rakista.dev.UPDATE_BUTTON";
    public static final String UPDATE_STATUS				= "com.rakista.dev.UPDATE_STATUS";
    public static final String OPEN_ACTIVITY				= "com.rakista.dev.OPEN_ACTIVITY";
    
    public static final String START_SLEEP					= "com.rakista.radio.dev.START_SLEEP";
    public static final String STOP_SLEEP					= "com.rakista.radio.dev.STOP_SLEEP";
    
    public static final String START_ALARM					= "com.rakista.radio.dev.START_ALARM";
    public static final String STOP_ALARM					= "com.rakista.radio.dev.STOP_ALARM";

}
