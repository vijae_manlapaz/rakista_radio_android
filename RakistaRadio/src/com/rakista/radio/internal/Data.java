package com.rakista.radio.internal;

public class Data {
	private String ID;
	private String Title;
	private String Artist;
	private String Lyrics;
	private String Album;
	private String AlbumYear;
	private String Duration;
	private String DurationDisplay;
	private String BUYCD;
	
	private String TOTAL;
	private String ARTIST;
	
	public Data(String id, String title, String artist, String lyrics, String album, String albumyear, String duration, String durationDisplay, String buycd) {
		ID = id;
		Title = title;
		ARTIST = artist;
		Lyrics = lyrics;
		Album = album;
		AlbumYear = albumyear;
		Duration = duration;
		DurationDisplay = durationDisplay;
		BUYCD = buycd;
	}
	
//	public Data(String total, String artist) {
//		TOTAL = total;
//		ARTIST = artist;
//	}
	
	public Data() {
	}

	/**
	 * @return the iD
	 */
	public String getID() {
		return ID;
	}
	/**
	 * @param iD the iD to set
	 */
	public void setID(String iD) {
		ID = iD;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return Title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		Title = title;
	}
	/**
	 * @return the artist
	 */
	public String getArtist() {
		return Artist;
	}
	/**
	 * @param artist the artist to set
	 */
	public void setArtist(String artist) {
		Artist = artist;
	}
	/**
	 * @return the lyrics
	 */
	public String getLyrics() {
		return Lyrics;
	}
	/**
	 * @param lyrics the lyrics to set
	 */
	public void setLyrics(String lyrics) {
		Lyrics = lyrics;
	}
	/**
	 * @return the album
	 */
	public String getAlbum() {
		return Album;
	}
	/**
	 * @param album the album to set
	 */
	public void setAlbum(String album) {
		Album = album;
	}
	/**
	 * @return the albumYear
	 */
	public String getAlbumYear() {
		return AlbumYear;
	}
	/**
	 * @param albumYear the albumYear to set
	 */
	public void setAlbumYear(String albumYear) {
		AlbumYear = albumYear;
	}
	/**
	 * @return the duration
	 */
	public String getDuration() {
		return Duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(String duration) {
		Duration = duration;
	}
	/**
	 * @return the durationDisplay
	 */
	public String getDurationDisplay() {
		return DurationDisplay;
	}
	/**
	 * @param durationDisplay the durationDisplay to set
	 */
	public void setDurationDisplay(String durationDisplay) {
		DurationDisplay = durationDisplay;
	}
	/**
	 * @return the bUYCD
	 */
	public String getBUYCD() {
		return BUYCD;
	}
	/**
	 * @param bUYCD the bUYCD to set
	 */
	public void setBUYCD(String bUYCD) {
		BUYCD = bUYCD;
	}

	/**
	 * @return the tOTAL
	 */
	public String getTOTAL() {
		return TOTAL;
	}

	/**
	 * @param tOTAL the tOTAL to set
	 */
	public void setTOTAL(String tOTAL) {
		TOTAL = tOTAL;
	}

	/**
	 * @return the aRTIST
	 */
	public String getARTIST() {
		return ARTIST;
	}

	/**
	 * @param aRTIST the aRTIST to set
	 */
	public void setARTIST(String aRTIST) {
		ARTIST = aRTIST;
	}
		

}
