package com.rakista.radio.internal;

import android.content.Context;
import android.os.AsyncTask;

public abstract class RECAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
	public Context context;

    public RECAsyncTask<Params, Progress, Result> setContext(Context c){
        this.context = c;
        return this;
    }

}
