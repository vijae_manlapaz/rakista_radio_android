package com.rakista.radio.internal;

import com.rakista.radio.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
 
public class ToastHelper extends Toast {
    private Context context;
     
    public ToastHelper(Context cont, int duration ) {
        super(cont);
        context = cont;
        this.setDuration(duration);
    }
 
    public void show(CharSequence header, CharSequence message) {
    	
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vi = (View) li.inflate(R.layout.toast_layout, null);
        
        TextView ToasTitle = (TextView) vi.findViewById(R.id.toast_layout_header);
        ImageView Icon = (ImageView) vi.findViewById(R.id.toast_layout_icon);
        TextView Message = (TextView) vi.findViewById(R.id.alarm_prev_message);
        
        this.setView(vi);
        
        ToasTitle.setText(header);
        Message.setText(message);
        
        if(header == CONSTANTS.VOTE) {
        	Icon.setBackgroundResource(R.drawable.vote);
        } else if (header == CONSTANTS.REQUEST) {
        	Icon.setBackgroundResource(R.drawable.request);
        } else if(header == CONSTANTS.BUY){
        	Icon.setBackgroundResource(R.drawable.buy);
        } else if(header == CONSTANTS.DEDICATE) {
        	Icon.setBackgroundResource(R.drawable.ic_dedication);
        } else {
        	Icon.setVisibility(View.GONE);
        	ToasTitle.setText(header);
        }
        
        super.show();
    }
}