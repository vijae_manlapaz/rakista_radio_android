package com.rakista.radio.sites;

import com.rakista.radio.R;
import com.rakista.radio.R.id;
import com.rakista.radio.R.layout;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class fbwebview extends Activity{

	private WebView fbwebView;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.fb);
		
		fbwebView = (WebView) findViewById(R.id.fbookwebview);
		fbwebView.getSettings().setJavaScriptEnabled(true);
		fbwebView.getSettings().setBuiltInZoomControls(true);
		fbwebView.setWebViewClient(new MyfbookWebViewClient());
		fbwebView.clearView();
		fbwebView.loadUrl("http://www.facebook.com/rakistaradio");
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK) && fbwebView.canGoBack()) {
	        fbwebView.goBack();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	private class MyfbookWebViewClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return (false);
            }
	}
	
//	public void onBackPressed() {
//    	moveTaskToBack(true);
//    }
	
	public void onDestroy() {
		super.onDestroy();
		Log.d("fbwebView", "Activity Destroyed");
	}
}
