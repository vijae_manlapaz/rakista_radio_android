package com.rakista.radio.sites;

import com.rakista.radio.R;
import com.rakista.radio.R.id;
import com.rakista.radio.R.layout;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class RakistaProgramsintent extends Activity {
	
	private WebView programswebview;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.rakistaprograms);
		
		programswebview = (WebView) findViewById(R.id.webview_layout_webview);
		programswebview.getSettings().setJavaScriptEnabled(true);
		programswebview.getSettings().setBuiltInZoomControls(true);
		programswebview.setWebViewClient(new MyProgramsWebViewClient());
		programswebview.loadUrl("http://www.rakista.com/radio/mobile/programs.html");
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK) && programswebview.canGoBack()) {
	    	programswebview.goBack();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	private class MyProgramsWebViewClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
        	view.loadUrl(url);
        return false;
            }
	}
	
//	public void onBackPressed() {
//    	moveTaskToBack(true);
//    }
	
	public void onDestroy() {
		super.onDestroy();
		Log.d("RakistaProgramsSite", "Activity Destroyed");
	}

}
