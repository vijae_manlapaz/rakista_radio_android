package com.rakista.radio.sites;

import com.rakista.radio.R;
import com.rakista.radio.R.id;
import com.rakista.radio.R.layout;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class twitterwebview extends Activity {
	
	private WebView webView;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.twitter);
		
		webView = (WebView) findViewById(R.id.twitterwebview);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setUserAgentString("Rakista_Radio");
		webView.setWebViewClient(new MyWebViewClient());
		webView.loadUrl("http://mobile.twitter.com/rakistaradio");
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
	        webView.goBack();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	private class MyWebViewClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return false;
            }
	}
	
	public void onResume() {
		super.onResume();
	}
	
	public void onDestroy() {
		super.onDestroy();
		Log.d("TwitterWebView", "Activity Destroyed");
	}
}


