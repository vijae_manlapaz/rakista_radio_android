package com.rakista.radio.sites;

import com.rakista.radio.R;
import com.rakista.radio.R.id;
import com.rakista.radio.R.layout;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

@SuppressLint("SetJavaScriptEnabled")
public class MobileChat extends Activity {

	private WebView chat;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.mobilechat);
		
		chat = (WebView) findViewById(com.rakista.radio.R.id.mobilechatwebview);
		chat.getSettings().setJavaScriptEnabled(true);
		chat.getSettings().setBuiltInZoomControls(true);
		chat.setWebViewClient(new chatWebViewClient());
		chat.clearView();
		
		chat.loadUrl("file:///android_asset/mobile-client/index.htm");
		//chat.loadUrl("http://www.rakista.com/chatrooms/mobile");
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK) && chat.canGoBack()) {
	    	chat.goBack();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	private class chatWebViewClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return (false);
            }
	}
	
	public void onBackPressed() {
    	/*AlertDialog dialog = new AlertDialog.Builder(this).create();
    	dialog.setTitle("Exit");
    	dialog.setMessage("Do you want to Exit Chatroom?");
    	dialog.setButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				finish();
				chat.clearCache(true);
			}
		});
    	dialog.setButton2("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				moveTaskToBack(true);
			}
		});
    	dialog.show();*/
		this.finish();
    }
	
	public void onResume() {
		super.onResume();
	}
	
	public void onDestroy() {
		super.onDestroy();
		Log.d("MobileChatWebView", "Activity Destroyed");
	}
}
