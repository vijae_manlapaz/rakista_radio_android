package com.rakista.radio.service;

//import java.sql.Date;
//import java.text.SimpleDateFormat;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.androidworkz.androidshoutcastlib.AndroidShoutcastLib;
import com.androidworkz.androidshoutcastlib.InvalidStreamURLException;
import com.androidworkz.androidshoutcastlib.Metadata;
import com.androidworkz.androidshoutcastlib.MetadataListener;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.rakista.radio.CallListener;
import com.rakista.radio.RadioActivity;
import com.rakista.radio.applicationwidget.WidgetIntentReceiver;
import com.rakista.radio.internal.IMediaPlayerServiceClient;
import com.rakista.radio.internal.Reachability;
import com.rakista.radio.internal.StatefulMediaPlayer;
import com.rakista.radio.internal.StatefulMediaPlayer.MPStates;
import com.rakista.radio.utility.Preferences;
import com.rakista.radio.utility.Utility;

/**
 * An extension of android.app.Service class which provides access to a StatefulMediaPlayer.<br>
 * @author Vijae Manlapaz
 * @company Ridzler Entertain Company (R.E.C)
 */
public class MediaPlayerService extends Service implements OnBufferingUpdateListener, OnInfoListener, OnPreparedListener, OnErrorListener, MetadataListener {
	private StatefulMediaPlayer mMediaPlayer;
	private final Binder mBinder                        = new MediaPlayerBinder();
	private IMediaPlayerServiceClient mClient;
	private AndroidShoutcastLib shoutcast = new AndroidShoutcastLib();
	//    private StreamServer streamserver = new StreamServer(null, shoutcast);
	private String artist, title;
	
	private WifiLock lock;
	private PowerManager.WakeLock wl;

	private GoogleAnalytics mGaIntance;
	//	private EasyTracker easyTracker;
	//	private Tracker tracker;

	/**
	 * A class for clients binding to this service. The client will be passed an object of this class
	 * via its onServiceConnected(ComponentName, IBinder) callback.
	 */
	public class MediaPlayerBinder extends Binder {

		/**
		 * Returns the instance of this service for a client to make method calls on it.
		 * @return the instance of this service.
		 */
		public MediaPlayerService getService() {
			return MediaPlayerService.this;
		}
	}

	/**
	 * Returns the contained StatefulMediaPlayer
	 * @return StatefulMediaPlayer
	 */
	public StatefulMediaPlayer getMediaPlayer() {
		return mMediaPlayer;
	}

	/**
	 * Initializes a StatefulMediaPlayer for streaming playback of the provided StreamStation
	 * @param station The StreamStation representing the station to play
	 */
	/*public void initializePlayer(StreamStation station) {
    	try {
			shoutcast.setShoutcastUrl(station.getStationUrl());
		} catch (InvalidStreamURLException e) {
			e.printStackTrace();
		}

    	shoutcast.setOnMetadataChangedListener(this);

        mClient.onInitializePlayerStart("Connecting...");
        mMediaPlayer = new StatefulMediaPlayer(station);

        mMediaPlayer.setOnBufferingUpdateListener(this);
        mMediaPlayer.setOnInfoListener(this);
        mMediaPlayer.setOnErrorListener(this);
        mMediaPlayer.setOnPreparedListener(this);
        mMediaPlayer.prepareAsync();
    }*/

	/**
	 * Initializes a StatefulMediaPlayer for streaming playback of the provided stream url
	 * @param streamUrl The URL of the stream to play.
	 */
	public void initializePlayer(String streamUrl) {
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakeLockTag");
		
		WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		lock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "WifiLockTag");
		lock.acquire();
		wl.acquire();
		
		mClient.onInitializePlayerStart("Connecting Please Wait..");

		try {
			shoutcast.setShoutcastUrl(streamUrl);
		} catch (InvalidStreamURLException e) {
			e.printStackTrace();
		}

		shoutcast.setOnMetadataChangedListener(this);
		
		mMediaPlayer = new StatefulMediaPlayer();
		
		mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

		try {
			mMediaPlayer.setDataSource(shoutcast.startStream());
		}
		
		catch (Exception e) {
			Log.e("MediaPlayerService", "error setting data source");
			mMediaPlayer.setState(MPStates.ERROR);
		}

		mMediaPlayer.setOnBufferingUpdateListener(this);
		mMediaPlayer.setOnInfoListener(this);
		mMediaPlayer.setOnErrorListener(this);
		mMediaPlayer.setOnPreparedListener(this);
		mMediaPlayer.prepareAsync();

	}

	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	public void onBufferingUpdate(MediaPlayer player, int percent) {
		Log.e(getClass().getSimpleName(), "BufferingUpdate: " + Integer.valueOf(percent).toString());
	}

	public boolean onError(MediaPlayer player, int what, int extra) {
		Log.e(getClass().getSimpleName(),"onError"  + Integer.valueOf(what).toString() + ", " + Integer.valueOf(extra).toString());
		mMediaPlayer.reset();
		mMediaPlayer.release();
		mClient.onError();
		return true;
	}

	public boolean onInfo(MediaPlayer mp, int what, int extra) {
		Log.e(getClass().getSimpleName(), "onInfo: " + Integer.valueOf(what).toString() + ", " + Integer.valueOf(extra).toString());
		if (what == 701) {
			Log.e(getClass().getSimpleName(), "PLAYER BUFFERING STARTED");
			mClient.streamBufferingStarted();
		} else if (what == 702) {
			Log.e(getClass().getSimpleName(), "PLAYER BUFFERING FINISHED");
			mClient.streamBufferingFinished();
		} else if (what == 703) {
			Log.e(getClass().getSimpleName(), "PLAYER NETOWRK BANDWIDTH");
		}
		return true;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mMediaPlayer.release();
		mClient.onError();
	}
	public void onPrepared(MediaPlayer player) {
		mClient.onInitializePlayerSuccess();
		Utility.sendSession(getApplicationContext());
		startMediaPlayer();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);  
		telephonyManager.listen(new CallListener(getApplicationContext()), PhoneStateListener.LISTEN_CALL_STATE);

		mGaIntance = GoogleAnalytics.getInstance(getApplicationContext());
		
		return START_STICKY;
	}

	/**
	 * Pauses the contained StatefulMediaPlayer
	 */
	public void pauseMediaPlayer() {
		Utility.sendSession(getApplicationContext());
		mMediaPlayer.pause();
		stopForeground(true);
	}

	/**
	 * Sets the client using this service.
	 * @param client The client of this service, which implements the IMediaPlayerServiceClient interface
	 */
	public void setClient(IMediaPlayerServiceClient client) {
		this.mClient = client;
	}

	/**
	 * 
	 * @return The client instance.
	 */
	public IMediaPlayerServiceClient getClient() {
		return mClient;
	}

	/**
	 * Starts the contained StatefulMediaPlayer and foregrounds the service to support
	 * persisted background playback.
	 */
	public void startMediaPlayer() {
		Preferences.setMediaIsPlaying(true);
		Context context = getApplicationContext();

		if (Preferences.getNotification()) {

			// set to foreground
			Notification notification = new Notification(com.rakista.radio.R.drawable.notification_logo, "Rakista Radio! Now Playing: " +  artist + " - " + title, System.currentTimeMillis());

			Intent notificationIntent = new Intent(this, RadioActivity.class);
			notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

			CharSequence contentTitle = "Rakista Radio! 100% Pinoy Rock!";
			CharSequence contentText = artist + " - " + title;

			notification.setLatestEventInfo(context, contentTitle, contentText, pendingIntent);
			startForeground(1, notification);
		}
		mMediaPlayer.start();
		mClient.onPlayback();
	}

	public void updateNotification(String Artist, String Title) {
		Context context = getApplicationContext();

		if (Preferences.getNotification()) {
			// set to foreground
			Notification notification = new Notification(com.rakista.radio.R.drawable.notification_logo, "Rakista Radio! Now Playing: " + Artist + " - " + Title, System.currentTimeMillis());

			Intent notificationIntent = new Intent(this, RadioActivity.class);
			notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

			CharSequence contentTitle = "Rakista Radio! 100% Pinoy Rock!";
			CharSequence contentText = Artist + " - " + Title;
			//mMediaPlayer.getStreamStation().getStationLabel();
			notification.tickerText = contentText;
			notification.setLatestEventInfo(context, contentTitle, contentText, pendingIntent);

			startForeground(1, notification);
		}
	}

	/**
	 * Stops the contained StatefulMediaPlayer.
	 */
	public void stopMediaPlayer() {
		stopPlayback();
		stopShoutcastStraem();
		
		Log.e(getClass().getSimpleName(), "Stopping MediaPlayer.");
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				if(mMediaPlayer != null) {
					mMediaPlayer.stop();
					mMediaPlayer.release();
				}
			}
		}).start();
		
		Log.e(getClass().getSimpleName(), "Mediaplayer has stopped.");
		stopForeground(true);
		
		if(lock.isHeld()) {
			lock.release();
		}
		
		if(wl.isHeld()) {
			wl.release();
		}
	}
	
	void stopPlayback() {
		Log.e(getClass().getSimpleName(), "Sending onStop Command to Client.");
		/*new Runnable() {
			@Override
			public void run() {
				mClient.onStop(true);
			}
		}.run();*/
		
		mClient.onStop(true);
	}

	void stopShoutcastStraem() {
		Log.e(getClass().getSimpleName(), "Stopping shoutcat stream.");
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				shoutcast.stopStream();
			}
		}).start();
		
		Log.e(getClass().getSimpleName(), "shoutcast stream has stopped.");
	}

	/**
	 * Resets the contained StatefulMediaPlayer.
	 */
	public void resetMediaPlayer() {
//		Utility.sendSession(getApplicationContext());
		stopForeground(true);
		shoutcast.stopStream();
		mMediaPlayer.reset();
		mClient.onReset();
	}

	public void OnMetadataChanged(Metadata item) {
		Log.e(getClass().getSimpleName(), "Metadata Changed");
		artist = item.artist;
		title = item.track;

		if(mMediaPlayer.getState().equals(MPStates.STARTED) || mMediaPlayer.getState().equals(MPStates.PREPARED)) {
			Log.e(getClass().getSimpleName(), "Sending Intents");
			//Update Notification for MetaData Changes.
			updateNotification(item.artist, item.track);

			/*Intent currentSongIntent = new Intent();		
			currentSongIntent.setAction("CURRENTSONG_METADATA");
			currentSongIntent.putExtra("artist", item.artist);
			currentSongIntent.putExtra("title", item.track);
			sendBroadcast(currentSongIntent);*/

			Intent currentSongDetails = new Intent();
			currentSongDetails.setAction("CURRENTSONG_DETAILS");
			currentSongDetails.putExtra("ARTIST", item.artist);
			currentSongDetails.putExtra("TITLE", item.track);
			sendBroadcast(currentSongDetails);

			Intent nextSongDetails = new Intent();
			nextSongDetails.setAction("NEXTSONG_DETAILS");
			sendBroadcast(nextSongDetails);

			WidgetIntentReceiver.updateTitle(this, item.artist + " - " + item.track);
		}
	}

}
