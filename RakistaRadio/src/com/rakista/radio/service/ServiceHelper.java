package com.rakista.radio.service;

public class ServiceHelper {
	
	static MediaPlayerService mediaplayer;
	
	public ServiceHelper(MediaPlayerService mp) {
		ServiceHelper.mediaplayer = mp;
	}
	
	public void setMediaPlayerIntance(MediaPlayerService mp) {
		ServiceHelper.mediaplayer = mp;
	}
	
	public void stopMediaPlayer() {
		//new Runnable() {
			//@Override
			//public void run() {
				mediaplayer.stopMediaPlayer();
			//}
		//}.run();
	}
}
