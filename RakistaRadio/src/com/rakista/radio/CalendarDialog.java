package com.rakista.radio;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressWarnings("unused")
public class CalendarDialog extends Dialog implements android.view.View.OnClickListener, OnItemClickListener {

	private Context mContext;
	static final float[] DIMENSIONS_LANDSCAPE = { 460, 260 };
	static final float[] DIMENSIONS_PORTRAIT = { 280, 420 };
	static final FrameLayout.LayoutParams WRAP_CONTENT = new FrameLayout.LayoutParams(
			ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	static final FrameLayout.LayoutParams FILL_PARENT = new FrameLayout.LayoutParams(
			ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
	static final int MARGIN = 4;
	static final int PADDING = 2;
	private LinearLayout mContent;
	private TextView mTitle, monthTitle;
	
	public GregorianCalendar month, itemmonth;// calendar instances.
	public CalendarAdapter adapter;// adapter instance
	public Handler handler;// for grabbing some event values for showing the dot marker.
	public ArrayList<String> items; // container to store calendar items which needs showing the event marker
	
	private CalendarDialogCallback callback;
	
	public CalendarDialog(Context context, CalendarDialogCallback listener) {
		super(context);
		mContext = context;
		callback = listener;
	}
	
	public CalendarDialog(Context context) {
		super(context);
		mContext = context;
	}
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		//setContentView(R.layout.calendar);
		Locale.setDefault(Locale.US);
		mContent = new LinearLayout(getContext());
		mContent.setBackgroundResource(R.drawable.toast_bg);
		mContent.setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));
		mContent.setLayoutParams(WRAP_CONTENT);
		mContent.setOrientation(LinearLayout.VERTICAL);
		
		setUpTitle();
		setupCalendar();

		/*Display display = getWindow().getWindowManager().getDefaultDisplay();
		final float scale = getContext().getResources().getDisplayMetrics().density;
		float[] dimensions = display.getWidth() < display.getHeight() ? DIMENSIONS_PORTRAIT : DIMENSIONS_LANDSCAPE;
		*/
		
		addContentView(mContent, new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
				/*(int) (dimensions[0] * scale + 0.5f), // Width
				(int) (dimensions[1] * scale + 0.5f))); // Height
*/				//280, 420));
	}
	
	private void setUpTitle() {
        //Drawable icon = getContext().getResources().getDrawable(R.drawable.appicon);
        mTitle = new TextView(getContext());
        mTitle.setText("Please choose week");
        mTitle.setTextColor(Color.WHITE);
        mTitle.setTypeface(Typeface.DEFAULT_BOLD);
        mTitle.setBackgroundColor(Color.DKGRAY);
        mTitle.setPadding(MARGIN + PADDING, MARGIN, MARGIN, MARGIN);
        mTitle.setCompoundDrawablePadding(MARGIN + PADDING);
        mTitle.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        mTitle.setBackgroundColor(Color.BLACK);
        mTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        mContent.addView(mTitle);
    }

	public void setupCalendar() {
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.calendar, null);
		
		month = (GregorianCalendar) GregorianCalendar.getInstance();
		itemmonth = (GregorianCalendar) month.clone();

		items = new ArrayList<String>();
		adapter = new CalendarAdapter(mContext, month);

		handler = new Handler();
		handler.post(calendarUpdater);

		monthTitle = (TextView) view.findViewById(R.id.title);
		monthTitle.setTextColor(Color.WHITE);
		monthTitle.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
		
		RelativeLayout previous = (RelativeLayout) view.findViewById(R.id.previous);
		previous.setOnClickListener(this);
		
		RelativeLayout next = (RelativeLayout) view.findViewById(R.id.next);
		next.setOnClickListener(this);
		
		GridView gridview = (GridView) view.findViewById(R.id.gridview);
		gridview.setOnItemClickListener(this);
		gridview.setAdapter(adapter);
		
		mContent.addView(view);
		
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.previous:
			Log.i("Previous Month Button", "onClick");
			setPreviousMonth();
			refreshCalendar();
			break;

		case R.id.next:
			Log.i("Next Month Button", "onClick");
			setNextMonth();
			refreshCalendar();
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		((CalendarAdapter) parent.getAdapter()).setSelected(v);
		
		String selectedGridDate = CalendarAdapter.dayString.get(position);
		String[] separatedTime = selectedGridDate.split("-");
		String gridvalueString = separatedTime[2].replaceFirst("^0*", "");// taking last part of date. ie; 2 from 2012-12-02.
		
		int gridvalue = Integer.parseInt(gridvalueString);
		
		// navigate to next or previous month on clicking offdays.
		if ((gridvalue > 10) && (position < 8)) {
			setPreviousMonth();
			refreshCalendar();
		} else if ((gridvalue < 7) && (position > 28)) {
			setNextMonth();
			refreshCalendar();
		}
		
		((CalendarAdapter) parent.getAdapter()).setSelected(v);
		
		try {
			getsunday(selectedGridDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		showToast(selectedGridDate);
	}
	
	/**
	 * Gets the previous and next sunday base on the given date.
	 * @param date to be parse and get it's previous and upcoming sunday.
	 * @throws ParseException
	 * 
	 * @author Manlapaz Vijae
	 */
	public void getsunday(String date) throws ParseException {
		
		String[] temp = date.split("-");
		
		int year = Integer.valueOf(temp[0]);
		int month1 = Integer.valueOf(temp[1]) - 1;
		int date2 = Integer.valueOf(temp[2]);
		
		//Date date = new Date(date1);
		GregorianCalendar gc = new GregorianCalendar(year, month1, date2);
		Calendar c = GregorianCalendar.getInstance(Locale.getDefault());
		
		//c.set(month.get(GregorianCalendar.YEAR), month.get(GregorianCalendar.MONTH), Calendar.SUNDAY);
		c.setTime(gc.getTime());
		//c.set(Calendar.DAY_OF_WEEK, month.get(GregorianCalendar.MONTH));
		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		c.set(Calendar.HOUR_OF_DAY,0);
		c.set(Calendar.MINUTE,0);
		c.set(Calendar.SECOND,0);
		
		String[] sundays = new String[2];
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
		
		Log.i(getClass().getSimpleName(),"Previous Sunday: " + df.format(c.getTime())); // This past Sunday [ May include today ]
		
		sundays[0] = df.format(c.getTime()); // First or previous Sunday of the selected week of the selected month.
		c.add(Calendar.DATE, 6);
		sundays[1] = df.format(c.getTime()); // Second Sunday of the selected week of the selected month.
		
		Log.i(getClass().getSimpleName(),"Next Sunday: " + df.format(c.getTime()));
		//c.add(Calendar.DATE,7);
		
		Log.i(getClass().getSimpleName(),"Sundays Return Value: " + sundays[0].toString() + " " + sundays[1].toString());

		callback.onResult(sundays);
	}
	
	public void setCallback(CalendarDialogCallback listener) {
		this.callback = listener;
	}
	
	public CalendarDialogCallback getCallback() {
		return callback;
	}
	
	public void show() {
		super.show();
	}

	protected void setNextMonth() {
		if (month.get(GregorianCalendar.MONTH) == month.getActualMaximum(GregorianCalendar.MONTH)) {
			month.set((month.get(GregorianCalendar.YEAR) + 1), month.getActualMinimum(GregorianCalendar.MONTH), 1);
		} else {
			month.set(GregorianCalendar.MONTH, month.get(GregorianCalendar.MONTH) + 1);
		}
	}

	protected void setPreviousMonth() {
		if (month.get(GregorianCalendar.MONTH) == month.getActualMinimum(GregorianCalendar.MONTH)) {
			month.set((month.get(GregorianCalendar.YEAR) - 1), month.getActualMaximum(GregorianCalendar.MONTH), 1);
		} else {
			month.set(GregorianCalendar.MONTH, month.get(GregorianCalendar.MONTH) - 1);
		}

	}

	protected void showToast(String string) {
		//Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
	}

	public void refreshCalendar() {
		monthTitle = (TextView) findViewById(R.id.title);

		adapter.refreshDays();
		adapter.notifyDataSetChanged();
		handler.post(calendarUpdater); // generate some calendar items

		monthTitle.setTextColor(Color.WHITE);
		monthTitle.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
	}
	
	private Runnable calendarUpdater = new Runnable() {
		@Override
		public void run() {
			items.clear();
			// Print dates of the current week
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
			String itemvalue;
			
			for (int i = 0; i < 7; i++) {
				itemvalue = df.format(itemmonth.getTime());
				itemmonth.add(GregorianCalendar.DATE, 1);
				items.add("2012-09-12");
				items.add("2012-10-07");
				items.add("2012-10-15");
				items.add("2012-10-20");
				items.add("2012-11-30");
				items.add("2012-11-28");
			}
			adapter.setItems(items);
			adapter.notifyDataSetChanged();
		}
	};
}
