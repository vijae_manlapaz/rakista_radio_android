package com.rakista.radio;

import com.google.analytics.tracking.android.EasyTracker;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
//import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

@SuppressLint("setJavaScriptEnabled")
public class RadioWebViewClient extends Activity {

	private WebView mWebView;
	private ProgressDialog mProgress;
	private String url = null;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mProgress = new ProgressDialog(this);
		mProgress.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mProgress.setCancelable(true);
		mProgress.setCanceledOnTouchOutside(false);
		mProgress.setMessage("Loading...");
		mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.webview_layout);
		
		url = getIntent().getStringExtra("url");
		
		EasyTracker.getInstance(this).activityStart(this);
		
		setUpWebView();
		
		mProgress.show();
	}
	
	public void setUpWebView() {
		mWebView = (WebView) findViewById(R.id.webview_layout_webview);
        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setHorizontalScrollBarEnabled(false);
        mWebView.setWebViewClient(new webViewClient());
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(url);
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
	    	mWebView.goBack();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	public void onDestroy() {
		super.onDestroy();
		mWebView.clearHistory();
		finish();
	}
	
	class webViewClient extends WebViewClient {
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			return false;
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			super.onReceivedError(view, errorCode, description, failingUrl);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
			
			if (!mProgress.isShowing()) {
//				mProgress.show();
			}
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			/*String title = webview.getTitle();
			if (title != null && title.length() > 0) {
				mTitle.setText(title);
			}*/
			if(mProgress.isShowing()) {
				mProgress.dismiss();
			}
		}
	}
}
