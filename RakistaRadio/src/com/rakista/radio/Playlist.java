package com.rakista.radio;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.rakista.radio.callback.BaseCallback;
import com.rakista.radio.internal.DataBaseHandler;
import com.rakista.radio.internal.ToastHelper;
import com.rakista.radio.threads.PlaylistRunnable;
import com.rakista.radio.utility.JSONParser;
import com.rakista.radio.utility.Preferences;
import com.rakista.radio.utility.Utility;

public class Playlist extends ListFragment implements OnScrollListener, OnRefreshListener<ListView>, BaseCallback {
	
//	private Utility util;
	private DataBaseHandler dbh;
//	private SQLiteDatabase SQLDB;
	private SampleAdapter adapter;

	private final String serverURL = "http://www.rakista.com/radio/api/rradio.php?method=getartists";
	private final String[] from = new String[] {"ARTIST", "TOTAL" };
	private final int[] to = new int[] { R.id.playlist_artist, R.id.playlist_artist_song_count };
	private String TABLE_PLAYLIST = "tblplaylist";
	private ToastHelper toast;
	private PullToRefreshListView lv;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.playlist_layout, container, false);
		
		lv = (PullToRefreshListView) v.findViewById(R.id.pull_refresh_list);
		return v;
	}

	public void onDestroyView() {
		super.onDestroyView();
		setListAdapter(null);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
//		util = new Utility(activity);
		toast = new ToastHelper(activity, Toast.LENGTH_SHORT);
		dbh = new DataBaseHandler(activity);
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
//		SQLDB = dbh.getWritableDatabase();
		Preferences.Load(getActivity().getApplicationContext());

		lv.setOnRefreshListener(this);

//		Drawable x = getResources().getDrawable(R.drawable.listdivider);
//		lv.setDivider(x);
		lv.setOnScrollListener(this);

		if(!dbh.isTableExists("tblplaylist")) {
			dbh.createTable("tblplaylist");
		}

		if(Utility.isConnected()) {
			if(dbh.getDataCount(TABLE_PLAYLIST) > 0) {
				adapter = new SampleAdapter(getActivity(), dbh.getAllData(TABLE_PLAYLIST), R.layout.playlist_view, from, to);
				setListAdapter(adapter);
			} else {
				if(Utility.isConnected()) {
					
					new LoadPlaylist(false).execute(serverURL);
//					new Thread(new PlaylistRunnable(this)).start();
				} else {
					toast.show(getString(R.string.Network),getString(R.string.not_reachable));
				}
			}
		} else {
			adapter = new SampleAdapter(getActivity().getApplicationContext(),
					dbh.getAllData(TABLE_PLAYLIST), R.layout.playlist_view,
					from, to);
			setListAdapter(adapter);
		}

		EasyTracker.getInstance(getActivity()).activityStart(getActivity());
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
	}

	/*======================================================================================
	 * 									ADAPTER CLASS
	 *======================================================================================
	 * */
	public class SampleAdapter extends SimpleAdapter {
		String[] from = new String[] { "ARTIST", "TOTAL" };
		int[] to = new int[] { R.id.playlist_artist, R.id.playlist_artist_song_count };
		private ArrayList<HashMap<String, String>> Data = new ArrayList<HashMap<String, String>>();
		private final Context mContext;

		public SampleAdapter(Context context, ArrayList<HashMap<String, String>> data, int resource, String[] from, int[] to) {
			super(context, data, resource, from, to);
			this.mContext = context;
			this.Data = data;
		}

		public void setData(ArrayList<HashMap<String, String>> data) {
			this.Data = data;
		}

		public View getView(final int position, View view, ViewGroup parent) {
			if (view == null) {
				LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = vi.inflate(R.layout.playlist_view, null);
			}

			TextView tv = (TextView) view.findViewById(R.id.playlist_artist);
			tv.setText(Data.get(position).get("ARTIST").toString());

			TextView songCount = (TextView) view.findViewById(R.id.playlist_artist_song_count);
			songCount.setText(Data.get(position).get("TOTAL").toString());

			view.setBackgroundResource(android.R.color.white);
			
			tv.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					Utility.sendClickEvent("VIEWING_ARTIST_SONGS", v.getContext());
					if(Utility.isConnected()) {
						Intent x = new Intent(v.getContext(), ArtistSong.class);
						x.putExtra("artist", Data.get(position).get("ARTIST").toString());
						startActivity(x);
					} else {
						toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
					}
				}
			});

			return view;
		}
	}

	/*==================================================================================
	 * 									ASYNCTASK
	 *==================================================================================
	 * */
	public class LoadPlaylist extends AsyncTask<String, Void, ArrayList<HashMap<String,String>>> {

		private ProgressDialog mProgress;
		private boolean enableProgress;

		public LoadPlaylist(boolean showProgressDialog) {
			enableProgress = showProgressDialog;
		}

		protected ArrayList<HashMap<String, String>> doInBackground(String... params) {
			/// JSON Node names
			final String RESULT = "result";
			final String TAG_ARTIST = "ARTIST";
			final String TAG_SONGCOUNT = "TOTAL";

			// contacts JSONArray
			JSONArray contacts = null;

			// Hashmap for ListView
			ArrayList<HashMap<String, String>> ArtistData = new ArrayList<HashMap<String, String>>();

			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			JSONObject json = null;

			while(!isCancelled()) {
				if(!this.isCancelled()) {
					try {
						json = jParser.getJSONFromUrl(params[0]);
					} catch (HttpException e1) {
						e1.printStackTrace();
						json = null;
					} catch (UnknownHostException e) {
						e.printStackTrace();
						json = null;
					} catch (ConnectTimeoutException e) {
						e.printStackTrace();
						json = null;
					} catch (NullPointerException e) {
						e.printStackTrace();
					}

					try {
						// Getting Array of Contacts
						if(json != null) {
							contacts = json.getJSONArray(RESULT);

							// looping through All Contacts
							for(int i = 0; i < contacts.length(); i++) {
								JSONObject c = contacts.getJSONObject(i);

								// Storing each json item in variable
								String Artist = c.getString(TAG_ARTIST);
								String Count = c.getString(TAG_SONGCOUNT);

								// creating new HashMap
								HashMap<String, String> map = new HashMap<String, String>();

								// adding each child node to HashMap key => value
								map.put(TAG_ARTIST, Artist);
								map.put(TAG_SONGCOUNT, Count);

								// adding HashList to ArrayList
								ArtistData.add(map);
							}
						}

					} catch (JSONException e) {
						e.printStackTrace();
						return null;
					} catch (NullPointerException e) {
						e.printStackTrace();
						return null;
					}
				}

				return ArtistData;
			}
			return null;
		}

		protected void onPreExecute() {
			if(enableProgress) {
				try {
					mProgress = new ProgressDialog(getActivity());
					mProgress.setMessage("Updating Playlist\nPlease Wait...");
					mProgress.setIndeterminate(true);
					mProgress.setCancelable(false);
//					mProgress.setOnCancelListener(this);
					mProgress.setCanceledOnTouchOutside(false);
					mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
					mProgress.show();
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
			}
		}

//		@Override
//		public void onCancel(DialogInterface dialog) {
//			Log.i(getClass().getSimpleName(), "Cancelling Thread");
//			this.cancel(true);
//		}

		protected void onCancelled() {
			Log.i(getClass().getSimpleName(), "Thread successfully cancelled");
//			mProgress.dismiss();
		}

		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {

			if(mProgress != null && mProgress.isShowing()) {
				mProgress.dismiss();
			}

			if(!result.isEmpty() && result != null) {

				adapter = new SampleAdapter(getActivity().getApplicationContext(),result, R.layout.playlist_view, from, to); // To
				setListAdapter(adapter);
				dbh.updateTable("tblplaylist", result);
			} else {
				if(mProgress != null && mProgress.isShowing()) {
					mProgress.dismiss();
				}
			}
			
			lv.onRefreshComplete();
		}
	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		if(Utility.isConnected()) {
			new LoadPlaylist(true).execute(serverURL);
//			new Thread(new PlaylistRunnable(this)).start();
		} else {
			toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
		}
	}

	@Override
	public void onComplete(final ArrayList<HashMap<String, String>> data) {
		if(!data.isEmpty() || data != null) {
			
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					adapter = new SampleAdapter(getActivity().getApplicationContext(), data, R.layout.playlist_view, from, to); // To
					setListAdapter(adapter);
					lv.onRefreshComplete();
				}
			});
			
			dbh.updateTable("tblplaylist", data);
		}
	}
}
