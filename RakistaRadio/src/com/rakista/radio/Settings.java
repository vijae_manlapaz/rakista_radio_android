package com.rakista.radio;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.AlertDialog;
import android.app.ListActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.rakista.radio.internal.ToastHelper;
import com.rakista.radio.utility.Preferences;
import com.rakista.radio.utility.Utility;

//import com.google.analytics.tracking.android.GoogleAnalytics;
//import com.google.analytics.tracking.android.Tracker;
//import android.app.NotificationManager;
//import android.content.Context;
//import com.rakista.radio.service.MediaPlayerService;
//import com.rakista.radio.service.MediaPlayerService.MediaPlayerBinder;
//import com.rakista.radio.internal.IMediaPlayerServiceClient;

/**
 * 
 * @author Vijae Manlapaz
 *
 */
public class Settings extends ListActivity implements OnItemClickListener, OnClickListener {
	//private Button AudioQuality, Notification, Accounts;
	private AlertDialog.Builder Adialog;
	private int mSelectedStream;
	//private String mSelectedNotification;
	private int NotificationState;
	private TextView versionLabel;
	private ImageView recButton;

	private com.rakista.radio.utility.Utility util;
	private com.rakista.radio.internal.ToastHelper toast;

	private int Buffkey;
	private int DisplayPhoto;
	//private ListAdapter adapter;
	private String[] from = new String[] {"items", "subitems"};
	private int[] to = new int[] {R.id.settings_layout_item, R.id.settings_layout_subitem};
	private ListView listview;

//	private EasyTracker easyTracker;
//	private GoogleAnalytics mGaIntance;
//	private Tracker tracker;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_layout);
		Preferences.Load(getApplication().getApplicationContext());

		util = new Utility(this);
		toast = new ToastHelper(this, Toast.LENGTH_LONG);

		recButton = (ImageView) findViewById(R.id.settings_layout_recbutton);
		recButton.setOnClickListener(this);

		versionLabel = (TextView) findViewById(R.id.settings_layout_versionLabel);
		versionLabel.setText(Utility.getAppVersion(getApplicationContext()));

		SimpleAdapter adapter = new SimpleAdapter(this, populateList(), R.layout.settings_view, from, to);
		setListAdapter(adapter);

		listview = getListView();
		listview.setOnItemClickListener(this);	
		
		EasyTracker.getInstance(this).activityStart(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.settings_layout_recbutton:
			Utility.sendClickEvent("Settings-RECLOGO-Button", this);
			if(util.isConnected()) {
				Utility.openWebview("http://www.recph.com", v.getContext());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			break;
		}
	}

	public ArrayList<HashMap<String, String>> populateList() {
		String[] item = {getString(R.string.settings_audio_quality_button),
				getString(R.string.settings_notification_button),
				getString(R.string.settings_Display_Artist_Photo),
				getString(R.string.settings_accounts_button)};
		String[] subs = {getString(R.string.settings_audio_quality)
				,getString(R.string.settings_notification_description),
				getString(R.string.settings_Display_Artist_Photo_Description),
				getString(R.string.settings_accounts_description)};

		final ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

		for(int x = 0; x < item.length; x++) {
			HashMap<String, String> temp = new HashMap<String, String>();
			temp.put("items", item[x]);
			temp.put("subitems", subs[x]);
			list.add(temp);
		}		

		return list;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		switch(position) {
		case 0: //Audio Quality 

			mSelectedStream = Preferences.getStreamSelectedSetting();
			final CharSequence[] choiceList = {"Low (32kbps)", "Medium (64kbps)" ,"High (128kbps)" }; // Sequence 0, 1, 2

			Adialog = new  AlertDialog.Builder(this);
			Adialog.setTitle("Choose Music Quality");
			Adialog.setSingleChoiceItems(choiceList, mSelectedStream, new DialogInterface.OnClickListener() {


				public void onClick(DialogInterface dialog, int which) {
					Buffkey = which;

					Log.i(getClass().getSimpleName(), Integer.valueOf(which).toString());

				}

			}).setPositiveButton("Set", new DialogInterface.OnClickListener() {


				public void onClick(DialogInterface dialog, int which) {
					Preferences.setStreamSelectedSetting(Buffkey);
				}

			}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {}
			});

			AlertDialog A = Adialog.create();
			A.show();

			break;

		case 1: // Notification

			final CharSequence[] list = {"On (Recommended)", "Off"}; // Sequence 0, 1,
			boolean x = Preferences.getNotification();

			// Convert Boolean to Integer
			NotificationState = x? 0 : 1;

			Adialog = new  AlertDialog.Builder(this);
			Adialog.setTitle("Toggle Notification");
			Adialog.setSingleChoiceItems(list, NotificationState, new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					NotificationState = which;
				}

			}).setPositiveButton("Set", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					if(NotificationState == 0) {
						Preferences.setEnableNotification(true);
						Preferences.setgcmEnabled(true);
						Utility.sendClickEvent("Settings-Notfication-Button_ON", getApplicationContext());
					} else {
						Preferences.setgcmEnabled(false);
						Preferences.setEnableNotification(false);
						Utility.sendClickEvent("Settings-Notfication-Button_OFF", getApplicationContext());
					}
				}

			}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {}
			});

			AlertDialog B = Adialog.create();
			B.show();

			break;
		case 2:
			final CharSequence[] list1 = {"On", "Off (Recommended for slow connections)"}; // Sequence 0, 1,
			boolean y = Preferences.getDisplayPhoto();

			// Convert Boolean to Integer
			DisplayPhoto = y? 0 : 1;

			Adialog = new  AlertDialog.Builder(this);
			Adialog.setTitle("Toggle Artist Photo");
			Adialog.setSingleChoiceItems(list1, DisplayPhoto, new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					DisplayPhoto = which;
				}

			}).setPositiveButton("Set", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					if(DisplayPhoto == 0) {
						Preferences.setEnableDisplayPhoto(true); 
						Preferences.setgcmEnabled(true);
						Utility.sendClickEvent("Settings-Notfication-Button_ON", getApplicationContext());
					} else {
						Preferences.setgcmEnabled(false);
						Preferences.setEnableDisplayPhoto(false);
						Utility.sendClickEvent("Settings-Notfication-Button_OFF", getApplicationContext());
					}
				}

			}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {}
			});

			AlertDialog C = Adialog.create();
			C.show();
			break;

		case 3: // Accounts
			Utility.sendClickEvent("Settings-Accounts-Button", this);
			Intent AccountloginIntent = new Intent(this, AccountsLogin.class);
			startActivity(AccountloginIntent);

			break;
		}

	}

	/*public void onClick(View view) {
		switch(view.getId()) {
		case R.id.settings_layout_audio_quality_button:

			mSelectedStream = Preferences.getStreamSelectedSetting();
			final CharSequence[] choiceList = {"Low 32kbps", "Medium 64kbps" ,"High 128kbps" }; // Sequence 0, 1, 2

			Adialog = new  AlertDialog.Builder(this);
			Adialog.setTitle("Default Stream Quality");
			Adialog.setSingleChoiceItems(choiceList, mSelectedStream, new DialogInterface.OnClickListener() {


				public void onClick(DialogInterface dialog, int which) {
					Buffkey = which;

					Log.i(getClass().getSimpleName(), Integer.valueOf(which).toString());

				}

			}).setPositiveButton("Set", new DialogInterface.OnClickListener() {


				public void onClick(DialogInterface dialog, int which) {
					Preferences.setStreamSelectedSetting(Buffkey);
				}

			}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {}
			});

			AlertDialog A = Adialog.create();
			A.show();

			break;

		case R.id.settings_layout_notification_button:

			final CharSequence[] list = {"On", "Off"}; // Sequence 0, 1, 2
			boolean x = Preferences.getNotification();

			// Convert Boolean to Integer
			NotificationState = x? 0 : 1;

			Adialog = new  AlertDialog.Builder(this);
			Adialog.setTitle("Toggle Notification");
			Adialog.setSingleChoiceItems(list, NotificationState, new DialogInterface.OnClickListener() {


				public void onClick(DialogInterface dialog, int which) {
					NotificationState = which;

					Log.i(getClass().getSimpleName(), Integer.valueOf(which).toString());

				}
			}).setPositiveButton("Set", new DialogInterface.OnClickListener() {


				public void onClick(DialogInterface dialog, int which) {
					if(NotificationState == 0) {
						Preferences.setEnableNotification(true);
					} else {
						Preferences.setEnableNotification(false);
					}
				}
			}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {}
			});

			AlertDialog B = Adialog.create();
			B.show();

			break;

		case R.id.settings_layout_accounts_button:
			Intent AccountloginIntent = new Intent(this, AccountsLogin.class);
			startActivity(AccountloginIntent);
			break;
		}
	}*/


}
