package com.rakista.radio;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ExpandableListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.BadTokenException;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.rakista.radio.core.DedicationDialog;
import com.rakista.radio.core.RequestSender;
import com.rakista.radio.core.VoteSender;
import com.rakista.radio.internal.RequestSenderCallback;
import com.rakista.radio.internal.ToastHelper;
import com.rakista.radio.utility.JSONParser;
import com.rakista.radio.utility.Utility;
import com.rakista.radio.widget.AutoScrollingTextView;

public class ArtistSong extends ExpandableListActivity {
	
	private String TAG = ArtistSong.class.getSimpleName();

	private static ExpandableListView listview;
//	private Utility util;
	private ToastHelper toast;
	private static SimpleExpandableAdapter adapter;
	private static ArrayList<HashMap<String, String>> Data = new  ArrayList<HashMap<String, String>>();
	private static String SearchURL = "http://www.rakista.com/radio/api/rradio.php?artist=";
	private static String Method = "&method=getArtistsSongs";
	private DedicationDialog dedicationDialog;
	private ProgressDialog mProgress;

//	private GoogleAnalytics mGaIntance;
//	private Tracker tracker;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.artistsongs_layout);

		dedicationDialog = new DedicationDialog(this);
//		util = new Utility(getApplicationContext());
		toast = new ToastHelper(getApplicationContext(), Toast.LENGTH_LONG);
		String artistName = getIntent().getStringExtra("artist");

		Drawable x = getResources().getDrawable(R.drawable.listdivider);

		listview = getExpandableListView();
		listview.setOnChildClickListener(this);
		listview.setGroupIndicator(null);
		listview.setDivider(x);

		TextView artistNameHeader = (TextView)findViewById(R.id.artistsong_layout_artistname);
		artistNameHeader.setText(artistName + " Songs");

//		mGaIntance = GoogleAnalytics.getInstance(this);
		//mGaIntance.setDebug(true);
		EasyTracker.getInstance(this).activityStart(this);

		getArtistSong(artistName);

	}

	/* Creating the Hashmap for the row */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static ArrayList<HashMap<String, String>> createGroupList() {
		ArrayList result = new ArrayList();
		for (int i = 0; i < Data.size(); ++i) {
			HashMap m = new HashMap();
			m.put("ID", Data.get(i).get("ID"));
			m.put("TITLE", Data.get(i).get("TITLE"));
			m.put("ARTIST", Data.get(i).get("ARTIST"));
			m.put("DURATION", Data.get(i).get("DURATION"));
			m.put("POSITION", Integer.valueOf(i + 1).toString());
			result.add(m);
		}
		return (ArrayList<HashMap<String, String>>) result;
	}

	/* creating the HashMap for the children */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static ArrayList<ArrayList<HashMap<String, String>>> createChildList() {
		ArrayList result = new ArrayList();
		for (int i = 0; i < Data.size(); ++i) {
			ArrayList<HashMap<String, String>> secList = new ArrayList<HashMap<String, String>>();
			for (int n = 0; n < 1; n++) {
				HashMap child = new HashMap();
				child.put("ID", Data.get(i).get("ID"));
				child.put("TITLE", Data.get(i).get("TITLE"));
				child.put("ARTIST", Data.get(i).get("ARTIST"));
				child.put("ALBUM", Data.get(i).get("ALBUM"));
				child.put("ALBUMYEAR" , Data.get(i).get("ALBUMYEAR"));
				child.put("LYRICS", Data.get(i).get("LYRICS"));
				child.put("BUYCD", Data.get(i).get("BUYCD"));
				child.put("DURATION", Data.get(i).get("DURATION"));
				secList.add(child);
			}
			result.add(secList);
		}
		return result;
	}

	private String formatTime(long millis) {
		String output = "00:00:00";
		long seconds = millis / 1000;
		long minutes = seconds / 60;
//		long hours = minutes / 60;

		seconds = seconds % 60;
		minutes = minutes % 60;
//		hours = hours % 60;

		String secondsD = String.valueOf(seconds);
		String minutesD = String.valueOf(minutes);
//		String hoursD = String.valueOf(hours);

		if (seconds < 10)
			secondsD = "0" + seconds;
		if (minutes < 10)
			minutesD = "0" + minutes;
//		if (hours < 10)
//			hoursD = "0" + hours;

		output = minutesD + " : " + secondsD;
		return output;
	}

	public class SimpleExpandableAdapter extends BaseExpandableListAdapter {
		private Context context;
		private LayoutInflater inflater;
		private ArrayList<HashMap<String,String>> Group = new ArrayList<HashMap<String,String>>();
		private ArrayList<ArrayList<HashMap<String,String>>> Child = new ArrayList<ArrayList<HashMap<String,String>>>();

		public SimpleExpandableAdapter(Context context, ArrayList<HashMap<String,String>> groupItem, ArrayList<ArrayList<HashMap<String,String>>> childItem) {
			this.Group = groupItem;
			this.Child = childItem;
			this.context = context;
		}

		public Object getChild(int groupPosition, int childPosition) {
			return Child.get(groupPosition).get(childPosition);
		}

		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
			if(convertView == null) {
				inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.artistsongs_view_child, null);
			}

			AutoScrollingTextView Title = (AutoScrollingTextView)convertView.findViewById(R.id.artistsongs_child_view_title);
			Title.setText(Child.get(groupPosition).get(0).get("TITLE"));

			AutoScrollingTextView Artist = (AutoScrollingTextView)convertView.findViewById(R.id.artistsongs_child_view_artist);
			Artist.setText(Child.get(groupPosition).get(0).get("ARTIST"));

			AutoScrollingTextView Album = (AutoScrollingTextView)convertView.findViewById(R.id.artistsongs_child_view_album);
			Album.setText(Child.get(groupPosition).get(0).get("ALBUM").length() < 1? "Not Available" : Child.get(groupPosition).get(0).get("ALBUM"));

			TextView Duration = (TextView)convertView.findViewById(R.id.artistsongs_child_view_duration);
			Duration.setText(formatTime(Integer.valueOf((Child.get(groupPosition).get(0).get("DURATION").toString()))));

			TextView Year = (TextView)convertView.findViewById(R.id.artistsongs_child_view_year);
			Year.setText(Child.get(groupPosition).get(0).get("ALBUMYEAR"));

			TextView Lyrics = (TextView) convertView.findViewById(R.id.artistsongs_child_view_lyrics);
			Lyrics.setText(Child.get(groupPosition).get(0).get("LYRICS").length() < 5? "Not Available" : Child.get(groupPosition).get(0).get("LYRICS"));
			Lyrics.setMovementMethod(new ScrollingMovementMethod());

			Button Vote = (Button) convertView.findViewById(R.id.artistsongs_child_view_button_vote);
			Vote.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					Utility.sendClickEvent("VOTE_BUTTON", v.getContext());
					if (Utility.isConnected()) {
						VoteSender Vote = new VoteSender(getApplicationContext().getApplicationContext());
						Vote.sendVote(Data.get(groupPosition).get("ID"));
					} else {
						toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
					}
				}
			});
			Button Request = (Button) convertView.findViewById(R.id.artistsongs_child_view_button_request);
			Request.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Utility.sendClickEvent("REQUEST_BUTTON", v.getContext());
					if(Utility.isConnected()) {
						new RequestSender(getApplicationContext(), new RequestSenderCallback() {
							@Override
							public void onComplete(HashMap<String, String> result) {
								if (result.containsKey("requestid")) {
									try {
										dedicationDialog.setRequestID(result.get("requestid").toString());
										dedicationDialog.show();
									} catch (BadTokenException e) {
										e.printStackTrace();
									}
								} else {
									toast.show("Request", result.get("err_msg"));
								}
							}
						}).exec(Data.get(groupPosition).get("ID"));
					} else {
						toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
					}
				}
			});

			Button Buy = (Button) convertView.findViewById(R.id.artistsongs_child_view_button_buy);
			if(!(Child.get(groupPosition).get(0).get("BUYCD").length() > 0)) {
				//null || Child.get(groupPosition).get(0).get("BUYCD") == "<null>" || Child.get(groupPosition).get(0).get("BUYCD") == "null") {
				Buy.setTextColor(Color.GRAY);
				Buy.setEnabled(false);
				Buy.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.buy_inactive), null, null, null);

			} else {
				Buy.setEnabled(true);
				Buy.setTextColor(Color.WHITE);
				Buy.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.buy), null, null, null);

				Buy.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Utility.sendClickEvent("BUY_BUTTON", v.getContext());
						if(Utility.isConnected()) {
							Log.e(TAG, "BUY URL: " + Child.get(groupPosition).get(0).get("BUYCD").toString());
							Intent i = new Intent(Intent.ACTION_VIEW);
							i.setData(Uri.parse(Child.get(groupPosition).get(0).get("BUYCD").toString()));
							startActivity(i);
						} else {
							toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
						}
					}
				});
			}

			return convertView;
		}

		public int getChildrenCount(int groupPosition) {
			return Child.get(groupPosition).size();
		}

		public Object getGroup(int groupPosition) {
			return Group.get(groupPosition);
		}

		public int getGroupCount() {
			return Group.size();
		}

		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
			if(convertView == null) {
				inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.artistsongs_view_group, null);
			}

			TextView Position = (TextView) convertView.findViewById(R.id.artistsongs_group_position);
			Position.setText(Group.get(groupPosition).get("POSITION") + ". ");

			AutoScrollingTextView Title = (AutoScrollingTextView) convertView.findViewById(R.id.artistsongs_group_title);
			Title.setText(Group.get(groupPosition).get("TITLE"));

			AutoScrollingTextView Artist = (AutoScrollingTextView) convertView.findViewById(R.id.artistsongs_group_artist);
			Artist.setText(Group.get(groupPosition).get("ARTIST"));

			TextView Length = (TextView) convertView.findViewById(R.id.artistsongs_group_song_length);
			Length.setText(formatTime(Long.valueOf(Group.get(groupPosition).get("DURATION"))));

			return convertView;
		}

		public boolean hasStableIds() {
			return true;
		}

		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
	}

	public class SongList extends AsyncTask<String, Void, ArrayList<HashMap<String, String>>> implements DialogInterface.OnCancelListener {

		public Context mContext;
		
		public SongList(Context context) {
			this.mContext = context;
		}
		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(String... params) {

			// JSON Node names
			final String RESULT = "result";
			final String TAG_ID = "ID";
			final String TAG_ARTIST = "ARTIST";
			final String TAG_TITLE = "TITLE";
//			final String TAG_DATE_ADDED = "DATE_ADDED";
//			final String TAG_DATEPLAYED = "DATE_PLAYED";
//			final String TAG_DATE_TITLEPLAYED = "DATE_TITLE_PLAYED";
			final String TAG_DURATION = "DURATION";
			final String TAG_ALBUM = "ALBUM";
			final String TAG_ALBUMYEAR = "ALBUMYEAR";
//			final String TAG_LABEL = "LABEL";
			final String TAG_LYRICS = "LYRICS";
			final String TAG_BUYCD = "BUYCD";
//			final String TAG_PICTURE = "PICTURE";

			// contacts JSONArray
			JSONArray contacts = null;
			while(!isCancelled()) {
				// Hashmap for ListView
				ArrayList<HashMap<String, String>> ArtistSongs = new ArrayList<HashMap<String, String>>();

				// Creating JSON Parser instance
				JSONParser jParser = new JSONParser();

				// getting JSON string from URL
				JSONObject json = null;
				try {
					json = jParser.getJSONFromUrl(params[0]);
				} catch (HttpException e1) {
					e1.printStackTrace();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				} catch (ConnectTimeoutException e) {
					toast.show(getString(R.string.error), "No Internet Connection Detected\nPlease check if you can browse website using your browser.");
					e.printStackTrace();
				} catch (NullPointerException e) {
					e.printStackTrace();
				}

				try {
					// Getting Array of Contacts
					contacts = json.getJSONArray(RESULT);

					// looping through All Contacts
					for(int i = 0; i < contacts.length(); i++){
						JSONObject c = contacts.getJSONObject(i);

						// Storing each json item in variable
						String ID = c.getString(TAG_ID);
						String Artist = c.getString(TAG_ARTIST);
						String Title = c.getString(TAG_TITLE);
						String Album = c.getString(TAG_ALBUM);
						String AlbumYear = c.getString(TAG_ALBUMYEAR);
						String Lyrics = c.getString(TAG_LYRICS);
						String Duration = c.getString(TAG_DURATION);
						String BuyCD = c.getString(TAG_BUYCD);

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						map.put(TAG_ID, ID);
						map.put(TAG_ARTIST, Artist);
						map.put(TAG_TITLE, Title);
						map.put(TAG_ALBUM, Album);
						map.put(TAG_ALBUMYEAR, AlbumYear);
						map.put(TAG_BUYCD, BuyCD);
						map.put(TAG_DURATION, Duration);
						map.put(TAG_LYRICS, Lyrics);

						// adding HashList to ArrayList
						ArtistSongs.add(map);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
				return ArtistSongs;
			}
			return null;
		}

		protected void onPreExecute() {
			mProgress = new ProgressDialog(mContext);
			mProgress.setMessage("Loading...");
			mProgress.setCancelable(true);
			mProgress.setIndeterminate(true);
			mProgress.setCanceledOnTouchOutside(true);
			mProgress.setOnCancelListener(this);
			mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgress.show();
		}

		@Override
		public void onCancel(DialogInterface dialog) {
			Log.i(getClass().getSimpleName(), "Cancelling Thread");
			this.cancel(true);
		}

		protected void onCancelled() {
			Log.i(getClass().getSimpleName(), "Thread successfully cancelled");
			mProgress.dismiss();
		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
			mProgress.dismiss();
			setResponse(result);
			adapter = new SimpleExpandableAdapter(getApplicationContext() , createGroupList(), createChildList());
			listview.setAdapter(adapter);
		}

	}

	public void getArtistSong(String artistName) {
		if(Utility.isConnected()) {
			Log.i("SEARCH URL", SearchURL + artistName + Method);
			try {
				new SongList(this).execute(SearchURL +  URLEncoder.encode(artistName, "UTF-8") + Method);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		} else {
			toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
		}
	}

	public void setResponse(ArrayList<HashMap<String, String>> data) {
		Data = data;
	}

	public ArrayList<HashMap<String, String>> getResponse() {
		return Data;
	}
}
