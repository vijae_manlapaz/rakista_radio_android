package com.rakista.radio.social;

import twitter4j.User;
import twitter4j.auth.AccessToken;

public interface TwitterLoginCallback {
	
	/**
	 * Returns an array of <strong>Object</strong>
	 * <lu><li>Index 0: User object</li>
	 * <li>Index 1: AccessToken object </li</lu>
	 * 
	 * @param object Array;
	 */
	void onAuthorizationSuccess(Object[] object);
}
