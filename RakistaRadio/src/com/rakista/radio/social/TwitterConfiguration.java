package com.rakista.radio.social;

public class TwitterConfiguration {

	private String CONSUMER_KEY;
	private String CONSUMER_SECRET;
	private String CALLBACK_URL;
	
	public String getConsumerKey() {
		return CONSUMER_KEY;
	}
	
	public void setConsumerKey(String consumerkey) {
		CONSUMER_KEY = consumerkey;
	}
	
	public String getConsumerSecret() {
		return CONSUMER_SECRET;
	}
	
	public void setConsumerSecret(String consumersecret) {
		CONSUMER_SECRET = consumersecret;
	}
	
	public String getCallbackUrl() {
		return CALLBACK_URL;
	}
	
	public void setCallbackUrl(String callbackurl) {
		CALLBACK_URL = callbackurl;
	}
	
}
