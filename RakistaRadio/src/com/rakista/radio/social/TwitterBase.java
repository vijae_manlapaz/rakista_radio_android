package com.rakista.radio.social;

import com.sugree.twitter.TwitterSessionStore;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import android.content.Context;
import android.util.Log;

public class TwitterBase {
	private static String TAG = TwitterBase.class.getSimpleName();
	
	private String CONSUMER_KEY;
	private String CONSUMER_SECRET;
	private String CALLBACK_URL;

	private TwitterBase(String key, String secret, String callback) {
		CONSUMER_KEY = key;
		CONSUMER_SECRET = secret;
		CALLBACK_URL = callback;
	}
	
	public static void sendTweet(String msg, Context context, String consumerkey, String consumersecret) {
		AccessToken at = new AccessToken(TwitterSessionStore.getAccessToken(context),
				TwitterSessionStore.getAccessTokenSecret(context),
				Long.valueOf(TwitterSessionStore.getUserId(context)).longValue());
		
		Twitter twitter = TwitterFactory.getSingleton();
		twitter.setOAuthConsumer(consumerkey, consumersecret);
		twitter.setOAuthAccessToken(at);
		
	    try {
			Status status = twitter.updateStatus(msg);
			Log.e(TAG, "Successfully updated status " + status.getText());
		} catch (TwitterException e) {
			e.printStackTrace();
		}
	}
	
	public static void logOut(Twitter twitter, Context context, TwitterLogOutCallback listener) {
		twitter.setOAuthAccessToken(null);
		TwitterSessionStore.clear(context);
		listener.onLogOutSuccess();
	}
	
	public static void logOut(Twitter twitter, Context context) {
		twitter.setOAuthAccessToken(null);
		TwitterSessionStore.clear(context);
	}
	
	public void logIn(Context context) {
		new TwitterDialog(context, CONSUMER_KEY, CONSUMER_SECRET, CALLBACK_URL).show();
	}
	
	public static void logIn(Context context, String consumerkey, String consumersecret, String callbackurl, TwitterLoginCallback listener, boolean usecallback) {
		if(usecallback) {
			new TwitterDialog(context, consumerkey, consumersecret, callbackurl, listener, usecallback).show();
		} else {
			new TwitterDialog(context, consumerkey, consumersecret, callbackurl).show();
		}
	}
}
