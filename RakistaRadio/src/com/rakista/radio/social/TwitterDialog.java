package com.rakista.radio.social;

import java.util.concurrent.ExecutionException;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class TwitterDialog extends Dialog {
	
	static final int TW_BLUE = 0xFFC0DEED;
    static final float[] DIMENSIONS_LANDSCAPE = {460, 260};
    static final float[] DIMENSIONS_PORTRAIT = {280, 420};
    static final FrameLayout.LayoutParams FILL = 
        new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    static final int MARGIN = 4;
    static final int PADDING = 2;
    
	private static String TAG = TwitterDialog.class.getSimpleName();
	
    private WebView webView;
    private LinearLayout mContent;
    
    private TwitterLoginCallback callback;
    private Twitter mTwitter;
    private RequestToken requestToken;
    
    private String CONSUMER_KEY = "";
    private String CONSUMER_SECRET = "";
    private String CALLBACK_URL = "";
    private boolean usecallback = false;
    public ProgressDialog mProgressDialog;
    private Handler mHandler;
    
    public TwitterDialog(Context context) {
		super(context);
	}
    
    public TwitterDialog(Context context, String consumerkey, String consumersecret, String callbackurl) {
		super(context);
		CONSUMER_KEY = consumerkey;
		CONSUMER_SECRET = consumersecret;
		CALLBACK_URL = callbackurl;
	}
    
    public TwitterDialog(Context context, String consumerkey, String consumersecret, String callbackurl, TwitterLoginCallback listener, boolean usecallback) {
		super(context);
		CONSUMER_KEY = consumerkey;
		CONSUMER_SECRET = consumersecret;
		CALLBACK_URL = callbackurl;
		callback = listener;
		this.usecallback = usecallback;
	}
    
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		mHandler = new Handler();
		
		webView = new WebView(getContext());

		mProgressDialog = new ProgressDialog(getContext());
		mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mProgressDialog.setMessage("Loading please wait...");
		mProgressDialog.setCancelable(false);
		mProgressDialog.setCanceledOnTouchOutside(false);
		
		setCanceledOnTouchOutside(false);

		mContent = new LinearLayout(getContext());
		mContent.setOrientation(LinearLayout.VERTICAL);

		Display display = getWindow().getWindowManager().getDefaultDisplay();
		final float scale = getContext().getResources().getDisplayMetrics().density;
		float[] dimensions = display.getWidth() < display.getHeight() ? DIMENSIONS_PORTRAIT : DIMENSIONS_LANDSCAPE;
		addContentView(mContent, new FrameLayout.LayoutParams((int) (dimensions[0] * scale + 0.5f), (int) (dimensions[1] * scale + 0.5f)));
		
		mContent.addView(webView, FILL);
		
		askOAuth();
	}
    
    public void setUpWebView(String url) {
    	Log.e(TAG, "URL TO LOAD: " + url);
    	
    	webView.setWebViewClient(new MyWebViewClient());
    	webView.setVerticalScrollBarEnabled(false);
    	webView.setHorizontalScrollBarEnabled(false);
    	webView.loadUrl(url);
    	
//        mContent.addView(webView);
        
    }
    
    private void askOAuth() {
		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.setOAuthConsumerKey(CONSUMER_KEY);
		configurationBuilder.setOAuthConsumerSecret(CONSUMER_SECRET);
		Configuration configuration = configurationBuilder.build();
		
		mTwitter = new TwitterFactory(configuration).getInstance();
		
//		new Task().execute();
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					requestToken = mTwitter.getOAuthRequestToken(CALLBACK_URL);
					
					final String url = requestToken.getAuthenticationURL();
					mHandler.post(new Runnable() {
						
						@Override
						public void run() {
							setUpWebView(url);
						}
					});
				} catch (TwitterException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
    
    public void getAccessToken(final String url) {
    	new GetAccessToken().execute(url);
    }
    
    class GetAccessToken extends AsyncTask<String, Void, Object[]> {
    	
		private Object[] objects;
		
		@Override
		protected Object[] doInBackground(String... params) {
			objects = new Object[2];
			try {
				Uri uri = Uri.parse(params[0]);
    			String verifier = uri.getQueryParameter(oauth.signpost.OAuth.OAUTH_VERIFIER);
    			
				AccessToken accessToken = mTwitter.getOAuthAccessToken(requestToken, verifier);
				Log.e(TAG, "TOKEN: " + accessToken.getToken());
				Log.e(TAG, "TOKEN SECRET: " + accessToken.getTokenSecret());
				Log.e(TAG, "USING CALLBACK INTERFACE: " + usecallback);
				
				objects[0] = mTwitter.showUser(accessToken.getScreenName());
				objects[1] = accessToken;
				
			} catch (TwitterException e) {
				e.printStackTrace();
			}
			return objects;
		}
		
		@Override
		protected void onPostExecute(Object[] result) {
			super.onPostExecute(result);
			if(usecallback && result != null) {
				if(callback != null) {
					((TwitterLoginCallback)callback).onAuthorizationSuccess(result);
				}
			}
			dismiss();
		}
    }
    
    public class MyWebViewClient extends WebViewClient {
    	@Override
    	public boolean shouldOverrideUrlLoading(WebView view, String url) {
    		Log.e(TAG, "SHOULD OVERRIDE URL LOADING: " + url);
    		if(url != null && url.startsWith(CALLBACK_URL)) {
    			Log.e(TAG, "1");
    			getAccessToken(url);
    			return true;
    		}
    		return false;
    	}
    	
    	@Override
    	public void onPageStarted(WebView view, String url, Bitmap favicon) {
    		Log.e(TAG, "ON PAGE STARTED: " + url);
    		if(mProgressDialog != null && !mProgressDialog.isShowing()) {
    			mProgressDialog.show();
    		}
    		
    		super.onPageStarted(view, url, favicon);
    	}
    	
    	@Override
    	public void onPageFinished(WebView view, String url) {
    		Log.e(TAG, "ON PAGE FINISHED: " + url);
    		
    		if(mProgressDialog != null && mProgressDialog.isShowing()) {
				mProgressDialog.dismiss();
			}
    		
    		super.onPageFinished(view, url);
    	}
    }
    
    /*class Task extends AsyncTask<String, Void, String> {
    	String x = "";
    	
    	@Override
    	protected void onPreExecute() {
    		super.onPreExecute();
    	}

		@Override
		protected String doInBackground(String... params) {
			try {
				requestToken = mTwitter.getOAuthRequestToken(CALLBACK_URL);
				return x = requestToken.getAuthenticationURL();
			} catch (TwitterException e) {
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
//			webView.loadUrl(result);
		}
    }*/
}