package com.rakista.radio;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.rakista.radio.internal.CONSTANTS;
import com.rakista.radio.utility.Preferences;
import com.rakista.radio.utility.Utility;

public class NowPlaying extends Fragment implements OnClickListener {

	private ImageView RadioLogo;
	private ImageView quote_open;
	private ImageView quote_close;
	//	private Animation animation1;
	//	private Animation animation2;

	private FrameLayout dedicationPopup;
	private TextView dedicationMessage;
	private TextView dedicationName;

//	private GoogleAnalytics mGaIntance;
//	private Tracker tracker;
//	private Utility util;
//	private ToastHelper toast;
//	private Bitmap bitmap;
	String[] temp;

//	private File myDir;
//	private File root;

	private int[] xy; 
	//private String[] files;
//	private boolean isDedicated = false;
	private String messageHolder;
	private String nameHolder;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.home_layout, container, false);
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

//		util = new Utility(getActivity().getApplicationContext());
//		toast = new ToastHelper(getActivity().getApplicationContext(), Toast.LENGTH_LONG);

//		root = Environment.getExternalStorageDirectory();
//		myDir = new File(root.getAbsolutePath() + "/RakistaRadio/");
		
		initializeStorage();
		
		if(!initializeStorage().exists()) {
			initializeStorage().mkdirs();
		}

		//animation1 = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.to_middle);
		//animation1.setAnimationListener(new anim1());

		//animation2 = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.from_middle);
		//animation2.setAnimationListener(new anim2());

		RadioLogo = (ImageView) getView().findViewById(R.id.home_layout_radiologo);
		RadioLogo.setImageResource(R.drawable.rakista_radio_logo);
		RadioLogo.setOnClickListener(this);

		quote_open = (ImageView) getView().findViewById(R.id.quote_open);
		quote_open.setImageResource(R.drawable.quote_open1);

		quote_close = (ImageView) getView().findViewById(R.id.quote_close);
		quote_close.setImageResource(R.drawable.quote_close1);

		dedicationPopup = (FrameLayout) getView().findViewById(R.id.dedicationPopup);
		dedicationPopup.setVisibility(View.GONE);
		dedicationName = (TextView) getView().findViewById(R.id.dedicationName);
		dedicationMessage = (TextView) getView().findViewById(R.id.dedicationMessage);
		
		if(Utility.isTablet(getActivity().getApplicationContext())) {
			dedicationName.setTextSize(18);
			dedicationMessage.setTextSize(20);
		}

		if (Preferences.getMediaIsPlaying()) {
			if (Preferences.getDisplayPhoto()) {
				if (Preferences.getMediaHasPicture()) { 
					Bitmap temp = null;
					try {
						temp = getDrawablefromStorage();
					} catch (FileNotFoundException e) {
						RadioLogo.setImageResource(R.drawable.rakista_radio_logo);
					}
					if(temp != null) {
						RadioLogo.setImageBitmap(temp);
					} else {
						RadioLogo.setImageResource(R.drawable.rakista_radio_logo);
					}
				} else {
					RadioLogo.setImageResource(R.drawable.rakista_radio_logo);
					dedicationPopup.setVisibility(View.VISIBLE);
				}
			} else {
				RadioLogo.setImageResource(R.drawable.rakista_radio_logo);
			}
		} else {
			RadioLogo.setImageResource(R.drawable.rakista_radio_logo);
			dedicationPopup.setVisibility(View.GONE);
		}

//		mGaIntance = GoogleAnalytics.getInstance(getActivity());
//		tracker = mGaIntance.getTracker("UA-148784-1");

//		bitmapDetails();
	}
	
	private File initializeStorage() {
		File DIR = new File(Environment.getExternalStorageDirectory() + "/RakistaRadio/");
		return DIR;
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		IntentFilter IF = new IntentFilter();
		IF.addAction(CONSTANTS.ALBUMART);
		getActivity().getBaseContext().registerReceiver(albumArtReceiver, IF);

		xy = bitmapDetails();
	}

	public void onResume() {
		super.onResume();
		if(!Preferences.getIsDedicated()) {
			dedicationPopup.setVisibility(View.GONE);
		} else {
			dedicationPopup.setVisibility(View.VISIBLE);
			dedicationName.setText("Requested by: " + nameHolder);
			dedicationMessage.setText(messageHolder);
		}
	}
	
	final Handler mHandler = new Handler();
	
	/*private void getArtistPhoto(final URI imageUrl) {
		Log.i(getClass().getSimpleName(), "Starting download of image in 3 seconds");
		try {
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					mHandler.post(downloadArtistPhoto(imageUrl));
				}
			});
			Thread.sleep(5000, 0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Log.i(getClass().getSimpleName(), "Download STARTED.");
	}*/
	
	final Runnable downloadArtistPhoto(final URI url) {
		Runnable runner = new Runnable() {
			@Override
			public void run() {
				try {
					new DownloadImage().execute(url.toURL().toString());
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
			}
		};
		
		return runner;
	}

	private BroadcastReceiver albumArtReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			
			Log.i(getClass().getCanonicalName(), "NowPlaying.class " + intent.getAction());
			temp = intent.getStringExtra("picture").split("/");

			//Delete previously downloaded albumart
			deletefoldercontent(initializeStorage());

			if (Utility.isConnected()) {
				if(Preferences.getDisplayPhoto()) {
					if (intent.getBooleanExtra("haspicture", Boolean.FALSE) == Boolean.TRUE) {
						try {
							URI url = new URI("http", "www.rakista.com", "/radio/images/pictures/" + temp[1], null);
							Log.i(getClass().getSimpleName(), "URL: " + url.toURL().toString());
							new DownloadImage().execute(url.toURL().toString());
//							getArtistPhoto(url);
						} catch (URISyntaxException e) {
							e.printStackTrace();
						} catch (MalformedURLException e) {
							e.printStackTrace();
						}
					} else {
						RadioLogo.setImageResource(R.drawable.rakista_radio_logo);
					}
				}
			} else {
				//toast.show(getString(R.string.error), getString(R.string.connection_error_message));
			}
			
			/*
			 * FOR DEDICATION POPUP
			 */
			if(intent.getBooleanExtra("isDedicated", false)) {
				nameHolder = intent.getStringExtra("name");
				messageHolder = intent.getStringExtra("message");
				
				if(messageHolder == "null" || nameHolder == " ") {
					dedicationPopup.setVisibility(View.GONE);
					Preferences.setIsDedicated(true);
				} else if (messageHolder == " " || nameHolder == "null") {
					dedicationPopup.setVisibility(View.GONE);
					Preferences.setIsDedicated(true);
				} else if (messageHolder != " " || nameHolder != "null") {
					dedicationPopup.setVisibility(View.VISIBLE);
					dedicationName.setText("Requested by: " + nameHolder);
					dedicationMessage.setText(messageHolder);
					Preferences.setIsDedicated(true);
				}
			} else {
				dedicationPopup.setVisibility(View.GONE);
				Preferences.setIsDedicated(false);
			}
		}
	};

	private boolean downloadisfinished = false;

	public static Bitmap resize(float newWidth, float newHeight, Bitmap bdImage) {

		Bitmap bitmapOrig = bdImage;
		float scaleWidth = ((float) newWidth) / bitmapOrig.getWidth();
		float scaleHeight = ((float) newHeight) / bitmapOrig.getHeight();
		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);
		Bitmap resizedBitmap = Bitmap.createBitmap(bitmapOrig, 0, 0, bitmapOrig.getWidth(), bitmapOrig.getHeight(), matrix, true);
		//BitmapDrawable bitmapDrawableResized = new BitmapDrawable(resizedBitmap);
		return resizedBitmap;
	}

	public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
		Bitmap resizedBitmap;
		int width;
		int height;
		float scaleWidth;
		float scaleHeight;
		
		if(bm != null) {
			width = bm.getWidth();
			height = bm.getHeight();
			scaleWidth = ((float) newWidth) / width;
			scaleHeight = ((float) newHeight) / height;

			// CREATE A MATRIX FOR THE MANIPULATION
			Matrix matrix = new Matrix();

			// RESIZE THE BIT MAP
			matrix.postScale(scaleWidth, scaleHeight);

			// RECREATE THE NEW BITMAP
			resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
		} else {
			resizedBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.rakista_radio_logo); 
		}
		
		//Bitmap.createScaledBitmap(bm, width, height, false);

		return resizedBitmap;

	}

	public int[] bitmapDetails() {
		float level = Utility.getScreenDensity(getActivity().getApplicationContext());
		int[] HW = new int[2];

		if(level == CONSTANTS.LOW_LEVEL) {
			HW[0] = 120;
			HW[1] = 120;

		} else if(level == CONSTANTS.MEDIUM_LEVEL) {
			if(Utility.getScreenWidth(getActivity().getApplicationContext()) == 600) {
				HW[0] = 300;
				HW[1] = 300;
			} else {
				HW[0] = 472;
				HW[1] = 472;
			}
		} else if(level == CONSTANTS.HIGH_LEVEL) {
			HW[0] = 380;
			HW[1] = 380;
		} else if(level == CONSTANTS.EXTRA_HIGH_LEVEL) {
			HW[0] = 664;
			HW[1] = 664;
		} else {
			HW[0] = 472;
			HW[1] = 472;
		}

		return HW;
	}

	/*public void saveImagetoExternalStorage(Bitmap bitmapsrc) {

		//get path to external storage (SD card)
		String iconsStoragePath = Environment.getExternalStorageDirectory() + "/RakistaRadio/";
		File sdIconStorageDir = new File(iconsStoragePath);

		Log.i("SAVE TO SD", iconsStoragePath);
		//create storage directories, if they don't exist
		if(!sdIconStorageDir.exists()) {
			sdIconStorageDir.mkdirs();
		}

		try {
			String filePath = sdIconStorageDir.toString() + "art.jpg";
			FileOutputStream fileOutputStream = new FileOutputStream(filePath);

			BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

			//choose another format if PNG doesn't suit you
			bitmapsrc.compress(CompressFormat.PNG, 100, bos);

			bos.flush();
			bos.close();

		} catch (FileNotFoundException e) {
			Log.w("TAG", "Error saving image file: " + e.getMessage());
		} catch (IOException e) {
			Log.w("TAG", "Error saving image file: " + e.getMessage());
		}

	}*/

	private void deletefoldercontent(File f) {
		
		if(f.exists()) {
			if(f.isDirectory()) { //f is a Directory
				for(File x : f.listFiles()) {
					x.delete();
				}
			} else {
				if(f.list().length != 0) {
					for(File x : f.listFiles()) {
						x.delete();
					}
				}
			}
		}
	}

	/*class anim1 implements AnimationListener {

		@Override
		public void onAnimationEnd(Animation animation) { 
			RadioLogo.startAnimation(animation2);
		}

		@Override
		public void onAnimationRepeat(Animation animation) {}

		@Override
		public void onAnimationStart(Animation animation) {}

	}

	class anim2 implements AnimationListener {

		@Override
		public void onAnimationEnd(Animation animation) {
			if(Preferences.getMediaIsPlaying()) {
				if(Preferences.getMediaHasPicture()) {
					RadioLogo.setImageBitmap(getimagefromstorage());
				} else {
					RadioLogo.setImageResource(R.drawable.rakista_radio_logo);
				}
			} else {
				RadioLogo.setImageResource(R.drawable.rakista_radio_logo);
			}
		}

		@Override	
		public void onAnimationRepeat(Animation animation) {}

		@Override
		public void onAnimationStart(Animation animation) {}

	}*/

	/*Bitmap getimagefromstorage() {
	Bitmap bmp;
	Log.i("GET IMAGE FROM STORAGE", Integer.valueOf(myDir.listFiles().length).toString());
	if(myDir.listFiles().length == 0) {
		bmp = BitmapFactory.decodeResource(getResources(), R.drawable.rakista_radio_logo);
	} else {
		File path = Environment.getExternalStorageDirectory();
		Log.i("GET IMAGE FROM STORAGE", "PATH: " + path + "/RakistaRadio/" + Array.get(myDir.list(), 0).toString());
		Log.i("GET IMAGE FROM STORAGE", "DIR LIST: " +  Array.get(myDir.list(), 0).toString());
		if(downloadisfinished) {
			bmp = getResizedBitmap(BitmapFactory.decodeFile(path + "/RakistaRadio/art.jpg"), xy[0], xy[1]);
		} else {
			bmp = BitmapFactory.decodeResource(getResources(), R.drawable.rakista_radio_logo);
		}
	}
	return bmp;
}*/

	Bitmap getDrawablefromStorage() throws FileNotFoundException {
		Bitmap bmp;
//		Bitmap temp = null;
		File path = Environment.getExternalStorageDirectory();
		
			try {
				bmp = getResizedBitmap(BitmapFactory.decodeFile(path + "/RakistaRadio/art.jpg"), xy[0], xy[1]);
			} catch (NullPointerException e) {
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.rakista_radio_logo);
			}

		return bmp;
	}

	/**
	 * Simple function to set a Drawable to the image View
	 * @param drawable
	 */
	private void setImage(Bitmap drawable) {
		if(downloadisfinished) {
			RadioLogo.setImageBitmap(getResizedBitmap(drawable, xy[1], xy[0]));
		}
	}

	public class DownloadImage extends AsyncTask<String, Integer, Bitmap> {
		Bitmap temp = null;
		@Override
		protected Bitmap doInBackground(final String... arg0) {
			
			// This is done in a background thread
			return downloadImage(arg0[0]);
		}

		/**
		 * Called after the image has been downloaded
		 * -> this calls a function on the main thread again
		 */
		protected void onPostExecute(Bitmap image) {
			setImage(image);
			Log.i("ARTIST PHOTO DOWNLOADER", "DOWNLOAD FINISHED");
		}

		/**
		 * Actually download the Image from the _url
		 * @param _url
		 * @return Bitmap.
		 */
		private Bitmap downloadImage(String _url) {
			//Prepare to download image
			URL url;
//			BufferedOutputStream out;
			InputStream in;
			BufferedInputStream buf;

			//BufferedInputStream buf;
			try {
				url = new URL(_url);
				in = url.openStream();

				/*
				 * THIS IS NOT NEEDED
				 * 
				 * YOU TRY TO CREATE AN ACTUAL IMAGE HERE, BY WRITING
				 * TO A NEW FILE
				 * YOU ONLY NEED TO READ THE INPUTSTREAM 
				 * AND CONVERT THAT TO A BITMAP
	            out = new BufferedOutputStream(new FileOutputStream("testImage.jpg"));
	            int i;

	             while ((i = in.read()) != -1) {
	                 out.write(i);
	             }
	             out.close();
	             in.close();
				 */

				// Read the inputstream 
				buf = new BufferedInputStream(in);

				// Convert the BufferedInputStream to a Bitmap
				Bitmap bMap = BitmapFactory.decodeStream(buf);
				if (in != null) {
					in.close();
				}

				if (buf != null) {
					buf.close();
				}

				downloadisfinished = true;
				saveImagetoStorage(bMap);
				
				return bMap;

			} catch (Exception e) {
				Log.e("Error reading file", e.toString());
				downloadisfinished = false;
			}
			return null;
		}
	}

	public void saveImagetoStorage(Bitmap imagetosave) {
		try {
			File storagePath = Environment.getExternalStorageDirectory();
			FileOutputStream output = new FileOutputStream(new File(storagePath + "/RakistaRadio/", "art.jpg"));

			BufferedOutputStream bos = new BufferedOutputStream(output);

			imagetosave.compress(CompressFormat.JPEG, 100, bos);

			bos.flush();
			bos.close();

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
	}

	/*private class DowdLoadImage extends AsyncTask<String, Void, Integer> {
		InputStream input;


		protected void onPreExecute() {
			Log.i(getClass().getSimpleName(), "Downloading Album Art");
		}

		protected void onPostExecute(Integer i) {
			Log.i(getClass().getSimpleName(), "Album Art Download Finished");
			RadioLogo.setImageBitmap(getimagefromstorage());
		}

		@Override
		protected Integer doInBackground(String... params) {
			deletefoldercontent(myDir);
			Log.i(getClass().getSimpleName(), "Saving Image to SDCARD");
			try {
				URL url = new URL(params[0]);
				input = url.openStream();

				// The sdcard directory e.g. '/sdcard' can be used directly, or
				// more safely abstracted with getExternalStorageDirectory()
				File storagePath = Environment.getExternalStorageDirectory();
				OutputStream output = new FileOutputStream(new File(storagePath + "/RakistaRadio/", "art.jpg"));

				try {
					byte[] buffer = new byte[1024];
					int bytesRead = 0;
					while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
						output.write(buffer, 0, bytesRead);
					}
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					output.close();
				}

				input.close();

			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
				downloadisfinished = false;
			} catch (IOException e1) {
				downloadisfinished = false;
				e1.printStackTrace();
			}

			Log.i(getClass().getSimpleName(), "Saving Image to SDCARD Complete");

			downloadisfinished = true;

			return 0;
		}

	}*/
}
