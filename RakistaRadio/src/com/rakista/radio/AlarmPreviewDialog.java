package com.rakista.radio;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AlarmPreviewDialog extends Dialog {

	private Context mContext;
	
	static final float[] DIMENSIONS_LANDSCAPE = { 460, 260 };
	static final float[] DIMENSIONS_PORTRAIT = { 280, 420 };
	
	static final FrameLayout.LayoutParams WRAP_CONTENT = new FrameLayout.LayoutParams(
			ViewGroup.LayoutParams.WRAP_CONTENT,
			ViewGroup.LayoutParams.WRAP_CONTENT);
	
	static final FrameLayout.LayoutParams FILL_PARENT = new FrameLayout.LayoutParams(
			ViewGroup.LayoutParams.MATCH_PARENT,
			ViewGroup.LayoutParams.MATCH_PARENT);
	
	static final int MARGIN = 4;
	static final int PADDING = 2;
	
	public TextView mTitle, message;
	
	private LinearLayout mContent;
	
	public AlarmPreviewDialog(Context context) {
		super(context);
		mContext = context;
	}
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setupTitle();
		setupContent();
	}
	
	private void setupTitle() {
		//Drawable icon = getContext().getResources().getDrawable(R.drawable.ic_alarm);
		mTitle = new TextView(getContext());
        mTitle.setText("Setup Alarm");
        mTitle.setTextColor(Color.WHITE);
        mTitle.setTypeface(Typeface.DEFAULT_BOLD);
        mTitle.setBackgroundColor(Color.DKGRAY);
        mTitle.setPadding(MARGIN, MARGIN + MARGIN, MARGIN, MARGIN + MARGIN);
        //mTitle.setCompoundDrawablePadding(MARGIN + PADDING);
        mTitle.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        mTitle.setBackgroundColor(Color.BLACK);
        //mTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        mContent.addView(mTitle);
	}
	
	private void setupContent() {
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.alarm_layout, null);
		
		message = (TextView) view.findViewById(R.id.alarm_prev_message);
		
		
		mContent.addView(view);
	}
	
}
