package com.rakista.radio;

//import java.io.Serializable;
//import java.util.Calendar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.rakista.radio.internal.ConstantCommand;
import com.rakista.radio.internal.IMediaPlayerServiceClient;
import com.rakista.radio.internal.StatefulMediaPlayer.MPStates;
import com.rakista.radio.service.MediaPlayerService;
import com.rakista.radio.service.MediaPlayerService.MediaPlayerBinder;
import com.rakista.radio.utility.Preferences;
import com.rakista.radio.utility.Utility;

public class RadioAlarmManagerReceiver extends BroadcastReceiver {
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Preferences.Load(context);
		RadioNotification.setContext(context);
		
		if(intent.getAction().equals(ConstantCommand.START_SLEEP)) {
			if (Utility.isMainActivityRunning(context) == true) {
				MediaPlayerBinder binder = (MediaPlayerBinder) peekService(context, new Intent(context, MediaPlayerService.class));
				IMediaPlayerServiceClient client = binder.getService().getClient();
				MediaPlayerService service = binder.getService();

				if (service.getMediaPlayer().getState() == MPStates.STARTED) {
					Log.i(getClass().getSimpleName(),"Sleep Receive Stopping Player");
					Preferences.setSleepTimeIndex(0);
					service.stopMediaPlayer();
					client.onStop(true);
				}
			}
		}
		
		if (intent.getAction().equals(ConstantCommand.START_ALARM)) {
			Log.i(getClass().getSimpleName(), "START ALARM RECEIVE");
			Intent i = new Intent();
			
			Log.i(getClass().getSimpleName(), "DISABLE ALARM: " + Boolean.valueOf(intent.getBooleanExtra("disableAlarm", false)).toString());
			
			if (intent.getBooleanExtra("disableAlarm", false) == true) {
				Preferences.setSetupAlarmIsEnabled(false);
			}
			
			if (com.rakista.radio.utility.Utility.isMainActivityRunning(context)) { // when activity is closed or not running.
				
				Log.i(getClass().getSimpleName(), "Main Activity is running sending broadcast");
				i.putExtra("ALARM", true);
				i.setAction("com.rakista.radio.dev.ALARM");
				context.sendBroadcast(i);
				
				Log.i(getClass().getSimpleName(), "Alarm Receive for: " + intent.getStringExtra("calendar"));
				Log.i(getClass().getSimpleName(), "Starting Rakista Radio");
				
			} else { // when activity is running.
				
				Log.i(getClass().getSimpleName(), "Main Activity is not running starting activity");
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				i.putExtra("ALARM", true);
				context.startActivity(i);
				 
				Log.i(getClass().getSimpleName(), "Alarm Receive for: " + intent.getStringExtra("calendar"));
				Log.i(getClass().getSimpleName(), "Starting Rakista Radio");
			}
			
			RadioNotification.showAlarmNotification();
		}
		
		if(intent.getAction().equals(ConstantCommand.STOP_ALARM)) {
			
		}
	}
}
