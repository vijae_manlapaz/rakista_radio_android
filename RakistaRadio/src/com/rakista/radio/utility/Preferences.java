package com.rakista.radio.utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

/**
 * 
 * @author Vijae Manlapaz
 *
 */
@SuppressLint({ "CommitPrefEdits", "SimpleDateFormat" })
public class Preferences {
	private static SharedPreferences settings;
	private static Editor editor;
	
	private static String PREF_FILENAME = "RadioPreferences";
	
	private static String KEY_SELECTEDSTREAM = "SELECTEDSTREAM";
	private static String KEY_SELECTEDNOTIFICATION = "SELECTEDNOTIFICATION";
	private static String KEY_SELECTEDDISPLAYPHOTO = "SELECTEDDISPLAYPHOTO";
	private static String KEY_ISDEDICATED = "IsDedicated";
	private static String KEY_FBUSERNAME = "FacebookUsername";
	private static String KEY_FBNAME = "FacebookName";
	private static String KEY_FBEMAIL = "FacebookEmail";
	private static String KEY_FB_ACCESS_TOKEN = "access_token";
	private static String KEY_FB_ACCESS_ESPIRES = "access_expires";
	
	private static String KEY_TOPVOTE_BUTTON = "topvotebutton_state";
	private static String KEY_TOPREQUESTED_BUTTON = "toprequestedbutton_state";
	
	private static String KEY_TOPVOTELASTUPDATE = "topvoteLastUpdate";
	private static String KEY_TOPREQUESTLASTUPDATE = "toprequestLastUpdate";
	private static String KEY_HISTORYLASTUPDATE = "historyLastUpdate";
	private static String KEY_DEDICATIONLASTUPDATE = "dedicationLastUpdate";
	private static String KEY_PLAYLISTLASTUPDATE = "playlistLastUpdate";
	
	private static String KEY_SLEEPTIME = "sleeptime";
	private static String KEY_ONRESUME = "resume";
	private static String KEY_SELECTED_DAY = "selectedDay";
	private static String KEY_ALARMTIME = "alarmtime";
	private static String KEY_ALARMTIME1 = "alarmtime1";
	
	private static String KEY_SETUPALARM = "setupalarm";
	
	private static String KEY_GCM_ISREGISTERED = "gcmIsRegistered";
	private static String KEY_GCM_EXPIRATIONTIME = "gcmExpirationTime";
	private static String KEY_GCM_EXPIRATIONTIMEONSERVER = "gcmExpirationTimeOnServer";
	private static String KEY_GCM_CURRENT_REGISTRATIONID = "gcmCurrentRegistrationID";
	private static String KEY_TIMEALARM = "timesetalarm";
	
	private static String KEY_DATEINSTANCE = "alarmDateInstance";
	
	public static String KEY_REPEATINGALARMS_DISABLEALARM = "disablealarm";
	public static String KEY_REPEATINGALARMS_ALARMTIME = "alarmtime";
	public static String KEY_REPEATINGALARMS_ALARMTIMESTRING = "alarmtimestring";
	public static String KEY_REPEATINGALARMS_POSITION = "position";
	public static String KEY_REPEATINGALARMS_SIZE = "alarmsize";
	public static String KEY_ALARMISCANCELLED = "isCancelled";
	
	public static String KEY_MEDIAPLAYER_PLAYING = "mediaplayerisplaying";
	
	public static String KEY_DEVICEID = "device_id";
	
	public static void Load(Context context) {
		settings = context.getSharedPreferences(PREF_FILENAME, Context.MODE_PRIVATE);
		editor = settings.edit();
	}
	
	public static void setMediaIsPlaying(boolean isplaying) {
		editor.putBoolean(KEY_MEDIAPLAYER_PLAYING, isplaying);
		editor.commit();
	}
	
	private static String KEY_MEDIAHASPICTURE = "mediahaspicture";
	public static boolean getMediaIsPlaying() {
		return settings.getBoolean(KEY_MEDIAPLAYER_PLAYING, false);
	}
	
	public static void setMediaHasPicture(boolean hasPicture) {
		editor.putBoolean(KEY_MEDIAHASPICTURE, hasPicture);
		editor.commit();
	}
	
	public static boolean getMediaHasPicture() {
		return settings.getBoolean(KEY_MEDIAHASPICTURE, Boolean.FALSE);
	}
	public static void saveRepeatingAlarm(int position, boolean disableAlarm, long timeinmillis, String datetimeinstring, boolean isCancelled) {
		editor.putBoolean(KEY_REPEATINGALARMS_DISABLEALARM + position, disableAlarm);
		editor.putLong(KEY_REPEATINGALARMS_ALARMTIME + position, timeinmillis);
		editor.putString(KEY_REPEATINGALARMS_ALARMTIMESTRING + position, datetimeinstring);
		editor.putBoolean(KEY_ALARMISCANCELLED + position, isCancelled);
		editor.commit();
		Log.i("Rakista Radio Preferences", "Saved Repeating Alarms: " + "Position: " + Integer.valueOf(position).toString() +"\nTime in Millis: " + Long.toString(timeinmillis).toString() + "\nDisable Alarm: " + Boolean.toString(disableAlarm).toString());
	}
	
	public static ArrayList<HashMap<String, Object>> getSavedRepeatingAlarm(int position) {
		ArrayList<HashMap<String, Object>> tempArrayList = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("disablealarm", settings.getBoolean(KEY_REPEATINGALARMS_DISABLEALARM + position, Boolean.FALSE));
		map.put("timeinmillis" + position, settings.getLong(KEY_REPEATINGALARMS_ALARMTIME + position, 0));
		map.put("datetimeinstring", settings.getString(KEY_REPEATINGALARMS_ALARMTIMESTRING + position, ""));
		map.put("iscancelled", settings.getBoolean(KEY_ALARMISCANCELLED + position, true))	;	
		tempArrayList.add(map);
		Log.i("Rakista Radio Preferences", "Get Saved Repeating Alarms: " + tempArrayList.toString());
		return tempArrayList;
	}
	
	public static void setTimeAlarm(String timeset) {
		editor.putString(KEY_TIMEALARM, timeset);
		editor.commit();
	}
	
	public static String getTimeAlarm() {
		return settings.getString(KEY_TIMEALARM, null);
	}
	
	public static void setAlarmTime(String alarmTime) {
		editor.putString(KEY_ALARMTIME, alarmTime);
		editor.commit();
	}
	
	public static void setAlarmDateInstance(int hour, int minute) {
		editor.putInt(KEY_DATEINSTANCE + "_Hours", hour);
		editor.putInt(KEY_DATEINSTANCE + "_Minute", minute);
		editor.commit();
	}
	
	public static int[] getAlarmDateInstance() {
		int[] instance = new int[2];
		instance[0] = settings.getInt(KEY_DATEINSTANCE + "_Hours", 0);
		instance[1] = settings.getInt(KEY_DATEINSTANCE + "_Minute", 0);
		return instance;
	}
	
	public static String getAlarmTime() {
		DateFormat df = new SimpleDateFormat("hh:mm a");
		Date date;
		Calendar cal = Calendar.getInstance(TimeZone.getDefault());
		date = cal.getTime();
		return settings.getString(KEY_ALARMTIME, df.format(date.getTime()).toString());
	}
	
	public static void setgcmCurrentRegistrationId(String regId) {
		editor.putString(KEY_GCM_CURRENT_REGISTRATIONID, regId);
		editor.commit();
	}
	
	public static String getgcmCurrentRegistrationId() {
		return settings.getString(KEY_GCM_CURRENT_REGISTRATIONID, "");
	}
	
	public static void setgcmExpirationOnServer(long exp_time) {
		editor.putLong(KEY_GCM_EXPIRATIONTIMEONSERVER, exp_time);
		editor.commit();
	}
	
	public static long getgcmExpirationTimeOnServer() {
		return settings.getLong(KEY_GCM_EXPIRATIONTIMEONSERVER, -1);
	}
	
	public static void setgcmExpirationTime(long exp_time) {
		editor.putLong(KEY_GCM_EXPIRATIONTIME, exp_time);
		editor.commit();
	}
	
	public static long getgcmExpirationTime() {
		return settings.getLong(KEY_GCM_EXPIRATIONTIME, -1);
	}
	
	public static void setgcmEnabled(boolean isEnabled) {
		editor.putBoolean(KEY_GCM_ISREGISTERED, isEnabled);
		editor.commit();
	}
	
	public static boolean getgcmEnabled() {
		return settings.getBoolean(KEY_GCM_ISREGISTERED, true);
	}
	
	public static void setAlarmTime1(int[] time) {
		
		for(int y =0; y < time.length; y++) {
			editor.putInt(KEY_ALARMTIME1 + "_position" + y, time[y]);
		}
		
		editor.putInt(KEY_ALARMTIME1 + "_size", time.length);
		editor.commit();
	}
	
	public static int[] getAlarmTime1() {
		//int timesize = settings.getInt(KEY_ALARMTIME1, 0);
		
		int[] temp = new int[2];
		
		for(int y = 0; y < temp.length; y++) {
			temp[y] = settings.getInt(KEY_ALARMTIME1 + "_position" + y, 0);
		}
		
		return temp;
	}
	
	public static void setSelectedDay(boolean[] item) {
		
		for(int i = 0; i < item.length; i++) {
			editor.putBoolean(KEY_SELECTED_DAY + "_position" + i, item[i]);
		}
		
		editor.putInt(KEY_SELECTED_DAY + "_size", item.length);
		editor.commit();
	}
	
	public static boolean[] getSelectedDay() {
		//int size = settings.getInt(KEY_SELECTED_DAY + "_size", 0);
		boolean[] temp = new boolean[7];
		
		for(int y = 0; y < temp.length; y++) {
			temp[y] = settings.getBoolean(KEY_SELECTED_DAY + "_position" + y, false);
		}
		
		return temp;
	}
	
	public static void setSetupAlarmIsEnabled(boolean value) {
		editor.putBoolean(KEY_SETUPALARM, value);
		editor.commit();
	}
	
	public static boolean getSetupAlarmIsEnabled() {
		return settings.getBoolean(KEY_SETUPALARM, false);
	}
	
	public static void setSleepTimeIndex(int index) {
		editor.putInt(KEY_SLEEPTIME, index);
		editor.commit();
	}
	
	public static int getSleepTimeIndex() {
		return settings.getInt(KEY_SLEEPTIME, 0);
	}
	
	public static void setPlaylistLastUpdate(String lastupdate) {
		editor.putString(KEY_PLAYLISTLASTUPDATE, lastupdate);
		editor.commit();
	}
	
	public static String getPlaylistLastUpdate() {
		return settings.getString(KEY_PLAYLISTLASTUPDATE, null);
	}
	
	public static void setApplicationIsOnResume(boolean isresume) {
		editor.putBoolean(KEY_ONRESUME, isresume);
		editor.commit();
	}
	
	public static boolean getApplicationIsOnResume() {
		return settings.getBoolean(KEY_ONRESUME, false);
	}
	
	public static boolean getVoteButtonState() {
		return settings.getBoolean(KEY_TOPVOTE_BUTTON, true);
	}
	
	public static void setVoteButtonState(boolean state) {
		editor.putBoolean(KEY_TOPVOTE_BUTTON, state);
		editor.commit();
	}
	
	public static void setTopVotedlastUpdate(String lastupdate) {
		editor.putString(KEY_TOPVOTELASTUPDATE, lastupdate);
		editor.commit();
	}
	
	public static String getTopVoteLastUpdate() {
		return settings.getString(KEY_TOPVOTELASTUPDATE, null);
	}
	
	public static void setTopRequestLastUpdate(String lastupdate) {
		editor.putString(KEY_TOPREQUESTLASTUPDATE, lastupdate);
		editor.commit();
	}
	
	public static String getTopRequestLastUpdate() {
		return settings.getString(KEY_TOPREQUESTLASTUPDATE, null);
	}
	
	public static boolean getRequestedButtonState() {
		return settings.getBoolean(KEY_TOPREQUESTED_BUTTON, false);
	}
	
	public static void setHistoryLastUpdate(String lastupdate) {
		editor.putString(KEY_HISTORYLASTUPDATE, lastupdate);
		editor.commit();
	}
	
	public static String getHistoryLastUpdate() {
		return settings.getString(KEY_HISTORYLASTUPDATE, null);
	}
	
	public static void setDedicationLastUpdate(String lastupdate) {
		editor.putString(KEY_DEDICATIONLASTUPDATE, lastupdate);
		editor.commit();
	}
	
	public static String getDedicationLastUpdate() {
		return settings.getString(KEY_DEDICATIONLASTUPDATE, null);
	}
	
	public static void setRequestedButtonState(boolean state) {
		editor.putBoolean(KEY_TOPREQUESTED_BUTTON, state);
		editor.commit();
	}
	
	/**
	 * Gets the current logged in user email.
	 * which was set by <strong>setFacebookEmail</strong>
	 * @return the KEY_FBEMAIL
	 * 
	 * @author Vijae Manlapaz
	 */
	public static String getFacebookEmail() {
		return settings.getString(KEY_FBEMAIL, null);
	}
	
	/**
	 * Sets the logged in user email.
	 * @param kEY_FBEMAIL the kEY_FBEMAIL to set
	 * 
	 * @author Vijae Manlapaz
	 */
	public static void setFacebookEmail(String email) {
		editor.putString(KEY_FBEMAIL, email);
		editor.commit();
	}
	
	/**
	 * Sets the name of the current logged in user.
	 * @param username name of the user returned by facebook.
	 *
	 * @author Vijae Manlapaz
	 */
	public static void setFacebookUserName(String username) {
		editor.putString(KEY_FBUSERNAME, username);
		editor.commit();
	}
	
	/**
	 * Gets the name of the current logged in user.
	 * @return the name of the user which was sets by <strong>setFacebookUserName</strong>
	 * 
	 * @author Vijae Manlapaz
	 */
	public static String getfacebookUserName() {
		return settings.getString(KEY_FBUSERNAME, null);
	}
	
	public static void setFacebookName(String name) {
		editor.putString(KEY_FBNAME, name);
		editor.commit();
	}
	
	public static String getFacebookName() {
		return settings.getString(KEY_FBNAME, null);
	}
	
	public static void setAccessToken(String accessToken) {
		editor.putString(KEY_FB_ACCESS_TOKEN, accessToken);
		editor.commit();
	}
	
	public static String getAccessToken() {
		return settings.getString(KEY_FB_ACCESS_TOKEN, null);
	}
	
	public static void setAccessExpires(long accessExpires) {
		editor.putLong(KEY_FB_ACCESS_ESPIRES, accessExpires);
		editor.commit();
	}
	
	public static long getAccessExpires() {
		return settings.getLong(KEY_FB_ACCESS_ESPIRES, 0);
	}

	/**
	 * 
	 * @return <b>Integer</b> value which was set by the <i><u>setStreamSelectedSetting</u></i>
	 * 
	 * @author Vijae Manlapaz
	 */
	public static int getStreamSelectedSetting() {
		return settings.getInt(KEY_SELECTEDSTREAM, 1);
	}
	
	/**
	 * Sets selected streaming quality
	 * @param <b>Integer</b> streamSelectedSetting
	 * 
	 * @author Vijae Manlapaz
	 */
	public static void setStreamSelectedSetting(int streamSelectedSetting) {
		editor.putInt(KEY_SELECTEDSTREAM, streamSelectedSetting);
		editor.commit();
	}
	
	/**
	 * Sets Notification State.
	 * @param <b>Boolean</b> enable
	 * 
	 * @author Vijae Manlapaz
	 */
	public static void setEnableNotification(boolean enable) {
		editor.putBoolean(KEY_SELECTEDNOTIFICATION, enable);
		editor.commit();
	}
	
	/**
	 * 
	 * @return <b>Boolean</b> value which was set by the <i><u>setEnableNotification</u></i>
	 * 
	 * @author Vijae Manlapaz
	 */
	public static boolean getNotification() {
		return settings.getBoolean(KEY_SELECTEDNOTIFICATION, true);
	}
	
	public static void setEnableDisplayPhoto(boolean enable){
		editor.putBoolean(KEY_SELECTEDDISPLAYPHOTO, enable);
		editor.commit();
	}
	
	public static boolean getDisplayPhoto(){
		return settings.getBoolean(KEY_SELECTEDDISPLAYPHOTO, true);
	}

	public static void setIsDedicated(boolean value) {
		editor.putBoolean(KEY_ISDEDICATED, value);
		editor.commit();
	}
	
	public static boolean getIsDedicated() {
		return settings.getBoolean(KEY_ISDEDICATED, false);
	}

	public static void setDeviceID(String string) {
		editor.putString(KEY_DEVICEID, string);
		editor.commit();
	}
	
	public static String getDeviceID() {
		return settings.getString(KEY_DEVICEID, "");
	}
}
