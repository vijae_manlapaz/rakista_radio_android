package com.rakista.radio.utility;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.util.Log;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.rakista.radio.RadioWebViewClient;
import com.rakista.radio.internal.Reachability;

public class Utility {

	private static Context mContext;
	static boolean Connected = false;

	public Utility(Context context) {
		mContext = context;
	}

	public static boolean checkNetworkStatus(Context context) {
		final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		if (wifi.isConnected() == true) {
			return wifi.isConnected();

		} else if (mobile.isConnected() == true) {
			return mobile.isConnected();
		}
		return false;
	}
	
	public static boolean isConnected() {
		return checkNetworkStatus(mContext);
	}
	
	public static boolean isReachable() {
		return Reachability.ping("http://www.rakista.com");
	}

	/** Determines if the MediaPlayerService is already running.
     * @return true if the service is running, false otherwise.
     * 
     * @author Vijae Manlapaz
     */
    public static boolean MediaPlayerServiceRunning(Context context) {
 
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
 
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("com.rakista.radio.service.MediaPlayerService".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Check if Main Activity is running
     * @param context
     * @return true if activity running false otherwise.
     * 
     * @author Vijae Manlapaz
     */
	public static boolean isMainActivityRunning(Context context) {
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> runningProcInfo = activityManager.getRunningAppProcesses();
		for (int i = 0; i < runningProcInfo.size(); i++) {
			if (runningProcInfo.get(i).processName.equals("com.rakista.radio")) {
				return true;
			}
		}
		return false;
	}
    
	public boolean isActivityRunning(Context context) {

		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> activities = activityManager.getRunningTasks(Integer.MAX_VALUE);
		for (int i = 0; i < activities.size(); i++) {
			if (activities.get(i).topActivity.toString().equalsIgnoreCase("ComponentInfo{com.rakista.radio/com.rakista.radio.RadioActivity}")) {
				return true;
			}
		}
		return false;
	}
	
	public static String convertTimeZones(final String fromDateTime) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		Date x = null;
		try {
			df.setTimeZone(TimeZone.getTimeZone("Asia/Manila"));
			x = df.parse(fromDateTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    final DateTimeZone fromTimeZone = DateTimeZone.forID("Asia/Manila");
	    final DateTimeZone toTimeZone = DateTimeZone.forID(TimeZone.getDefault().getID());
	    final DateTime dateTime = new DateTime(x, fromTimeZone);
	    
	    final DateTimeFormatter outputFormatter = DateTimeFormat.forPattern("MMM dd, yyyy hh:mm a").withZone(toTimeZone);
	    return outputFormatter.print(dateTime);
	}
	
	public static String convertServerTimeToLocaleTime(final String fromDateTime) {
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");
		Date x = null;
		try {
			df.setTimeZone(TimeZone.getTimeZone("Asia/Manila"));
			x = df.parse(fromDateTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    final DateTimeZone fromTimeZone = DateTimeZone.forID("Asia/Manila");
	    final DateTimeZone toTimeZone = DateTimeZone.forID(TimeZone.getDefault().getID());
	    final DateTime dateTime = new DateTime(x, fromTimeZone);
	    
	    final DateTimeFormatter outputFormatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm:ss a").withZone(toTimeZone);
//	    Log.i("To Timezone", outputFormatter.print(dateTime));
	    return outputFormatter.print(dateTime);
	}
	
	/*public static String convertStringtoTime(String value) {
		
		String convertedTime = null;
		DateFormat df = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Calendar cal = Calendar.getInstance();
		Date date = null;
		
		sdf.setTimeZone(TimeZone.getDefault());
		df.setTimeZone(TimeZone.getDefault());
		
		try {
			date = sdf.parse(value);
			//Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(value);
			//cal.setTime(date);
			//df.format(cal.getTime());
			//convertedTime = df.format(cal.getTime());
			
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		return df.format(date.toLocaleString());
				//convertedTime; 
	}*/
	
	public static long convertStringtimeToMillis(String value) {
		
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTimeZone(TimeZone.getDefault());
		
		try {
			Date date = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a").parse(value);
			cal.setTime(date);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		return cal.getTimeInMillis(); 
	}
	
	private static Account getAccount(AccountManager accountManager) {
	    Account[] accounts = accountManager.getAccountsByType("com.google");
	    Account account;
	    if (accounts.length > 0) {
	      account = accounts[0];
	    } else {
	      account = null;
	    }
	    return account;
	  }

	  public static String getEmail(Context context) {
	    AccountManager accountManager = AccountManager.get(context);
	    Account account = getAccount(accountManager);

	    if (account == null) {
	      return null;
	    } else {
	      return account.name;
	    }
	  }

	  public static String getUsername(Context context) {
	    // String email;
	    AccountManager manager = AccountManager.get(context);
	    Account account = getAccount(manager);
	    if (account == null) {
	      return "";
	    } else {
	      String email = account.name;
	      String[] parts = email.split("@");
	      if (parts.length > 0 && parts[0] != null)
	        return parts[0];
	      else
	        return "";
	    }
	  }
	  
	public static void openWebview(String url, Context context) {
		Intent i = new Intent(context, RadioWebViewClient.class);
		i.putExtra("url", url);
		context.startActivity(i);
	}
	
	public static String convertAppVersionToDouble(String value) {
		String newValue = null;
		String firstPart = value.substring(0, 3);
		String secondPart = value.substring(4);
		newValue = firstPart + secondPart;
		return newValue;
	}
	
	public static String getCurrentAppVersion(Context context) {
		String mVersionNumber = null;
		String firstPart = null;
		String secondpart = null;
		String newVersion = null;
		try {
			mVersionNumber = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
			firstPart = mVersionNumber.substring(0, 3);
			secondpart = mVersionNumber.substring(4);
			
			newVersion = firstPart + secondpart;
		} catch (NameNotFoundException e) {
			newVersion = null;
		}
		
		return newVersion;
	}
	
	public static String getAppVersion(Context context) {
		String mVersionNumber = null;
		try {
			mVersionNumber = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			mVersionNumber = null;
		}
		
		return mVersionNumber;
	}
	
	public static boolean isTablet(Context context) {
	    return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}
	
	public static float getScreenDensity(Context context) {
		return context.getApplicationContext().getResources().getDisplayMetrics().density;		
	}
	
	public static float getScreenWidth(Context context) {
		return context.getApplicationContext().getResources().getDisplayMetrics().widthPixels;
	}
	
	public static String formatTime(long millis) {
		String output = "00:00:00";
		long seconds = millis / 1000;
		long minutes = seconds / 60;
		long hours = minutes / 60;

		seconds = seconds % 60;
		minutes = minutes % 60;
		hours = hours % 60;

		String secondsD = String.valueOf(seconds);
		String minutesD = String.valueOf(minutes);
		@SuppressWarnings("unused")
		String hoursD = String.valueOf(hours);

		if (seconds < 10)
			secondsD = "0" + seconds;
		if (minutes < 10)
			minutesD = "0" + minutes;
		if (hours < 10)
			hoursD = "0" + hours;

		output = minutesD + " : " + secondsD;
		return output;
	}
	
	/*public void copyToSd() throws NotFoundException, IOException {
	InputStream in = getResources().openRawResource(R.raw.alarm);
	File file = new File(getExternalFilesDir(null), "alarm.mp3");
	
	try {
		OutputStream out = new FileOutputStream(file);
	byte[] buff = new byte[1024];
	int read = 0;

		while ((read = in.read(buff)) > 0) {
			out.write(buff, 0, read);
		}
		
		in.close();

		out.close();
	} catch (FileNotFoundException e1) {
		e1.printStackTrace();
	}
}*/
	
	/*private boolean[] toPrimitiveArray(final List<Boolean> booleanList) {
    final boolean[] primitives = new boolean[booleanList.size()];
    int index = 0;
    for (Boolean object : booleanList) {
        primitives[index++] = object;
    }
    return primitives;
}*/
	
	public static void sendClickEvent(String label, Context context) {
		EasyTracker easyTracker = EasyTracker.getInstance(context);
		easyTracker.send(MapBuilder
				.createEvent("UI_ACTION", 		//Category
						     "BUTTON_PRESS", 	//Action
						     label, 			//Label
						     null) 				//Value
				.build()
		);
	}
	
	public static void sendSession(Context context) {
		// May return null if a tracker has not yet been initialized.
//		Tracker tracker = GoogleAnalytics.getInstance(context).getDefaultTracker();

		// Start a new session. The next hit from this tracker will be the first in
		// a new session.
//		tracker.set(Fields.SESSION_CONTROL, "start");
	}
	
	public static void sendScreen() {}
	
	public static void sendSocialInteraction(String socialnetwork, String action, Context context) {
		Tracker easyTracker = EasyTracker.getInstance(context);
		easyTracker.send(MapBuilder
		    .createSocial(socialnetwork,                               // Social network (required)
		    			action,                                     // Social action (required)
		                  "http://www.rakista.com")   // Social target
		    .build()
		);
	}
	//
	public static String asciiCodeToString(int ascii) {
		String aChar = Character.valueOf((char)ascii).toString(); 
		return " " + aChar;
	}
}
