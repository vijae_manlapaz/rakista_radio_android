package com.rakista.radio;

import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.rakista.radio.test.OnRefreshListener;
import com.rakista.radio.core.DedicationDialog;
import com.rakista.radio.core.RequestSender;
import com.rakista.radio.core.VoteSender;
import com.rakista.radio.internal.CONSTANTS;
import com.rakista.radio.internal.DataBaseHandler;
import com.rakista.radio.internal.RequestSenderCallback;
import com.rakista.radio.internal.ToastHelper;
import com.rakista.radio.utility.JSONParser;
import com.rakista.radio.utility.Preferences;
import com.rakista.radio.utility.Utility;
import com.rakista.radio.widget.AutoScaleTextView;
import com.rakista.radio.widget.AutoScrollingTextView;
import com.rakista.radio.widget.ExpandableListFragment;

public class TopMusic extends ExpandableListFragment implements OnClickListener, OnScrollListener, OnRefreshListener {

	private static String TopRequestedURL = "http://rakista.com/radio/api/rradio.php?method=getTopRequestedSongs";
	private static String TopVotedSongURL = "http://rakista.com/radio/api/rradio.php?method=getVotedSong";
//	private Utility util;
	private String TABLE_TOPVOTED = "tbltopvoted";
	private String TABLE_TOPREQUESTED = "tbltoprequested";
	private DataBaseHandler dbh;
//	private SQLiteDatabase SQLDB;
	private test x;
	private SimpleExpandableAdapter adapter;
	private ToastHelper toast;
	private Button _TopVoted, _TopRequested;
	private Button _FilterByWeek;
	private AutoScaleTextView _FilterRange;

	private CalendarDialog calendar;

	private ArrayList<HashMap<String, String>> Data = new ArrayList<HashMap<String, String>>();

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.topmusic_layout_main, container, false);

		_TopRequested = (Button) v.findViewById(R.id.topmusic_button_toprequested);
		_TopVoted = (Button) v.findViewById(R.id.topmusic_button_topvotes);
		_FilterByWeek = (Button) v.findViewById(R.id.topmusic_button_filterbyweek);
		_FilterRange = (AutoScaleTextView) v.findViewById(R.id.topmusic_range);
		
		return v;
	}

	public void onDestroyView() {
		super.onDestroyView();
		setListAdapter(null);
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		toast = new ToastHelper(getActivity().getApplicationContext(), Toast.LENGTH_LONG);
//		util = new Utility(getActivity().getApplicationContext());
		dbh = new DataBaseHandler(getActivity().getApplicationContext());
//		SQLDB = dbh.getWritableDatabase();
		Preferences.Load(getActivity().getApplicationContext());
		calendar = new CalendarDialog(getActivity().getApplicationContext());
		
		_TopRequested.setOnClickListener(this);
		_TopVoted.setOnClickListener(this);
		_FilterByWeek.setOnClickListener(this);
		
		_TopVoted.setSelected(Preferences.getVoteButtonState());
		_TopRequested.setSelected(Preferences.getRequestedButtonState());
		Drawable y = getResources().getDrawable(R.drawable.listdivider);

		adapter = new SimpleExpandableAdapter(getActivity().getApplicationContext());
		
		x = (test) getExpandableListView();
		x.setDivider(y);

		x.setGroupIndicator(null);
		x.setOnScrollListener(this);
		x.setAdapter(adapter);
		
		x.setOnRefreshListener(this);
		
		if(!dbh.isTableExists(TABLE_TOPVOTED)) {
			dbh.createTable(TABLE_TOPVOTED);
		}

		if(!dbh.isTableExists(TABLE_TOPREQUESTED)) {
			dbh.createTable(TABLE_TOPREQUESTED);
		}

		if (_TopRequested.isSelected()) {
			if(Utility.isConnected()) {
				if(dbh.getDataCount(TABLE_TOPREQUESTED) > 0) {
					reloadAdapterData(TABLE_TOPREQUESTED);
				} else {
					new LoadTopMusic("Requested", false, null).execute(TopRequestedURL);
				}
			} else {
				reloadAdapterData(TABLE_TOPREQUESTED);
			}

			_FilterRange.setText("Top requested this week.");

		} else {
			if(Utility.isConnected()) {
				if(dbh.getDataCount(TABLE_TOPVOTED) > 0) {
					reloadAdapterData(TABLE_TOPVOTED);
				} else {
					new LoadTopMusic("Voted", false, null).execute(TopVotedSongURL);
				}
			} else {
				reloadAdapterData(TABLE_TOPVOTED);
			}
			_FilterRange.setText("Top voted this week.");
		}

		EasyTracker.getInstance(getActivity()).activityStart(getActivity());
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		
		if(firstVisibleItem <= 2 ) {
			getView().findViewById(R.id.topmusic_layout_topbar).setVisibility(View.VISIBLE);

		} else if(firstVisibleItem == 3) {
			getView().findViewById(R.id.topmusic_layout_topbar).setVisibility(View.GONE);
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
	}

	private void reloadAdapterData(String tableName) {
		getDataFromDB(tableName);
		adapter = (SimpleExpandableAdapter) getExpandableListView().getAdapter();
		adapter.setData(createGroupListFromDatabase(), createChildListFromDatabase());
		adapter.notifyDataSetChanged();
		x.onRefreshComplete();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.topmusic_button_topvotes:

			_TopVoted.setSelected(true);
			_TopRequested.setSelected(false);
			Preferences.setVoteButtonState(true);
			Preferences.setRequestedButtonState(false);

			if (_TopVoted.isSelected()) {
				if(dbh.getDataCount(TABLE_TOPVOTED) > 0) {
					reloadAdapterData(TABLE_TOPVOTED);
				} else {
					if(Utility.isConnected()) {
						new LoadTopMusic("Voted", false, null).execute(TopVotedSongURL);
					} else {
						toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
					}
				}
			}

			_FilterRange.setText("Top voted this week.");

			Utility.sendClickEvent("TOPVOTED_BUTTON", getActivity());
			break;

		case R.id.topmusic_button_toprequested:

			_TopRequested.setSelected(true);
			_TopVoted.setSelected(false);
			Preferences.setRequestedButtonState(true);
			Preferences.setVoteButtonState(false);

			if (_TopRequested.isSelected()) {
				if(dbh.getDataCount(TABLE_TOPREQUESTED) > 0) {
					reloadAdapterData(TABLE_TOPREQUESTED);
				} else {
					if(Utility.isConnected()) {
						new LoadTopMusic("Requested", false, null).execute(TopRequestedURL);
					} else {
						toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
					}
				}
			}

			_FilterRange.setText("Top requested this week.");

			Utility.sendClickEvent("TOPREQUESTED_BUTTON", getActivity());
			break;

		case R.id.topmusic_button_filterbyweek:
			calendar = new CalendarDialog(view.getContext(), new CalendarDialogCallback() {

				@Override
				public void onResult(String[] result) throws ParseException {
					if(Utility.isConnected()) {
						if(_TopVoted.isSelected()) {
							new LoadTopMusic("Voted", true, result[0]).execute(TopVotedSongURL);
							_FilterRange.setText("Top Voted from " + result[0] + " to " + result[1]);
						} else if(_TopRequested.isSelected()) {
							new LoadTopMusic("Requested", true, result[0]).execute(TopRequestedURL);
							_FilterRange.setText("Top Requested from " + result[0] + " to " + result[1]);
						}
						calendar.dismiss();
					} else {
						toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
					}
				}
			});
			calendar.show();
			Utility.sendClickEvent("FILTER_BUTTON", getActivity());
			break;
		}
	}

	/*===========================================================================
	 * 							FOR OFFLINE ACCESS ONLY
	 *=============================================================================
	 * */

	ArrayList<HashMap<String, String>> DB(String TableName) {
		return dbh.getAllData(TableName); 
	}

	private synchronized void getDataFromDB(String TableName) {
		Data = DB(TableName);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ArrayList<HashMap<String, String>> createGroupListFromDatabase() {
		final ArrayList result = new ArrayList();
		new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < Data.size(); ++i) {
					HashMap m = new HashMap();
					m.put("ID", Data.get(i).get("ID"));
					m.put("title", Data.get(i).get("title"));
					m.put("artist", Data.get(i).get("artist"));
					m.put("cnt", Data.get(i).get("cnt"));
					m.put("date_played", Data.get(i).get("date_played"));
					result.add(m);
				}
			}
		}).start();

		return (ArrayList<HashMap<String, String>>) result;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ArrayList<ArrayList<HashMap<String, String>>> createChildListFromDatabase() {
		final ArrayList result = new ArrayList();
		new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < Data.size(); ++i) {
					ArrayList<HashMap<String, String>> secList = new ArrayList<HashMap<String, String>>();
					for (int n = 0; n < 1; n++) {
						HashMap child = new HashMap();
						child.put("ID", Data.get(i).get("ID"));
						child.put("title", Data.get(i).get("title"));
						child.put("artist", Data.get(i).get("artist"));
						child.put("album", Data.get(i).get("album"));
						child.put("albumyear", Data.get(i).get("albumyear"));
						child.put("lyrics", Data.get(i).get("lyrics"));
						child.put("buycd", Data.get(i).get("buycd"));
						child.put("durationDisplay", Data.get(i).get("durationDisplay"));
						secList.add(child);
					}
					result.add(secList);
				}
			}
		}).start();

		return result;
	}

	/*====================================================================
	 * 							FOR ONLINE ACCESS ONLY
	 *====================================================================
	 * */

	/* Creating the Hashmap for the row */
	/*@SuppressWarnings({ "unchecked", "rawtypes" })
	private ArrayList<HashMap<String, String>> createGroupList() {
		ArrayList result = new ArrayList();
		for (int i = 0; i < Data.size(); ++i) { // 15 groups........
			HashMap m = new HashMap();
			m.put("ID", Data.get(i).get("ID"));
			m.put("title", Data.get(i).get("title")); // the key and it's value.
			m.put("artist", Data.get(i).get("artist"));
			m.put("cnt", Data.get(i).get("cnt"));
			m.put("date_played", Data.get(i).get("date_played"));
			m.put("position", i + 1);

			result.add(m);
		}
		return (ArrayList<HashMap<String, String>>) result;
	}

	 creating the HashMap for the children 
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ArrayList<ArrayList<HashMap<String, String>>> createChildList() {
		ArrayList result = new ArrayList();
		for (int i = 0; i < Data.size(); ++i) {
			ArrayList<HashMap<String, String>> secList = new ArrayList<HashMap<String, String>>();
			for (int n = 0; n < 1; n++) {
				HashMap child = new HashMap();
				child.put("ID", Data.get(i).get("ID"));
				child.put("title", Data.get(i).get("title"));
				child.put("artist", Data.get(i).get("artist"));
				child.put("album", Data.get(i).get("album"));
				child.put("albumyear", Data.get(i).get("albumyear"));
				child.put("lyrics", Data.get(i).get("lyrics"));
				child.put("buycd", Data.get(i).get("buycd"));
				child.put("durationDisplay", Data.get(i).get("durationDisplay"));
				secList.add(child);
			}
			result.add(secList);
		}
		return result;
	}*/

	/*	public void onContentChanged() {
		//System.out.println("onContentChanged");
		super.onContentChanged();
	}

	 This function is called on each child click 
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		return true;
	}

	 This function is called on expansion of the group 
	public void onGroupExpand(int groupPosition) {
		try {
			System.out.println("Group exapanding Listener => groupPosition = "
					+ groupPosition);
		} catch (Exception e) {
			System.out.println(" groupPosition Errrr +++ " + e.getMessage());
		}
	}*/

	public class SimpleExpandableAdapter extends BaseExpandableListAdapter {
		
		private Context context;
		private LayoutInflater inflater;
		private ArrayList<HashMap<String,String>> Group = new ArrayList<HashMap<String,String>>();
		private ArrayList<ArrayList<HashMap<String,String>>> Child = new ArrayList<ArrayList<HashMap<String,String>>>();

		public SimpleExpandableAdapter(Context context, ArrayList<HashMap<String,String>> groupItem, ArrayList<ArrayList<HashMap<String,String>>> childItem) {
			this.Group = groupItem;
			this.Child = childItem;
			this.context = context;
		}

		public SimpleExpandableAdapter(Context context) {
			this.context = context;
		};

		public void setData(ArrayList<HashMap<String,String>> groupItem, ArrayList<ArrayList<HashMap<String,String>>> childItem) {
			this.Group = groupItem;
			this.Child = childItem;
		}

		public Object getChild(int groupPosition, int childPosition) {
			return Child.get(groupPosition).get(childPosition);
		}

		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
			if(convertView == null) {
				inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.topmusic_view_child, null);
			}

			AutoScrollingTextView Title = (AutoScrollingTextView)convertView.findViewById(R.id.topmusic_child_view_title);
			Title.setText(Child.get(groupPosition).get(0).get("title"));

			AutoScrollingTextView Artist = (AutoScrollingTextView)convertView.findViewById(R.id.topmusic_child_view_artist);
			Artist.setText(Child.get(groupPosition).get(0).get("artist").toString());

			AutoScrollingTextView Album = (AutoScrollingTextView)convertView.findViewById(R.id.topmusic_child_view_album);
			Album.setText(Child.get(groupPosition).get(0).get("album").length() < 1? "Not Available" : Child.get(groupPosition).get(0).get("album"));

			TextView Duration = (TextView)convertView.findViewById(R.id.topmusic_child_view_duration);
			Duration.setText(Child.get(groupPosition).get(0).get("durationDisplay").toString());

			TextView Year = (TextView)convertView.findViewById(R.id.topmusic_child_view_year);
			Year.setText(Child.get(groupPosition).get(0).get("albumyear").toString());

			TextView Lyrics = (TextView) convertView.findViewById(R.id.topmusic_child_view_lyrics);
			Lyrics.setText(Child.get(groupPosition).get(0).get("lyrics").length() < 5? "Not Available" : Child.get(groupPosition).get(0).get("lyrics"));
			Lyrics.setMovementMethod(new ScrollingMovementMethod());

			Button Vote = (Button) convertView.findViewById(R.id.topmusic_child_view_button_vote);
			Vote.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					Utility.sendClickEvent("VOTE_BUTTON", getActivity());
					if (Utility.isConnected()) {
						VoteSender Vote = new VoteSender(getActivity().getApplicationContext());
						Vote.sendVote(Data.get(groupPosition).get("ID"));
					} else {
						toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
					}
				}
			});

			Button Request = (Button) convertView.findViewById(R.id.topmusic_child_view_button_request);
			Request.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					Utility.sendClickEvent("REQUEST_BUTTON", getActivity());
					if (Utility.isConnected()) {
						new RequestSender(getActivity().getApplicationContext(), new RequestSenderCallback() {

							@Override
							public void onComplete(HashMap<String, String> result) {
								if (result.containsKey("requestid")) {
									DedicationDialog x = new DedicationDialog(getActivity(), result.get("requestid").toString());
									x.show();
								} else {
									toast.show(CONSTANTS.REQUEST, result.get("err_msg"));
								}
							}
						}).exec(Data.get(groupPosition).get("ID"));
					} else {
						toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
					}
				}
			});

			Button Buy = (Button) convertView.findViewById(R.id.topmusic_child_view_button_buy);

			if(Child.get(groupPosition).get(0).get("buycd").toString().length() < 4) {
				//|| Child.get(groupPosition).get(0).get("buycd") == null || Child.get(groupPosition).get(0).get("buycd") == "" || Child.get(groupPosition).get(0).get("buycd") == "null" || Child.get(groupPosition).get(0).get("buycd") == "<null>") {
				Buy.setTextColor(Color.GRAY);
				Buy.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.buy_inactive), null, null, null);
				Buy.setEnabled(false);
			} else {
				Buy.setEnabled(true);
				Buy.setTextColor(Color.WHITE);
				Buy.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.buy), null, null, null);
				Buy.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Utility.sendClickEvent("BUY_BUTTON", getActivity());
						if(Utility.isConnected()) {
							Intent i = new Intent(Intent.ACTION_VIEW);
							i.setData(Uri.parse(Child.get(groupPosition).get(0).get("buycd").toString()));
							startActivity(i);
						} else {
							toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
						}
					}
				});
			}

			return convertView;
		}

		public int getChildrenCount(int groupPosition) {
			return Child.get(groupPosition).size();
		}

		public Object getGroup(int groupPosition) {
			return Group.get(groupPosition);
		}

		public int getGroupCount() {
			return Group.size();
		}

		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
			if(convertView == null) {
				inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.topmusic_view_group, null);
			}

			TextView numbering = (TextView) convertView.findViewById(R.id.topmusic_group_numbering);
			numbering.setText(String.valueOf(groupPosition + 1));

			TextView countLabel = (TextView) convertView.findViewById(R.id.topmusic_group_count_label);

			if(_TopRequested.isSelected()) {
				countLabel.setText("Requests: ");
			} else if(_TopVoted.isSelected()) {
				countLabel.setText("Votes: ");
			}

			AutoScrollingTextView Title = (AutoScrollingTextView) convertView.findViewById(R.id.topmusic_group_title);
			Title.setText(Group.get(groupPosition).get("title"));

			AutoScrollingTextView Artist = (AutoScrollingTextView) convertView.findViewById(R.id.topmusic_group_artist);
			Artist.setText(Group.get(groupPosition).get("artist"));

			TextView Length = (TextView) convertView.findViewById(R.id.topmusic_group_count_length);
			Length.setText(Group.get(groupPosition).get("cnt"));

			return convertView;
		}

		public boolean hasStableIds() {
			return true;
		}

		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
	}

	@Override
	public void onContentChanged() {
		super.onContentChanged();
	}

	public class LoadTopMusic extends AsyncTask<String, Void, ArrayList<HashMap<String, String>>> implements DialogInterface.OnCancelListener {

		private String MethodType = null;
		private ProgressDialog mProgress;
		private boolean showProgessDialog = false;
		private String filterParam = "&date=";
		private String filterDate = null;

		public LoadTopMusic() {}

		public LoadTopMusic (String methodType, boolean showDialog, String date) {
			this.MethodType = methodType;
			this.showProgessDialog = showDialog;
			this.filterDate = date;
		}

		public void setMethodType(String methodType) {
			this.MethodType = methodType;
		}

		public void setShowLoader(boolean loader) {
			this.showProgessDialog = loader;
		}

		public void setDateFilter(String date) {
			filterDate = date;
		}

		protected void onPreExecute() {
			super.onPreExecute();
			if(showProgessDialog == true) {
				mProgress = new ProgressDialog(getActivity());
				mProgress.setMessage("Updating" + " Top " + MethodType +"\nPlease Wait...");
				mProgress.setIndeterminate(true);
				mProgress.setCancelable(false);
//				mProgress.setOnCancelListener(this);
				mProgress.setCanceledOnTouchOutside(false);
				mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				mProgress.show();
			}
		}

		@Override
		public void onCancel(DialogInterface dialog) {
			Log.i(getClass().getSimpleName(), "Cancelling Thread");
			this.cancel(true);
		}

		protected void onCancelled() {
			Log.i(getClass().getSimpleName(), "Thread successfully cancelled");
			mProgress.dismiss();
		}

		protected ArrayList<HashMap<String, String>> doInBackground(String... params) {
			// / JSON Node names
			final String RESULT = "result";
			final String TAG_ID = "ID";
			final String TAG_ARTIST = "artist";
			final String TAG_TITLE = "title";
			final String TAG_LYRICS = "lyrics";
			final String TAG_ALBUM = "album";
			final String TAG_ALBUMYEAR = "albumyear";
			final String TAG_DURATION = "duration";
			final String TAG_DURATIONDISPLAY = "durationDisplay";
			final String TAG_BUY = "buycd";
			final String TAG_COUNT = "cnt";
			final String TAG_DATEPLAYED = "date_played";

			// contacts JSONArray
			JSONArray contacts = null;

			// Hashmap for ListView
			ArrayList<HashMap<String, String>> RecentSongs = new ArrayList<HashMap<String, String>>();

			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			JSONObject json = null;
			try {
				if(filterDate == null) {
					json = jParser.getJSONFromUrl(params[0]);
				} else {
					json = jParser.getJSONFromUrl(params[0] + filterParam + filterDate);
				}

			} catch (HttpException e1) {
				e1.printStackTrace();
				json = null;
			} catch (UnknownHostException e) {
				e.printStackTrace();
				json = null;
			} catch (ConnectTimeoutException e) {
//				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
				e.printStackTrace();
				json = null;
			}

			if(json == null) {
				Log.i("TOP MUSIC", "json is null");
			}

			try {
				// Getting Array of Contacts
				//contacts = json.getJSONArray(RESULT);     
				if(json.getJSONArray(RESULT) != null) {
					contacts = json.getJSONArray(RESULT);
					// looping through All Contacts
					for (int i = 0; i < contacts.length(); i++) {
						JSONObject c = contacts.getJSONObject(i);

						// Storing each json item in variable
						String ID = c.getString(TAG_ID);
						String Artist = c.getString(TAG_ARTIST);
						String Title = c.getString(TAG_TITLE);
						String Lyrics = c.getString(TAG_LYRICS);
						String Album = c.getString(TAG_ALBUM);
						String AlbumYear = c.getString(TAG_ALBUMYEAR);
						String Duration = c.getString(TAG_DURATION);
						String DuratioDisplay = c.getString(TAG_DURATIONDISPLAY);
						String BUYCD = c.getString(TAG_BUY);
						String Count = c.getString(TAG_COUNT);
						String DataPlayed = c.getString(TAG_DATEPLAYED);

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						map.put(TAG_ID, ID);
						map.put(TAG_ARTIST, Artist);
						map.put(TAG_TITLE, Title);
						map.put(TAG_LYRICS, Lyrics);
						map.put(TAG_ALBUM, Album);
						map.put(TAG_ALBUMYEAR, AlbumYear);
						map.put(TAG_DURATION, Duration);
						map.put(TAG_DURATIONDISPLAY, DuratioDisplay);
						map.put(TAG_BUY, BUYCD);
						map.put(TAG_COUNT, Count);
						map.put(TAG_DATEPLAYED, DataPlayed);

						// adding HashList to ArrayList
						RecentSongs.add(map);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			return RecentSongs;
		}

		@SuppressWarnings("unused")
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {

//			if(showProgessDialog) {
			if(mProgress != null && mProgress.isShowing()) {
				mProgress.dismiss();
			}

			if (!result.isEmpty() && result != null) {

				Data.clear();
				Data = result;

				if (MethodType.equals("Voted")) {
					if(filterDate != null) {
						_TopVoted.setSelected(true);
						_TopRequested.setSelected(false);
						Preferences.setRequestedButtonState(false);
						Preferences.setVoteButtonState(true);
						
						adapter = new SimpleExpandableAdapter(getActivity().getApplicationContext(), createGroupListFromDatabase(), createChildListFromDatabase());
						x.setAdapter(adapter);
						
					} else {
						_TopVoted.setSelected(true);
						_TopRequested.setSelected(false);
						Preferences.setRequestedButtonState(false);
						Preferences.setVoteButtonState(true);
						_FilterRange.setText("");
						dbh.updateTable(TABLE_TOPVOTED, result);
						reloadAdapterData(TABLE_TOPVOTED);
					}
				} else {
					if(filterDate != null) {
						_TopRequested.setSelected(true);
						_TopVoted.setSelected(false);
						Preferences.setRequestedButtonState(true);
						Preferences.setVoteButtonState(false);
						
						adapter = new SimpleExpandableAdapter(getActivity().getApplicationContext(), createGroupListFromDatabase(), createChildListFromDatabase());
						x.setAdapter(adapter);
						
					} else {
						_TopRequested.setSelected(true);
						_TopVoted.setSelected(false);
						Preferences.setRequestedButtonState(true);
						Preferences.setVoteButtonState(false);
						_FilterRange.setText("");
						dbh.updateTable(TABLE_TOPREQUESTED, result);
						reloadAdapterData(TABLE_TOPREQUESTED);
					}
				}

			} else {
				toast.show(getString(R.string.error), getString(R.string.connection_error_message));
			}
			
			x.onRefreshComplete();
		}
	}

	@Override
	public void onRefresh() {
		if(_TopVoted.isSelected()) {
			if(Utility.isConnected()) {
				new LoadTopMusic("Voted", true, null).execute(TopVotedSongURL);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
		} else {
			if(Utility.isConnected()) {
				new LoadTopMusic("Requested", true, null).execute(TopRequestedURL);
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
		}
	}
}
