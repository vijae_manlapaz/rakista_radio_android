package com.rakista.radio;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.rakista.radio.test.OnRefreshListener;
import com.rakista.radio.callback.BaseCallback;
import com.rakista.radio.core.VoteSender;
import com.rakista.radio.internal.DataBaseHandler;
import com.rakista.radio.internal.ToastHelper;
import com.rakista.radio.threads.HistoryRunnable;
import com.rakista.radio.utility.JSONParser;
import com.rakista.radio.utility.Preferences;
import com.rakista.radio.utility.Utility;
import com.rakista.radio.widget.AutoScrollingTextView;
import com.rakista.radio.widget.ExpandableListFragment;

public class History extends ExpandableListFragment implements OnScrollListener, OnRefreshListener, BaseCallback {

	private String serverURL = "http://www.rakista.com/radio/api/rradio.php?method=getrecentsongs";
//	private Utility util;
	private DataBaseHandler dbh;
//	private SQLiteDatabase SQLDB;
	private SimpleExpandableAdapter adapter;
	private ToastHelper toast;

	private test x;
	private ArrayList<HashMap<String, String>> Data = new ArrayList<HashMap<String, String>>();

//	private LayoutInflater inflater;
//	private View emptyView;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.history_layout, container, false);
		return v;
	}

	public void onDestroyView() {
		super.onDestroyView();
		setListAdapter(null);
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		toast = new ToastHelper(getActivity().getApplicationContext(), Toast.LENGTH_SHORT);
//		util = new Utility(getActivity().getApplicationContext());
		dbh = new DataBaseHandler(getActivity().getApplicationContext());
		Preferences.Load(getActivity().getApplicationContext());
//		SQLDB = dbh.getWritableDatabase();

		Drawable y = getResources().getDrawable(R.drawable.listdivider);

		adapter = new SimpleExpandableAdapter(getActivity().getApplicationContext());
		
		x = (test) getExpandableListView();
		x.setDivider(y);

		x.setGroupIndicator(null);
		x.setOnScrollListener(this);
		x.setAdapter(adapter);
		
		x.setOnRefreshListener(this);
		
		if(!dbh.isTableExists(DataBaseHandler.TABLE_HISTORY)) {
			dbh.createTable(DataBaseHandler.TABLE_HISTORY);
		}

		getDataFromDB();

		if(Utility.isConnected()) {
			if(dbh.getDataCount(DataBaseHandler.TABLE_HISTORY) > 0) {
				reloadAdapterData();
			} else {
				if(Utility.isConnected()) {
//					String serverURL = "http://www.rakista.com/radio/api/rradio.php?method=getrecentsongs";
					new LoadHistory(false).execute(serverURL);
//					new Thread(new HistoryRunnable(this)).start();
				} else {
					toast.show(getString(R.string.Network),getString(R.string.not_reachable));
				}
			}
		} else {
			reloadAdapterData();
		}

		EasyTracker.getInstance(getActivity()).activityStart(getActivity());
	}
	
	final android.os.Handler mHanler = new android.os.Handler();
	
	private Runnable reloadAdapter = new Runnable() {
		
		@Override
		public void run() {
			adapter.setData(createGroupListFromDatabase(), createChildListFromDatabase());
			adapter.notifyDataSetChanged();	
			x.onRefreshComplete();
		}
	};
	
	private void reloadAdapterData() {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				mHanler.post(reloadAdapter);
			}
		}).start();
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	/*==========================================================================================
	 * 									FOR OFFLINE ACCESS ONLY
	 *==========================================================================================
	 */
	ArrayList<HashMap<String, String>> DB() {
		return dbh.getAllData(DataBaseHandler.TABLE_HISTORY); 
	}

	private void getDataFromDB() {
		Data = DB();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ArrayList<HashMap<String, String>> createGroupListFromDatabase() {
		final ArrayList result = new ArrayList();
		//new Thread(new Runnable() {
			//@Override
			//public void run() {
				for (int i = 0; i < Data.size(); ++i) { // 15 groups........
					HashMap m = new HashMap();
					m.put("ID", Data.get(i).get("ID"));
					m.put("title", Data.get(i).get("title")); // the key and it's value.
					m.put("artist", Data.get(i).get("artist"));
					m.put("date_played", Data.get(i).get("date_played"));
					m.put("isRequested", Data.get(i).get("isRequested"));
					result.add(m);
				}
			//}
		//}).start();
		return (ArrayList<HashMap<String, String>>) result;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ArrayList<ArrayList<HashMap<String, String>>> createChildListFromDatabase() {
		final ArrayList result = new ArrayList();
		//new Thread(new Runnable() {
			//@Override
			//public void run() {
				for (int i = 0; i < Data.size(); ++i) {
					ArrayList<HashMap<String, String>> secList = new ArrayList<HashMap<String, String>>();
					for (int n = 0; n < 1; n++) {
						HashMap child = new HashMap();
						child.put("ID", Data.get(i).get("ID"));
						child.put("title", Data.get(i).get("title"));
						child.put("artist", Data.get(i).get("artist"));
						child.put("album", Data.get(i).get("album"));
						child.put("albumyear", Data.get(i).get("albumyear"));
						child.put("lyrics", Data.get(i).get("lyrics"));
						child.put("buycd", Data.get(i).get("buycd"));
						child.put("durationDisplay", Data.get(i).get("durationDisplay"));
						secList.add(child);
					}
					result.add(secList);
				}
			//}
		//}).start();
		return result;
	}

	/*============================================================================================
	 * 									FOR ONLINE ACCESS ONLY
	 *============================================================================================
	 */

	/* Creating the Hashmap for the row */
	/*@SuppressWarnings({ "unchecked", "rawtypes" })
	private ArrayList<HashMap<String, String>> createGroupList() {
		final ArrayList result = new ArrayList();
		Thread xy = new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < Data.size(); ++i) { // 15 groups........
					HashMap m = new HashMap();
					m.put("ID", Data.get(i).get("ID"));
					m.put("title", Data.get(i).get("title")); // the key and it's value.
					m.put("artist", Data.get(i).get("artist"));
					m.put("date_played", Data.get(i).get("date_played"));
					m.put("isRequested", Data.get(i).get("isRequested"));
					result.add(m);
				}
			}
		});
		xy.run();

		return (ArrayList<HashMap<String, String>>) result;
	}*/

	/* creating the HashMap for the children */
	/*@SuppressWarnings({ "unchecked", "rawtypes" })
	private ArrayList<ArrayList<HashMap<String, String>>> createChildList() {
		final ArrayList result = new ArrayList();
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < Data.size(); ++i) {
					ArrayList<HashMap<String, String>> secList = new ArrayList<HashMap<String, String>>();
					for (int n = 0; n < 1; n++) {
						HashMap child = new HashMap();
						child.put("ID", Data.get(i).get("ID"));
						child.put("title", Data.get(i).get("title"));
						child.put("artist", Data.get(i).get("artist"));
						child.put("album", Data.get(i).get("album"));
						child.put("albumyear", Data.get(i).get("albumyear"));
						child.put("lyrics", Data.get(i).get("lyrics"));
						child.put("buycd", Data.get(i).get("buycd"));
						child.put("durationDisplay", Data.get(i).get("durationDisplay"));
						secList.add(child);
					}
					result.add(secList);
				}
			}
		});
		t.run();

		return result;
	}*/

	//	public void onContentChanged() {
	//		//System.out.println("onContentChanged");
	//		super.onContentChanged();
	//	}

	public class SimpleExpandableAdapter extends BaseExpandableListAdapter  {

		private Context context;
		private LayoutInflater inflater;
		private ArrayList<HashMap<String,String>> Group = new ArrayList<HashMap<String,String>>();
		private ArrayList<ArrayList<HashMap<String,String>>> Child = new ArrayList<ArrayList<HashMap<String,String>>>();

		public SimpleExpandableAdapter(Context context, ArrayList<HashMap<String,String>> groupItem, ArrayList<ArrayList<HashMap<String,String>>> childItem) {
			this.Group = groupItem;
			this.Child = childItem;
			this.context = context;
		}

		public SimpleExpandableAdapter(Context context) {
			this.context = context;
		}

		public void setData(ArrayList<HashMap<String,String>> groupItem, ArrayList<ArrayList<HashMap<String,String>>> childItem) {
			this.Group = groupItem;
			this.Child = childItem;
		}

		public Object getChild(int groupPosition, int childPosition) {
			return Child.get(groupPosition).get(childPosition);
		}

		public String getChildValue(int groupPosition, int childPosition, String fieldKey) {
			return Child.get(groupPosition).get(childPosition).get(fieldKey);
		}

		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

			if(convertView == null) {
				inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.history_view_child, null);
			}

			AutoScrollingTextView Title = (AutoScrollingTextView)convertView.findViewById(R.id.history_child_view_title);
			Title.setText(Child.get(groupPosition).get(0).get("title"));

			AutoScrollingTextView Artist = (AutoScrollingTextView)convertView.findViewById(R.id.history_child_view_artist);
			Artist.setText(Child.get(groupPosition).get(0).get("artist"));

			AutoScrollingTextView Album = (AutoScrollingTextView)convertView.findViewById(R.id.history_child_view_album);
			Album.setText(Child.get(groupPosition).get(0).get("album").length() < 1? "Not Available" : Child.get(groupPosition).get(0).get("album"));

			TextView Duration = (TextView)convertView.findViewById(R.id.history_child_view_duration);
			Duration.setText(Child.get(groupPosition).get(0).get("durationDisplay"));

			TextView Year = (TextView)convertView.findViewById(R.id.history_child_view_year);
			Year.setText(Child.get(groupPosition).get(0).get("albumyear"));

			TextView Lyrics = (TextView) convertView.findViewById(R.id.history_child_view_lyrics);
			Lyrics.setText(Child.get(groupPosition).get(0).get("lyrics").length() < 5? "Not Available" : Child.get(groupPosition).get(0).get("lyrics"));
			Lyrics.setMovementMethod(new ScrollingMovementMethod());

			Button Vote = (Button) convertView.findViewById(R.id.history_child_view_button_vote);
			Vote.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Utility.sendClickEvent("VOTE_BUTTON", getActivity());
					if (Utility.isConnected()) {
						VoteSender voteSend = new VoteSender(getActivity().getApplicationContext());
						voteSend.sendVote(Child.get(groupPosition).get(0).get("ID").toString());
					} else {
						toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
					}
				}
			});

			Button Buy = (Button) convertView.findViewById(R.id.history_child_view_button_buy);

			if(Child.get(groupPosition).get(0).get("buycd").length() < 4) {
				Buy.setTextColor(Color.GRAY);
				Buy.setEnabled(false);
				Buy.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.buy_inactive), null, null, null);
			} else {
				Buy.setEnabled(true);
				Buy.setTextColor(Color.WHITE);
				Buy.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.buy), null, null, null);
				Buy.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Utility.sendClickEvent("BUY_BUTTON", getActivity());
						if(Utility.isConnected()) {
							//if(!Child.get(groupPosition).get(0).get("buycd").isEmpty()) {
								Intent i = new Intent(Intent.ACTION_VIEW);
								i.setData(Uri.parse(Child.get(groupPosition).get(0).get("buycd").toString()));
								startActivity(i);
							//}
						} else {
							toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
						}
					}
				});
			}
			return convertView;
		}

		public int getChildrenCount(int groupPosition) {
			return Child.get(groupPosition).size();
		}

		public Object getGroup(int groupPosition) {
			return Group.get(groupPosition);
		}

		public int getGroupCount() {
			return Group.size();
		}

		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

			if(convertView == null) {
				inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.history_view_group, null);
			}

			TextView numbering = (TextView) convertView.findViewById(R.id.history_group_numbering);
			numbering.setText(String.valueOf(groupPosition + 1) + ".");

			AutoScrollingTextView Title = (AutoScrollingTextView) convertView.findViewById(R.id.history_group_title);
			Title.setText(Group.get(groupPosition).get("title"));

			AutoScrollingTextView Artist = (AutoScrollingTextView) convertView.findViewById(R.id.history_group_artist);
			Artist.setText(Group.get(groupPosition).get("artist"));

			TextView requestIndicator = (TextView) convertView.findViewById(R.id.history_group_song_request);
			if(Boolean.parseBoolean(Group.get(groupPosition).get("isRequested"))) {
				requestIndicator.setText(Utility.asciiCodeToString(126) + "reguested");
			} else {
				requestIndicator.setText("");
			}

			TextView Length = (TextView) convertView.findViewById(R.id.history_group_song_length);
			Length.setText(Utility.convertTimeZones(Group.get(groupPosition).get("date_played")));

			return convertView;
		}

		public boolean hasStableIds() {
			return true;
		}

		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

//		public void notifyDataSetChanged() {
//			super.notifyDataSetChanged();
//		}

//		public void registerDataSetObserver(DataSetObserver observer) {
//			super.registerDataSetObserver(observer);
//		}
	}

	public class LoadHistory extends AsyncTask<String, Void, ArrayList<HashMap<String, String>>> {

		private boolean showProgressDialog;
		private ProgressDialog mProgress;

		public LoadHistory(boolean showProgress) {
			showProgressDialog = showProgress;
		}

		protected ArrayList<HashMap<String, String>> doInBackground(String... params) {
			// JSON Node names
			final String RESULT = "result";
			final String TAG_ID = "ID";
			final String TAG_ARTIST = "artist";
			final String TAG_TITLE = "title";
			final String TAG_LYRICS = "lyrics";
			final String TAG_ALBUM = "album";
			final String TAG_ALBUMYEAR = "albumyear";
			final String TAG_DURATION = "duration";
			final String TAG_DURATIONDISPLAY = "durationDisplay";
			final String TAG_DATEPLAYED = "date_played";
			final String TAG_BUY = "buycd";
			final String TAG_ISREQUESTED = "isRequested";
			final String TAG_ISDEDICATED = "isDedication";

			// contacts JSONArray
			JSONArray contacts = null;

			// Hashmap for ListView
			ArrayList<HashMap<String, String>> RecentSongs = new ArrayList<HashMap<String, String>>();

			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			JSONObject json = null;
			while(!isCancelled()) {
				try {
					json = jParser.getJSONFromUrl(params[0]);
				} catch (HttpException e1) {
					e1.printStackTrace();
					json = null;
				} catch (UnknownHostException e) {
					e.printStackTrace();
					json = null;
				} catch (ConnectTimeoutException e) {
					e.printStackTrace();
					json = null;
				} catch (NullPointerException e) {
					e.printStackTrace();
				}

				try {

					if(json != null) {
						// Getting Array of Contacts
						contacts = json.getJSONArray(RESULT);

						// looping through All Contacts
						for (int i = 0; i < contacts.length(); i++) {
							JSONObject c = contacts.getJSONObject(i);

							// Storing each json item in variable
							String ID = c.getString(TAG_ID);
							String Artist = c.getString(TAG_ARTIST);
							String Title = c.getString(TAG_TITLE);
							String Lyrics = c.getString(TAG_LYRICS);
							String Album = c.getString(TAG_ALBUM);
							String AlbumYear = c.getString(TAG_ALBUMYEAR);
							String Duration = c.getString(TAG_DURATION);
							String DuratioDisplay = c.getString(TAG_DURATIONDISPLAY);
							String BUYCD = c.getString(TAG_BUY);
							String Date_Played = c.getString(TAG_DATEPLAYED);
							boolean isRequested = c.getBoolean(TAG_ISREQUESTED);
							boolean isDedicated = c.getBoolean(TAG_ISDEDICATED);

							// creating new HashMap
							HashMap<String, String> map = new HashMap<String, String>();

							// adding each child node to HashMap key => value
							map.put(TAG_ID, ID);
							map.put(TAG_ARTIST, Artist);
							map.put(TAG_TITLE, Title);
							map.put(TAG_LYRICS, Lyrics);
							map.put(TAG_ALBUM, Album);
							map.put(TAG_ALBUMYEAR, AlbumYear);
							map.put(TAG_DURATION, Duration);
							map.put(TAG_DURATIONDISPLAY, DuratioDisplay);
							map.put(TAG_BUY, BUYCD);
							map.put(TAG_DATEPLAYED, Date_Played);
							map.put(TAG_ISREQUESTED, Boolean.toString(isRequested));
							map.put(TAG_ISDEDICATED, Boolean.toString(isDedicated));

							RecentSongs.add(map);
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (NullPointerException e) {
					e.printStackTrace();
				}

				return RecentSongs;
			}
			return null;
		}

//		@Override
//		public void onCancel(DialogInterface dialog) {
//			Log.e(getClass().getSimpleName(), "Cancelling Thread");
//			this.cancel(true);
//		}

		@Override
		protected void onCancelled() {
			Log.e(getClass().getSimpleName(), "Thread successfully cancelled");
//			mProgress.dismiss();
		}

		protected void onPreExecute() {
			if(showProgressDialog) {
				mProgress = new ProgressDialog(getActivity());
				mProgress.setMessage("Updating History\nPlease Wait...");
				mProgress.setIndeterminate(true);
				mProgress.setCancelable(false);
//				mProgress.setOnCancelListener(this);
				mProgress.setCanceledOnTouchOutside(false);
				mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				mProgress.show();
			}
		}

		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
			if(showProgressDialog) {
				mProgress.dismiss();
			}

			if(!result.isEmpty() || result != null) {
				Data.clear();
				Data = result;

//				reloadAdapterData();
				adapter = new SimpleExpandableAdapter(getActivity(), createGroupListFromDatabase(), createChildListFromDatabase());
//				adapter.setData(createGroupListFromDatabase(), createChildListFromDatabase());
				adapter.notifyDataSetChanged();
				x.setAdapter(adapter);
//				x.onRefreshComplete();
				
				dbh.updateTable(DataBaseHandler.TABLE_HISTORY, result);
			}
			
			x.onRefreshComplete();
		}
	}

	@Override
	public void onRefresh() {
		if(Utility.isConnected()) {
			new LoadHistory(true).execute(serverURL);
//			new Thread(new HistoryRunnable(this)).start();
		} else {
			toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
		}
	}

	@Override
	public void onComplete(ArrayList<HashMap<String, String>> data) {
		if(!data.isEmpty() || data != null) {
			Data.clear();
			Data = data;
			
			getActivity().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					adapter = new SimpleExpandableAdapter(getActivity(), createGroupListFromDatabase(), createChildListFromDatabase());
					adapter.notifyDataSetChanged();
					x.setAdapter(adapter);
					x.onRefreshComplete();
				}
			});

		}
		dbh.updateTable(DataBaseHandler.TABLE_HISTORY, data);
	}
}
