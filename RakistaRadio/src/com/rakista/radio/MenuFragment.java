package com.rakista.radio;

import java.util.Calendar;
import java.util.List;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Tracker;
import com.rakista.radio.internal.ConstantCommand;
import com.rakista.radio.internal.PagerAdapter;
import com.rakista.radio.internal.StatefulMediaPlayer.MPStates;
import com.rakista.radio.internal.ToastHelper;
import com.rakista.radio.service.MediaPlayerService;
import com.rakista.radio.service.MediaPlayerService.MediaPlayerBinder;
import com.rakista.radio.sites.MobileChat;
import com.rakista.radio.sites.twitterwebview;
import com.rakista.radio.sites.youTubewebviewactivity;
import com.rakista.radio.utility.Preferences;
import com.rakista.radio.utility.Utility;
import com.rakista.radio.widget.MyViewPager;

@SuppressLint("HandlerLeak")
@SuppressWarnings("unused")
public class MenuFragment extends Fragment implements OnClickListener {

	private TextView _Home, _Playlist, _TopMusic, _Dedications, _History,
	_Feed, _Profile, _Members, _Friends, _Mail, _Forum, _Blogs,
	_FeaturedEvents, _Marketplace, _Polls, _Quizzes, _Jokes, _Vidoes,
	_Photos, _Pages, _Chat, _MusicNews, _FacebookPage, _FacebookGroup, _FacebookWall,
	_Youtube, _Twitter, _Support, _About, _Exit, _Sleep, _Alarm,
	_Contact, _Rec;
	public OnItemClickListener listener;
	private GoogleAnalytics mGaIntance;
	
	private Tracker tracker;
//	private Utility Utility;
	private ToastHelper toast;
	
	public int selectedItem;
	public int Buffkey;
	private AlarmManager alarmManager;
	
	private MyViewPager mViewPager;
	private PagerAdapter mPagerAdapter;
	
	//public static MediaPlayerService mService;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.menu_layout, container, false);
		
		return v;
	}
	
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
//		Utility = new Utility(getActivity().getApplicationContext());
		toast = new ToastHelper(getActivity().getApplicationContext(), Toast.LENGTH_SHORT);
		
		mGaIntance = GoogleAnalytics.getInstance(getActivity());
		tracker = mGaIntance.getTracker("UA-148784-1");
		
		//mService = RadioActivity.mService;
		
		List<Fragment> fragments = new Vector<Fragment>();
		fragments.add(Fragment.instantiate(getActivity(), NowPlaying.class.getName()));
		fragments.add(Fragment.instantiate(getActivity(), Playlist.class.getName()));
		fragments.add(Fragment.instantiate(getActivity(), History.class.getName()));
		fragments.add(Fragment.instantiate(getActivity(), TopMusic.class.getName()));
		fragments.add(Fragment.instantiate(getActivity(), Dedication.class.getName()));
		this.mPagerAdapter = new PagerAdapter(super.getFragmentManager(), fragments);

		this.mViewPager = (MyViewPager) getActivity().findViewById(R.id.viewpager);
		this.mViewPager.setAdapter(this.mPagerAdapter);
		
		_Home = (TextView) getView().findViewById(R.id.menu_layout_button_home);
		_Home.setOnClickListener(this);

		_Playlist = (TextView) getView().findViewById(R.id.menu_layout_button_playlist);
		_Playlist.setOnClickListener(this);

		_TopMusic = (TextView) getView().findViewById(R.id.menu_layout_button_topmusic);
		_TopMusic.setOnClickListener(this);

		_Dedications = (TextView) getView().findViewById(R.id.menu_layout_button_dedication);
		_Dedications.setOnClickListener(this);

		_History = (TextView) getView().findViewById(R.id.menu_layout_button_history);
		_History.setOnClickListener(this);

		_Feed = (TextView) getView().findViewById(R.id.menu_layout_button_feed);
		_Feed.setOnClickListener(this);

		_Profile = (TextView) getView().findViewById(R.id.menu_layout_button_profile);
		_Profile.setOnClickListener(this);

		_Members = (TextView) getView().findViewById(R.id.menu_layout_button_members);
		_Members.setOnClickListener(this);

		_Friends = (TextView) getView().findViewById(R.id.menu_layout_button_friends);
		_Friends.setOnClickListener(this);

		_Mail = (TextView) getView().findViewById(R.id.menu_layout_button_mail);
		_Mail.setOnClickListener(this);

		_Forum = (TextView) getView().findViewById(R.id.menu_layout_button_forum);
		_Forum.setOnClickListener(this);

		_Blogs = (TextView) getView().findViewById(R.id.menu_layout_button_blog);
		_Blogs.setOnClickListener(this);

		_FeaturedEvents = (TextView) getView().findViewById(R.id.menu_layout_button_featuredevents);
		_FeaturedEvents.setOnClickListener(this);

		_Marketplace = (TextView) getView().findViewById(R.id.menu_layout_button_marketplace);
		_Marketplace.setOnClickListener(this);

		_Polls = (TextView) getView().findViewById(R.id.menu_layout_button_polls);
		_Polls.setOnClickListener(this);

		_Quizzes = (TextView) getView().findViewById(R.id.menu_layout_button_quizzes);
		_Quizzes.setOnClickListener(this);

		_Jokes = (TextView) getView().findViewById(R.id.menu_layout_button_jokes);
		_Jokes.setOnClickListener(this);

		_Vidoes = (TextView) getView().findViewById(R.id.menu_layout_button_vidoes);
		_Vidoes.setOnClickListener(this);

		_Photos = (TextView) getView().findViewById(R.id.menu_layout_button_photos);
		_Photos.setOnClickListener(this);

		_Pages = (TextView) getView().findViewById(R.id.menu_layout_button_pages);
		_Pages.setOnClickListener(this);

//		_Chat = (TextView) getView().findViewById(R.id.menu_layout_button_chat);
//		_Chat.setOnClickListener(this);

		_MusicNews = (TextView) getView().findViewById(R.id.menu_layout_button_musicnews);
		_MusicNews.setOnClickListener(this);

		_FacebookPage = (TextView) getView().findViewById(R.id.menu_layout_button_facebook_page);
		_FacebookPage.setOnClickListener(this);
		
		_FacebookGroup = (TextView) getView().findViewById(R.id.menu_layout_button_facebook_group);
		_FacebookGroup.setOnClickListener(this);

		_FacebookWall = (TextView) getView().findViewById(R.id.menu_layout_button_facebookwall);
		_FacebookWall.setOnClickListener(this);

		/*_FBFanPhoto = (TextView) findViewById(R.id.menu_layout_button_fbfanphoto);
		_FBFanPhoto.setOnClickListener(this);*/

		_Youtube = (TextView) getView().findViewById(R.id.menu_layout_button_youtube);
		_Youtube.setOnClickListener(this);

		_Twitter = (TextView) getView().findViewById(R.id.menu_layout_button_twitter);
		_Twitter.setOnClickListener(this);

		_Support = (TextView) getView().findViewById(R.id.menu_layout_button_support);
		_Support.setOnClickListener(this);
		
		_Contact = (TextView) getView().findViewById(R.id.menu_layout_button_contact);
		_Contact.setOnClickListener(this);
		
		_About = (TextView) getView().findViewById(R.id.menu_layout_button_about);
		_About.setOnClickListener(this);

		_Rec = (TextView) getView().findViewById(R.id.menu_layout_button_rec);
		_Rec.setOnClickListener(this);
		
		_Exit = (TextView) getView().findViewById(R.id.menu_layout_button_exit);
		_Exit.setOnClickListener(this);
		
		_Sleep = (TextView) getView().findViewById(R.id.menu_layout_button_sleep);
		_Sleep.setOnClickListener(this);
		
		_Alarm = (TextView) getView().findViewById(R.id.menu_layout_button_alarm);
		_Alarm.setOnClickListener(this);
	}

	public void onClick(View view) {
		switch (view.getId()) {

		case R.id.menu_layout_button_home:
			mViewPager.setCurrentItem(0, true);
			// mTabHost.setCurrentTab(0);
			Utility.sendClickEvent("Menu_Button_Home", getActivity());
			break;

		case R.id.menu_layout_button_playlist:
			mViewPager.setCurrentItem(1, true);
			// mTabHost.setCurrentTab(1);
			Utility.sendClickEvent("Menu_Button_Playlist", getActivity());
			break;

		case R.id.menu_layout_button_history:
			mViewPager.setCurrentItem(2, true);
			// mTabHost.setCurrentTab(2);
			Utility.sendClickEvent("Menu_Button_History", getActivity());
			break;

		case R.id.menu_layout_button_topmusic:
			mViewPager.setCurrentItem(3, true);
			// mTabHost.setCurrentTab(3);
			Utility.sendClickEvent("Menu_Button_TopMusic", getActivity());
			break;

		case R.id.menu_layout_button_dedication:
			mViewPager.setCurrentItem(4, true);
			// mTabHost.setCurrentTab(4);
			Utility.sendClickEvent("Menu_Button_Dedication", getActivity());
			break;

		case R.id.menu_layout_button_feed:
			if (Utility.isConnected()) {
				Utility.openWebview(getString(R.string.feed_url), getActivity());
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_Feed", getActivity());
			break;

		case R.id.menu_layout_button_profile:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.profile_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_Profile", getActivity());
			break;

		case R.id.menu_layout_button_members:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.members_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			
			break;

		case R.id.menu_layout_button_friends:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.friends_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_Friends", getActivity());
			
			break;

		case R.id.menu_layout_button_mail:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.mail_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_Mail", getActivity());
			
			break;

		case R.id.menu_layout_button_forum:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.forum_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}

			Utility.sendClickEvent("Menu_Button_Forum", getActivity());
			break;

		case R.id.menu_layout_button_blog:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.blogs_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_Blog", getActivity());
			
			break;
			
		case R.id.menu_layout_button_musicnews:
			
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.musicnews_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_MusicNews", getActivity());
			break;

		case R.id.menu_layout_button_featuredevents:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.featuredevents_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}

			Utility.sendClickEvent("Menu_Button_FeaturedEvents", getActivity());
			break;

		case R.id.menu_layout_button_marketplace:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.marketplace_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}

			Utility.sendClickEvent("Menu_Button_MarketPlace", getActivity());
			break;

		case R.id.menu_layout_button_polls:
			
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.polls_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_Polls", getActivity());
			break;

		case R.id.menu_layout_button_quizzes:
			
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.quizzes_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_Quizzes", getActivity());
			break;

		case R.id.menu_layout_button_jokes:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.jokes_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_Jokes", getActivity());
			break;

		case R.id.menu_layout_button_vidoes:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.vidoes_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_Videos", getActivity());
			break;

		case R.id.menu_layout_button_photos:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.photos_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_Photos", getActivity());
			break;

		case R.id.menu_layout_button_pages:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.pages_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_Pages", getActivity());
			break;

//		case R.id.menu_layout_button_chat:
//			if (Utility.isConnected()) {
//				final Intent chatIntent = new Intent(getActivity(), MobileChat.class);
//				startActivity(chatIntent);
//			} else {
//				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
//			}
//			Utility.sendClickEvent("Menu_Button_Chat", getActivity());
//			break;

		case R.id.menu_layout_button_facebook_page:
			if (Utility.isConnected()) {
				Utility.openWebview("http://www.facebook.com/rakistaradio", getActivity());
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			
			Utility.sendClickEvent("Menu_Button_FacebookPage", getActivity());
			
			break;

		case R.id.menu_layout_button_facebookwall:
			
			if(Utility.isConnected()) {
				Utility.openWebview("http://rakista.com/radio/mobile/fbwall.html", getActivity());
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_FacebookWall", getActivity());
			
			break;
			
		case R.id.menu_layout_button_facebook_group:
			if(Utility.isConnected()) {
				Utility.openWebview("https://www.facebook.com/groups/rakistajam/", getActivity());
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_facebookGroup", getActivity());
			break;

		/*case R.id.menu_layout_button_fbfanphoto:
			if (Utility.isConnected()) {
				if (SessionStore.restore(mFacebook, getApplicationContext())) {
					Intent x = new Intent(getActivity(), FacebookShare.class);
					startActivity(x);
					menuDrawer.closeMenu(true);
				} else {
					Toast.makeText(
							getApplicationContext(),
							"Please Login to your facebook account\nGo to Settings > Accounts > Facebook",
							Toast.LENGTH_LONG).show();
				}
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			break;*/

		case R.id.menu_layout_button_youtube:
			if (Utility.isConnected()) {
				final Intent youtubeIntent = new Intent(getActivity(), youTubewebviewactivity.class);
				startActivity(youtubeIntent);
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_YouTube", getActivity());
			break;

		case R.id.menu_layout_button_twitter:
			if (Utility.isConnected()) {
				final Intent twitterIntent = new Intent(getActivity(), twitterwebview.class);
				startActivity(twitterIntent);
			} else {
				toast.show(getString(R.string.Network), getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_Twitter", getActivity());
			break;

		case R.id.menu_layout_button_support:
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.rakista.radio&hl=en"));
			startActivity(intent);
			Utility.sendClickEvent("Menu_Button_Support", getActivity());
			break;

		case R.id.menu_layout_button_contact:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.contactus_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_Contact", getActivity());
			break;
		case R.id.menu_layout_button_about:
			if(Utility.isConnected()) {
				Utility.openWebview(getString(R.string.about_url), getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_About", getActivity());
			break;
			
		case R.id.menu_layout_button_rec:
			if(Utility.isConnected()) {
				Utility.openWebview("http://www.recph.com", getActivity());
			} else {
				toast.show(getString(R.string.Network),getString(R.string.connection_error_message));
			}
			Utility.sendClickEvent("Menu_Button_REC", getActivity());
			break;

		case R.id.menu_layout_button_exit:
			
			if(RadioActivity.mService.getMediaPlayer() != null) {
				if(RadioActivity.mService.getMediaPlayer().getState() == MPStates.STARTED) {
					stopPlayer.sendEmptyMessage(1);
				}
			}
			
			Utility.sendClickEvent("Menu_Button_Exit", getActivity());
			//Preferences.setApplicationIsOnResume(true);
			getActivity().moveTaskToBack(true);
			break;
			
		case R.id.menu_layout_button_sleep:
			
			Utility.sendClickEvent("Menu_Button_Sleep", getActivity());
			// Follow this sequence order (zero base).
			final CharSequence[] choiceList = {"Off", "15 Minutes", "20 Minutes", "25 Minutes", "30 Minutes", "35 Minutes",
					"40 Minutes", "50 Minutes", "55 Minutes", "1 Hour"};
			
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("Select sleep time");
			
			builder.setSingleChoiceItems(choiceList, Preferences.getSleepTimeIndex(), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, final int which) {
					Buffkey = which;	
					}
				});
			
			builder.setPositiveButton("Set", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Preferences.setSleepTimeIndex(Buffkey);
					//if(mService.getMediaPlayer().getState() == MPStates.STARTED) {
						setSleepTime(Buffkey);
					//} else {
					//	setSleepTime(Buffkey);
					//}
				}
			});
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			
			AlertDialog SleepDialog = builder.create();
			SleepDialog.show();
			
			break;
			
		case R.id.menu_layout_button_alarm:
			
			AlarmDialog AD = new AlarmDialog(getActivity());
			AD.show();
			
			break;
		}
	}
	
	public void setSleepTime(int position) {
		int x;
		alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(getActivity().getBaseContext(), RadioAlarmManagerReceiver.class);
		intent.setAction(ConstantCommand.START_SLEEP);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		// Getting current time and add the seconds on it.
		Calendar cal = Calendar.getInstance();

		switch (position) {
		case 1:
			x = 60 * 15;
			//x = 3;
			cal.add(Calendar.SECOND, x);
			break;
			
		case 2:
			x = 60 * 20;
			cal.add(Calendar.SECOND, x);
			break;
			
		case 3:
			x = 60 * 25;
			cal.add(Calendar.SECOND, x);
			break;
			
		case 4:
			x = 60 * 30;
			cal.add(Calendar.SECOND, x);
			break;
			
		case 5:
			x = 60 * 25;
			cal.add(Calendar.SECOND, x);
			break;

		case 6:
			x = 60 * 40;
			cal.add(Calendar.SECOND, x);
			break;
		case 7:
			x = 60 * 45;
			cal.add(Calendar.SECOND, x);
			break;
			
		case 8:
			x = 60 * 50;
			cal.add(Calendar.SECOND, x);
			break;

		case 9:
			x = 60 * 55;
			cal.add(Calendar.SECOND, x);
			break;

		case 10:
			x = 60 * 60;
			cal.add(Calendar.SECOND, x);
			break;
		}

		if(position == 0) {
			alarmManager.cancel(pendingIntent);
		} else {
			alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
					pendingIntent);
		}
	}
	
	public Handler stopPlayer = new Handler() {
		public void handleMessage(Message msg) {
			if(msg.what == 1) {
				if(RadioActivity.mService.getMediaPlayer().getState() == MPStates.STARTED) {
					RadioActivity.mService.stopMediaPlayer();
				}
			}
		}
	};
}
