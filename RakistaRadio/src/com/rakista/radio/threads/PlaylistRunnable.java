package com.rakista.radio.threads;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.rakista.radio.callback.BaseCallback;
import com.rakista.radio.callback.PlaylistCallback;
import com.rakista.radio.utility.JSONParser;

public class PlaylistRunnable implements Runnable {

	private final String serverURL = "http://www.rakista.com/radio/api/rradio.php?method=getartists";
	private BaseCallback callback;

	public PlaylistRunnable() {}

	public PlaylistRunnable(BaseCallback listener) {
		callback = listener;
	}

	@Override
	public void run() {
		/// JSON Node names
		final String RESULT = "result";
		final String TAG_ARTIST = "ARTIST";
		final String TAG_SONGCOUNT = "TOTAL";

		// contacts JSONArray
		JSONArray contacts = null;

		// Hashmap for ListView
		ArrayList<HashMap<String, String>> ArtistData = new ArrayList<HashMap<String, String>>();

		// Creating JSON Parser instance
		JSONParser jParser = new JSONParser();

		// getting JSON string from URL
		JSONObject json = null;

		try {
			json = jParser.getJSONFromUrl(serverURL);
		} catch (HttpException e1) {
			e1.printStackTrace();
			json = null;
		} catch (UnknownHostException e) {
			e.printStackTrace();
			json = null;
		} catch (ConnectTimeoutException e) {
			e.printStackTrace();
			json = null;
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

		try {
			// Getting Array of Contacts
			if(json != null) {
				contacts = json.getJSONArray(RESULT);

				// looping through All Contacts
				for(int i = 0; i < contacts.length(); i++) {
					JSONObject c = contacts.getJSONObject(i);

					// Storing each json item in variable
					String Artist = c.getString(TAG_ARTIST);
					String Count = c.getString(TAG_SONGCOUNT);

					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					// adding each child node to HashMap key => value
					map.put(TAG_ARTIST, Artist);
					map.put(TAG_SONGCOUNT, Count);

					// adding HashList to ArrayList
					ArtistData.add(map);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

		callback.onComplete(ArtistData);
	}
}
