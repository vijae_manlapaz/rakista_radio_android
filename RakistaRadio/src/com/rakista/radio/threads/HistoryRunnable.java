package com.rakista.radio.threads;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.rakista.radio.callback.BaseCallback;
import com.rakista.radio.utility.JSONParser;

public class HistoryRunnable implements Runnable {

	private BaseCallback callback;
	private String serverURL = "http://www.rakista.com/radio/api/rradio.php?method=getrecentsongs";

	public HistoryRunnable() {}

	public HistoryRunnable(BaseCallback listener) {
		callback = listener;
	}

	@Override
	public void run() {
		// JSON Node names
		final String RESULT = "result";
		final String TAG_ID = "ID";
		final String TAG_ARTIST = "artist";
		final String TAG_TITLE = "title";
		final String TAG_LYRICS = "lyrics";
		final String TAG_ALBUM = "album";
		final String TAG_ALBUMYEAR = "albumyear";
		final String TAG_DURATION = "duration";
		final String TAG_DURATIONDISPLAY = "durationDisplay";
		final String TAG_DATEPLAYED = "date_played";
		final String TAG_BUY = "buycd";
		final String TAG_ISREQUESTED = "isRequested";
		final String TAG_ISDEDICATED = "isDedication";

		// contacts JSONArray
		JSONArray contacts = null;

		// Hashmap for ListView
		ArrayList<HashMap<String, String>> RecentSongs = new ArrayList<HashMap<String, String>>();

		// Creating JSON Parser instance
		JSONParser jParser = new JSONParser();

		// getting JSON string from URL
		JSONObject json = null;
			try {
				json = jParser.getJSONFromUrl(serverURL);
			} catch (HttpException e1) {
				e1.printStackTrace();
				json = null;
			} catch (UnknownHostException e) {
				e.printStackTrace();
				json = null;
			} catch (ConnectTimeoutException e) {
				e.printStackTrace();
				json = null;
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			try {

				if(json != null) {
					// Getting Array of Contacts
					contacts = json.getJSONArray(RESULT);

					// looping through All Contacts
					for (int i = 0; i < contacts.length(); i++) {
						JSONObject c = contacts.getJSONObject(i);

						// Storing each json item in variable
						String ID = c.getString(TAG_ID);
						String Artist = c.getString(TAG_ARTIST);
						String Title = c.getString(TAG_TITLE);
						String Lyrics = c.getString(TAG_LYRICS);
						String Album = c.getString(TAG_ALBUM);
						String AlbumYear = c.getString(TAG_ALBUMYEAR);
						String Duration = c.getString(TAG_DURATION);
						String DuratioDisplay = c.getString(TAG_DURATIONDISPLAY);
						String BUYCD = c.getString(TAG_BUY);
						String Date_Played = c.getString(TAG_DATEPLAYED);
						boolean isRequested = c.getBoolean(TAG_ISREQUESTED);
						boolean isDedicated = c.getBoolean(TAG_ISDEDICATED);

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						map.put(TAG_ID, ID);
						map.put(TAG_ARTIST, Artist);
						map.put(TAG_TITLE, Title);
						map.put(TAG_LYRICS, Lyrics);
						map.put(TAG_ALBUM, Album);
						map.put(TAG_ALBUMYEAR, AlbumYear);
						map.put(TAG_DURATION, Duration);
						map.put(TAG_DURATIONDISPLAY, DuratioDisplay);
						map.put(TAG_BUY, BUYCD);
						map.put(TAG_DATEPLAYED, Date_Played);
						map.put(TAG_ISREQUESTED, Boolean.toString(isRequested));
						map.put(TAG_ISDEDICATED, Boolean.toString(isDedicated));

						RecentSongs.add(map);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			callback.onComplete(RecentSongs);
	}
}
