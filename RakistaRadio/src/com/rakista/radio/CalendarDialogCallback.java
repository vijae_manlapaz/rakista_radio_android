package com.rakista.radio;

import java.text.ParseException;

public interface CalendarDialogCallback {
	void onResult(String[] result) throws ParseException; 
}
