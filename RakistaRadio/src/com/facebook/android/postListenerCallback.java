package com.facebook.android;

public interface postListenerCallback {
	public void postSuccessful(String response);
	public void postFailed(String response);
}
