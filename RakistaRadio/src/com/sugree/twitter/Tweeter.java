package com.sugree.twitter;

import java.util.regex.Pattern;
import android.content.Context;

import com.rakista.radio.callback.TweeterPostCallback;
import com.rakista.radio.social.TwitterBase;
import com.rakista.radio.social.TwitterConstant;

public class Tweeter {
	
	public final static Pattern ID_PATTERN = Pattern.compile(".*?\"id_str\":\"(\\d*)\".*");
    public final static Pattern SCREEN_NAME_PATTERN = Pattern.compile(".*?\"screen_name\":\"([^\"]*).*");
    public static String tweetmsg;
    public Context mContext;
    
    public final TweeterPostCallback callback;

    public Tweeter(Context context, TweeterPostCallback listener) {
        this.mContext = context;
        this.callback = listener;
    }
    
    @SuppressWarnings("null")
	public boolean tweet(String message) {
        if (message == null && message.length() > 140) {
        	throw new IllegalArgumentException("message cannot be null and must be less than 140 chars");
        } else {
        	tweetmsg = message;
        	sendPost();
        	return true;
        }
    }
    
    public void sendPost() {
		new Thread() {
			@Override
			public void run() {
				
		        //String name = "";
		        //int what = 1;
				String jsonResponseStr = null;
		        
		       /* try {
		            HttpClient httpClient = new DefaultHttpClient();
		            Uri.Builder builder = new Uri.Builder();
		            builder.appendPath("statuses").appendPath("update.json").appendQueryParameter("status", tweetmsg);
		            Uri man = builder.build();
		            HttpPost post = new HttpPost("https://api.twitter.com/1.1" + man.toString());
		            Log.d("man", man.toString());
		            oAuthConsumer.sign(post);
		            HttpResponse resp = httpClient.execute(post);
		            jsonResponseStr = convertStreamToString(resp.getEntity().getContent());
		            Log.i(TAG,"response: " + jsonResponseStr);
		            String id = getFirstMatch(ID_PATTERN,jsonResponseStr);
		            Log.i(TAG,"id: " + id);
		            String screenName = getFirstMatch(SCREEN_NAME_PATTERN,jsonResponseStr);
		            Log.i(TAG,"screen name: " + screenName);
		            
		            //UserName = screenName;
		            final String url = MessageFormat.format("https://twitter.com/#!/{0}/status/{1}",screenName,id);
		            Log.i(TAG,"url: " + url);
		           
		            //return resp.getStatusLine().getStatusCode() == 200;
		            //runnable.run();
		            
		        } catch (Exception e) {
		            Log.e(TAG,"trying to tweet: " + tweetmsg, e);
		            //return false;
		        }*/
				
				TwitterBase.sendTweet(tweetmsg, mContext, TwitterConstant.CONSUMER_KEY, TwitterConstant.CONSUMER_SECRET);
		        
		        callback.onSuccess(jsonResponseStr);
			}
		}.start();
	}
    
    /*private static String convertStreamToString(java.io.InputStream is) {
        try {
            return new java.util.Scanner(is).useDelimiter("\\A").next();
        } catch (java.util.NoSuchElementException e) {
            return "";
        }
    }

	private static String getFirstMatch(Pattern pattern, String str){
        Matcher matcher = pattern.matcher(str);
        if(matcher.matches()){
            return matcher.group(1);
        }
        return null;
    }*/
}