package com.sugree.twitter;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
/**
 * This class will serve as a Session Storage for twitter
 * 
 * @author Vijae Manlapaz
 */
public class TwitterSessionStore {
	
	private static final String ACCESS_TOKEN = "twitter_access_token";
	private static final String SECRET_TOKEN = "twitter_secret_token";
	private static final String TWITTER_KEY = "twitter-session";
	private static final String TWITTER_NAME = "twitter-name";
	private static final String TWITTER_USERID = "twitter-userid";
	
	/**
	 * @param session Twitter Session
	 * @param context Application Context
	 * @return true if session is valid, false otherwise.
	 * 
	 * @author Vijae Manlapaz
	 */
	public static boolean isValidSession(Context context) {
		SharedPreferences savedSession = context.getSharedPreferences(TWITTER_KEY, Context.MODE_PRIVATE);
		boolean validsession = savedSession.getString(ACCESS_TOKEN, null) != null && savedSession.getString(SECRET_TOKEN, "") != null ? true : false;
        return validsession;
	}
	
	/**
	 * Saves twitter Access Token and Secret Token.
	 * @param Token Twitter Access Token
	 * @param Secret Twitter Secret Token
	 * @param context Application Context
	 * 
	 * @author Vijae Manlapaz
	 */
	public static boolean save(String Token, String Secret, Context context) {
		Editor editor = context.getSharedPreferences(TWITTER_KEY, Context.MODE_PRIVATE).edit();
		editor.putString(ACCESS_TOKEN, Token);
		editor.putString(SECRET_TOKEN, Secret);
		return editor.commit();
	}
	
	public static String getAccessToken(Context context) {
		SharedPreferences settings = context.getSharedPreferences(TWITTER_KEY, Context.MODE_PRIVATE);
		return settings.getString(ACCESS_TOKEN, null);
	}
	
	public static String getAccessTokenSecret(Context context) {
		SharedPreferences settings = context.getSharedPreferences(TWITTER_KEY, Context.MODE_PRIVATE);
		return settings.getString(SECRET_TOKEN, null);
	}
	
	public static void setName(Context context, String name) {
		Editor editor = context.getSharedPreferences(TWITTER_KEY, Context.MODE_PRIVATE).edit();
		editor.putString(TWITTER_NAME, name);
		editor.commit();
	}
	
	public static String getName(Context context) {
		SharedPreferences settings = context.getSharedPreferences(TWITTER_KEY, Context.MODE_PRIVATE);
		return settings.getString(TWITTER_NAME, "");
	}
	
	public static void setUserId(Context context, long userid) {
		Editor editor = context.getSharedPreferences(TWITTER_KEY, Context.MODE_PRIVATE).edit();
		editor.putLong(TWITTER_USERID, userid);
		editor.commit();
	}
	
	public static long getUserId(Context context) {
		SharedPreferences settings = context.getSharedPreferences(TWITTER_KEY, Context.MODE_PRIVATE);
		return settings.getLong(TWITTER_USERID, -1);
	}
	
	
	/**
	 * Clear the current session.
	 * @param context Application Context
	 * 
	 * @author Vijae Manlapaz
	 */
	public static void clear(Context context) {
		Editor editor = context.getSharedPreferences(TWITTER_KEY, context.MODE_PRIVATE).edit();
		editor.clear();
		editor.commit();
	}
}
