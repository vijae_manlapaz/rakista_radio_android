package com.androidworkz.androidshoutcastlib;

import java.util.EventListener;

public interface MetadataListener extends EventListener {
	void OnMetadataChanged(Metadata item);
}