package com.androidworkz.androidshoutcastlib;

public class InvalidStreamURLException extends Exception {
 
    public InvalidStreamURLException() {
        super("Empty or invalid Stream URL specified.");
    }
}
