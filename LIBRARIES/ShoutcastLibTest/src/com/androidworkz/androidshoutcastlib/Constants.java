package com.androidworkz.androidshoutcastlib;

import android.os.Environment;

public class Constants {

	public static final String SERVICECMD = "com.androidworkz.androidshoutcastlib";
	
	// mediabutton - service command ids	
	public static final String PLAYSTATE_CHANGED = SERVICECMD+".playstatechanged";
	public static final String META_CHANGED = SERVICECMD+".metachanged";
	public static final String PLAYBACK_COMPLETE = SERVICECMD+".playbackcomplete";
	public static final String PLAYMODE_CHANGED = SERVICECMD+".playmodechanged";
	public static final String PLAYER_ERROR = SERVICECMD+".playererror";
	public static final String ASYNC_OPEN_COMPLETE = SERVICECMD+".asyncopencomplete";
	public static final String PREFS_CHANGED = SERVICECMD+".preferenceschanged";
	public static final String CONNECTED = SERVICECMD+".connected";
	public static final String DISCONNECTED = SERVICECMD+".disconnected";
	public static final String CONNECTING = SERVICECMD+".connecting";
	public static final String BUFFERING = SERVICECMD+".buffering";
	public static final String STREAMING = SERVICECMD+".streaming";
	public static final String PAUSED = SERVICECMD+".paused";
	public static final String BITRATE = SERVICECMD+".bitrate";
	public static final String NETWORK_DISCONNECTED = SERVICECMD+".networkdisconnected";
	public static final String NETWORK_CONNECTED = SERVICECMD+".networkconnected";
	public static final String RESUME = SERVICECMD+".resumeplay";
	
	
}
