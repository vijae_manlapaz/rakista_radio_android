package com.androidworkz.androidshoutcastlib;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.webkit.URLUtil;

import com.androidworkz.androidshoutcastlib.IcyMeta.IcyListener;
import com.androidworkz.androidshoutcastlib.IcyMeta.tag.TagParseEvent;
import com.androidworkz.androidshoutcastlib.IcyMeta.tag.TagParseListener;


public class AndroidShoutcastLib implements TagParseListener {
	
	private Boolean isValidStreamURL = false;
	private String shoutcastUrl = null;
	private StreamServer proxy;
	private IcyListener stream = null;
	
	private Metadata metadata = null;
	MetadataListener metadataListener = null;
	
	public void setShoutcastUrl(String shoutcastUrl) throws InvalidStreamURLException {
		if (URLUtil.isValidUrl(shoutcastUrl)) {
			this.shoutcastUrl = shoutcastUrl;
			isValidStreamURL = true;
		}
		else {
			throw new InvalidStreamURLException();
		}
	}
	
	private void stopProxy() {
		if (proxy != null) {
			proxy.stop();
			proxy = null;
			stream = null;
		}
	}

	private void newProxy() {
		stream = null;
		stream = new IcyListener();
		stopProxy();
		proxy = new StreamServer(stream, this);
		proxy.init();
		proxy.start();
	}
	
	public String startStream() throws InvalidStreamURLException, MalformedURLException {
		if ((shoutcastUrl != null) && (isValidStreamURL)) {
			URL url = new URL(shoutcastUrl);
			try {
				URLConnection urlConnection = url.openConnection();
				stopProxy();
				newProxy();
				String playUrl = String.format("http://127.0.0.1:%d/%s", proxy.getPort(),
						shoutcastUrl);
				return playUrl;
			} catch (IOException e) {
				//e.printStackTrace();
			}			
		}
		else {
			throw new InvalidStreamURLException();
		}
		return shoutcastUrl;
	}
	
	public void stopStream() {
		stopProxy();
	}
	
	public void setOnMetadataChangedListener(MetadataListener listener) {
		metadataListener = listener;
	}

	@Override
	public void tagParsed(TagParseEvent tpe) {
		String streamTitle = stream.getStreamTitle();
		String[] meta = streamTitle.split(" - ");
		if (meta.length > 1) {
			metadata = new Metadata();
			metadata.artist = meta[0];
			metadata.track = meta[1];
			if(metadataListener != null) {
				metadataListener.OnMetadataChanged(metadata);
		    }
		}
		else if (meta.length > 0) {
			metadata = new Metadata();
			metadata.artist = null;
			metadata.track = meta[0];
			if(metadataListener != null) {
				metadataListener.OnMetadataChanged(metadata);
		    }
		}
	}
}
