/*******************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 * Copyright 2013 Naver Business Platform Corp.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.handmark.pulltorefresh.library;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;

import com.handmark.pulltorefresh.library.internal.EmptyViewMethodAccessor;
import com.handmark.pulltorefresh.library.internal.LoadingLayout;

public class PullToRefreshExpandableListView extends PullToRefreshAdapterViewBase<ExpandableListView> {

	private Drawable indicatorDrawable = null, groupDivider = null;
	private int dividerHeight = 1;
	
	public PullToRefreshExpandableListView(Context context) {
		super(context);
	}

	public PullToRefreshExpandableListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public PullToRefreshExpandableListView(Context context, Mode mode) {
		super(context, mode);
	}

	public PullToRefreshExpandableListView(Context context, Mode mode, Class<? extends LoadingLayout> loadingLayoutClazz) {
		super(context, mode, loadingLayoutClazz);
	}

	@Override
	public final Orientation getPullToRefreshScrollDirection() {
		return Orientation.VERTICAL;
	}
	
	public final void setGroupIndicator(Drawable drawable) {
		indicatorDrawable = drawable;
	}
	
	public final void setGroupDivider(Drawable divider) {
		groupDivider = divider;
	}
	
	public final void setGroupDividerHeight(int height) {
		Log.i("MADZ", "set DIVIDER HEIGHT IS: " + height);
		dividerHeight = height;
	}
	
	public final int getGroupDividerHeight() {
		Log.i("MADZ", "DIVIDER HEIGHT IS: " + dividerHeight);
		return dividerHeight;
	}

	@Override
	protected ExpandableListView createRefreshableView(Context context, AttributeSet attrs) {
		final ExpandableListView lv;
		
		if (VERSION.SDK_INT >= VERSION_CODES.GINGERBREAD) {
			Log.i("MADZ", "TEST: " + groupDivider);
			lv = new InternalExpandableListViewSDK9(context, attrs, groupDivider);
			lv.setGroupIndicator(indicatorDrawable);
//			lv.setDivider(groupDivider);
//			lv.setDividerHeight(2);
		} else {
			Log.i("MADZ", "TEST1: " + groupDivider.toString());
			
			lv = new InternalExpandableListView(context, attrs, groupDivider);
			lv.setGroupIndicator(indicatorDrawable);
//			lv.setDividerHeight(2);
//			lv.setDivider(groupDivider);
		}

		// Set it to this so it can be used in ListActivity/ListFragment
		lv.setId(android.R.id.list);
		
		
		return lv;
	}

	class InternalExpandableListView extends ExpandableListView implements EmptyViewMethodAccessor {

		public InternalExpandableListView(Context context, AttributeSet attrs, Drawable drawable) {
			super(context, attrs);
			super.setDivider(drawable);
			super.setDividerHeight(2);
		}

		@Override
		public void setEmptyView(View emptyView) {
			PullToRefreshExpandableListView.this.setEmptyView(emptyView);
		}

		@Override
		public void setEmptyViewInternal(View emptyView) {
			super.setEmptyView(emptyView);
		}

		@Override
		public void setGroupDividerDrawable(Drawable drawable) {
			super.setDivider(drawable);
		}
	}

	@TargetApi(9)
	final class InternalExpandableListViewSDK9 extends InternalExpandableListView {

		public InternalExpandableListViewSDK9(Context context, AttributeSet attrs, Drawable drawable) {
			super(context, attrs, drawable);
		}

		@Override
		protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX,
				int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {

			final boolean returnValue = super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX,
					scrollRangeY, maxOverScrollX, maxOverScrollY, isTouchEvent);

			// Does all of the hard work...
			OverscrollHelper.overScrollBy(PullToRefreshExpandableListView.this, deltaX, scrollX, deltaY, scrollY,
					isTouchEvent);

			return returnValue;
		}
	}
}
